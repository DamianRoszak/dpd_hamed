@echo off

git add .

echo Enter Commit message

:top

set /p value=
 
git commit -m "%value%"

git push

call "C:\Program Files (x86)\Microsoft Visual Studio 14.0\Common7\Tools\VsDevCmd.bat"

echo Enter app version 

:top

set /p version=

C:\SonarQube\MSBuild.SonarQube.Runner.exe begin /n:"DPD WF-MAG" /k:DPDWFMAG /v:"%version%"
msbuild /t:Rebuild
C:\SonarQube\MSBuild.SonarQube.Runner.exe end

pause