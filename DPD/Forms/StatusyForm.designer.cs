﻿namespace DPD.Forms
{
    partial class StatusyForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            var resources = new System.ComponentModel.ComponentResourceManager(typeof(StatusyForm));
            this.dgStatusy = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.dgStatusy)).BeginInit();
            this.SuspendLayout();
            // 
            // dgStatusy
            // 
            this.dgStatusy.AllowUserToAddRows = false;
            this.dgStatusy.AllowUserToResizeColumns = false;
            this.dgStatusy.AllowUserToResizeRows = false;
            this.dgStatusy.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgStatusy.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgStatusy.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgStatusy.Location = new System.Drawing.Point(0, 0);
            this.dgStatusy.MultiSelect = false;
            this.dgStatusy.Name = "dgStatusy";
            this.dgStatusy.ReadOnly = true;
            this.dgStatusy.RowHeadersVisible = false;
            this.dgStatusy.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgStatusy.Size = new System.Drawing.Size(666, 274);
            this.dgStatusy.TabIndex = 45;
            // 
            // StatusyForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(666, 297);
            this.Controls.Add(this.dgStatusy);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "StatusyForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Status";
            ((System.ComponentModel.ISupportInitialize)(this.dgStatusy)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgStatusy;

    }
}