﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using DPD.Models;
using DPD.Repos;

namespace DPD.Forms
{
    public partial class PlatnikForm : Form
    {
        private List<Numkat> _numkaty = new List<Numkat>();
        public bool IsAccpted = false;
        public int SelectedFid = 0;

        public PlatnikForm(string typ)
        {
            InitializeComponent();

            SendMsg(txtNumer, "Numer");
            SendMsg(txtNazwa, "Nazwa");

            label1.Text = $"Wybierz numer Numkat dla płatnika typu {typ}";

            PobierzNumkaty();
        }

        private void FormatDgNumkat()
        {
            if (dgNumkat.Rows.Count == 0)
                return;

            //dgNumkat.Columns["domyslny"].Width = 50;
            dgNumkat.Columns["numer"].ReadOnly = false;
            dgNumkat.Columns["nazwa"].ReadOnly = false;
            dgNumkat.Columns["domyslny"].ReadOnly = false;

        }

        private void PobierzNumkaty()
        {
            _numkaty = WfMag.PobierzNumkaty(txtNumer.Text, txtNazwa.Text);
            foreach (var num in _numkaty)
            {
                num.Domyslny = num.Numer.ToString() == Properties.Settings.Default.FID;
            }
            dgNumkat.DataSource = _numkaty;
            FormatDgNumkat();
        }

        private void txtNumer_TextChanged(object sender, EventArgs e)
        {
            PobierzNumkaty();
        }

        private const int EmSetcuebanner = 0x1501;
        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        private static extern int SendMessage(IntPtr hWnd, int msg, int wParam, [MarshalAs(UnmanagedType.LPWStr)]string lParam);

        private void SendMsg(Control control, string text)
        {
            SendMessage(control.Handle, EmSetcuebanner, 0, text);
            control.LostFocus += new EventHandler(delegate(object o, EventArgs ea)
            {
                if (control.Text == "")
                    SendMessage(control.Handle, EmSetcuebanner, 0, text);
            });
        }

        private void txtNazwa_TextChanged(object sender, EventArgs e)
        {
            PobierzNumkaty();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            IsAccpted = true;
            SelectedFid = (int)dgNumkat.SelectedRows[0].Cells["numer"].Value;
            Close();
        }

        private void dgNumkat_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            IsAccpted = true;
            SelectedFid = (int)dgNumkat.SelectedRows[0].Cells["numer"].Value;
            Close();
        }

        private void txtNumer_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
                e.Handled = true;

            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
                e.Handled = true;
        }
    }
}
