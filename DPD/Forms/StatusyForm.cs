﻿using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using DPD.Repos;

namespace DPD.Forms
{
    public partial class StatusyForm : Form
    {
        private readonly DpdStatusResponse _statusResponse;
        private readonly List<DpdEvent> _statusy;

        public StatusyForm(DpdStatusResponse statusResponse)
        {
            InitializeComponent();

            Text = statusResponse.Waybill;

            _statusResponse = statusResponse;

            _statusy = _statusResponse.Events.OrderByDescending(e => e.Date).ToList();

            dgStatusy.DataSource = _statusy;

            FormatDg();
        }

        private void FormatDg()
        {
            dgStatusy.Columns["description"].HeaderText = "Opis";
            dgStatusy.Columns["depot"].HeaderText = "Oddział";
            dgStatusy.Columns["date"].HeaderText = "Data";
            dgStatusy.Columns["packageRef"].HeaderText = "Nr referencyjny przesyłki";
            dgStatusy.Columns["parcelRef"].HeaderText = "Nr referencyjny paczki";

            dgStatusy.Columns["packageRef"].Visible = false;
            dgStatusy.Columns["parcelRef"].Visible = false;

        }
    }
}
