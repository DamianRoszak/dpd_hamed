﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows.Forms;
using DPD.DpdApi;
using DPD.Helpers;
using DPD.Models;
using DPD.Repos;
using static DPD.Properties.Settings;

namespace DPD.Forms
{
    public partial class Ustawienia2 : Form
    {
        private readonly string[] _uwierzytelnianieOpcje = new string[2] { "Windows Authentication", "Sql Server Authentication" };

        private readonly string[] _polaDodatkowe = { "POLE1", "POLE2", "POLE3", "POLE4", "POLE5",
                              "POLE6", "POLE7", "POLE8", "POLE9", "POLE10" };

        private readonly string[] _polaDpd = { "Ref1", "Ref2", "Zawartość", "Uwagi", };
        private readonly string[] _polaDpdWartoscDokumentu = { "Przesyłka wartościowa", "Pobranie C.O.D.", "Obydwa pola", };
        private readonly DPDPackageObjServicesService _dpdService = new DPDPackageObjServicesService();
        private readonly authDataV1 _authData = new authDataV1();
        public List<string> cbZablokowane = new List<string>();
        public List<string> cbDokumentyZwrotne = new List<string>();

        public Ustawienia2()
        {
            InitializeComponent();

            foreach (var c in pnlGlowny.Controls.OfType<Panel>())
                c.Location = new Point(5, 0);

            lWersjaAplikacji.Text = "v" + Application.ProductVersion;
            cbDoreczenie.DataSource = Form1.DoreczeniaTyp.Clone();
            cbUwierz.DataSource = _uwierzytelnianieOpcje.Clone();
            cbZam.DataSource = _polaDodatkowe.Clone();
            cbMag.DataSource = _polaDodatkowe.Clone();
            cbHand.DataSource = _polaDodatkowe.Clone();
            cbWartoscBrutto.DataSource = _polaDpdWartoscDokumentu.Clone();

            SendMsg(txtNaOkreslonaGodzine, "HH:MM");

            cbUwagi.DataSource = _polaDpd.Clone();
            cbNrDok.DataSource = _polaDpd.Clone();
            cbNrZamKlienta.DataSource = _polaDpd.Clone();
            cbPole1.DataSource = _polaDpd.Clone();
            cbPole2.DataSource = _polaDpd.Clone();
            cbPole3.DataSource = _polaDpd.Clone();
            cbPole4.DataSource = _polaDpd.Clone();
            cbPole5.DataSource = _polaDpd.Clone();
            cbPole6.DataSource = _polaDpd.Clone();
            cbPole7.DataSource = _polaDpd.Clone();
            cbPole8.DataSource = _polaDpd.Clone();
            cbPole9.DataSource = _polaDpd.Clone();
            cbPole10.DataSource = _polaDpd.Clone();

            if (string.IsNullOrEmpty(Default.etykietySciezka))
                Default.etykietySciezka = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + "\\Etykiety";

            if (string.IsNullOrEmpty(Default.protokolySciezka))
                Default.protokolySciezka = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + "\\Protokoły";

            if (string.IsNullOrEmpty(Default.raportySciezka))
                Default.raportySciezka = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + "\\Raporty";

            if (!Directory.Exists(Default.etykietySciezka))
            {
                Directory.CreateDirectory(Default.etykietySciezka);
            }

            if (!Directory.Exists(Default.protokolySciezka))
            {
                Directory.CreateDirectory(Default.protokolySciezka);
            }

            if (!Directory.Exists(Default.raportySciezka))
            {
                Directory.CreateDirectory(Default.raportySciezka);
            }

            Default.Save();

            SetEnables();

            PreparePrinters();

            LoadSettings();

            PobierzNumkaty();
            
            lbKategorie.SelectedIndex = 0;

            PobierzUstawieniePobrania();

            grSzablon.Enabled = chWlaczSzablon.Checked;

            _checkStatusWorker.DoWork += checkStatusWorker_DoWork;

            _checkStatusWorker.RunWorkerAsync();
        }

        private void checkStatusWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            var statusInfo = new DpdStatusInfoClass(Default.loginDpd, Crypto.DecryptPasswordString(Default.passDpd));

            _isStatusServiceAvailable = statusInfo.CheckAvailability();
        }

        private readonly BackgroundWorker _checkStatusWorker = new BackgroundWorker();

        private static void SendMsg(Control control, string text)
        {
            SendMessage(control.Handle, EmSetcuebanner, 0, text);
            control.LostFocus += new EventHandler(delegate(object o, EventArgs ea)
            {
                if (control.Text == "")
                    SendMessage(control.Handle, EmSetcuebanner, 0, text);
            });
        }

        private const int EmSetcuebanner = 0x1501;
        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        private static extern int SendMessage(IntPtr hWnd, int msg, int wParam, [MarshalAs(UnmanagedType.LPWStr)]string lParam);

        private void SetEnables()
        {
            var controls = new Control[] { chWartoscBrutto, chUwagi, chNrDok, chNrZamKlienta, chPole1, chPole2, chPole3, chPole4, chPole5, chPole6, chPole7, chPole8, chPole9, chPole10 };

            foreach (var c in controls)
            {
                var tmp = groupBox6.Controls.Find(c.Name.Replace("ch", "cb"), false).ToList();
                if (tmp.Count <= 0) continue;
                var cb = (ComboBox)tmp[0];
                var ch = (CheckBox)c;
                ch.CheckedChanged += CheckedChanged;
                cb.Enabled = ch.Checked;
            }
        }

        private void CheckedChanged(object o, EventArgs ea)
        {
            var ch = (CheckBox)o;
            var tmp = groupBox6.Controls.Find(ch.Name.Replace("ch", "cb"), false).ToList();
            if (tmp.Count > 0)
            {
                var cb = (ComboBox)tmp[0];
                cb.Enabled = ch.Checked;
            }
        }

        private void PreparePrinters()
        {
            var drukarki = System.Drawing.Printing.PrinterSettings.InstalledPrinters.Cast<string>().ToList();

            cbDrukarkaEtykieta.DataSource = drukarki.ToArray().Clone();
            cbDrukarkaProtokol.DataSource = drukarki.ToArray().Clone();

            cbDrukarkaEtykieta.SelectedItem = Default.drukarkaEtykiet;
            cbDrukarkaProtokol.SelectedItem = Default.drukarkaProtokol;
        }

        private void lbKategorie_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (lbKategorie.SelectedItem.ToString())
            {
                case "Połączenie z Wf-Magiem": { pnlPolaczenie.BringToFront(); break; }
                case "Etykiety i protokoły": { pnlEP.BringToFront(); break; }
                case "Adres nadawcy": { pnlAdres.BringToFront(); break; }
                case "Dane konta webserwisu DPD":{ pnlDPD.BringToFront(); PobierzNumkaty();break; }
                case "Miejsce zapisu nr listu": { pnlZapis.BringToFront(); break; }
                case "Dane dodatkowe z Wf-Maga": { pnlDaneDod.BringToFront(); break; }
                case "Szablon przesyłki": { pnlSzablon.BringToFront(); break; }
                case "Inne": { pnlInne.BringToFront(); break; }
                case "Pobranie C.O.D.": { PobierzUstawieniePobrania(); pnlPobranieCOD.BringToFront(); break; }
                case "Ustawienia pobierania danych": { pnlUstawieniaPolDostawy.BringToFront(); break; }
                case "Blokuj generowanie listu": { PobierzZaznaczoneDoZablokowania(); pnlBlokujGenerowanieListu.BringToFront(); break; }
                case "Generuj zwrot dokumentow": { PobierzZaznaczoneDokumentyZwrotne(); pnlGenerujZwrotDokumetnow.BringToFront(); break; }
            }
        }

        public void BlokujZaznaczonePozycjeFormDostawy()
        {
            //foreach (CheckBox item in ListaZablokowanychFormDostawy)
            //{
            //    if(item.Checked)
            //    {
                    
            //    }
            //}
        }

        private void Ustawienia2_Load(object sender, EventArgs e)
        {
        }

        private void Ustawienia2_FormClosed(object sender, FormClosedEventArgs e)
        {
            Konfi.IsOpen = false;
        }

        private void cbUwierz_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbUwierz.SelectedValue.ToString() != "Sql Server Authentication")
            {
                txtLoginDb.Enabled = false;
                txtHasloDb.Enabled = false;
            }
            else
            {
                txtLoginDb.Enabled = true;
                txtHasloDb.Enabled = true;
            }
        }

        private string CreateConnStr()
        {
            string auth = null, serwer = null, db = null;
            ThreadSafe(() =>
            {
                auth = cbUwierz.SelectedValue.ToString() == "Windows Authentication" ? "Integrated Security=true;" : $"User Id={txtLoginDb.Text};Password={txtHasloDb.Text};";

                serwer = txtSerwer.Text;
                db = txtDb.Text;       
            });
            return $"Data Source={serwer};Initial Catalog={db};{auth}";
        }

        private void btnTest_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtDb.Text))
            {
                lblInfo.Text = @"Uzupełnij nazwę katalogu";
                lblInfo.ForeColor = Color.Red;
                return;
            }

            lblInfo.ForeColor = Color.Black;

            var t1 = new Thread(new ThreadStart(CheckConnection));
            t1.Start();

            while (lblInfo.Text == @"Czekaj...")
            {
                Thread.Sleep(100);
            }

            PobierzNumkaty();
        }

        private void CheckConnection()
        {
            ThreadSafe(() => { lblInfo.Text = @"Czekaj..."; });
            var connStr = CreateConnStr();
            try
            {
                using (var conn = new SqlConnection(connStr))
                {
                    conn.Open();
                    Sql.ConnStr = connStr;
                }
                ThreadSafe(() =>
                {
                    lblInfo.Text = "OK!";
                    lblInfo.ForeColor = Color.Green;
                });

                WfMag.CreateTablesDpd();
                WfMag.CreateTableNumkat();
            }
            catch (Exception ex)
            {
                ThreadSafe(() =>
                {
                    lblInfo.Text = "Niepoprawne dane!";
                    lblInfo.ForeColor = Color.Red;
                });
                Log.AddLog(ex.ToString());
            }
        }

        private void ThreadSafe(MethodInvoker method)
        {
            if (InvokeRequired)
                Invoke(method);
            else
                method();
        }

        private void btnEtykieta_Click(object sender, EventArgs e)
        {
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                txtEtykieta.Text = folderBrowserDialog1.SelectedPath;

                if (!Directory.Exists(folderBrowserDialog1.SelectedPath + "\\tmp"))
                {
                    var di = Directory.CreateDirectory(folderBrowserDialog1.SelectedPath + "\\tmp");
                    di.Attributes = FileAttributes.Directory | FileAttributes.Hidden;
                }
            }
        }

        private void btnProtokol_Click(object sender, EventArgs e)
        {
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
                txtProtokol.Text = folderBrowserDialog1.SelectedPath;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            SaveSettings();

            MessageBox.Show("Zmiany zostaną wprowadzone po ponownym uruchomieniu");
        }

        private void SaveSettings()
        {
            if (chWlaczSzablon.Checked)
                SaveSzablon();

            ZapiszZaznaczoneDoZablokowania();
            ZapiszZaznaczoneDokumentyZwrotne();

            Default.bCheckStatus = chSprawdzanieStatusow.Checked;
            Default.bWlaczSzablon = chWlaczSzablon.Checked;
            Default.serverDb = txtSerwer.Text;
            Default.catalogDb = txtDb.Text;
            Default.authDb = cbUwierz.SelectedIndex;
            Default.loginDb = txtLoginDb.Text;
            Default.passDb = Crypto.EncryptPasswordString(txtHasloDb.Text);

            Default.etykietySciezka = txtEtykieta.Text;
            Default.protokolySciezka = txtProtokol.Text;
            Default.raportySciezka = txtRaporty.Text;

            Default.drukarkaEtykiet = cbDrukarkaEtykieta.SelectedValue.ToString();
            Default.drukarkaProtokol = cbDrukarkaProtokol.SelectedValue.ToString();
            Default.autoWydrukPoZapisie = chAutoWydrukPoZapisie.Checked;

            Default.etykieciarka = chEtykieciarka.Checked;

            if (rbPdf.Checked)
                Default.formatWydruku = "PDF";
            else
                if (rbEpl.Checked)
                    Default.formatWydruku = "EPL";
                else
                    Default.formatWydruku = "ZPL";

            Default.loginDpd = txtLoginDPD.Text;
            Default.passDpd = Crypto.EncryptPasswordString(txtHasloDPD.Text);
            Default.DpdWSDL = txtWSDL.Text.Replace(" ","");

            var fid = _numkaty.FirstOrDefault(n => n.Domyslny == true);
            if (fid != null)
                Default.FID = fid.Numer.ToString();

            Default.nazwaNad = txtNazwaNad.Text;
            Default.imieNazwiskoNad = txtImieNazwiskoNad.Text;
            Default.ulicaNad = txtUlicaNad.Text;
            Default.kodNad = txtKodNad.Text;
            Default.miejscowoscNad = txtMiejscNad.Text;
            Default.krajNad = txtkrajNad.Text;
            Default.telefonNad = txtTelNad.Text;
            Default.mailNad = txtMailNad.Text;
            
            Default.handUwagi = chUwagiHand.Checked;
            Default.handPD = chPolDodHand.Checked;
            if (chPolDodHand.Checked)
                Default.handPDnr = cbHand.SelectedValue.ToString();

            Default.magUwagi = chUwagiMag.Checked;
            Default.magPD = chPolDodMag.Checked;
            if (chPolDodMag.Checked)
                Default.magPDnr = cbMag.SelectedValue.ToString();

            Default.zamUwagi = chUwagiZam.Checked;
            Default.zamInfDod = chInfDodZam.Checked;
            Default.zamKomentarz = chKomentarz.Checked;
            Default.zamPD = chPoleDodZam.Checked;
            Default.cbAktywnyAdresDostawy = cbAktywnyAdresDostawy.Checked;

            if (chPoleDodZam.Checked)
                Default.zamPDnr = cbZam.SelectedValue.ToString();
            Default.zamKolumnaNrListu = chZamZapiszNrListuV2.Checked;
            Default.adresNadawcyZUstawien = chDomyslnyAdresNadawcy.Checked;


            Default.bWartoscBrutto = chWartoscBrutto.Checked;
            if (chWartoscBrutto.Checked)
                Default.vWartoscBrutto = cbWartoscBrutto.SelectedValue.ToString();

            Default.bUwagi = chUwagi.Checked;
            if (chUwagi.Checked)
                Default.vUwagi = cbUwagi.SelectedValue.ToString();

            Default.bnrDok = chNrDok.Checked;
            if (chNrDok.Checked)
                Default.vnrDok = cbNrDok.SelectedValue.ToString();

            Default.bNrZamKlienta = chNrZamKlienta.Checked;
            if (chNrZamKlienta.Checked)
                Default.vNrZamKlienta = cbNrZamKlienta.SelectedValue.ToString();

            Default.bPole1 = chPole1.Checked;
            if (chPole1.Checked)
                Default.vPole1 = cbPole1.SelectedValue.ToString();
            
            Default.bPole2 = chPole2.Checked;
            if (chPole2.Checked)
                Default.vPole2 = cbPole2.SelectedValue.ToString();

            Default.bPole3 = chPole3.Checked;
            if (chPole3.Checked)
                Default.vPole3 = cbPole3.SelectedValue.ToString();

            Default.bPole4 = chPole4.Checked;
            if (chPole4.Checked)
                Default.vPole4 = cbPole4.SelectedValue.ToString();

            Default.bPole5 = chPole5.Checked;
            if (chPole5.Checked)
                Default.vPole5 = cbPole5.SelectedValue.ToString();

            Default.bPole6 = chPole6.Checked;
            if (chPole6.Checked)
                Default.vPole6 = cbPole6.SelectedValue.ToString();

            Default.bPole7 = chPole7.Checked;
            if (chPole7.Checked)
                Default.vPole7 = cbPole7.SelectedValue.ToString();

            Default.bPole8 = chPole8.Checked;
            if (chPole8.Checked)
                Default.vPole8 = cbPole8.SelectedValue.ToString();

            Default.bPole9 = chPole9.Checked;
            if (chPole9.Checked)
                Default.vPole9 = cbPole9.SelectedValue.ToString();

            Default.bPole10 = chPole10.Checked;
            if (chPole10.Checked)
                Default.vPole10 = cbPole10.SelectedValue.ToString();

            Default.domyslnaWaga = txtWaga.Text.ToDecimal();
            Default.domyslnaWagaZMaga = rbWagaZMaga.Checked;

            Default.poleNazwa = rbNazwa.Checked ? "NAZWA" : "NAZWA_PELNA";

            Default.Save();

            ZapiszUstawieniaPlatnosci();

            Close();
        }

        private void SaveSzablon()
        {
            Default.bDoreczenieGwarantowane = chDorGwaran.Checked;
            Default.vDoreczenieGwarantowane = cbDoreczenie.SelectedItem.ToString();
            if (cbDoreczenie.SelectedItem.ToString() == "DPD na godzinę")
                Default.vDoreczenieGwarantowaneGodzina = txtNaOkreslonaGodzine.Text;

            Default.bDoreczenieDoRakWlasnych = chDorDoRakWl.Checked;
            Default.bDoreczenieNaAdresPrywatny = chDorNaAdrPryw.Checked;

            Default.bPobranieCOD = chPobranie.Checked;
            Default.cbAktywnyAdresDostawy = cbAktywnyAdresDostawy.Checked;
            Default.bPobranieZWfMaga = chPobierzKwoteZWfMaga.Checked;
            if (chPobranie.Checked)
                Default.vPobranieCOD = txtPobranie.Text;
            else
                Default.vPobranieCOD = string.Empty;

            Default.bOdbiorWlasny = chOdbiorWlasny.Checked;

            if (chOdbiorWlasny.Checked)
                Default.vOdbiorWlasny = rbOsoba.Checked ? "Osoba" : "Firma";
            else
                Default.vOdbiorWlasny = string.Empty;

            Default.bDokumentyZwrotne = chDokZwrot.Checked;
            Default.bDOX = chPrzesList.Checked;
            Default.bPrzesylkaZwrotna = chPrzesZwrot.Checked;
            Default.bOpony = chOpony.Checked;
            Default.bOponyMiedzynarodowe = chTiresExport.Checked;
            
            Default.bPrzesylkaWartosciowa = chDeklWart.Checked;
            Default.bPrzesylkaWartosciowaZWfMaga = chPrzesylkaWartosciowaZWfMaga.Checked;

            if (chDeklWart.Checked)
                Default.vPrzesylkaWartosciowa = txtDeklarowanaWartosc.Text;
            else
                Default.vPrzesylkaWartosciowa = string.Empty;

            Default.bPudo = chPudo.Checked;
            Default.vPudo = chPudo.Checked ? txtPudo.Text : string.Empty;

            Default.bServicePallet = chPallet.Checked;

        }

        private void LoadSzablon()
        {
            chDorGwaran.Checked = Default.bDoreczenieGwarantowane;
            if (chDorGwaran.Checked)
            {
                cbDoreczenie.SelectedItem = Default.vDoreczenieGwarantowane;
                if (cbDoreczenie.SelectedItem.ToString() == "DPD na godzinę")
                    txtNaOkreslonaGodzine.Text = Default.vDoreczenieGwarantowaneGodzina;
            }
            chDorNaAdrPryw.Checked = Default.bDoreczenieNaAdresPrywatny;
            chDorDoRakWl.Checked = Default.bDoreczenieDoRakWlasnych;

            chPobranie.Checked = Default.bPobranieCOD;
            chPobierzKwoteZWfMaga.Checked = Default.bPobranieZWfMaga;
            //if (chPobranie.Checked && !chPobierzKwoteZWfMaga.Checked)
            txtPobranie.Text = Default.vPobranieCOD;

            chOdbiorWlasny.Checked = Default.bOdbiorWlasny;
            if (chOdbiorWlasny.Checked)
                if (Default.vOdbiorWlasny == "Osoba") rbOsoba.Checked = true; else rbFirma.Checked = true;

            chDokZwrot.Checked = Default.bDokumentyZwrotne;
            chPrzesList.Checked = Default.bDOX;
            chPrzesZwrot.Checked = Default.bPrzesylkaZwrotna;
            chOpony.Checked = Default.bOpony;
            chTiresExport.Checked = Default.bOponyMiedzynarodowe;
            cbAktywnyAdresDostawy.Checked = Default.cbAktywnyAdresDostawy;
            chDeklWart.Checked = Default.bPrzesylkaWartosciowa;
            chPrzesylkaWartosciowaZWfMaga.Checked = Default.bPrzesylkaWartosciowaZWfMaga;
            if (chDeklWart.Checked)
                txtDeklarowanaWartosc.Text = Default.vPrzesylkaWartosciowa;

            chPudo.Checked = Default.bPudo;
            if (chPudo.Checked)
                txtPudo.Text = Default.vPudo;

            chPallet.Checked = Default.bServicePallet;
        }

        private void LoadSettings()
        {
            if (Default.bWlaczSzablon)
                chWlaczSzablon.Checked = true;

            LoadSzablon();
            rbNazwa.Checked = Default.poleNazwa == "NAZWA" ? true : false;
            rbNazwaPelna.Checked = Default.poleNazwa == "NAZWA_PELNA" ? true : false;

            chSprawdzanieStatusow.Checked = Default.bCheckStatus;

            PobierzZaznaczoneDokumentyZwrotne();
            PobierzZaznaczoneDoZablokowania();

            txtSerwer.Text = Default.serverDb;
            txtDb.Text = Default.catalogDb;
            cbUwierz.SelectedIndex = Default.authDb;
            txtLoginDb.Text = Default.loginDb;
         
                txtHasloDb.Text = Crypto.DecryptPasswordString(Default.passDb);
          
            rbWagaZMaga.Checked = Default.domyslnaWagaZMaga;
            rbWagaDomyslna.Checked = !Default.domyslnaWagaZMaga;

            txtRaporty.Text = Default.raportySciezka;
            txtEtykieta.Text = Default.etykietySciezka;
            txtProtokol.Text = Default.protokolySciezka;
            chAutoWydrukPoZapisie.Checked = Default.autoWydrukPoZapisie;

            chEtykieciarka.Checked = Default.etykieciarka;

            switch (Default.formatWydruku)
            {
                case "PDF": { rbPdf.Checked = true; break; }
                case "EPL": { rbEpl.Checked = true; break; }
                case "ZPL": { rbZpl.Checked = true; break; }
            }

            txtLoginDPD.Text = Default.loginDpd;
          
            txtHasloDPD.Text = Crypto.DecryptPasswordString(Default.passDpd);
           
           
            txtWSDL.Text = Default.DpdWSDL;

            txtNazwaNad.Text = Default.nazwaNad;
            txtImieNazwiskoNad.Text = Default.imieNazwiskoNad;
            txtUlicaNad.Text = Default.ulicaNad;
            txtKodNad.Text = Default.kodNad;
            txtMiejscNad.Text = Default.miejscowoscNad;
            txtkrajNad.Text = Default.krajNad;
            txtTelNad.Text = Default.telefonNad;
            txtMailNad.Text = Default.mailNad;

            chUwagiHand.Checked = Default.handUwagi;
            chPolDodHand.Checked = Default.handPD;
            if (chPolDodHand.Checked)
                cbHand.SelectedItem = Default.handPDnr;

            chUwagiMag.Checked = Default.magUwagi;
            chPolDodMag.Checked = Default.magPD;
            if (chPolDodMag.Checked)
                cbMag.SelectedItem = Default.magPDnr;

            chUwagiZam.Checked = Default.zamUwagi;
            chInfDodZam.Checked = Default.zamInfDod;
            chKomentarz.Checked = Default.zamKomentarz;
            chPoleDodZam.Checked = Default.zamPD;
            if (chPoleDodZam.Checked)
                cbZam.SelectedItem = Default.zamPDnr;
            chZamZapiszNrListuV2.Checked = Default.zamKolumnaNrListu;
            chDomyslnyAdresNadawcy.Checked = Default.adresNadawcyZUstawien;

            chWartoscBrutto.Checked = Default.bWartoscBrutto;
            if (chWartoscBrutto.Checked)
                cbWartoscBrutto.SelectedItem = Default.vWartoscBrutto;

            chUwagi.Checked = Default.bUwagi;
            if (chUwagi.Checked)
                cbUwagi.SelectedItem = Default.vUwagi;

            chNrDok.Checked = Default.bnrDok;
            if (chNrDok.Checked)
                cbNrDok.SelectedItem = Default.vnrDok;

            chNrZamKlienta.Checked = Default.bNrZamKlienta;
            if (chNrZamKlienta.Checked)
                cbNrZamKlienta.SelectedItem = Default.vNrZamKlienta;

            chPole1.Checked = Default.bPole1;
            if (chPole1.Checked)
                cbPole1.SelectedItem = Default.vPole1;

            chPole8.Checked = Default.bPole8;
            if (chPole8.Checked)
                cbPole8.SelectedItem = Default.vPole8;

            chPole7.Checked = Default.bPole7;
            if (chPole7.Checked)
                cbPole7.SelectedItem = Default.vPole7;

            chPole6.Checked = Default.bPole6;
            if (chPole6.Checked)
                cbPole6.SelectedItem = Default.vPole6;

            chPole5.Checked = Default.bPole5;
            if (chPole5.Checked)
                cbPole5.SelectedItem = Default.vPole5;

            chPole4.Checked = Default.bPole4;
            if (chPole4.Checked)
                cbPole4.SelectedItem = Default.vPole4;

            chPole3.Checked = Default.bPole3;
            if (chPole3.Checked)
                cbPole3.SelectedItem = Default.vPole3;

            chPole2.Checked = Default.bPole2;
            if (chPole2.Checked)
                cbPole2.SelectedItem = Default.vPole2;

            chPole9.Checked = Default.bPole9;
            if (chPole9.Checked)
                cbPole9.SelectedItem = Default.vPole9;

            chPole10.Checked = Default.bPole10;
            if (chPole10.Checked)
                cbPole10.SelectedItem = Default.vPole10;

            txtWaga.Text = Default.domyslnaWaga.ToString(CultureInfo.InvariantCulture).Replace(".", ",");
        }

        private void btnDodajNumkat_Click(object sender, EventArgs e)
        {
            var numkat = 0;
            if (!string.IsNullOrEmpty(txtNumkatNumer.Text))
                int.TryParse(txtNumkatNumer.Text, out numkat);

            if (numkat != 0)
                DodajNumkat(numkat, txtNumkatNazwa.Text);
        }

        private void DodajNumkat(int nr, string nazwa)
        {
            if (Sql.RunQuery($"insert into _DPD_NUMKAT ([numer],[nazwa],[domyslny]) values ({nr},'{nazwa}',0)"))
            {
                PobierzNumkaty();
            }
            else
            {
                MessageBox.Show("Sprawdź połączenie z bazą danych");
            }
        }

        private List<Numkat> _numkaty = new List<Numkat>();

        private void PobierzNumkaty()
        {
            _numkaty = WfMag.PobierzNumkaty("", "");
            dgNumkat.DataSource = _numkaty;
            foreach (var num in _numkaty)
            {
                num.Domyslny = num.Numer.ToString() == Default.FID;
            }
            FormatDgNumkat();
        }

        private void btnUsunNumkat_Click(object sender, EventArgs e)
        {
            if (dgNumkat.SelectedRows.Count == 0)
                return;

            var numkat = 0;
            if (!string.IsNullOrEmpty(dgNumkat.SelectedRows[0].Cells["numer"].Value.ToString()))
                int.TryParse(dgNumkat.SelectedRows[0].Cells["numer"].Value.ToString(), out numkat);

            if (numkat != 0)
                UsunNumkat(numkat);
        }

        private void UsunNumkat(int nr)
        {
            if (Sql.RunQuery($"delete from _DPD_NUMKAT where numer = {nr}"))
                PobierzNumkaty();
        }

        private void FormatDgNumkat()
        {
            if (dgNumkat.Rows.Count == 0)
                return;

            //dgNumkat.Columns["domyslny"].Width = 50;
            dgNumkat.Columns["numer"].ReadOnly = false;
            dgNumkat.Columns["nazwa"].ReadOnly = false;
            dgNumkat.Columns["domyslny"].ReadOnly = false;
        }

        private void chPolDodHand_CheckedChanged(object sender, EventArgs e)
        {
            cbHand.Enabled = chPolDodHand.Checked;
        }

        private void chPolDodMag_CheckedChanged(object sender, EventArgs e)
        {
            cbMag.Enabled = chPolDodMag.Checked;
        }

        private void chPoleDodZam_CheckedChanged(object sender, EventArgs e)
        {
            cbZam.Enabled = chPoleDodZam.Checked;
        }

        private void txtNumkatNumer_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != ','))
                e.Handled = true;

            if ((e.KeyChar == ',') && ((sender as TextBox).Text.IndexOf(',') > -1))
                e.Handled = true;
        }
        
        public bool Rollback = false;
        private bool _isStatusServiceAvailable;
       
        private void button2_Click_1(object sender, EventArgs e)
        {
            MessageBox.Show(
                AppDomain.CurrentDomain.SetupInformation.ConfigurationFile + Environment.NewLine +
                System.Reflection.Assembly.GetExecutingAssembly().Location + Environment.NewLine +
                AppDomain.CurrentDomain.SetupInformation.ConfigurationFile + Environment.NewLine +
                Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + Environment.NewLine +
                Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData)
           );
        }

        private void CreateOpDod(object sender, EventArgs e)
        {
            if (!Sql.CheckConnection())
            {
                MessageBox.Show("Skonfiguruj najpierw połączenie z bazą danych");
                return;
            }
            var dodano = false;

            var btn = sender as Button;
            switch (btn.Name)
            {
                case "btnOpZam": { dodano = WfMag.CreateOpDod("ZAM_BRW", "_DPD", "dpdzam"); break; }
                case "btnOpMag": { dodano = WfMag.CreateOpDod("DOKMAG_BRW", "_DPD", "dpdmag"); break; }
                case "btnOpHan": { dodano = WfMag.CreateOpDod("DOKHAN_BRW", "_DPD", "dpdhan"); break; }
            }
            if (dodano)
                MessageBox.Show("Utworzono operację dodatkową");
            else
                MessageBox.Show("Utworzono operację dodatkową");

        }

        private void PobierzWszystkieFormyDostawy(object sender, EventArgs e)
        {
            var pobraneFormyDostawy = Sql.PobierzDane(Query.PobierzWszystkieFormyDostawy()); int licz = 0;
            foreach (System.Data.DataRow row in pobraneFormyDostawy.Rows)
            {
                if(clbBlokowanieFormDostaw.Items.Count < row.Table.Rows.Count)
                {
                    clbBlokowanieFormDostaw.Items.Insert(licz, row["NAZWA"]);
                }
                if(clbGenerujZwrotDokumetnow.Items.Count < row.Table.Rows.Count)
                {
                    clbGenerujZwrotDokumetnow.Items.Insert(licz, row["NAZWA"]);
                }
                licz++;
            }
        }

        
        
        public void PobierzZaznaczoneDoZablokowania()
        {
            clbBlokowanieFormDostaw.Items.Clear();
            var formyDostawy = new WfMag();
            var dt = formyDostawy.PobierzFormyDostawy_Blokowanie();

            if (dt == null)
                return;

            for (var i = 0; i < dt.Rows.Count; i++)
            {
                clbBlokowanieFormDostaw.Items.Add(dt.Rows[i]["nazwa"].ToString());
                if (dt.Rows[i]["is_enabled"].ToString() == "1")
                {
                    clbBlokowanieFormDostaw.SetItemChecked(i, true);
                }
            }
        }
        public void ZapiszZaznaczoneDoZablokowania()
        {
            WfMag.DeleteFormyPobrania_Blokowanie();

            for (var i = 0; i < clbBlokowanieFormDostaw.Items.Count; i++)
            {
                var isChecked = 0;
                if (clbBlokowanieFormDostaw.GetItemCheckState(i).ToString() == "Checked")
                {
                    isChecked = 1;
                }
                WfMag.InsertFormyPobrania_Blokowanie(isChecked, clbBlokowanieFormDostaw.Items[i].ToString());
            }
        }



        public void PobierzZaznaczoneDokumentyZwrotne()
        {
            clbGenerujZwrotDokumetnow.Items.Clear();
            var formyDostawy = new WfMag();
            var dt = formyDostawy.PobierzFormyDostawy_DokumentZwrotny();

            if (dt == null)
                return;

            for (var i = 0; i < dt.Rows.Count; i++)
            {
                clbGenerujZwrotDokumetnow.Items.Add(dt.Rows[i]["nazwa"].ToString());
                if (dt.Rows[i]["is_enabled"].ToString() == "1")
                {
                    clbGenerujZwrotDokumetnow.SetItemChecked(i, true);
                }
            }
        }
        public void ZapiszZaznaczoneDokumentyZwrotne()
        {
            WfMag.DeleteFormyPobrania_DokumentZwrotny();

            for (var i = 0; i < clbGenerujZwrotDokumetnow.Items.Count; i++)
            {
                var isChecked = 0;
                if (clbGenerujZwrotDokumetnow.GetItemCheckState(i).ToString() == "Checked")
                {
                    isChecked = 1;
                }
                WfMag.InsertFormyPobrania_DokumentZwrotny(isChecked, clbGenerujZwrotDokumetnow.Items[i].ToString());
            }
        }

        private void btnSprawdzDPD_Click(object sender, EventArgs e)
        {
            var kod = new postalCodeV1() { countryCode = "PL", zipCode = "60128" };
            _authData.login = txtLoginDPD.Text;
            _authData.password = txtHasloDPD.Text;

            Numkat num = null;
            num = _numkaty.FirstOrDefault(n => n.Domyslny == true);
            
            if (num == null)
            {
                MessageBox.Show("Dodaj numkat i wskaż domyślny");
                return;
            }

            _authData.masterFid = num.Numer;
            _authData.masterFidSpecified = true;
            try
            {
                _dpdService.Url = txtWSDL.Text;
                var resp = _dpdService.findPostalCodeV1(kod, _authData);

                if (resp.status == "OK")
                {
                    MessageBox.Show("Poprawne dane");

                    try
                    {
                        var statusInfo = new DpdStatusInfoClass(txtLoginDPD.Text, txtHasloDPD.Text);
                        _isStatusServiceAvailable = statusInfo.CheckAvailability();
                    }
                    catch (Exception)
                    {

                    }
                }
                else
                    MessageBox.Show("Niepoprawne dane");
            }
            catch (Exception )
            {
                MessageBox.Show("Niepoprawne dane");
            }
        }

        private void txtWaga_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != ','))
                e.Handled = true;

            if ((e.KeyChar == ',') && ((sender as TextBox).Text.IndexOf(',') > -1))
                e.Handled = true;
        }

        private void chEtykieciarka_CheckedChanged(object sender, EventArgs e)
        {
           
            if (chEtykieciarka.Checked)
            {
                label14.Enabled = true;
                rbPdf.Enabled = true;
                rbEpl.Enabled = true;
                rbZpl.Enabled = true;
            }
            else
            {
                label14.Enabled = false;
                rbPdf.Enabled = false;
                rbEpl.Enabled = false;
                rbZpl.Enabled = false;
            }
        }

        private void dgNumkat_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex != 2) return;
            if (dgNumkat.SelectedRows.Count == 0)
                return;

            var num = (Numkat)dgNumkat.Rows[dgNumkat.SelectedCells[0].RowIndex].DataBoundItem;

            foreach (var n in _numkaty)
            {
                n.Domyslny = n.Numer == num.Numer;
            }
            dgNumkat.DataSource = _numkaty;
            FormatDgNumkat();
            dgNumkat.Refresh();
        }

        private void chDorGwaran_CheckedChanged(object sender, EventArgs e)
        { 
            cbDoreczenie.Visible = chDorGwaran.Checked;

            if (!chDorGwaran.Checked)
                cbDoreczenie.SelectedIndex = 0;
        }

        private void cbDoreczenie_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtNaOkreslonaGodzine.Visible = cbDoreczenie.SelectedValue.ToString() == "DPD na godzinę";
        }

        private void chPobranie_CheckedChanged(object sender, EventArgs e)
        {
            txtPobranie.Enabled = chPobranie.Checked;
            if (!chPobranie.Checked) return;
            var tip = new ToolTip {IsBalloon = true};
            tip.Show("Od 0.01 do 12 000 PLN", chPobranie);
        }

        private void chOdbiorWlasny_CheckedChanged(object sender, EventArgs e)
        {
            gbOdbiorWlasny.Enabled = chOdbiorWlasny.Checked;
        }

        private void chDeklWart_CheckedChanged(object sender, EventArgs e)
        {
            gbDeklarowanaWartosc.Enabled = chDeklWart.Checked;
            txtDeklarowanaWartosc.Enabled = chDeklWart.Checked;
            if (chDeklWart.Checked)
            {
                var tip = new ToolTip();
                tip.Show("Kwota z przedziału 0.01 - 500 000 PLN", chDeklWart);
            }
        }

        private void chWlaczSzablon_CheckedChanged(object sender, EventArgs e)
        {
            grSzablon.Enabled = chWlaczSzablon.Checked;
        }

        private void chPobierzKwoteZWfMaga_CheckedChanged(object sender, EventArgs e)
        {
            txtPobranie.Enabled = !chPobierzKwoteZWfMaga.Checked;
        }

        private void chPrzesylkaWartosciowaZWfMaga_CheckedChanged(object sender, EventArgs e)
        {
            txtDeklarowanaWartosc.Enabled = !chPrzesylkaWartosciowaZWfMaga.Checked;
        }

        private void btnRaporty_Click(object sender, EventArgs e)
        {
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
                txtRaporty.Text = folderBrowserDialog1.SelectedPath;
        }

        private void chSprawdzanieStatusow_Click(object sender, EventArgs e)
        {
            if (!_isStatusServiceAvailable)
            {
                chSprawdzanieStatusow.Checked = false;
                MessageBox.Show("Zgłoś się do swojego opiekuna handlowego w celu nadania uprawnień");
            }
        }

      

        private void PobierzUstawieniePobrania()
        {
            clbPobranieCOD.Items.Clear();
            var przewoznicy = new WfMag();
            var dt = przewoznicy.PobierzZmapowanePlatnosci();

            if (dt == null)
                return;

            for (var i = 0; i < dt.Rows.Count; i++)
            {
                clbPobranieCOD.Items.Add(dt.Rows[i]["nazwa"].ToString());
                if (dt.Rows[i]["is_enabled"].ToString() == "1")
                {
                    clbPobranieCOD.SetItemChecked(i, true);
                }
            }
        }


        private void ZapiszUstawieniaPlatnosci()
        {
            WfMag.DeletePlatnoscCod();
       
            for (var i = 0; i < clbPobranieCOD.Items.Count; i++)
            {
                var isChecked = 0;
                if (clbPobranieCOD.GetItemCheckState(i).ToString() == "Checked")
                {
                    isChecked = 1;
                }
                WfMag.InsertPlatnoscCod(isChecked, clbPobranieCOD.Items[i].ToString());

            }

        }

        private void btnPobierzPlatnosci_Click(object sender, EventArgs e)
        {
            try
            {
                clbPobranieCOD.Items.Clear();

                var wfMag = new WfMag();
                var dt = wfMag.PobierzPlatnosci();

                for (var i = 0; i < dt.Rows.Count; i++)
                {
                    clbPobranieCOD.Items.Add(dt.Rows[i]["nazwa"].ToString());
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(@"Nie udało się pobrać płatności.");
                Log.AddLog("Nie można pobrać Płatności dla C.O.D. : " + ex.Message);
            }
        }

        private void grSzablon_Enter(object sender, EventArgs e)
        {

        }

        private void chPudo_CheckedChanged(object sender, EventArgs e)
        {
            txtPudo.Enabled = chPudo.Checked;
            if (!chPudo.Checked)
                txtPudo.Text = string.Empty;
        }

        private void btnFindPickup_Click(object sender, EventArgs e)
        {
            var parcelShopSearch = new ParcelShopSearch();
            parcelShopSearch.ShowDialog();

            if (parcelShopSearch.DialogResult != DialogResult.OK || (string.IsNullOrEmpty(parcelShopSearch.ReturnVal) || parcelShopSearch.ReturnVal.Equals("-"))) return;
            chPudo.Checked = true;
            txtPudo.Text = parcelShopSearch.ReturnVal;
        }

        private void chDuty_CheckedChanged(object sender, EventArgs e)
        {
            txtDuty.Enabled = chDuty.Checked;
        }
    }
}
