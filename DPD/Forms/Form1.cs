﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;
using DPD.DpdApi;
using DPD.Helpers;
using DPD.Models;
using DPD.Repos;
using static DPD.Properties.Settings;

namespace DPD.Forms
{
    public partial class Form1 : Form
    {
        private readonly CheckBox _checkboxHeaderZam = new CheckBox();
        private readonly CheckBox _checkboxHeaderHan = new CheckBox();
        private readonly CheckBox _checkboxHeaderMag = new CheckBox();
        private readonly CheckBox _checkboxHeaderListy = new CheckBox();

        private readonly string[] _platnikTyp = {"Nadawca", "Odbiorca", "Trzecia strona"};

        public static readonly string[] DoreczeniaTyp =
            {"DPD 9:30", "DPD 12:00", "DPD na godzinę", "Sobota", "DPD NEXTDAY"};

        private readonly string[] _filtrDokuemntow = {"wszystkie", "wysłane", "niewysłane", "zaznaczone"};
        private string _connStr;
        private string _id;
        private int _fid;
        private TypDokumentu _typ;
        private readonly authDataV1 _authData = new authDataV1();
        private WfMag _wfmag;
        private readonly DPDPackageObjServicesService _dpdService = new DPDPackageObjServicesService();
        private DpdStatusInfoClass _statusInfo;
        private int _aktywnyPanel = 2;
        private bool _isStatusServiceEnabled;
        private readonly List<string> _pobranieCodDla = new List<string>();
        private bool _isPobranie;
        bool BrakZablokowanychFormDostawy;
        private packageAddressOpenUMLFeV1 _addressSender;
        private packageAddressOpenUMLFeV1 _addressReceiver;
        private packageOpenUMLFeV3 _pkg;

        public Form1()
        {
            InitializeComponent();

            CheckDebugMode();

            SprawdzPolaczenie();

            AktualizujTabeleDpdListy();

            Init();

            SetDefaultFid();

            _authData.login = Default.loginDpd;

            _authData.password = Crypto.DecryptPasswordString(Default.passDpd);

            _authData.masterFid = _fid;
            _authData.masterFidSpecified = true;
            lblFID.Text = _fid.ToString();


            _dpdService.Url = Default.DpdWSDL;

            SprawdzLoginDpd();

            var arguments = Environment.GetCommandLineArgs();

            PobierzDane(arguments);
            WypelnijMagazyny(cbMagazynZamowienie);

            if (Default.bCheckStatus)
            {
                _checkStatusStartWorker.DoWork += checkStatusStartWorker_DoWork;
                _checkStatusWorker.DoWork += checkStatusWorker_DoWork;

                _checkStatusStartWorker.RunWorkerAsync();
            }

            if(_typ == TypDokumentu.Dokument_Handlowy)
            {
                cbAdresy.Enabled = false;
                cbAdresy.Visible = false;
                if (!Default.cbAktywnyAdresDostawy)
                {
                    txtNazwaOdb.Enabled = false;
                    txtImieNazwiskoOdb.Enabled = false;
                    txtUlicaOdb.Enabled = false;
                    txtKodOdb.Enabled = false;
                    txtkrajOdb.Enabled = false;
                    txtMiejscOdb.Enabled = false;
                    txtTelOdb.Enabled = false;
                    txtMailOdb.Enabled = false;
                }
            }
            AktualizujTabeleDpdListy();

            CheckDokumentZwrotu();

        }

        private static void CheckDebugMode()
        {
#if DEBUG
            MessageBox.Show(@"Wersja developerska - proszę skontaktować się z Administratorem");
#endif
        }

        private void SetDefaultFid()
        {
            try
            {
                _fid = Convert.ToInt32(Default.FID);
            }
            catch (Exception)
            {
                Log.AddLog("Brak domyślnego FID");
            }
        }

        private readonly BackgroundWorker _checkStatusWorker = new BackgroundWorker();

        private void checkStatusWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            var list = PobierzStatusy();

            if (list != null)
            {
                var date = DateTime.Now;
                AktualizujStatusDlaDgLista(list, date);
                _wfmag.UpdateStatusList(list, date);
            }
            else
            {
                MessageBox.Show(@"Nie udało się pobrać statusów - system zwrócił błąd");
            }
        }

        private List<string[]> PobierzStatusy()
        {
            List<string[]> zaznaczone;
            var cnt = PobierzZaznaczoneListy(out zaznaczone);

            if (cnt == dgLista.Rows.Count)
                if (Default.dti != 0)
                {
                    var lastCheck = new DateTime(Default.dti);
                    var diff = DateTime.Now - lastCheck;
                    if (diff < new TimeSpan(2, 0, 0))
                    {
                        MessageBox.Show(
                            @"Grupowe odświeżanie statusów możliwe nie częściej niż co 2 godziny. Bieżący status pojedyńczej przesyłki dostępny po zaznaczeniu jednej pozycji z listy");
                        return null;
                    }
                }
                else
                {
                    Default.dti = DateTime.Now.Ticks;
                    Default.Save();
                }

            switch (cnt)
            {
                case 0:
                    MessageBox.Show(@"Nie zaznaczono żadnej przesyłki");
                    return null;
                case 1:
                    var status = _statusInfo.GetLastStatus(zaznaczone.First()[1]);

                    if (status == null)
                        MessageBox.Show(@"Nie udało się pobrać statusów - system zwrócił błąd");
                    else
                        return new List<string[]> {new[] {zaznaczone.First()[0], status}};

                    break;
                default:
                    if (cnt > 1)
                    {
                        var resp = _statusInfo.GetLastStatusList(zaznaczone);
                        if (resp == null)
                            MessageBox.Show(@"Nie udało się pobrać statusów - system zwrócił błąd");
                        else
                            return resp;
                    }

                    break;
            }

            return null;
        }

        private void AktualizujStatusDlaDgLista(List<string[]> list, DateTime data)
        {
            foreach (DataGridViewRow row in dgLista.Rows)
            {
                var chk = (DataGridViewCheckBoxCell) row.Cells["checked"];
                if (chk.Value != null && (bool) chk.Value)
                    foreach (var item in list)
                        if (row.Cells["id"].Value.ToString() == item[0])
                        {
                            row.Cells["status"].Value = item[1];
                            row.Cells["data_ostatniego_sprawdzenia"].Value = data;
                        }
            }

            dgLista.Refresh();
        }

        private int PobierzZaznaczoneListy(out List<string[]> zaznaczone)
        {
            var cnt = 0;
            zaznaczone = new List<string[]>();

            foreach (DataGridViewRow row in dgLista.Rows)
            {
                var chk = (DataGridViewCheckBoxCell) row.Cells["checked"];
                if (chk.Value != null && (bool) chk.Value)
                {
                    cnt++;
                    var numeryPaczek = row.Cells["nr_listu"].Value.ToString()
                        .Split(new[] {Environment.NewLine}, StringSplitOptions.None);
                    var idListu = row.Cells["id"].Value.ToString();

                    var dataListuStr = row.Cells["data_listu"].Value.ToString();

                    var datListu = Convert.ToDateTime(dataListuStr);

                    if (!SprawdzCzyStarszeNizMiesiac(datListu))
                        zaznaczone.Add(new[] {idListu, numeryPaczek.First()});
                }
            }

            return cnt;
        }

        private bool SprawdzCzyStarszeNizMiesiac(DateTime data)
        {
            if (data.AddDays(30) > DateTime.Now)
                return false;
            return true;
        }

        private void AktualizujWszystkieStatusy()
        {
            if (_wfmag == null || _statusInfo == null)
            {
                Log.AddLog("AktualizujWszystkieStatusy - comarch/statusInfo null");
                return;
            }

            if (Default.dti != 0)
            {
                var lastCheck = new DateTime(Default.dti);
                var diff = DateTime.Now - lastCheck;
                if (diff < new TimeSpan(2, 0, 0)) return;
            }
            else
            {
                Default.dti = DateTime.Now.Ticks;
                Default.Save();
            }

            var list = _wfmag.PobierzWszystkieListy();

            var resp = _statusInfo.GetLastStatusList(list);

            if (resp == null)
            {
                MessageBox.Show(@"Nie udało się pobrać statusów - system zwrócił błąd");
            }
            else
            {
                var data = DateTime.Now;
                _wfmag.UpdateStatusList(resp, data);
            }
        }

        private void checkStatusStartWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            SprawdzDostepnoscSerwisuDoStatusow();

            if (_isStatusServiceEnabled)
            {
                if (InvokeRequired)
                    btnPobierzStatusy.Invoke((MethodInvoker) delegate
                    {
                        btnPobierzStatusy.Enabled = _isStatusServiceEnabled;
                    });
                else
                    btnPobierzStatusy.Enabled = _isStatusServiceEnabled;

                AktualizujWszystkieStatusy();
            }
        }

        private readonly BackgroundWorker _checkStatusStartWorker = new BackgroundWorker();

        private void SprawdzDostepnoscSerwisuDoStatusow()
        {
            _statusInfo = new DpdStatusInfoClass(Default.loginDpd,
                Crypto.DecryptPasswordString(Default.passDpd));
            _isStatusServiceEnabled = _statusInfo.CheckAvailability();
        }
        
        private void AktualizujTabeleDpdListy()
        {
            try
            {
                Sql.RunQuery(@"if not exists(select * from sys.columns 
            where Name = N'adres_nadawcy_imieNazwisko' and Object_ID = Object_ID(N'_DPD_LISTY'))
begin
    alter table _DPD_LISTY add adres_nadawcy_imieNazwisko varchar(100)
end");

                Sql.RunQuery(@"if not exists(select * from sys.columns 
            where Name = N'adres_odbiorcy_imieNazwisko' and Object_ID = Object_ID(N'_DPD_LISTY'))
begin
    alter table _DPD_LISTY add adres_odbiorcy_imieNazwisko varchar(100)
end");

                Sql.RunQuery(@"if not exists(select * from sys.columns 
            where Name = N'status' and Object_ID = Object_ID(N'_DPD_LISTY'))
begin
    alter table _DPD_LISTY add status varchar(200)
end");

                Sql.RunQuery(@"if not exists(select * from sys.columns 
            where Name = N'data_ostatniego_sprawdzenia' and Object_ID = Object_ID(N'_DPD_LISTY'))
begin
    alter table _DPD_LISTY add data_ostatniego_sprawdzenia datetime
end");

                Sql.RunQuery(@"if not exists(select * from sys.columns 
            where Name = N'id_magazynu' and Object_ID = Object_ID(N'_DPD_LISTY'))
begin
    alter table _DPD_LISTY add id_magazynu varchar(100)
end");
            }
            catch (Exception ex)
            {
                Log.AddLog("AktualizujTabeleDpdListy(): " + ex);
            }
        }

        private void SprawdzLoginDpd()
        {
            try
            {
                var kod = new postalCodeV1 {countryCode = "PL", zipCode = "60128"};
                _authData.login = Default.loginDpd;
                _authData.password = Crypto.DecryptPasswordString(Default.passDpd);
                _authData.masterFid = _fid;
                _authData.masterFidSpecified = true;

                var resp = _dpdService.findPostalCodeV1(kod, _authData);
                if (resp.status != "OK")
                    MessageBox.Show(@"Niepoprawne dane logowania do serwisu DPD lub serwis niedostępny");
            }
            catch (Exception)
            {
                MessageBox.Show(@"Niepoprawne dane logowania do serwisu DPD  lub serwis niedostępny");
            }
        }

        private void checkboxHeaderZ_CheckedChanged(object sender, EventArgs e)
        {
            dgZamowienia.EndEdit();
            for (var i = 0; i < dgZamowienia.Rows.Count; i++)
            {
                var chk = (DataGridViewDisableCheckBoxCell) dgZamowienia.Rows[i].Cells[0];
                if (chk.Enabled)
                    chk.Checked = _checkboxHeaderZam.Checked;
            }

            dgZamowienia.Refresh();
        }

        private void checkboxHeaderM_CheckedChanged(object sender, EventArgs e)
        {
            dgMagazynowe.EndEdit();
            for (var i = 0; i < dgMagazynowe.Rows.Count; i++)
            {
                var chk = (DataGridViewDisableCheckBoxCell) dgMagazynowe.Rows[i].Cells[0];
                if (chk.Enabled)
                    chk.Checked = _checkboxHeaderMag.Checked;
            }

            dgMagazynowe.Refresh();
        }

        private void checkboxHeaderH_CheckedChanged(object sender, EventArgs e)
        {
            dgHandlowe.EndEdit();
            for (var i = 0; i < dgHandlowe.Rows.Count; i++)
            {
                var chk = (DataGridViewDisableCheckBoxCell) dgHandlowe.Rows[i].Cells[0];
                if (chk.Enabled)
                    chk.Checked = _checkboxHeaderHan.Checked;
            }

            dgHandlowe.Refresh();
        }

        private void checkboxHeaderListy_CheckedChanged(object sender, EventArgs e)
        {
            dgLista.EndEdit();
            for (var i = 0; i < dgLista.Rows.Count; i++)
                if (!dgLista.Rows[i].Cells[0].ReadOnly)
                    dgLista.Rows[i].Cells[0].Value = _checkboxHeaderListy.Checked;
        }

        private void SprawdzPolaczenie()
        {
            try
            {
                CreateConnStr();

                _sqlStatus = SqlCheckStatus.Checking;

                var t1 = new Thread(TestConnection);
                t1.Start();

                while (_sqlStatus != SqlCheckStatus.Finished) Thread.Sleep(100);

                _sqlStatus = SqlCheckStatus.Nothing;

                if (!_isConnStrOk)
                {
                    MessageBox.Show(
                        @"Połączenie z bazą danych nie zostało poprawnie skonfigurowane lub baza jest niedostępna");

                    try
                    {
                        SettingsHelper.LoadPropertiesFromOlderVersion("1.0.0.0");
                    }
                    catch (Exception ex)
                    {
                        Log.AddLog(ex.ToString());
                    }

                    _ustawieniaForm = new Ustawienia2();
                    _ustawieniaForm.ShowDialog();

                    CreateConnStr();

                    t1 = new Thread(TestConnection);
                    t1.Start();

                    if (!_isConnStrOk)
                    {
                        Close();
                        Application.Exit();
                        Application.ExitThread();
                        Environment.Exit(0);
                    }
                    else
                    {
                        Sql.ConnStr = _connStr;
                    }
                }
                else
                {
                    Sql.ConnStr = _connStr;
                }
            }
            catch (Exception ex)
            {
                Log.AddLog(ex.ToString());
                MessageBox.Show(@"Wystąpił błąd! Sprawdź szczegóły w logach");
            }
        }

        private bool _isConnStrOk;

        private enum SqlCheckStatus
        {
            Nothing,
            Checking,
            Finished
        }

        private SqlCheckStatus _sqlStatus = SqlCheckStatus.Nothing;

        private void TestConnection()
        {
            try
            {
                if (Default.serverDb != null && string.IsNullOrEmpty(Default.serverDb))
                    throw new Exception();

                using (var conn = new SqlConnection(_connStr))
                {
                    conn.Open();
                    using (var cmd = new SqlCommand("select 1", conn))
                    {
                        cmd.ExecuteNonQuery();
                    }
                }

                _isConnStrOk = true;
            }
            catch (Exception ex)
            {
                _isConnStrOk = false;
                Log.AddLog("test false:" + ex);
            }

            _sqlStatus = SqlCheckStatus.Finished;
        }

        private void CreateConnStr()
        {
            var auth = Default.authDb == 0
                ? "Integrated Security=true;"
                : $"User Id={Default.loginDb};Password={Crypto.DecryptPasswordString(Default.passDb)};";

            _connStr = $"Data Source={Default.serverDb};Initial Catalog={Default.catalogDb};{auth}";
        }

        private void TurnOffSortingDgColumn(DataGridView dg)
        {
            foreach (DataGridViewColumn col in dg.Columns)
                col.SortMode = DataGridViewColumnSortMode.NotSortable;
        }

        private void Init()
        {
            Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;

            btnPobierzStatusy.Enabled = _isStatusServiceEnabled;

            pnlGenerowanie.Location = new Point(0, 23);
            pnlDokumenty.Location = new Point(0, 23);
            pnlPrzygotowane.Location = new Point(0, 23);

            dpZamFrom.Value = new DateTime(2010, 1, 1);
            dpMagFrom.Value = new DateTime(2010, 1, 1);
            dpHanFrom.Value = new DateTime(2010, 1, 1);
            dpListyFrom.Value = new DateTime(2010, 1, 1);

            dpZamFrom.Value = DateTime.Today;
            dpMagFrom.Value = DateTime.Today;
            dpHanFrom.Value = DateTime.Today;
            dpListyTo.Value = DateTime.Today;
            dpListyFrom.Value = DateTime.Today;

            chDrukuj.Checked = Default.autoWydrukPoZapisie;
            chGenerujEtykiete.Checked = Default.autoWydrukPoZapisie;

            SendMsg(txtNrZam, "Dokument WF-Mag");
            SendMsg(txtNrMag, "Dokument WF-Mag");
            SendMsg(txtNrHan, "Dokument WF-Mag");
            SendMsg(txtNumerDok, "Dokument");
            SendMsg(txtOdbiorca, "Odbiorca");
            SendMsg(txtNadawca, "Nadawca");
            SendMsg(txtKontrahentZam, "Kontrahent");
            SendMsg(txtKontrahentMag, "Kontrahent");
            SendMsg(txtKontrahentHan, "Kontrahent");
            SendMsg(txtFindNrListu, "Nr listu");
            SendMsg(txtNrProtokolu, "Nr protokołu");
            SendMsg(txtNaOkreslonaGodzine, "HH:MM");

            pnlZamowienia.Location = new Point(0, 0);
            pnlMagazynowe.Location = new Point(0, 0);
            pnlHandlowe.Location = new Point(0, 0);

            cbPlatnik.DataSource = _platnikTyp.Clone();
            cbDoreczenie.DataSource = DoreczeniaTyp.Clone();
            cbKrajowe.SelectedIndex = 0;

            cbFiltrZam.DataSource = _filtrDokuemntow.Clone();
            cbFiltrMag.DataSource = _filtrDokuemntow.Clone();
            cbFiltrHan.DataSource = _filtrDokuemntow.Clone();

            var rect = dgZamowienia.GetCellDisplayRectangle(0, -1, true);
            rect.X = rect.Location.X + 10;
            rect.Y = rect.Location.Y + 4;
            rect.Width = rect.Size.Width;
            rect.Height = rect.Size.Height;

            var rect2 = dgZamowienia.GetCellDisplayRectangle(0, -1, true);
            rect2.X = rect2.Location.X + 5;
            rect2.Y = rect2.Location.Y + 4;
            rect2.Width = rect2.Size.Width;
            rect2.Height = rect2.Size.Height;

            _checkboxHeaderZam.Name = "checkboxHeaderZ";
            _checkboxHeaderZam.Size = new Size(15, 15);
            _checkboxHeaderZam.Location = rect.Location;
            _checkboxHeaderZam.CheckedChanged += checkboxHeaderZ_CheckedChanged;
            dgZamowienia.Controls.Add(_checkboxHeaderZam);

            _checkboxHeaderMag.Name = "checkboxHeaderM";
            _checkboxHeaderMag.Size = new Size(15, 15);
            _checkboxHeaderMag.Location = rect.Location;
            _checkboxHeaderMag.CheckedChanged += checkboxHeaderM_CheckedChanged;
            dgMagazynowe.Controls.Add(_checkboxHeaderMag);

            _checkboxHeaderHan.Name = "checkboxHeaderH";
            _checkboxHeaderHan.Size = new Size(15, 15);
            _checkboxHeaderHan.Location = rect.Location;
            _checkboxHeaderHan.CheckedChanged += checkboxHeaderH_CheckedChanged;
            dgHandlowe.Controls.Add(_checkboxHeaderHan);

            _checkboxHeaderListy.Name = "checkboxHeaderListy";
            _checkboxHeaderListy.Size = new Size(15, 15);
            _checkboxHeaderListy.Location = rect2.Location;
            _checkboxHeaderListy.CheckedChanged += checkboxHeaderListy_CheckedChanged;
            dgLista.Controls.Add(_checkboxHeaderListy);

            if (Default.etykieciarka)
            {
                var tab = new List<Control>
                {
                    lblPozycjaEtykiety,
                    lblPozycjaEtykietyP,
                    btnQ1,
                    btnQ2,
                    btnQ3,
                    btnQ4,
                    btnpQ1,
                    btnpQ2,
                    btnpQ3,
                    btnpQ4
                };
                foreach (var c in tab)
                    c.Visible = false;
            }
        }

        private void PobierzPola()
        {
            var pola = _wfmag.PobierzPola();
            if (pola == null)
                return;

            var props = new List<string>
            {
                "bWartoscBrutto",
                "bnrDok",
                "bNrZamKlienta",
                "bUwagi",
                "bPole1",
                "bPole2",
                "bPole3",
                "bPole4",
                "bPole5",
                "bPole6",
                "bPole7",
                "bPole8",
                "bPole9",
                "bPole10"
            };

            foreach (SettingsProperty currentProperty in Default.Properties)
                if (props.Contains(currentProperty.Name))
                    if ((bool) Default[currentProperty.Name])
                        switch (Default[currentProperty.Name.Replace("b", "v")].ToString())
                        {
                            case "Ref1":
                            {
                                if (txtRef1.Text.Length > 0)
                                    txtRef1.Text += ";" + pola.ReturnValue(currentProperty.Name);
                                else txtRef1.Text += pola.ReturnValue(currentProperty.Name);
                                break;
                            }
                            case "Ref2":
                            {
                                if (txtRef2.Text.Length > 0)
                                    txtRef2.Text += ";" + pola.ReturnValue(currentProperty.Name);
                                else txtRef2.Text += pola.ReturnValue(currentProperty.Name);
                                break;
                            }
                            case "Zawartość":
                            {
                                if (dgPaczki.Rows[0].Cells["content"].Value != null &&
                                    dgPaczki.Rows[0].Cells["content"].Value.ToString().Length > 0)
                                    dgPaczki.Rows[0].Cells["content"].Value += ";" +
                                                                               pola.ReturnValue(currentProperty.Name);
                                else
                                    dgPaczki.Rows[0].Cells["content"].Value += pola.ReturnValue(currentProperty.Name);
                                break;
                            }
                            case "Uwagi":
                            {
                                if (dgPaczki.Rows[0].Cells["cust1"].Value != null &&
                                    dgPaczki.Rows[0].Cells["cust1"].Value.ToString().Length > 0)
                                    dgPaczki.Rows[0].Cells["cust1"].Value += ";" +
                                                                             pola.ReturnValue(currentProperty.Name);
                                else
                                    dgPaczki.Rows[0].Cells["cust1"].Value += pola.ReturnValue(currentProperty.Name);
                                break;
                            }
                            case "Przesyłka wartościowa":
                            {
                                txtDeklarowanaWartosc.Text = $"{_wfmag.KwotaDokumentu:0.00}";
                                break;
                            }
                            case "Pobranie C.O.D.":
                            {
                                txtPobranie.Text = $"{_wfmag.KwotaDokumentu:0.00}";
                                break;
                            }
                            case "Obydwa pola":
                            {
                                txtDeklarowanaWartosc.Text = $"{_wfmag.KwotaDokumentu:0.00}";
                                txtPobranie.Text = $"{_wfmag.KwotaDokumentu:0.00}";
                                break;
                            }
                        }
        }

        private void SendMsg(Control control, string text)
        {
            SendMessage(control.Handle, EmSetcuebanner, 0, text);
            control.LostFocus += delegate
            {
                if (control.Text == "")
                    SendMessage(control.Handle, EmSetcuebanner, 0, text);
            };
        }

        private const int EmSetcuebanner = 0x1501;

        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        private static extern int SendMessage(IntPtr hWnd, int msg, int wParam,
            [MarshalAs(UnmanagedType.LPWStr)] string lParam);

        public void PobierzDane(string[] args)
        {
            WindowState = FormWindowState.Minimized;
            Show();
            WindowState = FormWindowState.Normal;

            var wfMag = new WfMag();

            try
            {
                var dataPobranieCod = new DataTable();
                if (_pobranieCodDla.Count == 0)
                {
                    dataPobranieCod = wfMag.PobierzZmapowanePlatnosci();

                    if (dataPobranieCod?.Rows != null)
                        for (var i = 0; i < dataPobranieCod.Rows.Count; i++)
                            if (dataPobranieCod.Rows[i]["is_enabled"].ToString() == "1")
                                _pobranieCodDla.Add(dataPobranieCod.Rows[i]["nazwa"].ToString());
                }
            }
            catch (Exception ex)
            {
                Log.AddLog(ex.ToString());
            }

            if (args.Count() > 1)
            {
                CreateConnStrFromArgs(args);

                var param1 = args[2].Split(',');
                _id = param1[0];

                switch (param1[1])
                {
                    case "202":
                    {
                        _typ = TypDokumentu.Zamowienie;
                        break;
                    }
                    case "1":
                    {
                        _typ = TypDokumentu.Dokument_Magazynowy;
                        break;
                    }
                    case "4":
                    {
                        _typ = TypDokumentu.Dokument_Handlowy;
                        break;
                    }
                }

                PobierzDaneDokumentuWfMag(true);
            }
            else
            {
                WyczyscFormularz();

                _wfmag = new WfMag();

                PokazPanel(11);
                PobierzZam(null, null);
            }
        }

        private void DodajDomyslnaPaczke()
        {
            dgPaczki.Rows.Clear();
            var startRow = new DataGridViewRow();
            startRow.CreateCells(dgPaczki);
            startRow.Cells[0].Value = "1";
            if (Default.domyslnaWagaZMaga)
                startRow.Cells[1].Value = $"{_wfmag.Waga:0.00}".Replace(".", ",");
            else
                startRow.Cells[1].Value = $"{Default.domyslnaWaga:0.00}".Replace(".", ",");
            dgPaczki.Rows.Add(startRow);
        }

        private void PobierzDaneDokumentuWfMag(bool pokazPnlGenerowanie)
        {
            try
            {
                WyczyscFormularz();

                _wfmag = new WfMag(_id, _typ);

                PobierzAdresy();

                DodajDomyslnaPaczke();

                PobierzPola();

                if (Default.bWlaczSzablon)
                    PobierzUslugiZUstawien();

                if (pokazPnlGenerowanie)
                    PokazPanel(2);

                CheckDokumentZwrotu();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                Log.AddLog(ex.ToString());
            }
        }

        private void PobierzUslugiZUstawien()
        {
            var usl = UslugiHelper.PobierzUslugiZUstawien();
            usl.PrzeniesDaneDoKontrolek(pnlGenerowanie);

            if (usl.Pobranie && usl.PobranieZWfMaga &&
                _pobranieCodDla.Contains(_wfmag.PobierzRodzajPlatnosciDlaWybranegoDokumentu(_wfmag.IdDokumentu, _typ)))
            {
                txtPobranie.Text = $"{_wfmag.KwotaDokumentu:0.00}";
                chPobranie.Checked = true;
                _isPobranie = true;
            }
            else
            {
                chPobranie.Checked = false;
                txtPobranie.Text = "";
                _isPobranie = false;
            }


            if (usl.PrzesylkaWartosciowa && usl.PrzesylkaWartosciowaZWfMaga)
                txtDeklarowanaWartosc.Text = $"{_wfmag.KwotaDokumentu:0.00}";
        }

        private void CreateConnStrFromArgs(string[] args)
        {
            var param = args[1].Split(',');
            Sql.ConnStr = "Data Source= " + param[0] + ";Initial Catalog=" + param[1] + ";User Id=" + param[2] +
                          ";Password=" + param[3] + " ;";
        }

        private Ustawienia2 _ustawieniaForm;

        private void ustawieniaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!Konfi.IsOpen)
            {
                _ustawieniaForm = new Ustawienia2();
                _ustawieniaForm.Show();
                Konfi.IsOpen = true;
            }
            else
            {
                _ustawieniaForm?.BringToFront();
            }
        }

        private void CheckDokumentZwrotu()
        {
            try
            {
                if(_typ == TypDokumentu.Dokument_Handlowy)
                {
                    var obecnyDokument = Sql.PobierzDane(Query.AdresDostawyZDokumentuMagazynowego(_wfmag.IdDokumentu));
                    var wszystkieZaznaczoneDokumentyZwrotne = _wfmag.PobierzFormyDostawy_DokumentZwrotny();
                    if (wszystkieZaznaczoneDokumentyZwrotne.Rows.Count > 0)
                    {
                        for (var i = 0; i < wszystkieZaznaczoneDokumentyZwrotne.Rows.Count; i++)
                        {
                            if (wszystkieZaznaczoneDokumentyZwrotne.Rows[i]["nazwa"].ToString().Equals(obecnyDokument.Rows[0]["nazwa"].ToString()))
                            {
                                if (wszystkieZaznaczoneDokumentyZwrotne.Rows[i]["is_enabled"].ToString().Equals("1"))
                                {
                                    chDokZwrot.Checked = true;
                                }
                                else
                                {
                                    chDokZwrot.Checked = false;
                                }
                            }
                        }
                    }
                    else
                    {
                        chDokZwrot.Checked = false;
                    }
                }
                

                
            }
            catch(Exception e)
            {
                Log.AddLog(e.ToString());
            }
            
        }

        private void cbAdresy_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbAdresy.SelectedValue == null)
                return;

            var tab = cbAdresy.SelectedValue.ToString().Split(new[] {"&&"}, StringSplitOptions.None);

            txtNazwaOdb.Text = tab[1];
            txtImieNazwiskoOdb.Text = tab[2];
            txtUlicaOdb.Text = tab[3];
            txtKodOdb.Text = tab[4];
            txtMiejscOdb.Text = tab[5];
            txtkrajOdb.Text = tab[6];
            txtTelOdb.Text = tab[7];
        }

        private int _cwiartka = 1;

        private void ZmienPozycje(object sender, EventArgs e)
        {
            _cwiartka = Convert.ToInt32(((Button) sender).Name.Split('Q')[1]);

            var tab = new List<Control> {btnQ1, btnQ2, btnQ3, btnQ4, btnpQ1, btnpQ2, btnpQ3, btnpQ4};
            tab.ForEach(c => c.BackColor = SystemColors.Control);
            switch (_cwiartka)
            {
                case 1:
                {
                    btnQ1.BackColor = Color.Gray;
                    btnpQ1.BackColor = Color.Gray;
                    break;
                }
                case 2:
                {
                    btnQ2.BackColor = Color.Gray;
                    btnpQ2.BackColor = Color.Gray;
                    break;
                }
                case 3:
                {
                    btnQ3.BackColor = Color.Gray;
                    btnpQ3.BackColor = Color.Gray;
                    break;
                }
                case 4:
                {
                    btnQ4.BackColor = Color.Gray;
                    btnpQ4.BackColor = Color.Gray;
                    break;
                }
            }
        }

        private void FormatDgZam()
        {
            if (dgZamowienia.Rows.Count > 0)
            {
                _checkboxHeaderZam.Visible = true;

                dgZamowienia.Columns["id"].Visible = false;
                dgZamowienia.Columns["id_kontrahenta"].Visible = false;
                dgZamowienia.Columns["DPD"].Visible = false;
                dgZamowienia.Columns["wartosc_brutto"].Visible = false;

                dgZamowienia.Columns["numer"].HeaderText = "Dokument WF-Mag";
                dgZamowienia.Columns["data"].HeaderText = "Data";
                dgZamowienia.Columns["nazwa"].HeaderText = "Kontrahent";
                dgZamowienia.Columns["wartosc_brutto"].HeaderText = "Kwota";
                dgZamowienia.Columns["wartosc_brutto"].DefaultCellStyle.Format = "N2";
                dgZamowienia.Columns["FORMA_PLATNOSCI"].DefaultCellStyle.Format = "Forma płatności";

                dgZamowienia.Columns["numer"].Width = 100;
                dgZamowienia.Columns["data"].Width = 80;
                dgZamowienia.Columns["wartosc_brutto"].Width = 100;
                dgZamowienia.Columns["zaznaczZ"].Width = 30;


                dgZamowienia.Columns["id"].ReadOnly = true;
                dgZamowienia.Columns["id_kontrahenta"].ReadOnly = true;
                dgZamowienia.Columns["numer"].ReadOnly = true;
                dgZamowienia.Columns["data"].ReadOnly = true;
                dgZamowienia.Columns["nazwa"].ReadOnly = true;
                dgZamowienia.Columns["wartosc_brutto"].ReadOnly = true;
                dgZamowienia.Columns["FORMA_PLATNOSCI"].ReadOnly = true;

                for (var i = 0; i < dgZamowienia.Rows.Count; i++)
                {
                    if (_pobranieCodDla != null && _pobranieCodDla.Count > 0 &&
                        _pobranieCodDla.Contains(dgZamowienia.Rows[i].Cells["FORMA_PLATNOSCI"].Value.ToString()))
                        dgZamowienia.Rows[i].DefaultCellStyle.BackColor = Color.LightGoldenrodYellow;


                    var cell = (DataGridViewDisableCheckBoxCell) dgZamowienia.Rows[i].Cells[0];

                    if (dgZamowienia.Rows[i].Cells["DPD"].Value.ToString() == "1")
                    {
                        cell.Enabled = false;
                        cell.Checked = true;
                        dgZamowienia.Rows[i].DefaultCellStyle.ForeColor = Color.LightGray;
                    }
                    else
                    {
                        cell.Enabled = true;
                        cell.Checked = false;
                    }
                }
            }
            else
            {
                _checkboxHeaderZam.Visible = false;
            }
        }

        private void FormatDgMag()
        {
            if (dgMagazynowe.Rows.Count > 0)
            {
                _checkboxHeaderMag.Visible = true;

                dgMagazynowe.Columns["id"].Visible = false;
                dgMagazynowe.Columns["id_kontrahenta"].Visible = false;
                dgMagazynowe.Columns["DPD"].Visible = false;
                dgMagazynowe.Columns["wartosc_brutto"].Visible = false;

                dgMagazynowe.Columns["numer"].HeaderText = "Dokument WF-Mag";
                dgMagazynowe.Columns["data"].HeaderText = "Data";
                dgMagazynowe.Columns["nazwa"].HeaderText = "Kontrahent";
                dgMagazynowe.Columns["wartosc_brutto"].HeaderText = "Kwota";
                dgMagazynowe.Columns["wartosc_brutto"].DefaultCellStyle.Format = "N2";

                dgMagazynowe.Columns["numer"].Width = 100;
                dgMagazynowe.Columns["data"].Width = 80;
                dgMagazynowe.Columns["wartosc_brutto"].Width = 100;
                dgMagazynowe.Columns["zaznaczM"].Width = 30;

                dgMagazynowe.Columns["id"].ReadOnly = true;
                dgMagazynowe.Columns["id_kontrahenta"].ReadOnly = true;
                dgMagazynowe.Columns["numer"].ReadOnly = true;
                dgMagazynowe.Columns["data"].ReadOnly = true;
                dgMagazynowe.Columns["nazwa"].ReadOnly = true;
                dgMagazynowe.Columns["wartosc_brutto"].ReadOnly = true;

                for (var i = 0; i < dgMagazynowe.Rows.Count; i++)
                {
                    var cell = (DataGridViewDisableCheckBoxCell) dgMagazynowe.Rows[i].Cells[0];

                    if (dgMagazynowe.Rows[i].Cells["DPD"].Value.ToString() == "1")
                    {
                        cell.Enabled = false;
                        cell.Checked = true;
                    }
                    else
                    {
                        cell.Enabled = true;
                        cell.Checked = false;
                    }
                }
            }
            else
            {
                _checkboxHeaderMag.Visible = false;
            }
        }

        private void FormatDgHan()
        {
            if (dgHandlowe.Rows.Count > 0)
            {
                _checkboxHeaderHan.Visible = true;

                dgHandlowe.Columns["id"].Visible = false;
                dgHandlowe.Columns["id_kontrahenta"].Visible = false;
                dgHandlowe.Columns["DPD"].Visible = false;
                dgHandlowe.Columns["wartosc_brutto"].Visible = false;

                dgHandlowe.Columns["numer"].HeaderText = "Dokument WF-Mag";
                dgHandlowe.Columns["data"].HeaderText = "Data wystawienia";
                dgHandlowe.Columns["nazwa"].HeaderText = "Kontrahent";
                dgHandlowe.Columns["wartosc_brutto"].HeaderText = "Kwota";
                dgHandlowe.Columns["wartosc_brutto"].DefaultCellStyle.Format = "N2";
                dgHandlowe.Columns["FORMA_PLATNOSCI"].DefaultCellStyle.Format = "Forma płatności";

                dgHandlowe.Columns["numer"].Width = 100;
                dgHandlowe.Columns["data"].Width = 80;
                dgHandlowe.Columns["wartosc_brutto"].Width = 100;
                dgHandlowe.Columns["zaznaczH"].Width = 30;
                dgHandlowe.Columns["FORMA_PLATNOSCI"].ReadOnly = true;


                dgHandlowe.Columns["id"].ReadOnly = true;
                dgHandlowe.Columns["id_kontrahenta"].ReadOnly = true;
                dgHandlowe.Columns["numer"].ReadOnly = true;
                dgHandlowe.Columns["data"].ReadOnly = true;
                dgHandlowe.Columns["nazwa"].ReadOnly = true;
                dgHandlowe.Columns["wartosc_brutto"].ReadOnly = true;

                for (var i = 0; i < dgHandlowe.Rows.Count; i++)
                {
                    if (_pobranieCodDla != null && _pobranieCodDla.Count > 0 &&
                        _pobranieCodDla.Contains(dgHandlowe.Rows[i].Cells["FORMA_PLATNOSCI"].Value.ToString()))
                        dgHandlowe.Rows[i].DefaultCellStyle.BackColor = Color.LightGoldenrodYellow;
                    var cell = (DataGridViewDisableCheckBoxCell) dgHandlowe.Rows[i].Cells[0];

                    if (dgHandlowe.Rows[i].Cells["DPD"].Value.ToString() == "1")
                    {
                        cell.Enabled = false;
                        cell.Checked = true;
                    }
                    else
                    {
                        cell.Enabled = true;
                        cell.Checked = false;
                    }
                }
            }
            else
            {
                _checkboxHeaderHan.Visible = false;
            }
        }

        private void PrzejdzDoWidokuGenerowania(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.RowIndex == -1 || e.ColumnIndex == 0)
                return;

            var dg = (DataGridView) sender;

            var chk = (DataGridViewDisableCheckBoxCell) dg.Rows[e.RowIndex].Cells[0];
            if (!chk.Enabled)
                return;
            _uslugi = null;
            switch (dg.Name)
            {
                case "dgZamowienia":
                {
                    Default.cbMagazynGlobal = cbMagazynZamowienie.SelectedIndex;
                    Default.Save();
                    _id = dgZamowienia.SelectedRows[0].Cells["id"].Value.ToString();
                    _typ = TypDokumentu.Zamowienie;
                    break;
                }
                case "dgMagazynowe":
                {
                    Default.cbMagazynGlobal = cbMagazynMag.SelectedIndex;
                    Default.Save();
                    _id = dgMagazynowe.SelectedRows[0].Cells["id"].Value.ToString();
                    _typ = TypDokumentu.Dokument_Magazynowy;
                    break;
                }
                case "dgHandlowe":
                {
                    Default.cbMagazynGlobal = cbMagazynyHan.SelectedIndex;
                    Default.Save();
                    _id = dgHandlowe.SelectedRows[0].Cells["id"].Value.ToString();
                    _typ = TypDokumentu.Dokument_Handlowy;
                    break;
                }
            }

            PobierzDaneDokumentuWfMag(true);

            SetEnableForPnlGenerowanie(true);
        }

        private void PobierzAdresy()
        {
            PobierzAdresNadawcy();

            PoberzAdresOdbiorcy();
        }

        private void PoberzAdresOdbiorcy()
        {
            var adresy = new Dictionary<string, string>();
            try
            {
                if (_wfmag.AdresyOdbiorcy == null || _wfmag.AdresyOdbiorcy.Count == 0)
                {
                    cbAdresy.DataSource = null;

                    txtNazwaOdb.Text = "";
                    txtImieNazwiskoOdb.Text = "";
                    txtUlicaOdb.Text = "";
                    txtKodOdb.Text = "";
                    txtMiejscOdb.Text = "";
                    txtkrajOdb.Text = "";
                    txtTelOdb.Text = "";
                    txtMailOdb.Text = "";
                }
                else
                {
                    foreach (var adres in _wfmag.AdresyOdbiorcy)
                        try
                        {
                            adresy.Add(adres.Nazwa + " " + adres.ImieNazwisko + " " + adres.Ulica + " " +
                                       adres.Kod + " " + adres.Miejscowosc + " " +
                                       adres.Kraj + " " + adres.Telefon,
                                "&&" + adres.Nazwa + "&&" + adres.ImieNazwisko + "&&" + adres.Ulica + "&&" +
                                adres.Kod + "&&" + adres.Miejscowosc + "&&" +
                                adres.Kraj + "&&" + adres.Telefon + "&&"
                            );
                        }
                        catch (Exception ex)
                        {
                            Log.AddLog("PoberzAdresOdbiorcy1#" + ex);
                        }

                    cbAdresy.DataSource = new BindingSource(adresy, null);
                    cbAdresy.DisplayMember = "Key";
                    cbAdresy.ValueMember = "Value";

                    if (_wfmag != null && _wfmag.AdresEmail != null)
                        txtMailOdb.Text = _wfmag.AdresEmail;
                }
            }
            catch (Exception ex)
            {
                Log.AddLog("PoberzAdresOdbiorcy2#" + ex);
            }
        }

        private void PobierzAdresNadawcy()
        {
            if (Default.adresNadawcyZUstawien)
            {
                txtNazwaNad.Text = Default.nazwaNad;
                txtImieNazwiskoNad.Text = Default.imieNazwiskoNad;
                txtUlicaNad.Text = Default.ulicaNad;
                txtKodNad.Text = Default.kodNad;
                txtMiejscNad.Text = Default.miejscowoscNad;
                txtkrajNad.Text = Default.krajNad;
                txtTelNad.Text = Default.telefonNad;
                txtMailNad.Text = Default.mailNad;
            }
            else
            {
                var adres = _wfmag.AdresNadawcy;
                if (adres == null)
                    return;

                txtNazwaNad.Text = adres.Nazwa;
                txtUlicaNad.Text = adres.Ulica;
                txtKodNad.Text = adres.Kod;
                txtMiejscNad.Text = adres.Miejscowosc;
                txtkrajNad.Text = adres.Kraj;
                txtTelNad.Text = adres.Telefon;
                txtMailNad.Text = "";
            }
        }

        private DokumentyFiltr ParseEnum1(string value)
        {
            return (DokumentyFiltr) Enum.Parse(typeof(DokumentyFiltr), value.ToUpper(), true);
        }

        private DokumentyFiltr ParseEnum(int index)
        {
            return (DokumentyFiltr) index;
        }

        private void PobierzZam(object sender, EventArgs e)
        {
            PokazDokumenty(ParseEnum(cbFiltrZam.SelectedIndex), dgZamowienia, true);
        }

        private void PobierzMag(object sender, EventArgs e)
        {
            PokazDokumenty(ParseEnum(cbFiltrMag.SelectedIndex), dgMagazynowe, true);
        }

        private void PobierzHan(object sender, EventArgs e)
        {
            PokazDokumenty(ParseEnum(cbFiltrHan.SelectedIndex), dgHandlowe, true);
        }

        private void dokumentyToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void generowanieToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (_aktywnyPanel != 2)
            {
                PokazPanel(2);
                WyczyscFormularz();
                SetEnableForPnlGenerowanie(true);
                _wfmag = new WfMag();
            }
        }

        private void przygotowaneToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (_aktywnyPanel != 3)
            {
                PokazPanel(3);
                PobierzListy();
            }
        }

        private void btnGenerujList_Click(object sender, EventArgs e)
        {
            var formaDostawyObecnegoDokumentu = Sql.PobierzDane(Query.AdresDostawyZDokumentuMagazynowego(_wfmag.IdDokumentu));
            var wszystkieZablokowaneFormyDostawy = _wfmag.PobierzFormyDostawy_Blokowanie();

            if (chPobranie.Checked)
                _isPobranie = true;
            else
                _isPobranie = false;

            
            if (wszystkieZablokowaneFormyDostawy.Rows.Count > 0)
            {
                for (var i = 0; i < wszystkieZablokowaneFormyDostawy.Rows.Count; i++)
                {
                    if(wszystkieZablokowaneFormyDostawy.Rows[i]["nazwa"].ToString().Equals(formaDostawyObecnegoDokumentu.Rows[0]["nazwa"].ToString()))
                    {
                        if(wszystkieZablokowaneFormyDostawy.Rows[i]["is_enabled"].ToString().Equals("1"))
                        {
                            BrakZablokowanychFormDostawy = false;
                        }
                        else
                        {
                            BrakZablokowanychFormDostawy = true;
                        }
                    }
                }
            }
            else
            {
                BrakZablokowanychFormDostawy = true;
            }


            if(BrakZablokowanychFormDostawy == true)
            {
                if (dgPaczki.Rows.Count == 1)
                {
                    MessageBox.Show(@"Brak zdefiniowanych paczek");
                    return;
                }

                GenerujList();

                if (!string.IsNullOrEmpty(txtNrListu.Text))
                    MessageBox.Show(@"Wygenerowano numer listu");
                else
                    MessageBox.Show(@"Nie wygenerowano numeru listu");

                if (_licznikWygenerowanychEtykiet > 0)
                    MessageBox.Show(@"Zapisano!");

                _licznikWygenerowanychEtykiet = 0;
            }
            else
            {
                MessageBox.Show($@"Uwaga! Nie można nadać przesyłki. Sposób dostawy do odbiorcy jest {formaDostawyObecnegoDokumentu.Rows[0]["nazwa"]}");
            }
            
        }

        private void GenerujList()
        {
            txtNrListu.Text = "";
            PrzygotujPaczki();
            PrzygotujAdresy();
            PrzygotujUslugi();
            PrzygotujPlatnika();

            Wyslij();
        }


        private int _licznikWygenerowanychEtykiet;

        private void Wyslij()
        {
            var umlf = new packageOpenUMLFeV3[1];

            if (!string.IsNullOrEmpty(txtReference.Text))
                _pkg.reference = txtReference.Text.Substring2(100);
            if (!string.IsNullOrEmpty(txtRef1.Text))
                _pkg.ref1 = txtRef1.Text.Substring2(100);
            if (!string.IsNullOrEmpty(txtRef2.Text))
                _pkg.ref2 = txtRef2.Text.Substring2(100);

            _pkg.ref3 = "WFM";

            umlf[0] = _pkg;
            var policy = pkgNumsGenerationPolicyV1.IGNORE_ERRORS;

            try
            {
                var response = _dpdService.generatePackagesNumbersV4(umlf, policy, true, "PL", _authData);


                if (response == null)
                {
                    MessageBox.Show(@"Nie przetworzono żądania");
                    return;
                }

                var invalidFields = response.Packages[0].ValidationDetails;

                if (invalidFields == null)
                {
                    if (response.Status != "OK")
                    {
                        MessageBox.Show(@"Operacja nie powiodła się. Status odpowiedzi: " + response.Status);
                        PokazZwroconeBledy(response);

                        return;
                    }

                    foreach (var p in response.Packages[0].Parcels)
                        txtNrListu.Text += p.Waybill + ";";

                    if (!string.IsNullOrEmpty(txtNrListu.Text.Replace(";", "")))
                    {
                        txtNrListu.Text = txtNrListu.Text.Substring(0, txtNrListu.Text.Length - 1);

                        ZapiszListDoTabeli(_id, _typ, txtNrListu.Text);
                        _wfmag.ZapiszNumerPrzesylki(txtNrListu.Text);
                        _wfmag.ZapiszNrListuNaDokumencie(txtNrListu.Text);

                        btnGenerujList.Enabled = false;
                        if (!chGenerujEtykiete.Checked) return;
                        var paczki = response.Packages[0].Parcels.Select(p => new parcelDSPV1 {waybill = p.Waybill}).ToList();

                        var sessionType = sessionTypeDSPEnumV1.DOMESTIC;
                        if (_addressReceiver.countryCode != "PL")
                            sessionType = sessionTypeDSPEnumV1.INTERNATIONAL;

                        var etykieta = GenerujEtykiete(paczki, sessionType);

                        if (!etykieta.Exists) return;
                        _licznikWygenerowanychEtykiet++;

                        var zap = "update _DPD_Listy set etykieta = 1 where nr_listu = '" + txtNrListu.Text +
                                  "'";
                        Sql.RunQuery(zap);

                        ModifyPdf.Create(_cwiartka, etykieta.FullName, response.Packages[0].Parcels.Length);

                        if (!chDrukuj.Checked) return;
                        if (etykieta.Extension.ToLower() == ".pdf")
                            PrinterClass.PrintFile(etykieta.FullName, Default.drukarkaEtykiet);
                        else
                            RawPrinterHelper.SendFileToPrinter(Default.drukarkaEtykiet,
                                etykieta.FullName);
                    }
                    else
                    {
                        MessageBox.Show(@"Nie wygenerowano listu");
                    }
                }
                else
                {
                    var errors = new StringBuilder().AppendLine("Wystąpiły następujące błedy podczas przetwarzania:");
                    foreach (var invalid in invalidFields)
                        errors.AppendLine(invalid.Info);
                    MessageBox.Show(errors.ToString());
                }
            }
            catch (Exception ex)
            {
                if (ex.Message == "Login failed")
                    MessageBox.Show("Błąd logowania");
                Log.AddLog(ex.ToString());
            }
        }

        private void PokazZwroconeBledy(packagesGenerationResponseV2 response)
        {
            var errors = new List<validationInfoPGRV2>();

            foreach (var pkg in response.Packages)
            foreach (var parcel in pkg.Parcels)
                if (parcel.ValidationDetails != null)
                    Log.AddLog("PARCELID : " + parcel.ParcelId ?? "null" +
                               "; WAYBILL : " + parcel.Waybill ?? "null" +
                               "; STATUS : " + parcel.Status ?? "null");

            foreach (var pkg in response.Packages)
            foreach (var parcel in pkg.Parcels)
                if (parcel.ValidationDetails != null)
                    errors.AddRange(parcel.ValidationDetails);

            if (errors.Count == 0)
                return;
            var form = new BledyForm(errors);
            form.ShowDialog();
        }

        private void ZapiszListDoTabeli(string idDok, TypDokumentu typDok, string nrListu)
        {
            try
            {
                var sessionType = "";
                sessionType = _addressReceiver.countryCode == "PL" ? "DOMESTIC" : "INTERNATIONAL";

                var sb = new StringBuilder();

                sb.Append(@"declare @tab table ( id int)

insert into _DPD_LISTY ([id_dok]
      ,[typ]
      ,[numer_dok]
      ,[nr_listu]
      ,[nr_protokolu]
      ,[etykieta]
      ,[data_listu]
      ,[data_protokolu]
      ,[sessionType]
      ,[adres_nadawcy_nazwa]
      ,[adres_nadawcy_imieNazwisko]
      ,[adres_nadawcy_ulica]
      ,[adres_nadawcy_kod]
      ,[adres_nadawcy_kraj]
      ,[adres_nadawcy_miejscowosc]
      ,[adres_nadawcy_tel]
      ,[adres_nadawcy_mail]
      ,[adres_odbiorcy_nazwa]
      ,[adres_odbiorcy_imieNazwisko]
      ,[adres_odbiorcy_ulica]
      ,[adres_odbiorcy_kod]
      ,[adres_odbiorcy_kraj]
      ,[adres_odbiorcy_miejscowosc]
      ,[adres_odbiorcy_tel]
      ,[adres_odbiorcy_mail]
      ,[ref1]
      ,[ref2]
      ,[platnik]
      ,[id_magazynu]
      ,[platnikFID]
       )  output inserted.id into @tab

  values (");

                sb.AppendFormat("{0},", idDok ?? "null");
                sb.AppendFormat("{0},", (int) typDok);
                sb.AppendFormat("'{0}',", _wfmag?.NumerDokumentu);
                sb.AppendFormat("'{0}',", nrListu);
                sb.AppendFormat("null,");
                sb.AppendFormat("0,");
                sb.AppendFormat("getdate(),");
                sb.AppendFormat("null,");
                sb.AppendFormat("'{0}',", sessionType);

                sb.AppendFormat("'{0}',", _addressSender?.company);
                sb.AppendFormat("'{0}',", _addressSender?.name);
                sb.AppendFormat("'{0}',", _addressSender?.address);
                sb.AppendFormat("'{0}',", _addressSender?.postalCode);
                sb.AppendFormat("'{0}',", _addressSender?.countryCode);
                sb.AppendFormat("'{0}',", _addressSender?.city);
                sb.AppendFormat("'{0}',", _addressSender?.phone);
                sb.AppendFormat("'{0}',", _addressSender?.email);

                sb.AppendFormat("'{0}',", _addressReceiver?.company);
                sb.AppendFormat("'{0}',", _addressReceiver?.name);
                sb.AppendFormat("'{0}',", _addressReceiver?.address);
                sb.AppendFormat("'{0}',", _addressReceiver?.postalCode);
                sb.AppendFormat("'{0}',", _addressReceiver?.countryCode);
                sb.AppendFormat("'{0}',", _addressReceiver?.city);
                sb.AppendFormat("'{0}',", _addressReceiver?.phone);
                sb.AppendFormat("'{0}',", _addressReceiver?.email);

                sb.AppendFormat("'{0}',", txtRef1.Text);
                sb.AppendFormat("'{0}',", txtRef2.Text);
                sb.AppendFormat("'{0}',", cbPlatnik?.SelectedValue);

                sb.AppendFormat("'{0}',", GetIdMagSaveListToTable(_typ, idDok));

                sb.AppendFormat("{0}) select id from @tab",
                    cbPlatnik?.SelectedItem?.ToString() == "Nadawca" ? _fid : _platnikFid);

                var idDpd = Sql.RunQueryScalarWithoutTry(sb?.ToString());

                if (idDpd == null)
                    return;

                ZapiszUslugi(idDpd);
                ZapiszPaczki(idDpd);
            }
            catch (Exception ex)
            {
                MessageBox.Show(@"Wystapił błąd zapisu listu do bazy danych. Sprawdź szczegóły w logach");
                Log.AddLog(ex.ToString());
            }
        }

        private string GetIdMagSaveListToTable(TypDokumentu typ, string idDok)
        {
            try
            {
                switch (typ)
                {
                    case TypDokumentu.Zamowienie:
                    {
                        var selectedItem = ((KeyValuePair<int, string>?) cbMagazynZamowienie?.SelectedItem)?.Key
                            .ToString();

                        if (!string.IsNullOrEmpty(selectedItem) && cbMagazynZamowienie?.SelectedIndex > -1)
                            return selectedItem;

                        var query = Sql.RunQueryScalar("SELECT ID_MAGAZYNU FROM ZAMOWIENIE WHERE ID_ZAMOWIENIA = " +
                                                       idDok);

                        return string.IsNullOrEmpty(query) ? "0" : query;
                    }
                    case TypDokumentu.Dokument_Magazynowy:
                    {
                        var selectedItem = ((KeyValuePair<int, string>?) cbMagazynMag?.SelectedItem)?.Key.ToString();

                        if (!string.IsNullOrEmpty(selectedItem) && cbMagazynMag?.SelectedIndex > -1)
                            return selectedItem;

                        var query =
                            Sql.RunQueryScalar(
                                "SELECT ID_MAGAZYNU FROM DOKUMENT_MAGAZYNOWY WHERE ID_DOK_MAGAZYNOWEGO = " + idDok);

                        return string.IsNullOrEmpty(query) ? "0" : query;
                    }
                    case TypDokumentu.Dokument_Handlowy:
                    {
                        var selectedItem = ((KeyValuePair<int, string>?) cbMagazynyHan?.SelectedItem)?.Key.ToString();

                        if (!string.IsNullOrEmpty(selectedItem) && cbMagazynyHan?.SelectedIndex > -1)
                            return selectedItem;

                        var query =
                            Sql.RunQueryScalar(
                                "SELECT ID_MAGAZYNU FROM DOKUMENT_HANDLOWY WHERE ID_DOKUMENTU_HANDLOWEGO = " + idDok);

                        return string.IsNullOrEmpty(query) ? "0" : query;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.AddLog(ex.Message);
            }

            return "0";
        }

        private void ZapiszUslugi(string idDpd)
        {
            var sb = new StringBuilder();

            sb.Append(@"insert into _DPD_USLUGI 
                        (
                                   [id]                             --1
                                   ,[doreczenieGwarantowane]        --2
                                   ,[doreczenieGwarantowaneTyp]     --3
                                   ,[doreczenieGwarantowaneWartosc] --4
                                   ,[doreczenieDoRakWlasnych]       --5
                                   ,[doreczenieNaAdresPryw]         --6
                                   ,[pobranie]                      --7
                                   ,[pobranieKwota]                 --8
                                   ,[odbiorWlasny]                  --9
                                   ,[odbiorWlasnyTyp]               --10
                                   ,[dokumentyZwrotne]              --11
                                   ,[DOX]                           --12
                                   ,[przesylkaZwrotna]              --13
                                   ,[opony]                         --14
                                   ,[oponyExport]                   --15
                                   ,[przesylkaWartosciowa]          --16
                                   ,[przesylkaWartosciowaKwota]     --17
                                   ,[przesylkaPaletowa]             --18
                                   ,pudo                            --19
                                   ,pudoWartosc                     --20
                                   ,duty                            --21
                                   ,dutyWartosc                     --22
                                   ,dpdExpress                      --23
                        ) values (");

            sb.AppendFormat("{0},", idDpd); //1
            sb.AppendFormat("{0},", chDorGwaran.Checked ? "1" : "0"); //2

            string doreczenieTyp = null;
            string doreczenieWartosc = null;
            if (chDorGwaran.Checked)
            {
                doreczenieTyp = cbDoreczenie.SelectedValue.ToString();
                if (doreczenieTyp == "DPD na godzinę")
                    doreczenieWartosc = txtNaOkreslonaGodzine.Text;
            }

            sb.AppendFormat("{0},", doreczenieTyp == null ? "null" : $"'{doreczenieTyp}'"); //3
            sb.AppendFormat("{0},", doreczenieWartosc == null ? "null" : $"'{doreczenieWartosc}'"); //4

            sb.AppendFormat("{0},", chDorDoRakWl.Checked ? "1" : "0"); //5
            sb.AppendFormat("{0},", chDorNaAdrPryw.Checked ? "1" : "0"); //6
            sb.AppendFormat("{0},", chPobranie.Checked ? "1" : "0"); //7

            string pobranieKwota = null;
            if (chPobranie.Checked && !string.IsNullOrEmpty(txtPobranie.Text))
                pobranieKwota = txtPobranie.Text;
            sb.AppendFormat("{0},", string.IsNullOrEmpty(pobranieKwota) ? "null" : pobranieKwota.Replace(",", ".")); //8

            sb.AppendFormat("{0},", chOdbiorWlasny.Checked ? "1" : "0"); //9

            string odbiorWlasnyTyp = null;
            if (chOdbiorWlasny.Checked)
                odbiorWlasnyTyp = rbOsoba.Checked ? "Osoba" : "Firma";
            sb.AppendFormat("{0},", odbiorWlasnyTyp == null ? "null" : $"'{odbiorWlasnyTyp}'"); //10

            sb.AppendFormat("{0},", chDokZwrot.Checked ? "1" : "0"); //11
            sb.AppendFormat("{0},", chPrzesList.Checked ? "1" : "0"); //12
            sb.AppendFormat("{0},", chPrzesZwrot.Checked ? "1" : "0"); //13
            sb.AppendFormat("{0},", chOpony.Checked ? "1" : "0"); //14
            sb.AppendFormat("{0},", chTiresExport.Checked ? "1" : "0"); //15
            sb.AppendFormat("{0},", chDeklWart.Checked ? "1" : "0"); //16

            string przesylkaWartosciowaKwota = null;
            if (chDeklWart.Checked)
                przesylkaWartosciowaKwota = txtDeklarowanaWartosc.Text;

            sb.AppendFormat("{0},", chDeklWart.Checked ? przesylkaWartosciowaKwota?.Replace(",", ".") : "0"); //17

            sb.AppendFormat("{0},", chPallet.Checked ? "1" : "0"); //18

            sb.AppendFormat("{0},", chPudo.Checked ? "1" : "0"); //19
            sb.AppendFormat("{0},", string.IsNullOrEmpty(txtPudo.Text) ? "null" : $"'{txtPudo.Text}'"); //20
            sb.AppendFormat("{0},", chDuty.Checked ? "1" : "0"); //21
            sb.AppendFormat("{0},", string.IsNullOrEmpty(txtDuty.Text) ? "null" : $"'{txtDuty.Text}'"); //22
            sb.AppendFormat("{0}", chDpdExpress.Checked ? "1" : "0"); //23
            sb.AppendFormat(")");
            
            Sql.RunQueryWithoutTry(sb.ToString());
        }

        private void ZapiszPaczki(string idDpd)
        {
            var sb = new StringBuilder();
            sb.Append(@"insert into _DPD_PACZKI (
                       [id]
                      ,[lp]
                      ,[waga]
                      ,[zawartosc]
                      ,[uwagi]
                      ,[dlugosc]
                      ,[szerokosc]
                      ,[wysokosc]
                ) values ");

            for (var i = 0; i < dgPaczki.Rows.Count - 1; i++)
            {
                sb.AppendFormat("({0},", idDpd);
                sb.AppendFormat("{0},", dgPaczki.Rows[i].Cells["LP"].Value);
                sb.AppendFormat("{0},", dgPaczki.Rows[i].Cells["waga"].Value.ToString().Replace(",", ".") ?? "");
                sb.AppendFormat("{0},", dgPaczki.Rows[i].Cells["content"].Value == null
                    ? "null"
                    : $"'{dgPaczki.Rows[i].Cells["content"].Value}'");
                sb.AppendFormat("{0},", dgPaczki.Rows[i].Cells["cust1"].Value == null
                    ? "null"
                    : $"'{dgPaczki.Rows[i].Cells["cust1"].Value}'");
                sb.AppendFormat("{0},", dgPaczki.Rows[i].Cells["długość"].Value ?? "null");
                sb.AppendFormat("{0},", dgPaczki.Rows[i].Cells["szerokość"].Value ?? "null");
                sb.AppendFormat("{0}),", dgPaczki.Rows[i].Cells["wysokość"].Value ?? "null");
            }

            sb.Remove(sb.Length - 1, 1);

            Sql.RunQueryWithoutTry(sb.ToString());
        }

        private void PrzygotujAdresy()
        {
            _addressSender = new packageAddressOpenUMLFeV1
            {
                address = txtUlicaNad.Text.Substring2(100),
                city = txtMiejscNad.Text.Substring2(50),
                name = txtImieNazwiskoNad.Text.Substring2(100),
                countryCode = txtkrajNad.Text?.Trim()?.ToUpper()?.Substring2(2),
                email = txtMailNad.Text.Substring2(100),
                phone = txtTelNad.Text.Substring2(100),
                postalCode = txtKodNad.Text?.Replace("-", "")?.Replace(" ", "")?.Substring2(10),
                company = txtNazwaNad.Text.Substring2(100)
            };

            _addressReceiver = new packageAddressOpenUMLFeV1
            {
                address = txtUlicaOdb.Text.Substring2(100),
                city = txtMiejscOdb.Text.Substring2(50),
                company = txtNazwaOdb.Text.Substring2(100),
                name = txtImieNazwiskoOdb.Text.Substring2(100),
                countryCode = txtkrajOdb.Text?.Trim()?.ToUpper()?.Substring2(2),
                email = txtMailOdb.Text.Substring2(100),
                phone = txtTelOdb.Text.Substring2(100),
                postalCode = txtKodOdb.Text?.Replace("-", "")?.Replace(" ", "")?.Substring2(10)
            };

            _pkg.receiver = _addressReceiver;
            _pkg.sender = _addressSender;
        }

        private void PrzygotujPlatnika()
        {
            if (cbPlatnik.SelectedValue.ToString() == "Nadawca")
            {
                _pkg.payerType = payerTypeEnumOpenUMLFeV1.SENDER;
                _pkg.sender.fid = _fid;
                _pkg.sender.fidSpecified = true;
            }

            if (cbPlatnik.SelectedValue.ToString() == "Odbiorca")
            {
                _pkg.payerType = payerTypeEnumOpenUMLFeV1.RECEIVER;
                _pkg.receiver.fid = _platnikFid;
                _pkg.receiver.fidSpecified = true;
            }

            if (cbPlatnik.SelectedValue.ToString() == "Trzecia strona")
            {
                _pkg.payerType = payerTypeEnumOpenUMLFeV1.THIRD_PARTY;
                _pkg.thirdPartyFID = _platnikFid;
                _pkg.thirdPartyFIDSpecified = true;
            }

            _pkg.payerTypeSpecified = true;
        }

        private void PrzygotujPaczki()
        {
            _pkg = new packageOpenUMLFeV3();

            var liczbaPaczek = dgPaczki.Rows.Count - 1;
            _pkg.parcels = new parcelOpenUMLFeV1[liczbaPaczek]; //ile paczek

            for (var i = 0; i < liczbaPaczek; i++)
            {
                var parcel1 = new parcelOpenUMLFeV1();

                if (!string.IsNullOrEmpty((string) dgPaczki.Rows[i].Cells["długość"].Value))
                {
                    parcel1.sizeXSpecified = true;
                    parcel1.sizeX = Convert.ToInt32(dgPaczki.Rows[i].Cells["długość"].Value);
                }

                if (!string.IsNullOrEmpty((string) dgPaczki.Rows[i].Cells["szerokość"].Value))
                {
                    parcel1.sizeYSpecified = true;
                    parcel1.sizeY = Convert.ToInt32(dgPaczki.Rows[i].Cells["szerokość"].Value);
                }

                if (!string.IsNullOrEmpty((string) dgPaczki.Rows[i].Cells["wysokość"].Value))
                {
                    parcel1.sizeZSpecified = true;
                    parcel1.sizeZ = Convert.ToInt32(dgPaczki.Rows[i].Cells["wysokość"].Value);
                }

                parcel1.weightSpecified = true;
                parcel1.content = dgPaczki.Rows[i].Cells["content"].Value == null
                    ? null
                    : dgPaczki.Rows[i].Cells["content"].Value.ToString().Substring2(300);
                parcel1.customerData1 = dgPaczki.Rows[i].Cells["cust1"].Value == null
                    ? null
                    : dgPaczki.Rows[i].Cells["cust1"].Value.ToString().Substring2(200);

                try
                {
                    var w = dgPaczki.Rows[i].Cells[1].Value.ToString().Replace(",", ".");

                    var waga = Convert.ToDouble(w);
                    parcel1.weight = waga;
                }
                catch (Exception ex)
                {
                    Log.AddLog(ex.ToString());
                }

                _pkg.parcels[i] = parcel1;
            }
        }

        private void PrzygotujUslugi()
        {
            var services = new servicesOpenUMLFeV4();

            _uslugi?.PrzeniesDaneDoKontrolek(pnlGenerowanie);

            var wfMag = new WfMag();
            if (chPobranie.Checked)
            {
                var cod = new serviceCODOpenUMLFeV1();
                if (!string.IsNullOrEmpty(txtPobranie.Text))
                {
                    cod.amount = txtPobranie.Text.Replace(",", ".");
                    cod.currency = serviceCurrencyEnum.PLN;
                    cod.currencySpecified = true;
                    services.cod = cod;
                }
            }
            else
            {
                txtPobranie.Text = "";
                chPobranie.Checked = false;
            }

            if (chOpony.Checked)
                services.tires = new serviceTiresOpenUMLFeV1();

            if (chTiresExport.Checked)
                services.tiresExport = new serviceTiresExportOpenUMLFeV1();

            if (chWniesienie.Checked)
                services.carryIn = new serviceCarryInOpenUMLFeV1();

            if (chPrzesZwrot.Checked)
                services.cud = new serviceCUDOpenUMLeFV1();

            if (chDeklWart.Checked)
            {
                var deklWart = new serviceDeclaredValueOpenUMLFeV1();
                if (!string.IsNullOrEmpty(txtDeklarowanaWartosc.Text))
                    deklWart.amount = txtDeklarowanaWartosc.Text.Replace(",", ".");
                deklWart.currency = serviceCurrencyEnum.PLN;
                deklWart.currencySpecified = true;

                services.declaredValue = deklWart;
            }

            if (chPrzesList.Checked)
            {
                var list = new servicePalletOpenUMLFeV1();
                services.dox = list;
            }

            if (chDorGwaran.Checked)
            {
                var guarantee = new serviceGuaranteeOpenUMLFeV1();

                if (cbDoreczenie.SelectedValue.ToString() == "DPD 9:30")
                {
                    guarantee.type = serviceGuaranteeTypeEnumOpenUMLFeV1.TIME0930;
                }

                else if (cbDoreczenie.SelectedValue.ToString() == "DPD 12:00")
                {
                    guarantee.type = serviceGuaranteeTypeEnumOpenUMLFeV1.TIME1200;
                }
                else if (cbDoreczenie.SelectedValue.ToString() == "DPD NEXTDAY")
                {
                    guarantee.type = serviceGuaranteeTypeEnumOpenUMLFeV1.DPDNEXTDAY;
                }

                else if (cbDoreczenie.SelectedValue.ToString() == "DPD na godzinę")
                {
                    guarantee.type = serviceGuaranteeTypeEnumOpenUMLFeV1.TIMEFIXED;
                    guarantee.value = txtNaOkreslonaGodzine.Text;
                }

                else if (cbDoreczenie.SelectedValue.ToString() == "Sobota")
                {
                    guarantee.type = serviceGuaranteeTypeEnumOpenUMLFeV1.SATURDAY;
                }

                guarantee.typeSpecified = true;
                services.guarantee = guarantee;
            }

            if (chPallet.Checked)
                services.pallet = new servicePalletOpenUMLFeV1();

            if (chDorDoRakWl.Checked)
                services.inPers = new serviceInPersOpenUMLFeV1();

            if (chDorNaAdrPryw.Checked)
                services.privPers = new servicePrivPersOpenUMLFeV1();

            if (chDokZwrot.Checked)
                services.rod = new serviceRODOpenUMLFeV1();

            if (chPudo.Checked)
                services.dpdPickup = new serviceDpdPickupOpenUMLFeV1
                {
                    pudo = txtPudo.Text
                };

            if (chOdbiorWlasny.Checked)
            {
                var self = new serviceSelfColOpenUMLFeV1
                {
                    receiver =
                        rbFirma.Checked
                            ? serviceSelfColReceiverTypeEnumOpenUMLFeV1.COMP
                            : serviceSelfColReceiverTypeEnumOpenUMLFeV1.PRIV,
                    receiverSpecified = true
                };
                services.selfCol = self;
            }

            if (chDpdExpress.Checked)
                services.dpdExpress = new serviceFlagOpenUMLF();

            if (chDuty.Checked)
                services.duty = new serviceDutyOpenUMLeFV2
                {
                    amount = !string.IsNullOrEmpty(txtDuty.Text)? txtDuty.Text : "0",
                    currency = serviceCurrencyEnum.PLN,
                    currencySpecified = true
                };

            _pkg.services = services;
        }

        private void PobierzListy()
        {
            try
            {
                if (_wfmag == null)
                    return;

                var idki = PobierzZaznaczone(dgLista);

                if (chZaznaczoneL.Checked)
                {
                    DataTable dt = null;
                    dt = _wfmag.PobierzListy(txtNumerDok.Text, txtNadawca.Text, txtOdbiorca.Text, txtFindNrListu.Text,
                        txtNrProtokolu.Text, dpListyFrom.Value, dpListyTo.Value, cbKrajowe.SelectedItem.ToString());

                    var dtZaznaczone = dt.Clone();
                    dtZaznaczone.Clear();

                    for (var i = 0; i < dt.Rows.Count; i++)
                        if (idki.Contains(dt.Rows[i]["id"].ToString()))
                            dtZaznaczone.ImportRow(dt.Rows[i]);

                    dgLista.DataSource = dtZaznaczone;
                }
                else
                {
                    dgLista.DataSource = _wfmag.PobierzListy(txtNumerDok.Text, txtNadawca.Text, txtOdbiorca.Text,
                        txtFindNrListu.Text, txtNrProtokolu.Text, dpListyFrom.Value,
                        dpListyTo.Value, cbKrajowe.SelectedItem.ToString());
                }

                try
                {
                    dgLista.Columns["adres_odbiorcy_nazwa"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;
                    dgLista.Columns["adres"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;
                    dgLista.Columns["adresNadania"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;
                    dgLista.Columns["nr_listu"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;
                    dgLista.Columns["adres_nadawcy_nazwa"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;
                }
                catch (Exception ex)
                {
                    Log.AddLog(ex.ToString());
                }

                var liczbaPaczek = 0;

                if (dgLista.DataSource == null)
                {
                    Checked.Visible = false;
                    lblBrakWynikow.Visible = true;
                }
                else
                {
                    Checked.Visible = true;
                    lblBrakWynikow.Visible = false;

                    foreach (DataGridViewRow row in dgLista.Rows)
                    {
                        row.Cells["nr_listu"].Value = row.Cells["nr_listu"].Value.ToString()
                            .Replace(";", Environment.NewLine);

                        var paczki =
                            row.Cells["nr_listu"].Value.ToString()
                                .Split(new[] {Environment.NewLine}, StringSplitOptions.None)
                                .Length;
                        row.Cells["paczki"].Value = paczki;
                        liczbaPaczek += paczki;

                        row.Cells["adresNadania"].Value = row.Cells["adres_nadawcy_nazwa"].Value + Environment.NewLine +
                                                          row.Cells["adresNadania"].Value;

                        row.Cells["adres_odbiorcy_nazwa"].Value = row.Cells["adres_odbiorcy_nazwa"].Value +
                                                                  Environment.NewLine +
                                                                  row.Cells["adres"].Value;
                    }
                }

                lblLiczbaDok.Text = "Liczba przesyłek: " + dgLista.Rows.Count;
                lblLiczbaPaczek.Text = "Liczba paczek: " + liczbaPaczek;

                FormatDgLista();
                ZaznaczIdki(idki, dgLista);
            }
            catch (Exception ex)
            {
                Log.AddLog(ex.ToString());
            }
        }

        private void FormatDgLista()
        {
            if (dgLista.Rows.Count == 0)
            {
                _checkboxHeaderListy.Visible = false;
                return;
            }

            dgLista.EndEdit();

            _checkboxHeaderListy.Visible = true;
            dgLista.Columns["id"].Visible = false;
            dgLista.Columns["sessionType"].Visible = false;

            dgLista.Columns["nr_listu"].Width = 80; // nr listu
            dgLista.Columns["nr_protokolu"].Width = 75; // nr protokolu
            dgLista.Columns["adres"].Width = 195; //adres 
            // dgLista.Columns["telefon"].Width = 75;//tel
            dgLista.Columns["data_listu"].Width = 75; //data listu
            dgLista.Columns["data_protokolu"].Width = 75; //data prot
            dgLista.Columns["etykieta"].Width = 30; //etykieta
            dgLista.Columns["paczki"].Width = 38;
            dgLista.Columns["numer"].Width = 130;
            dgLista.Columns["adresNadania"].Width = 160;
            dgLista.Columns["adres_odbiorcy_nazwa"].Width = 160; // kontrahent

            dgLista.Columns["adres_odbiorcy_nazwa"].HeaderText = "Odbiorca";
            dgLista.Columns["adres_odbiorcy_nazwa"].DisplayIndex = 6;

            dgLista.Columns["nr_listu"].HeaderText = "Nr listu"; // nr listu
            dgLista.Columns["nr_protokolu"].HeaderText = "Nr protokołu"; // nr protokolu
            dgLista.Columns["adres"].HeaderText = "Adresy"; //adres 
            dgLista.Columns["adres"].Visible = false;

            //dgLista.Columns["telefon"].HeaderText = "Telefon";//tel
            dgLista.Columns["data_listu"].HeaderText = "Data listu"; //data listu
            dgLista.Columns["data_protokolu"].HeaderText = "Data protokołu"; //data prot
            dgLista.Columns["etykieta"].HeaderText = "Etyk"; //etykieta
            dgLista.Columns["paczki"].HeaderText = "Paczki";
            dgLista.Columns["numer"].HeaderText = "Dokument WF-Mag";
            dgLista.Columns["adresNadania"].HeaderText = "Nadawca";
            dgLista.Columns["adresNadania"].DisplayIndex = 3;
            dgLista.Columns["adres_odbiorcy_nazwa"].DisplayIndex = 4;
            //dgLista.Columns["adresNadania"].Visible = false;

            dgLista.Columns["nr_protokolu"].AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
            dgLista.Columns["nr_protokolu"].Width = 55;

            //dgLista.Columns["nr_listu"].AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
            //dgLista.Columns["nr_listu"].Width = 100;

            dgLista.Columns["numer"].AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
            dgLista.Columns["numer"].Width = 80;

            //dgLista.Columns["data_listu"].AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
            //dgLista.Columns["data_listu"].Width = 75;
            dgLista.Columns["data_listu"].Visible = false;
            dgLista.Columns["adresNadania"].Visible = true;
            dgLista.Columns["adres_odbiorcy_kraj"].Visible = false;
            //dgLista.Columns["data_protokolu"].AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
            //dgLista.Columns["data_protokolu"].Width = 75;

            dgLista.Columns["etykieta"].AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
            dgLista.Columns["etykieta"].Width = 30;

            dgLista.Columns["paczki"].AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
            dgLista.Columns["paczki"].Width = 37;


            dgLista.Columns["colStatus"].DisplayIndex = dgLista.Columns.Count - 1;
            dgLista.Columns["colStatus"].Width = 50;

            try
            {
                dgLista.Columns["adres_nadawcy_nazwa"].Visible = false;
                dgLista.Columns["adres_nadawcy_ulica"].Visible = false;
                dgLista.Columns["adres_nadawcy_kod"].Visible = false;
                dgLista.Columns["adres_nadawcy_miejscowosc"].Visible = false;
                dgLista.Columns["adres_nadawcy_kraj"].Visible = false;
                dgLista.Columns["adres_nadawcy_tel"].Visible = false;
            }
            catch (Exception ex)
            {
                Log.AddLog(ex.ToString());
            }

            dgLista.Columns["adresNadania"].Width = 160;

            foreach (DataGridViewColumn col in dgLista.Columns)
                if (col.Index != 0)
                    col.ReadOnly = true;

            foreach (DataGridViewRow row in dgLista.Rows)
                if (!string.IsNullOrEmpty(row.Cells["COD"].Value.ToString()) &&
                    decimal.Parse(row.Cells["COD"].Value.ToString()) > 0)
                    row.DefaultCellStyle.BackColor = Color.LightGoldenrodYellow;

            UstawWidocznoscKolumnZUstawien();
        }

        private void UstawWidocznoscKolumnZUstawien()
        {
            dgLista.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.None;

            dgLista.Columns["numer"].Visible = Default.bColNrDokumentu;
            dgLista.Columns["adresNadania"].Visible = Default.bColAdresNadawcy;
            dgLista.Columns["adres_odbiorcy_nazwa"].Visible = Default.bColAdresOdbiorcy;
            dgLista.Columns["nr_listu"].Visible = Default.bColNrListu;
            dgLista.Columns["nr_protokolu"].Visible = Default.bColNrProtokolu;
            dgLista.Columns["paczki"].Visible = Default.bColPaczki;
            dgLista.Columns["data_listu"].Visible = Default.bColDataListu;
            dgLista.Columns["data_protokolu"].Visible = Default.bColDataProtokolu;
            dgLista.Columns["etykieta"].Visible = Default.bColEtykieta;
            dgLista.Columns["COD"].Visible = Default.bColPobranieCOD;
            dgLista.Columns["Przesyłka Wartościowa"].Visible = Default.bColPrzesylkaWartosciowa;
            dgLista.Columns["status"].Visible = Default.bColStatus;
            dgLista.Columns["colStatus"].Visible = Default.bColStatusPrzycisk;
            dgLista.Columns["waga"].Visible = Default.bColWaga;
            dgLista.Columns["Usługi"].Visible = Default.bColUslugi;
            dgLista.Columns["data_ostatniego_sprawdzenia"].Visible = Default.bColDataOstatniegoSprawdzenia;
            dgLista.Columns["id_magazynu"].Visible = Default.Id_mag;
            //   dgLista.Columns["Palety"].Visible = Default.bColPaleta;

            dgLista.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
        }

        private void btnUsun_Click(object sender, EventArgs e)
        {
            UsunListy();
        }

        private void UsunListy()
        {
            bool saNaProtokole;
            List<string> idZaznaczonych;
            var tab = PobierzZaznaczone(out idZaznaczonych, out saNaProtokole);
            if (tab.Count == 0)
            {
                MessageBox.Show("Nie wybrano żadnego listu");
                return;
            }

            if (saNaProtokole)
            {
                if (DialogResult.No ==
                    MessageBox.Show(
                        "Zaznaczone przesyłki znajdują się na protokole przekazania. Po usunięciu przesyłek należy ponownie wygenerować protokół. Czy nadal chesz usunąć zaznaczone pozycje?",
                        "", MessageBoxButtons.YesNo))
                    return;

                var sb = new StringBuilder();
                sb.AppendFormat(@"
update _DPD_Listy set nr_protokolu = null , data_protokolu = null
where nr_protokolu in (select nr_protokolu from _DPD_Listy where id in ({0})) 
    and nr_protokolu is not null

delete _DPD_USLUGI where id in ({0}) 
delete _DPD_PACZKI where id in ({0}) 
delete _DPD_Listy  where id in ({0}) ",
                    string.Join(",", idZaznaczonych.ToArray()));

                if (Sql.RunQuery(sb.ToString()))
                    MessageBox.Show("Usunięto zaznaczone przesyłki");
            }
            else
            {
                if (DialogResult.No ==
                    MessageBox.Show("Zaznaczone pozycje zostaną trwale usunięte. Czy chesz kontynuować?", "",
                        MessageBoxButtons.YesNo))
                    return;

                var sb = new StringBuilder();
                sb.AppendFormat(
                    @"delete _DPD_USLUGI where id in ({0}) delete _DPD_PACZKI where id in ({0}) delete _DPD_Listy where id in ({0}) ",
                    string.Join(",", idZaznaczonych.ToArray()));

                if (Sql.RunQuery(sb.ToString()))
                    MessageBox.Show("Usunięto zaznaczone przesyłki");
            }

            PobierzListy();
        }

        private List<parcelDSPV1> PobierzZaznaczone(out List<string> idZaznaczone, out bool hasProtokol)
        {
            var results = new List<parcelDSPV1>();
            idZaznaczone = new List<string>();
            hasProtokol = false;

            foreach (var row in from DataGridViewRow row in dgLista.Rows
                let chk = (DataGridViewCheckBoxCell) row.Cells["checked"]
                where chk.Value != null && (bool) chk.Value
                select row)
            {
                idZaznaczone.Add(row.Cells["id"].Value.ToString());
                if (!string.IsNullOrEmpty(row.Cells["nr_protokolu"].Value.ToString()))
                    hasProtokol = true;

                var nrPaczek = row.Cells["nr_listu"].Value.ToString()
                    .Split(new[] {Environment.NewLine}, StringSplitOptions.None);

                results.AddRange(nrPaczek.Select(nrPaczki => new parcelDSPV1 {waybill = nrPaczki}));
            }

            return results;
        }

        private void PobierzZaznaczone2(out List<parcelDSPV1> domesticParcels,
            out List<parcelDSPV1> internationalParcels,
            out List<string> idDomestic, out List<string> idInter, out List<Adres> adresyNadawcyDomestic,
            out List<Adres> adresyNadawcyInternational)
        {
            domesticParcels = new List<parcelDSPV1>();
            internationalParcels = new List<parcelDSPV1>();
            idDomestic = new List<string>();
            idInter = new List<string>();
            adresyNadawcyDomestic = new List<Adres>();
            adresyNadawcyInternational = new List<Adres>();

            foreach (DataGridViewRow row in dgLista.Rows)
            {
                var chk = (DataGridViewCheckBoxCell) row.Cells["checked"];
                if (chk.Value == null || !(bool) chk.Value) continue;

                var numeryPaczek = row.Cells["nr_listu"].Value.ToString()
                    .Split(new[] {Environment.NewLine}, StringSplitOptions.None);


                if (row.Cells["sessionType"].Value.ToString().ToLower() == "domestic")
                {
                    adresyNadawcyDomestic.Add(PobierzAdresZWiersza(row));
                    idDomestic.Add(row.Cells["id"].Value.ToString());
                    domesticParcels.AddRange(numeryPaczek.Select(nrPaczki => new parcelDSPV1 {waybill = nrPaczki}));
                }
                else
                {
                    adresyNadawcyInternational.Add(PobierzAdresZWiersza(row));
                    idInter.Add(row.Cells["id"].Value.ToString());
                    internationalParcels.AddRange(numeryPaczek.Select(nrPaczki =>
                        new parcelDSPV1 {waybill = nrPaczki}));
                }
            }
        }

        private Adres PobierzAdresZWiersza(DataGridViewRow row)
        {
            var adres = new Adres
            {
                Nazwa = row.Cells["adres_nadawcy_nazwa"].Value.ToString(),
                Ulica = row.Cells["adres_nadawcy_ulica"].Value.ToString(),
                Kod = row.Cells["adres_nadawcy_kod"].Value.ToString(),
                Miejscowosc = row.Cells["adres_nadawcy_miejscowosc"].Value.ToString(),
                Kraj = row.Cells["adres_nadawcy_kraj"].Value.ToString(),
                Telefon = row.Cells["adres_nadawcy_tel"].Value.ToString()
            };
            return adres;
        }

        private void btnGenerujEtykiety_Click(object sender, EventArgs e)
        {
            PrzygotujDaneDoGenerowaniaEtykiet();
        }

        private void PrzygotujDaneDoGenerowaniaEtykiet()
        {
            try
            {
                FileInfo etykietaI = null, etykietaD = null;
                List<parcelDSPV1> paczkiKrajowe, paczkiInternational;
                List<string> idPaczekKrajowych, idPaczejInternational;
                List<Adres> pusta;

                PobierzZaznaczone2(out paczkiKrajowe, out paczkiInternational,
                    out idPaczekKrajowych, out idPaczejInternational, out pusta, out pusta);

                if (paczkiKrajowe.Count + paczkiInternational.Count == 0)
                {
                    MessageBox.Show("Nie wybrano żadnego listu");
                }
                else
                {
                    if (paczkiKrajowe.Count > 0)
                    {
                        etykietaD = GenerujEtykiete(paczkiKrajowe, sessionTypeDSPEnumV1.DOMESTIC);

                        if (etykietaD != null && etykietaD.Exists)
                        {
                            ModifyPdf.Create(_cwiartka, etykietaD.FullName, paczkiKrajowe.Count);
                            MessageBox.Show("Zapisano!");

                            if (chDrukujL.Checked)
                                if (etykietaD.Extension.ToLower() == ".pdf")
                                    PrinterClass.PrintFile(etykietaD.FullName, Default.drukarkaEtykiet);
                                else
                                    RawPrinterHelper.SendFileToPrinter(Default.drukarkaEtykiet,
                                        etykietaD.FullName);
                        }
                    }

                    if (paczkiInternational.Count > 0)
                    {
                        etykietaI = GenerujEtykiete(paczkiInternational, sessionTypeDSPEnumV1.INTERNATIONAL);

                        if (etykietaI != null && etykietaI.Exists)
                        {
                            ModifyPdf.Create(_cwiartka, etykietaI.FullName, paczkiInternational.Count);
                            MessageBox.Show("Zapisano!");

                            if (chDrukujL.Checked)
                                if (etykietaI.Extension.ToLower() == ".pdf")
                                    PrinterClass.PrintFile(etykietaI.FullName, Default.drukarkaEtykiet);
                                else
                                    RawPrinterHelper.SendFileToPrinter(Default.drukarkaEtykiet,
                                        etykietaI.FullName);
                        }
                    }

                    if (paczkiInternational != null && paczkiInternational.Count > 0 && etykietaI != null &&
                        etykietaI.Exists ||
                        paczkiKrajowe != null && paczkiKrajowe.Count > 0 && etykietaD != null && etykietaD.Exists)
                    {
                        var idZaznaczonych = new List<string>();

                        foreach (var s in idPaczekKrajowych)
                            idZaznaczonych.Add(s);

                        foreach (var s in idPaczejInternational)
                            idZaznaczonych.Add(s);

                        var zap = "update _DPD_Listy set etykieta = 1 where id in (" +
                                  string.Join(",", idZaznaczonych.ToArray()) + ") and etykieta = 0";

                        Sql.RunQuery(zap);
                    }
                }

                PobierzListy();
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
            }
        }

        private FileInfo GenerujEtykiete(List<parcelDSPV1> tab, sessionTypeDSPEnumV1 sessType)
        {
            var response = new documentGenerationResponseV1();
            var pkg9 = new packageDSPV1 {parcels = tab.ToArray()};

            var session = new sessionDSPV1
            {
                packages = new packageDSPV1[1] {pkg9},
                sessionType = sessType,
                sessionTypeSpecified = true
            };

            var param = new dpdServicesParamsV1
            {
                policy = policyDSPEnumV1.IGNORE_ERRORS,
                policySpecified = true,
                session = session
            };

            var pageFormat = outputDocPageFormatDSPEnumV1.A4;
            var format = outputDocFormatDSPEnumV1.PDF;
            var rozszerzeniePliku = "pdf";

            if (Default.etykieciarka)
            {
                pageFormat = outputDocPageFormatDSPEnumV1.LBL_PRINTER;
                switch (Default.formatWydruku)
                {
                    case "PDF":
                    {
                        format = outputDocFormatDSPEnumV1.PDF;
                        break;
                    }
                    case "EPL":
                    {
                        rozszerzeniePliku = "epl";
                        format = outputDocFormatDSPEnumV1.EPL;
                        break;
                    }
                    case "ZPL":
                    {
                        rozszerzeniePliku = "zpl";
                        format = outputDocFormatDSPEnumV1.ZPL;
                        break;
                    }
                }
            }

            try
            {
                response = _dpdService.generateSpedLabelsV1(param, format, true, pageFormat, true, _authData);

                if (response?.documentData == null)
                {
                    MessageBox.Show(@"Serwis nie zwrócił etykiety");
                }
                else
                {
                    byte[] data;
                    if (Default.etykieciarka && Default.formatWydruku == "EPL")
                        data = Encoding.Convert(Encoding.UTF8, Encoding.GetEncoding("Windows-1250"),
                            response.documentData);
                    else
                        data = response.documentData;

                    var plikEtykiet = Default.etykietySciezka + @"\DPD_" +
                                      DateTime.Now.Millisecond + "_" +
                                      DateTime.Today.ToShortDateString().Replace("/", "-").Replace("\\", "-") +
                                      "_label." + rozszerzeniePliku;

                    File.WriteAllBytes(plikEtykiet, data);

                    return new FileInfo(plikEtykiet);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return null;
            }

            return null;
        }

        private string GenerujProtokol(List<parcelDSPV1> tab, sessionTypeDSPEnumV1 sessionType, Adres adresNadania)
        {
            var pkg9 = new packageDSPV1 {parcels = tab.ToArray()};

            var session = new sessionDSPV1
            {
                packages = new packageDSPV1[1] {pkg9},
                sessionType = sessionType,
                sessionTypeSpecified = true
            };


            var param = new dpdServicesParamsV1
            {
                policy = policyDSPEnumV1.IGNORE_ERRORS,
                policySpecified = true,
                session = session
            };

            var pickup = new pickupAddressDSPV1
            {
                address = adresNadania.Ulica,
                city = adresNadania.Miejscowosc,
                company = adresNadania.Nazwa,
                countryCode = adresNadania.Kraj,
                postalCode = adresNadania.Kod,
                name = "",
                phone = adresNadania.Telefon
            };

            param.pickupAddress = pickup;

            var response =
                _dpdService.generateProtocolV1(param, outputDocFormatDSPEnumV1.PDF, true,
                    outputDocPageFormatDSPEnumV1.LBL_PRINTER, true, _authData);

            if (response == null)
            {
                MessageBox.Show(@"response null");
                throw new Exception();
            }

            if (response.documentData == null)
            {
                MessageBox.Show(response.session.statusInfo.status.ToString(), response.session.statusInfo.description);
                throw new Exception();
            }

            var data = response.documentData;

            if (data != null)
            {
                var plikProtokol = Default.protokolySciezka + @"\DPD_" + DateTime.Now.Millisecond + "_" +
                                   DateTime.Today.ToShortDateString().Replace("/", "-").Replace("\\", "-") +
                                   "_protocol.pdf";
                File.WriteAllBytes(plikProtokol, data);

                ModifyPdf.DoublePageProtocol(plikProtokol);

                if (chDrukujL.Checked)
                    PrinterClass.PrintFile(plikProtokol, Default.drukarkaProtokol);
            }

            return response.documentId;
        }

        private void btnGenerujProtokol_Click(object sender, EventArgs e)
        {
            PrzygotujDaneDoProtokolu();
        }

        private void PrzygotujDaneDoProtokolu()
        {
            try
            {
                string nrProtD = null, nrProtI = null;

                var paczkiKrajowe = new List<parcelDSPV1>();
                var paczkiInternational = new List<parcelDSPV1>();
                var idPaczekKrajowych = new List<string>();
                var idPaczekInternational = new List<string>();
                var adresyNadawcyPaczekKrajowych = new List<Adres>();
                var adresyNadawcyPaczekInternational = new List<Adres>();

                PobierzZaznaczone2(out paczkiKrajowe, out paczkiInternational,
                    out idPaczekKrajowych, out idPaczekInternational, out adresyNadawcyPaczekKrajowych,
                    out adresyNadawcyPaczekInternational);

                var ad = adresyNadawcyPaczekKrajowych.Distinct().ToList();

                if (paczkiKrajowe.Count + paczkiInternational.Count == 0)
                {
                    MessageBox.Show("Nie wybrano żadnego listu");
                }
                else
                {
                    if (paczkiKrajowe != null && paczkiKrajowe.Count > 0)
                    {
                        if (SprawdzCzyKilkaAdresow(adresyNadawcyPaczekKrajowych))
                        {
                            MessageBox.Show(
                                "Protokół można wygenerować tylko dla paczek wysyłanych z tego samego adresu");
                            return;
                        }

                        nrProtD = GenerujProtokol(paczkiKrajowe, sessionTypeDSPEnumV1.DOMESTIC,
                            adresyNadawcyPaczekKrajowych[0]);
                        if (!string.IsNullOrEmpty(nrProtD))
                        {
                            var zap = @"update _DPD_Listy set nr_protokolu = " + nrProtD +
                                      ", data_protokolu = getdate() where id in (" +
                                      string.Join(",", idPaczekKrajowych.ToArray()) + ") ";
                            Sql.RunQuery(zap);

                            MessageBox.Show("Wygenerowano protokół");
                        }
                        else
                        {
                            MessageBox.Show("Wygenerowanie protokołu nie powiodło się");
                        }
                    }

                    if (paczkiInternational != null && paczkiInternational.Count > 0)
                    {
                        if (SprawdzCzyKilkaAdresow(adresyNadawcyPaczekInternational))
                        {
                            MessageBox.Show(
                                "Protokół można wygenerować tylko dla paczek wysyłanych z tego samego adresu");
                            return;
                        }

                        nrProtI = GenerujProtokol(paczkiInternational, sessionTypeDSPEnumV1.INTERNATIONAL,
                            adresyNadawcyPaczekInternational[0]);
                        if (!string.IsNullOrEmpty(nrProtI))
                        {
                            var zap = @"update _DPD_Listy set nr_protokolu = " + nrProtI +
                                      ", data_protokolu = getdate() where id in (" +
                                      string.Join(",", idPaczekInternational.ToArray()) + ") ";
                            Sql.RunQuery(zap);
                        }
                        else
                        {
                            MessageBox.Show("Wygenerowanie protokołu nie powiodło się");
                        }
                    }

                    PobierzListy();
                }
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                Log.AddLog(ex.ToString());
                MessageBox.Show("Nie udało się wygenerować protokołu. Szczegóły błędu w logach.");
            }
        }

        private bool SprawdzCzyKilkaAdresow(List<Adres> adresy)
        {
            for (var i = 0; i < adresy.Count - 1; i++)
            for (var j = 1; j < adresy.Count; j++)
                if (!adresy[i].Equals(adresy[j]))
                    return true;

            return false;
        }

        private void txtKontrahent_TextChanged(object sender, EventArgs e)
        {
            PobierzListy();
        }

        private void txtFindNrListu_TextChanged(object sender, EventArgs e)
        {
            PobierzListy();
        }

        private void txtNrProtokolu_TextChanged(object sender, EventArgs e)
        {
            PobierzListy();
        }

        private void dpListyFrom_ValueChanged(object sender, EventArgs e)
        {
        }

        private void dpListyTo_ValueChanged(object sender, EventArgs e)
        {
        }

        private void dgPaczki_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == dgPaczki.Columns["usun"].Index)
            {
                if (e.RowIndex == 0 && dgPaczki.Rows.Count - 1 == 1)
                    MessageBox.Show("Przesyłka musi składać się przynajmniej z jednej paczki");
                else
                    dgPaczki.Rows.RemoveAt(e.RowIndex);

                for (var i = 1; i < dgPaczki.Rows.Count; i++)
                    dgPaczki.Rows[i - 1].Cells[0].Value = i.ToString();
            }
        }

        private void PokazPanel(int pnl)
        {
            _aktywnyPanel = pnl;

            pnlDokumenty.Visible = false;
            pnlGenerowanie.Visible = false;
            pnlPrzygotowane.Visible = false;

            switch (pnl)
            {
                case 11:
                {
                    pnlDokumenty.Visible = true;
                    pnlZamowienia.BringToFront();
                    break;
                }
                case 12:
                {
                    pnlDokumenty.Visible = true;
                    pnlMagazynowe.BringToFront();
                    break;
                }
                case 13:
                {
                    pnlDokumenty.Visible = true;
                    pnlHandlowe.BringToFront();
                    break;
                }
                case 2:
                {
                    pnlGenerowanie.Visible = true;
                    break;
                }
                case 3:
                {
                    pnlPrzygotowane.Visible = true;
                    break;
                }
            }
        }

        public void WypelnijMagazyny(ComboBox comboBoxList)
        {
            try
            {
                var magazynyZam = new Dictionary<int, string>();
                var connectionString = Sql.ConnStr;

                if (string.IsNullOrEmpty(connectionString))
                    return;

                using (var sqlConnection = new SqlConnection(connectionString))
                {
                    var sqlCmd = new SqlCommand(Query.GetMagazynNazwaId(), sqlConnection);
                    sqlConnection.Open();
                    var sqlReader = sqlCmd.ExecuteReader();

                    while (sqlReader.Read())
                        magazynyZam.Add(Convert.ToInt32(sqlReader["ID_MAGAZYNU"]), sqlReader["NAZWA"].ToString());

                    sqlReader.Close();
                }

                comboBoxList.DataSource = new BindingSource(magazynyZam, null);

                comboBoxList.DisplayMember = "Value";
                comboBoxList.ValueMember = "Key";
                comboBoxList.Update();

                comboBoxList.SelectedIndex = Default.cbMagazynGlobal;
            }
            catch (Exception ex)
            {
                Log.AddLog(ex.ToString());
            }
        }

        private void zamowieniaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (_aktywnyPanel != 11)
            {
                PokazPanel(11);
                try
                {
                    WypelnijMagazyny(cbMagazynZamowienie);
                    PokazDokumenty(ParseEnum(cbFiltrZam.SelectedIndex), dgZamowienia, false);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                    throw;
                }
            }
        }

        private void dokumentyMagazynoweToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (_aktywnyPanel != 12)
            {
                PokazPanel(12);
                try
                {
                    WypelnijMagazyny(cbMagazynMag);

                    PokazDokumenty(ParseEnum(cbFiltrMag.SelectedIndex), dgMagazynowe, false);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                    throw;
                }
            }
        }

        private void dokumentyHadnloweToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (_aktywnyPanel != 13)
            {
                PokazPanel(13);
                WypelnijMagazyny(cbMagazynyHan);

                PokazDokumenty(ParseEnum(cbFiltrHan.SelectedIndex), dgHandlowe, false);
            }
        }

        private void chDorGwaran_CheckedChanged(object sender, EventArgs e)
        {
            cbDoreczenie.Visible = chDorGwaran.Checked;
            if (!chDorGwaran.Checked) cbDoreczenie.SelectedIndex = 0;
        }

        private void chOdbiorWlasny_CheckedChanged(object sender, EventArgs e)
        {
            gbOdbiorWlasny.Enabled = chOdbiorWlasny.Checked;
        }

        private void chPobranie_CheckedChanged(object sender, EventArgs e)
        {
            txtPobranie.Enabled = chPobranie.Checked;
        }

        private void chDeklWart_CheckedChanged(object sender, EventArgs e)
        {
            txtDeklarowanaWartosc.Enabled = chDeklWart.Checked;
        }

        private void txtKontrahentZam_TextChanged(object sender, EventArgs e)
        {
            PokazDokumenty(ParseEnum(cbFiltrZam.SelectedIndex), dgZamowienia, true);
        }

        private void txtKontrahentHan_TextChanged(object sender, EventArgs e)
        {
            PokazDokumenty(ParseEnum(cbFiltrHan.SelectedIndex), dgHandlowe, true);
        }

        private void txtKontrahentMag_TextChanged(object sender, EventArgs e)
        {
            PokazDokumenty(ParseEnum(cbFiltrMag.SelectedIndex), dgMagazynowe, true);
        }

        private void dgPaczki_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            e.Control.KeyPress -= Column1_KeyPress;
            e.Control.KeyPress -= ColumnWaga_KeyPress;

            if (dgPaczki.CurrentCell.ColumnIndex == dgPaczki.Columns["szerokość"].Index ||
                dgPaczki.CurrentCell.ColumnIndex == dgPaczki.Columns["długość"].Index ||
                dgPaczki.CurrentCell.ColumnIndex == dgPaczki.Columns["wysokość"].Index)
            {
                var tb = e.Control as TextBox;
                if (tb != null)
                    tb.KeyPress += Column1_KeyPress;
            }

            if (dgPaczki.CurrentCell.ColumnIndex == dgPaczki.Columns["waga"].Index)
            {
                var tb = e.Control as TextBox;
                if (tb != null)
                    tb.KeyPress += ColumnWaga_KeyPress;
            }
        }

        private void Column1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
                e.Handled = true;
        }

        private void ColumnWaga_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && e.KeyChar != ',')
                e.Handled = true;

            if (e.KeyChar == ',' && (sender as TextBox).Text.IndexOf(',') > -1)
                e.Handled = true;
        }

        private void chGenerujEtykiete_CheckedChanged(object sender, EventArgs e)
        {
            chDrukuj.Enabled = chGenerujEtykiete.Checked;
        }

        private void dgPaczki_UserAddedRow(object sender, DataGridViewRowEventArgs e)
        {
            for (var i = 1; i < dgPaczki.Rows.Count; i++)
                dgPaczki.Rows[i - 1].Cells[0].Value = i.ToString();
        }

        private void dgLista_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                dgLista.CommitEdit(DataGridViewDataErrorContexts.Commit);

                if (e.ColumnIndex == colStatus.Index)


                {
                    _statusInfo.GetEvents(dgLista.Rows[e.RowIndex].Cells["nr_listu"].Value.ToString().Trim());

                    var eventList =
                        _statusInfo.GetEvents(dgLista.Rows[e.RowIndex].Cells["nr_listu"].Value.ToString().Trim());

                    if (eventList != null && eventList.Events != null)
                    {
                        var lastStatus = eventList.Events.OrderBy(l => l.Date).Last().Description;
                        var list = new List<string[]>
                        {
                            new[] {dgLista.Rows[e.RowIndex].Cells["id"].Value.ToString(), lastStatus}
                        };

                        var data = DateTime.Now;
                        _wfmag.UpdateStatusList(list, data);

                        dgLista.Rows[e.RowIndex].Cells["status"].Value = lastStatus;

                        var form = new StatusyForm(eventList);
                        form.ShowDialog();
                    }
                    else
                    {
                        MessageBox.Show("Nie udało się pobrać statusów - system zwrócił błąd");
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(@"Nie udało się pobrać statusów - system zwrócił błąd");
                Log.AddLog("Błąd podczas sprawdzania statusu przesyłki: " + ex);
            }
        }

        private int _platnikFid = -1;

        private void cbPlatnik_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbPlatnik.SelectedValue.ToString() != "Nadawca")
            {
                var form = new PlatnikForm(cbPlatnik.SelectedValue.ToString());
                form.ShowDialog();

                if (form.IsAccpted)
                {
                    _platnikFid = form.SelectedFid;
                    lblFID.Text = _platnikFid.ToString();
                }
                else
                {
                    cbPlatnik.SelectedIndex = 0;
                    lblFID.Text = _fid.ToString();
                }
            }
            else
            {
                lblFID.Text = _fid.ToString();
            }
        }

        private void cbDoreczenie_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtNaOkreslonaGodzine.Visible = cbDoreczenie.SelectedValue.ToString() == "DPD na godzinę";
        }

        private void txtPobranie_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && e.KeyChar != ',')
                e.Handled = true;

            if (e.KeyChar == ',' && (sender as TextBox).Text.IndexOf(',') > -1)
                e.Handled = true;
        }

        private void txtDeklarowanaWartosc_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && e.KeyChar != ',')
                e.Handled = true;

            if (e.KeyChar == ',' && (sender as TextBox).Text.IndexOf(',') > -1)
                e.Handled = true;
        }

        private void btnGenZam_Click(object sender, EventArgs e)
        {
            Default.cbMagazynGlobal = cbMagazynZamowienie.SelectedIndex;
            Default.Save();

            GenerujListyDlaZaznaczonych();
        }

        private UslugiHelper _uslugi;

        private void GenerujListyDlaZaznaczonych()
        {
            try
            {
                if (Default.bWlaczSzablon)
                {
                    var form = new SzablonForm();
                    form.ShowDialog();

                    if (!form.JestZatwierdzone)
                    {
                        MessageBox.Show("Anulowano operację");
                        return;
                    }

                    _uslugi = form.Uslugi;
                }
                //return;

                var idki = new List<string>();
                var seletedIndex = dgZamowienia.SelectedRows[0].Index;

                foreach (DataGridViewRow row in dgZamowienia.Rows)
                {
                    var chk = (DataGridViewDisableCheckBoxCell) row.Cells[0];
                    if (!chk.Checked || !chk.Enabled)
                        continue;

                    _id = row.Cells["id"].Value.ToString();
                    _typ = TypDokumentu.Zamowienie;
                    idki.Add(_id);

                    PobierzDaneDokumentuWfMag(false);
                    GenerujList();
                }

                _uslugi = null;

                if (_licznikWygenerowanychEtykiet > 0)
                    MessageBox.Show("Zapisano!");

                _licznikWygenerowanychEtykiet = 0;

                if (idki.Count == 0)
                    return;

                PokazDokumenty(ParseEnum(cbFiltrZam.SelectedIndex), dgZamowienia, false);
                ZaznaczIdki(idki, dgZamowienia);

                dgZamowienia.Rows[seletedIndex].Selected = true;
                dgZamowienia.FirstDisplayedScrollingRowIndex = seletedIndex;

                SprawdzWygenerowane(idki, dgZamowienia);
            }
            catch (Exception ex)
            {
                Log.AddLog(ex.ToString());
            }
        }

        private void SprawdzWygenerowane(List<string> idki, DataGridView dg)
        {
            var wygenerowane = 0;

            foreach (DataGridViewRow row in dg.Rows)
            {
                var chk = (DataGridViewDisableCheckBoxCell) row.Cells[0];
                if (idki.Contains(row.Cells["id"].Value.ToString()) && !chk.Enabled)
                    wygenerowane++;
            }

            if (wygenerowane == idki.Count)
                MessageBox.Show(
                    "Dla zaznaczonych dokumentów wygenerowano numery przesyłek. Szczegóły w zakładce 'Wysyłki'");
            else if (wygenerowane == 0)
                MessageBox.Show("Nie wygenerowano żadnych numerów");
            else
                MessageBox.Show("Nie udało się wygenerować numerów przesyłek dla wszystkich zaznaczonych dokumentów");
        }

        private void ZaznaczIdki(List<string> idki, DataGridView dg)
        {
            foreach (DataGridViewRow row in dg.Rows)
            {
                _id = row.Cells["id"].Value.ToString();
                if (idki.Contains(_id))
                    if (dg.Name == "dgLista")
                    {
                        row.Cells[0].Value = true;
                    }
                    else
                    {
                        var chk = (DataGridViewDisableCheckBoxCell) row.Cells[0];
                        chk.Checked = true;
                    }
            }
        }

        private void btnGenHan_Click(object sender, EventArgs e)
        {
            try
            {
                Default.cbMagazynGlobal = cbMagazynyHan.SelectedIndex;
                Default.Save();
                if (Default.bWlaczSzablon)
                {
                    var form = new SzablonForm();
                    form.ShowDialog();

                    if (!form.JestZatwierdzone)
                    {
                        MessageBox.Show("Anulowano operację");
                        return;
                    }

                    _uslugi = form.Uslugi;
                }

                var idki = new List<string>();
                var seletedIndex = dgHandlowe.SelectedRows[0].Index;

                foreach (var row in from DataGridViewRow row in dgHandlowe.Rows
                    let chk = (DataGridViewDisableCheckBoxCell) row.Cells[0]
                    where chk.Checked && chk.Enabled
                    select row)
                {
                    _id = row.Cells["id"].Value.ToString();
                    _typ = TypDokumentu.Dokument_Handlowy;
                    idki.Add(_id);

                    PobierzDaneDokumentuWfMag(false);
                    GenerujList();
                }

                _uslugi = null;

                if (_licznikWygenerowanychEtykiet > 0)
                    MessageBox.Show("Zapisano!");

                _licznikWygenerowanychEtykiet = 0;

                if (idki.Count == 0)
                    return;

                PokazDokumenty(ParseEnum(cbFiltrHan.SelectedIndex), dgHandlowe, false);
                ZaznaczIdki(idki, dgHandlowe);

                dgHandlowe.Rows[seletedIndex].Selected = true;
                dgHandlowe.FirstDisplayedScrollingRowIndex = seletedIndex;

                SprawdzWygenerowane(idki, dgHandlowe);
            }
            catch (Exception ex)
            {
                Log.AddLog(ex.ToString());
            }
        }

        private void btnGenMag_Click(object sender, EventArgs e)
        {
            try
            {
                Default.cbMagazynGlobal = cbMagazynMag.SelectedIndex;
                Default.Save();
                if (Default.bWlaczSzablon)
                {
                    var form = new SzablonForm();
                    form.ShowDialog();

                    if (!form.JestZatwierdzone)
                    {
                        MessageBox.Show("Anulowano operację");
                        return;
                    }

                    _uslugi = form.Uslugi;
                }

                var idki = new List<string>();
                var seletedIndex = dgMagazynowe.SelectedRows[0].Index;

                foreach (DataGridViewRow row in dgMagazynowe.Rows)
                {
                    var chk = (DataGridViewDisableCheckBoxCell) row.Cells[0];

                    if (!chk.Checked || !chk.Enabled)
                        continue;

                    _id = row.Cells["id"].Value.ToString();
                    _typ = TypDokumentu.Dokument_Magazynowy;
                    idki.Add(_id);

                    PobierzDaneDokumentuWfMag(false);
                    GenerujList();
                }

                _uslugi = null;

                if (_licznikWygenerowanychEtykiet > 0)
                    MessageBox.Show(@"Zapisano!");

                _licznikWygenerowanychEtykiet = 0;

                if (idki.Count == 0)
                    return;

                PokazDokumenty(ParseEnum(cbFiltrMag.SelectedIndex), dgMagazynowe, false);
                ZaznaczIdki(idki, dgMagazynowe);

                dgMagazynowe.Rows[seletedIndex].Selected = true;
                dgMagazynowe.FirstDisplayedScrollingRowIndex = seletedIndex;

                SprawdzWygenerowane(idki, dgMagazynowe);
            }
            catch (Exception ex)
            {
                Log.AddLog(ex.ToString());
            }
        }

        private List<string> PobierzZaznaczone(DataGridView dg)
        {
            var idki = new List<string>();

            for (var i = 0; i < dg.Rows.Count; i++)
                if (dg.Name == "dgLista")
                {
                    if (dg.Rows[i].Cells[0].Value == null || !(bool) dg.Rows[i].Cells[0].Value ||
                        dg.Rows[i].Cells[0].Style.BackColor == Color.DarkGray)
                        continue;
                    idki.Add(dg.Rows[i].Cells["id"].Value.ToString());
                }
                else
                {
                    var chk = (DataGridViewDisableCheckBoxCell) dg.Rows[i].Cells[0];
                    if (!chk.Enabled || !chk.Checked)
                        continue;
                    idki.Add(dg.Rows[i].Cells["id"].Value.ToString());
                }

            return idki;
        }

        private enum DokumentyFiltr
        {
            Wszystkie = 0,
            Wysłane = 1,
            Niewysłane = 2,
            Zaznaczone = 3
        }

        private void PokazDokumenty(DokumentyFiltr dokFiltr, DataGridView dg, bool zaznacz)
        {
            var idki = PobierzZaznaczone(dg);
            DataTable dt = null;
            if (cbMagazynZamowienie.Items.Count > 0)
                switch (dg.Name)
                {
                    case "dgZamowienia":

                        var idWybranegoMagazynuZam = ((KeyValuePair<int, string>) cbMagazynZamowienie.SelectedItem).Key;

                    {
                        dt = _wfmag.PobierzZamowienia(txtNrZam.Text, dpZamFrom.Value, dpZamTo.Value,
                            txtKontrahentZam.Text, idWybranegoMagazynuZam.ToString());
                        break;
                    }
                    case "dgMagazynowe":

                        var idWybranegoMagazynuMag = ((KeyValuePair<int, string>) cbMagazynMag.SelectedItem).Key;
                    {
                        dt = _wfmag.PobierzDokumentyMagazynowe(txtNrMag.Text, dpMagFrom.Value, dpMagTo.Value,
                            txtKontrahentMag.Text, idWybranegoMagazynuMag.ToString());
                        break;
                    }

                    case "dgHandlowe":
                        var idWybranegoMagazynuHan = ((KeyValuePair<int, string>) cbMagazynyHan.SelectedItem).Key;
                    {
                        dt = _wfmag.PobierzDokumentyHandlowe(txtNrHan.Text, dpHanFrom.Value, dpHanTo.Value,
                            txtKontrahentHan.Text, idWybranegoMagazynuHan.ToString());
                        break;
                    }
                }


            if (dt != null)
            {
                var dtFiltr = dt.Clone();
                dtFiltr.Clear();

                switch (dokFiltr)
                {
                    case DokumentyFiltr.Wszystkie:
                    {
                        dtFiltr = dt;
                        break;
                    }
                    case DokumentyFiltr.Wysłane:
                    {
                        for (var i = 0; i < dt.Rows.Count; i++)
                            if (dt.Rows[i]["DPD"].ToString() == "1")
                                dtFiltr.ImportRow(dt.Rows[i]);
                        break;
                    }
                    case DokumentyFiltr.Niewysłane:
                    {
                        for (var i = 0; i < dt.Rows.Count; i++)
                            if (dt.Rows[i]["DPD"].ToString() != "1")
                                dtFiltr.ImportRow(dt.Rows[i]);
                        break;
                    }
                    case DokumentyFiltr.Zaznaczone:
                    {
                        for (var i = 0; i < dt.Rows.Count; i++)
                            if (idki.Contains(dt.Rows[i]["id"].ToString()))
                                dtFiltr.ImportRow(dt.Rows[i]);
                        break;
                    }
                }

                dg.DataSource = dtFiltr;
            }
            else
            {
                dg.DataSource = null;
            }

            switch (dg.Name)
            {
                case "dgZamowienia":
                {
                    FormatDgZam();
                    break;
                }
                case "dgMagazynowe":
                {
                    FormatDgMag();
                    break;
                }
                case "dgHandlowe":
                {
                    FormatDgHan();
                    break;
                }
            }

            if (zaznacz)
                ZaznaczIdki(idki, dg);
        }

        private void dgZamowienia_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
        }

        private void SetEnableForPnlGenerowanie(bool val)
        {
            foreach (Control c in pnlGenerowanie.Controls)
                if (c is TextBox)
                    try
                    {
                        var txt = c as TextBox;
                        txt.ReadOnly = !val;
                    }
                    catch (Exception)
                    {
                    }
                else if (c is DataGridView)
                    c.Enabled = val;
                else
                    c.Enabled = val;

            rbOsoba.Enabled = val;
            rbFirma.Enabled = val;
            chDrukuj.Enabled = false;
        }

        private void dgLista_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.RowIndex < 0)
                return;
            var idListu = dgLista.Rows[e.RowIndex].Cells["id"].Value.ToString();

            dgPaczki.Rows.Clear();

            PokazPanel(2);
            SetEnableForPnlGenerowanie(false);
            PobierzSzczegolyPaczki(idListu);
            PobierzUslugiPaczki(idListu);
            PobierzPaczki(idListu);
        }

        private void PobierzPaczki(string idListu)
        {
            try
            {
                var dt = _wfmag.PobierzPaczkiListu(idListu);

                for (var i = 0; i < dt.Rows.Count; i++)
                    dgPaczki.Rows.Add(dt.Rows[i]["lp"].ToString(), dt.Rows[i]["waga"].ToString().Replace(".", ","),
                        dt.Rows[i]["zawartosc"].ToString(), dt.Rows[i]["uwagi"].ToString(),
                        dt.Rows[i]["dlugosc"].ToString(), dt.Rows[i]["szerokosc"].ToString(),
                        dt.Rows[i]["wysokosc"].ToString());
            }
            catch (Exception ex)
            {
                Log.AddLog("PobierzPaczki;" + ex);
            }
        }

        private void PobierzUslugiPaczki(string idListu)
        {
            try
            {
                var dt = _wfmag.PobierzUslugiListu(idListu);

                if (dt == null)
                    return;

                chDorGwaran.Checked = dt.Rows[0]["doreczenieGwarantowane"].ToString() == "True" ? true : false;
                if (chDorGwaran.Checked)
                {
                    cbDoreczenie.SelectedItem = dt.Rows[0]["doreczenieGwarantowaneTyp"].ToString();
                    if (dt.Rows[0]["doreczenieGwarantowaneTyp"].ToString() == "DPD na godzinę")
                        txtNaOkreslonaGodzine.Text = dt.Rows[0]["doreczenieGwarantowaneWartosc"].ToString();
                }

                chDorDoRakWl.Checked = dt.Rows[0]["doreczenieDoRakWlasnych"].ToString() == "True" ? true : false;
                chDorNaAdrPryw.Checked = dt.Rows[0]["doreczenieNaAdresPryw"].ToString() == "True" ? true : false;
                chPobranie.Checked = dt.Rows[0]["pobranie"].ToString() == "True" ? true : false;

                if (chPobranie.Checked)
                    txtPobranie.Text = dt.Rows[0]["pobranieKwota"].ToString();

                chOdbiorWlasny.Checked = dt.Rows[0]["odbiorWlasny"].ToString() == "True" ? true : false;

                if (chOdbiorWlasny.Checked)
                    if (dt.Rows[0]["odbiorWlasnyTyp"].ToString() == "Osoba")
                        rbOsoba.Checked = true;
                    else
                        rbFirma.Checked = true;

                

                chDokZwrot.Checked = dt.Rows[0]["dokumentyZwrotne"].ToString() == "True" ? true : false;
                chPrzesList.Checked = dt.Rows[0]["DOX"].ToString() == "True" ? true : false;
                chPrzesZwrot.Checked = dt.Rows[0]["przesylkaZwrotna"].ToString() == "True" ? true : false;
                chOpony.Checked = dt.Rows[0]["opony"].ToString() == "True" ? true : false;
                chTiresExport.Checked = dt.Rows[0]["oponyExport"].ToString() == "True" ? true : false;
                chDeklWart.Checked = dt.Rows[0]["przesylkaWartosciowa"].ToString() == "True" ? true : false;

                if (chDeklWart.Checked)
                    txtDeklarowanaWartosc.Text = dt.Rows[0]["przesylkaWartosciowaKwota"].ToString();

                chPudo.Checked = dt.Rows[0]?["pudo"].ToString() == "True";

                if (chPudo.Checked)
                    txtPudo.Text = dt.Rows?[0]?["pudoWartosc"].ToString();


                chPallet.Checked = dt.Rows[0]["przesylkaPaletowa"].ToString() == "True";
                chDuty.Checked = dt.Rows[0]["duty"].ToString() == "True";
                chDpdExpress.Checked = dt.Rows[0]["dpdExpress"].ToString() == "True";
                txtDuty.Text = chDuty.Checked ? dt.Rows[0]["dutyWartosc"].ToString() : string.Empty;
            }
            catch (Exception ex)
            {
                Log.AddLog("PobierzUslugiPaczki;" + ex);
            }
        }

        private void PobierzSzczegolyPaczki(string idListu)
        {
            try
            {
                var dt = _wfmag.PobierzSzczegolyListu(idListu);

                txtNrListu.Text = dt.Rows[0]["nr_listu"].ToString();
                txtNazwaNad.Text = dt.Rows[0]["adres_nadawcy_nazwa"].ToString();
                txtImieNazwiskoNad.Text = dt.Rows[0]["adres_nadawcy_imieNazwisko"].ToString();
                txtUlicaNad.Text = dt.Rows[0]["adres_nadawcy_ulica"].ToString();
                txtKodNad.Text = dt.Rows[0]["adres_nadawcy_kod"].ToString();
                txtMiejscNad.Text = dt.Rows[0]["adres_nadawcy_miejscowosc"].ToString();
                txtkrajNad.Text = dt.Rows[0]["adres_nadawcy_kraj"].ToString();
                txtTelNad.Text = dt.Rows[0]["adres_nadawcy_tel"].ToString();
                txtMailNad.Text = dt.Rows[0]["adres_nadawcy_mail"].ToString();

                txtNazwaOdb.Text = dt.Rows[0]["adres_odbiorcy_nazwa"].ToString();
                txtImieNazwiskoOdb.Text = dt.Rows[0]["adres_odbiorcy_imieNazwisko"].ToString();
                txtUlicaOdb.Text = dt.Rows[0]["adres_odbiorcy_ulica"].ToString();
                txtKodOdb.Text = dt.Rows[0]["adres_odbiorcy_kod"].ToString();
                txtMiejscOdb.Text = dt.Rows[0]["adres_odbiorcy_miejscowosc"].ToString();
                txtkrajOdb.Text = dt.Rows[0]["adres_odbiorcy_kraj"].ToString();
                txtTelOdb.Text = dt.Rows[0]["adres_odbiorcy_tel"].ToString();
                txtMailOdb.Text = dt.Rows[0]["adres_odbiorcy_mail"].ToString();

                txtRef1.Text = dt.Rows[0]["ref1"].ToString();
                txtRef2.Text = dt.Rows[0]["ref2"].ToString();
            }
            catch (Exception ex)
            {
                Log.AddLog("pobierzSzczegółyPaczki;" + ex);
            }
        }

        private void btnWyczyscFormularz_Click(object sender, EventArgs e)
        {
            _wfmag = new WfMag();
            _id = null;
            _typ = TypDokumentu.Brak;

            WyczyscFormularz();
        }


        private void WyczyscFormularz()
        {
            cbAdresy.DataSource = null;

            txtNrListu.Text = "";
            txtNazwaNad.Text = "";
            txtImieNazwiskoNad.Text = "";
            txtUlicaNad.Text = "";
            txtKodNad.Text = "";
            txtMiejscNad.Text = "";
            txtkrajNad.Text = "";
            txtTelNad.Text = "";
            txtMailNad.Text = "";

            txtNazwaOdb.Text = "";
            txtImieNazwiskoOdb.Text = "";
            txtUlicaOdb.Text = "";
            txtKodOdb.Text = "";
            txtMiejscOdb.Text = "";
            txtkrajOdb.Text = "";
            txtTelOdb.Text = "";
            txtMailOdb.Text = "";

            txtRef1.Text = "";
            txtRef2.Text = "";
            txtDuty.Text = "";
            txtPudo.Text = "";
            btnGenerujList.Enabled = true;

            chDorGwaran.Checked = false;
            chDorDoRakWl.Checked = false;
            chDorNaAdrPryw.Checked = false;
            chPobranie.Checked = false;
            chOdbiorWlasny.Checked = false;
            chDokZwrot.Checked = false;
            chPrzesList.Checked = false;
            chPrzesZwrot.Checked = false;
            chOpony.Checked = false;
            chTiresExport.Checked = false;
            chDeklWart.Checked = false;
            chPudo.Checked = false;
            chDuty.Checked = false;
            chDpdExpress.Checked = false;
            chPallet.Checked = false;

            if (!Default.autoWydrukPoZapisie)
            {
                chDrukuj.Checked = false;
                chGenerujEtykiete.Checked = false;
            }

            txtPobranie.Text = "";
            txtDeklarowanaWartosc.Text = "";

            dgPaczki.Rows.Clear();
        }

        private void chZaznaczoneL_CheckedChanged(object sender, EventArgs e)
        {
            PobierzListy();
        }

        private void btnPobierzAdresZUstawien_Click(object sender, EventArgs e)
        {
            txtNazwaNad.Text = Default.nazwaNad;
            txtImieNazwiskoNad.Text = Default.imieNazwiskoNad;
            txtUlicaNad.Text = Default.ulicaNad;
            txtKodNad.Text = Default.kodNad;
            txtMiejscNad.Text = Default.miejscowoscNad;
            txtkrajNad.Text = Default.krajNad;
            txtTelNad.Text = Default.telefonNad;
            txtMailNad.Text = Default.mailNad;
        }

        private void txtNumerDok_TextChanged(object sender, EventArgs e)
        {
            PobierzListy();
        }

        private void cbFiltrZam_SelectedIndexChanged(object sender, EventArgs e)
        {
            PokazDokumenty(ParseEnum(cbFiltrZam.SelectedIndex), dgZamowienia, true);
        }

        private void cbFiltrHan_SelectedIndexChanged(object sender, EventArgs e)
        {
            PokazDokumenty(ParseEnum(cbFiltrHan.SelectedIndex), dgHandlowe, true);
        }

        private void cbFiltrMag_SelectedIndexChanged(object sender, EventArgs e)
        {
            PokazDokumenty(ParseEnum(cbFiltrMag.SelectedIndex), dgMagazynowe, true);
        }

        private void txtNadawca_TextChanged(object sender, EventArgs e)
        {
            PobierzListy();
        }

        private void Form1_SizeChanged(object sender, EventArgs e)
        {
            lblBrakWynikow.Location = new Point(Width / 2 - 80, Height / 2 - 50);
        }

        private void dgLista_Sorted(object sender, EventArgs e)
        {
            if (dgLista.DataSource == null)
            {
                Checked.Visible = false;
                lblBrakWynikow.Visible = true;
            }
            else
            {
                Checked.Visible = true;
                lblBrakWynikow.Visible = false;

                foreach (DataGridViewRow row in dgLista.Rows)
                {
                    row.Cells["nr_listu"].Value =
                        row.Cells["nr_listu"].Value.ToString().Replace(";", Environment.NewLine);

                    var paczki = row.Cells["nr_listu"].Value.ToString()
                        .Split(new[] {Environment.NewLine}, StringSplitOptions.None).Length;
                    row.Cells["paczki"].Value = paczki;
                }
            }
        }

        private void dgLista_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right && dgLista.HitTest(e.X, e.Y).RowIndex == -1)
            {
                var cm = new ContextMenu();

                cm.MenuItems.Add(new MenuItem("Konfiguracja kolumn", delegate
                {
                    var form = new KolumnyForm(_isStatusServiceEnabled);
                    form.ShowDialog();

                    if (form.IsSaved)
                        FormatDgLista();
                }));

                cm.Show(dgLista, e.Location);

                return;
            }

            if (e.Button == MouseButtons.Right &&
                dgLista.HitTest(e.X, e.Y).ColumnIndex == dgLista.Columns["nr_listu"].Index)
            {
                var currentRowIndex = dgLista.HitTest(e.X, e.Y).RowIndex;
                var currentColumnIndex = dgLista.HitTest(e.X, e.Y).ColumnIndex;
                dgLista.Rows[currentRowIndex].Selected = true;

                var cm = new ContextMenu();

                cm.MenuItems.Add(new MenuItem("Kopiuj nr listu", delegate
                {
                    Clipboard.SetText(dgLista.Rows[currentRowIndex].Cells[currentColumnIndex].Value.ToString()
                        .Replace(Environment.NewLine, ";"));
                    Clipboard.SetText(dgLista.Rows[currentRowIndex].Cells[currentColumnIndex].Value.ToString());
                }));

                var item = new MenuItem("Pokaż szczegółowy status", delegate
                {
                    var eventList =
                        _statusInfo.GetEvents(dgLista.Rows[currentRowIndex].Cells[currentColumnIndex].Value.ToString());

                    if (eventList != null && eventList.Events != null)
                    {
                        var lastStatus = eventList.Events.OrderBy(l => l.Date).Last().Description;
                        var list = new List<string[]>
                        {
                            new[] {dgLista.Rows[currentRowIndex].Cells["id"].Value.ToString(), lastStatus}
                        };

                        var data = DateTime.Now;
                        _wfmag.UpdateStatusList(list, data);

                        dgLista.Rows[currentRowIndex].Cells["status"].Value = lastStatus;

                        var form = new StatusyForm(eventList);
                        form.ShowDialog();
                    }
                    else
                    {
                        MessageBox.Show("Nie udało się pobrać statusów - system zwrócił błąd");
                    }
                });

                item.Enabled = _isStatusServiceEnabled;

                cm.MenuItems.Add(item);

                cm.Show(dgLista, e.Location);
            }
        }

        private void cbKrajowe_SelectedIndexChanged(object sender, EventArgs e)
        {
            PobierzListy();
        }

        private void dpListyFrom_CloseUp(object sender, EventArgs e)
        {
            PobierzListy();
        }

        private void dpListyTo_CloseUp(object sender, EventArgs e)
        {
            PobierzListy();
        }

        private void btnGenerujRaport_Click(object sender, EventArgs e)
        {
            try
            {
                var idki = PobierzIdkiListow();
                var nazwaRaportu = $"raport{DateTime.Now:ddMMyyyyHHmm}.csv";
                string path = null;

                if (string.IsNullOrEmpty(Default.raportySciezka))
                    path = nazwaRaportu;
                else
                    path = Default.raportySciezka + "\\" + nazwaRaportu;

                if (Directory.Exists(new FileInfo(path).DirectoryName))
                {
                    Raport.Generuj(path, idki);

                    MessageBox.Show($"Wygenerowano raport o nazwie {nazwaRaportu}");
                }
                else
                {
                    MessageBox.Show("Nie wygenerowano raportu - błędna ścieżka zapisu");
                }
            }
            catch (Exception ex)
            {
                Log.AddLog(ex.Message);
            }
        }

        private List<string> PobierzIdkiListow()
        {
            var lista = new List<string>();

            foreach (DataGridViewRow row in dgLista.Rows)
                lista.Add(row.Cells["id"].Value.ToString());

            return lista;
        }

        private readonly ToolTip _tipPobranie = new ToolTip();

        private void chPobranie_MouseHover(object sender, EventArgs e)
        {
            _tipPobranie.AutomaticDelay = 500;
            _tipPobranie.AutoPopDelay = 2000;
            _tipPobranie.Show("Od 0.01 do 12 000 PLN", chPobranie);
        }

        private readonly ToolTip _tipPrzesylkaWartosciowa = new ToolTip();

        private void chDeklWart_MouseHover(object sender, EventArgs e)
        {
            _tipPrzesylkaWartosciowa.AutomaticDelay = 500;
            _tipPrzesylkaWartosciowa.AutoPopDelay = 2000;
            _tipPrzesylkaWartosciowa.Show("Kwota z przedziału 0.01 - 500 000 PLN", chDeklWart);
        }

        private void dgZamowienia_Sorted(object sender, EventArgs e)
        {
            FormatDgZam();
        }

        private void btnPokaz_Click(object sender, EventArgs e)
        {
            pnlZamowienia.Refresh();
            zamowieniaToolStripMenuItem.PerformClick();
            PokazDokumenty(ParseEnum(cbFiltrZam.SelectedIndex), dgZamowienia, true);
        }

        private void btnPokazHandlowy_Click(object sender, EventArgs e)
        {
            pnlMagazynowe.Refresh();
            dokumentyMagazynoweToolStripMenuItem.PerformClick();
            PokazDokumenty(ParseEnum(cbFiltrMag.SelectedIndex), dgMagazynowe, true);
        }

        private void btnPokazHand_Click(object sender, EventArgs e)
        {
            pnlHandlowe.Refresh();
            dokumentyHadnloweToolStripMenuItem.PerformClick();
            PokazDokumenty(ParseEnum(cbFiltrHan.SelectedIndex), dgHandlowe, true);
        }

        private void btnPobierzStatusy_Click(object sender, EventArgs e)
        {
            _checkStatusWorker.RunWorkerAsync();
        }

        private void cbMagazynZamowienie_SelectedValueChanged(object sender, EventArgs e)
        {
            pnlZamowienia.Refresh();

            PokazDokumenty(ParseEnum(cbFiltrZam.SelectedIndex), dgZamowienia, true);
        }

        private void cbMagazynyHan_SelectedIndexChanged(object sender, EventArgs e)
        {
            pnlHandlowe.Refresh();

            PokazDokumenty(ParseEnum(cbFiltrHan.SelectedIndex), dgHandlowe, true);
        }

        private void cbMagazynMag_SelectedValueChanged(object sender, EventArgs e)
        {
            pnlMagazynowe.Refresh();

            PokazDokumenty(ParseEnum(cbFiltrMag.SelectedIndex), dgMagazynowe, true);
        }

        private void chPudo_CheckedChanged(object sender, EventArgs e)
        {
            txtPudo.Enabled = chPudo.Checked;
            if (!chPudo.Checked)
                txtPudo.Text = string.Empty;
        }

        private void dgHandlowe_Sorted(object sender, EventArgs e)
        {
            FormatDgHan();
        }

        private void dgMagazynowe_Sorted(object sender, EventArgs e)
        {
            FormatDgMag();
        }

        private void btnFindPickup_Click(object sender, EventArgs e)
        {
            var parcelShopSearch = new ParcelShopSearch();
            parcelShopSearch.ShowDialog();

            if (parcelShopSearch.DialogResult != DialogResult.OK) return;
            if (string.IsNullOrEmpty(parcelShopSearch.ReturnVal) || parcelShopSearch.ReturnVal.Equals("-")) return;
            chPudo.Checked = true;
            txtPudo.Text = parcelShopSearch.ReturnVal;
        }

        private void chDuty_CheckedChanged(object sender, EventArgs e)
        {
            txtDuty.Enabled = chDuty.Checked;
        }

        private void btnCallCourier_Click(object sender, EventArgs e)
        {
            var callCourierForm = new CallCourierForm(_dpdService,_authData);
            callCourierForm.WypelnijAdresZamowieniaKuriera(_wfmag.IdDokumentu);
            callCourierForm.ShowDialog();

        }
    }

    public class DataGridViewDisableCheckBoxColumn : DataGridViewCheckBoxColumn
    {
        public DataGridViewDisableCheckBoxColumn()
        {
            CellTemplate = new DataGridViewDisableCheckBoxCell();
        }
    }

    public class DataGridViewDisableCheckBoxCell : DataGridViewCheckBoxCell
    {
        protected override bool MouseClickUnsharesRow(DataGridViewCellMouseEventArgs e)
        {
            if (Enabled)
                Checked = !Checked;
            return base.MouseClickUnsharesRow(e);
        }

        public bool Enabled { get; set; }

        public bool Checked { get; set; }

        public override object Clone()
        {
            var cell = (DataGridViewDisableCheckBoxCell) base.Clone();
            cell.Enabled = Enabled;
            return cell;
        }

        protected override void Paint(Graphics graphics, Rectangle clipBounds, Rectangle cellBounds,
            int rowIndex, DataGridViewElementStates elementState, object value,
            object formattedValue, string errorText, DataGridViewCellStyle cellStyle,
            DataGridViewAdvancedBorderStyle advancedBorderStyle, DataGridViewPaintParts paintParts)
        {
            var cellBackground = new SolidBrush(cellStyle.BackColor);
            graphics.FillRectangle(cellBackground, cellBounds);
            cellBackground.Dispose();
            PaintBorder(graphics, clipBounds, cellBounds,
                cellStyle, advancedBorderStyle);
            var checkBoxArea = cellBounds;
            var buttonAdjustment = BorderWidths(advancedBorderStyle);
            checkBoxArea.X += buttonAdjustment.X;
            checkBoxArea.Y += buttonAdjustment.Y;

            checkBoxArea.Height -= buttonAdjustment.Height;
            checkBoxArea.Width -= buttonAdjustment.Width;
            var drawInPoint = new Point(cellBounds.X + cellBounds.Width / 2 - 7,
                cellBounds.Y + cellBounds.Height / 2 - 7);

            if (Enabled)
            {
                CheckBoxRenderer.DrawCheckBox(graphics, drawInPoint,
                    Checked ? CheckBoxState.CheckedNormal : CheckBoxState.UncheckedNormal);
            }
            else
            {
                CheckBoxRenderer.DrawCheckBox(graphics, drawInPoint,
                    Checked ? CheckBoxState.CheckedDisabled : CheckBoxState.UncheckedDisabled);
            }
        }
    }
}