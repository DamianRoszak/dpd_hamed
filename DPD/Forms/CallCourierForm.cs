﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using DPD.DpdApi;
using DPD.Repos;

namespace DPD.Forms
{
    public partial class CallCourierForm : Form
    {
        private readonly DPDPackageObjServicesService _dpdService;

        private readonly authDataV1 _authData;

        private readonly Dictionary<pickupCallOperationTypeDPPEnumV1, string> _operationTypeDict =
            new Dictionary<pickupCallOperationTypeDPPEnumV1, string>
            {
                {pickupCallOperationTypeDPPEnumV1.INSERT, @"DODAJ"},
                {pickupCallOperationTypeDPPEnumV1.UPDATE, @"AKTUALIZUJ"},
                {pickupCallOperationTypeDPPEnumV1.CANCEL, @"ANULUJ"}
            };

        private readonly Dictionary<pickupCallOrderTypeDPPEnumV1, string> _orderTypeDict =
            new Dictionary<pickupCallOrderTypeDPPEnumV1, string>
            {
                {pickupCallOrderTypeDPPEnumV1.DOMESTIC, @"KRAJOWE"},
                {pickupCallOrderTypeDPPEnumV1.INTERNATIONAL, @"ZAGRANICZNE"},
                {pickupCallOrderTypeDPPEnumV1.EXPRESS, @"EXPRESS"}
            };


        public CallCourierForm(DPDPackageObjServicesService dpdService, authDataV1 authData)
        {
            _dpdService = dpdService;
            _authData = authData;

            InitializeComponent();
            cbOperationType.DataSource = new BindingSource(_operationTypeDict, null);
            cbOperationType.ValueMember = "Key";
            cbOperationType.DisplayMember = "Value";

            cbOrderType.DataSource = new BindingSource(_orderTypeDict, null);
            cbOrderType.ValueMember = "Key";
            cbOrderType.DisplayMember = "Value";
            pickupCallDtoBindingSource.DataSource = new PickupCallDto();
        }

        public void WypelnijAdresZamowieniaKuriera(string idDokumentu)
        {
            var firma = Helpers.Sql.PobierzDane(Query.AdresFirmy(idDokumentu));

            textBox5.Text = string.IsNullOrEmpty(firma.Rows[0]["Nazwa"].ToString()) ? "Brak danych" : firma.Rows[0]["Nazwa"].ToString();
            textBox6.Text = string.IsNullOrEmpty(firma.Rows[0]["Ulica"].ToString()) ? "Brak danych" : firma.Rows[0]["Ulica"].ToString();
            textBox10.Text = string.IsNullOrEmpty(firma.Rows[0]["KodPocztowy"].ToString()) ? "Brak danych" : firma.Rows[0]["KodPocztowy"].ToString();
            textBox9.Text = string.IsNullOrEmpty(firma.Rows[0]["Miejscowosc"].ToString()) ? "Brak danych" : firma.Rows[0]["Miejscowosc"].ToString();
            textBox11.Text = string.IsNullOrEmpty(firma.Rows[0]["NumerTelefonu"].ToString()) ? "Brak danych" : firma.Rows[0]["NumerTelefonu"].ToString();
        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            var data = pickupCallDtoBindingSource.DataSource as PickupCallDto;

            var pickupCallData = new dpdPickupCallParamsV3
            {
                pickupCallSimplifiedDetails = new pickupCallSimplifiedDetailsDPPV1
                {
                    packagesParams = new pickupPackagesParamsDPPV1
                    {
                        dox = data.PackagesDox > 0,
                        doxCount = data.PackagesDox,
                        pallet = data.Pallets > 0,
                        palletsCount = data.Pallets,
                        parcelsCount = data.Packages,
                        standardParcel = data.Packages > 0,
                        palletMaxHeightSpecified = true,
                        palletMaxWeightSpecified = true,
                        palletsWeightSpecified = true,
                        parcelMaxDepthSpecified = true,
                        parcelMaxHeightSpecified = true,
                        parcelMaxWeightSpecified = true,
                        parcelMaxWidthSpecified = true,
                        parcelsWeightSpecified = true,
                        doxCountSpecified = true,
                        doxSpecified = true,
                        palletSpecified = true,
                        palletsCountSpecified = true,
                        parcelsCountSpecified = true,
                        standardParcelSpecified = true,
                    },
                    pickupCustomer = new pickupCustomerDPPV1
                    {
                        customerFullName = data.CustomerFullName,
                        customerName = data.CustomerName,
                        customerPhone = data.Phone
                    },
                    pickupPayer = new pickupPayerDPPV1
                    {
                        payerCostCenter = "",
                        payerName = data.PayerName,
                        payerNumber = data.PayerNumber,
                        payerNumberSpecified = true
                    },
                    pickupSender = new pickupSenderDPPV1
                    {
                        senderAddress = data.Address,
                        senderCity = data.City,
                        senderFullName = data.CustomerFullName,
                        senderName = data.CustomerName,
                        senderPhone = data.AddressPhone,
                        senderPostalCode = data.PostCode
                    }
                },
                operationType = data.OperationType,
                operationTypeSpecified = true,
                orderType = data.OrderType,
                orderTypeSpecified = true,
                pickupDate = data.Date.ToString("yyyy-MM-dd"),
                pickupTimeFrom = data.TimeFrom.ToString("HH:mm"),
                pickupTimeTo = data.TimeTo.ToString("HH:mm"),
                waybillsReady = data.WaybillReady,
                waybillsReadySpecified = true,

            };

            if (data.Checksum > 0)
            {
                pickupCallData = new dpdPickupCallParamsV3
                {
                    orderNumber = data.OrderName,
                    checkSum = data.Checksum,
                    checkSumSpecified = data.Checksum > 0,
                    operationType = data.OperationType,
                    operationTypeSpecified = true,
                };
            }
            SendRequest(pickupCallData);
        }

        private void SendRequest(dpdPickupCallParamsV3 pickupCallData)
        {
            try
            {
                var resp = _dpdService.packagesPickupCallV4(pickupCallData, _authData);

                if (resp != null && resp.statusInfo.status != "OK")
                {
                    var errors =
                        new StringBuilder().AppendLine("Wystąpiły błedy podczas przetwarzania: " +
                                                       resp.statusInfo.status);
                    MessageBox.Show(errors.ToString());
                }
                else
                {
                    if (pickupCallData.operationType == pickupCallOperationTypeDPPEnumV1.CANCEL)
                    {
                        MessageBox.Show("Zamówienie zostało anulowane.");
                    }
                    else
                    {
                        MessageBox.Show("Zamówiono kuriera. Numer zamówienia: " + resp.orderNumber +
                                        " Suma kontrolna: " + resp.checkSum);
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        private void ActiveSubmit()
        {
            var dataObj = pickupCallDtoBindingSource.DataSource as PickupCallDto;
            if (dataObj?.OperationType == pickupCallOperationTypeDPPEnumV1.CANCEL)
            {
                btnSubmit.Enabled = !string.IsNullOrEmpty(dataObj?.OrderName) && dataObj.Checksum > 0;
            }
            else
            {
                btnSubmit.Enabled = !string.IsNullOrEmpty(dataObj?.PayerName) &&
                                    dataObj?.PayerNumber > 0 &&
                                    dataObj?.PayerCostCenter != null &&
                                    !string.IsNullOrEmpty(dataObj?.CustomerFullName) &&
                                    !string.IsNullOrEmpty(dataObj?.CustomerName) &&
                                    !string.IsNullOrEmpty(dataObj?.Phone) &&
                                    !string.IsNullOrEmpty(dataObj?.AddressName) &&
                                    !string.IsNullOrEmpty(dataObj?.Address) &&
                                    !string.IsNullOrEmpty(dataObj?.PostCode) &&
                                    !string.IsNullOrEmpty(dataObj?.City) &&
                                    !string.IsNullOrEmpty(dataObj?.AddressPhone) &&
                                    dataObj.Checksum == 0 &&
                                    string.IsNullOrEmpty(dataObj?.OrderName);
            }
        }

        private void pickupCallDtoBindingSource_CurrentChanged(object sender, EventArgs e)
        {
            ActiveSubmit();
        }

        private void pickupCallDtoBindingSource_CurrentItemChanged(object sender, EventArgs e)
        {
            ActiveSubmit();
        }
    }
}
