﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DPD.Classess;
using DPD.Models;

namespace DPD.Forms
{
    public partial class ParcelShopSearch : Form
    {
        public string ReturnVal { get; set; }

        public ParcelShopSearch()
        {
            InitializeComponent();
        }

        private void btnFindParcelShop_Click(object sender, EventArgs e)
        {
            if (rbFindByAddress.Checked)
            {
                GetParcelsListByAddress(cbFindByAddressCountry?.SelectedItem?.ToString(),txtFindByAddressCity.Text,txtFindByAddressPostCode.Text, dtDateFrom.Value);
            }
            else
            {
                GetParcelsListByGeo(txtLongitude.Text, txtLatiude.Text, numDistance.Value, dtDateFrom.Value);
            }
        }

        private void GetParcelsListByGeo(string longitude, string latidute, decimal numDistanceValue, DateTime dateFrom)
        {
            var parcel = new ParcelShopClass("https://mypudo.dpd.com.pl/", "11111","1");

            var resp = parcel.GetParcelShopByLondLat(longitude, latidute, numDistanceValue.ToString(CultureInfo.InvariantCulture), dateFrom);
            dgParcelShopList.DataSource = resp?.Data?.PUDO_ITEMS;

            lError.Text = resp?.StatusDescription;
        }

        private void GetParcelsListByAddress(string country, string city, string postCode, DateTime dateFrom)
        {
            var parcel = new ParcelShopClass("https://mypudo.dpd.com.pl/", "4444", "1");

            var resp = parcel.GetParcelShopByAddress(country, postCode.Replace("-",""),city, dateFrom);

            dgParcelShopList.DataSource = resp?.Data?.PUDO_ITEMS;
            lError.Text = resp?.StatusDescription;
        }

        private void lError_TextChanged(object sender, EventArgs e)
        {
            lError.Visible = !string.IsNullOrEmpty(lError.Text) && lError.Text != @"OK";
        }

        private void rbFindByGeo_CheckedChanged(object sender, EventArgs e)
        {
            gbAddress.Enabled = !rbFindByGeo.Checked;
            gbGeo.Enabled = rbFindByGeo.Checked;
        }

        private void rbFindByAddress_CheckedChanged(object sender, EventArgs e)
        {
            gbGeo.Enabled = !rbFindByAddress.Checked;
            gbAddress.Enabled = rbFindByAddress.Checked;
        }

        private void SetCurrentPickupData(PUDO_ITEM currentSelect)
        {
            lPickupNumber.Text= currentSelect.PudoId;
            lPickupAddress.Text= currentSelect.Address1;
            lPickupPostCode.Text= currentSelect.Zipcode;
            lPickupCity.Text = currentSelect.City;
            lPickupLongitude.Text = currentSelect.Longitude;
            lPickupLatitude.Text = currentSelect.Latitude;

            if (currentSelect.OPENING_HOURS_ITEMS.Count >= 0)
            {
                lMondayFrom.Text = currentSelect.OPENING_HOURS_ITEMS[0].StartTm;
                lMondayTo.Text = currentSelect.OPENING_HOURS_ITEMS[0].EndTm;
            }

            if (currentSelect.OPENING_HOURS_ITEMS.Count >= 1)
            {
                lThuFrom.Text = currentSelect.OPENING_HOURS_ITEMS[1].StartTm;
                lThuTo.Text = currentSelect.OPENING_HOURS_ITEMS[1].EndTm;
            }

            if (currentSelect.OPENING_HOURS_ITEMS.Count >= 2)
            {
                lWenFrom.Text = currentSelect.OPENING_HOURS_ITEMS[2].StartTm;
                lWenTo.Text = currentSelect.OPENING_HOURS_ITEMS[2].EndTm;
            }

            if (currentSelect.OPENING_HOURS_ITEMS.Count >= 3)
            {
                lTheFrom.Text = currentSelect.OPENING_HOURS_ITEMS[3].StartTm;
                lTheTo.Text = currentSelect.OPENING_HOURS_ITEMS[3].EndTm;
            }

            if (currentSelect.OPENING_HOURS_ITEMS.Count >=4)
            {
                lFriFrom.Text = currentSelect.OPENING_HOURS_ITEMS[4].StartTm;
                lFriTo.Text = currentSelect.OPENING_HOURS_ITEMS[4].EndTm;
            }

            if (currentSelect.OPENING_HOURS_ITEMS.Count >=6)
            {
                lSatFrom.Text = currentSelect.OPENING_HOURS_ITEMS[5].StartTm;
                lSatTo.Text = currentSelect.OPENING_HOURS_ITEMS[5].EndTm;
            }

        }
        
        private void label7_Click(object sender, EventArgs e)
        { }

        private void Ulica_Click(object sender, EventArgs e)
        {

        }

        private void label9_Click(object sender, EventArgs e)
        {

        }

        private void label12_Click(object sender, EventArgs e)
        {

        }

        private void dgParcelShopList_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
           
        }


        private void btnClose_Click(object sender, EventArgs e)
        {
            ReturnVal = string.Empty;
            DialogResult = DialogResult.Cancel;
            Close();
        }

        private void btnConfirm_Click(object sender, EventArgs e)
        {
            ReturnVal = lPickupNumber.Text;
            DialogResult = DialogResult.OK;
            Close();
        }

        private void dgParcelShopList_SelectionChanged(object sender, EventArgs e)
        {
            if (dgParcelShopList.SelectedRows.Count <= 0)
                return;

            var currentSelect = (PUDO_ITEM)dgParcelShopList.SelectedRows[0].DataBoundItem;
            SetCurrentPickupData(currentSelect);
        }
    }
}
