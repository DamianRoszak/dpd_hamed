﻿namespace DPD.Forms
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.chDeklWart = new System.Windows.Forms.CheckBox();
            this.gbDeklarowanaWartosc = new System.Windows.Forms.GroupBox();
            this.txtDeklarowanaWartosc = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.chOdbiorWlasny = new System.Windows.Forms.CheckBox();
            this.chDorNaAdrPryw = new System.Windows.Forms.CheckBox();
            this.gbOdbiorWlasny = new System.Windows.Forms.GroupBox();
            this.rbOsoba = new System.Windows.Forms.RadioButton();
            this.rbFirma = new System.Windows.Forms.RadioButton();
            this.chPrzesList = new System.Windows.Forms.CheckBox();
            this.chDokZwrot = new System.Windows.Forms.CheckBox();
            this.chDorGwaran = new System.Windows.Forms.CheckBox();
            this.chDorDoRakWl = new System.Windows.Forms.CheckBox();
            this.chPrzesZwrot = new System.Windows.Forms.CheckBox();
            this.chPobranie = new System.Windows.Forms.CheckBox();
            this.dgPaczki = new System.Windows.Forms.DataGridView();
            this.LP = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Waga = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.content = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cust1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Długość = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Szerokość = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Wysokość = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Usun = new System.Windows.Forms.DataGridViewLinkColumn();
            this.txtPobranie = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtNrListu = new System.Windows.Forms.TextBox();
            this.btnGenerujList = new System.Windows.Forms.Button();
            this.cbAdresy = new System.Windows.Forms.ComboBox();
            this.label25 = new System.Windows.Forms.Label();
            this.txtkrajOdb = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.txtTelOdb = new System.Windows.Forms.TextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.txtNazwaOdb = new System.Windows.Forms.TextBox();
            this.label33 = new System.Windows.Forms.Label();
            this.txtUlicaOdb = new System.Windows.Forms.TextBox();
            this.label34 = new System.Windows.Forms.Label();
            this.txtKodOdb = new System.Windows.Forms.TextBox();
            this.txtMiejscOdb = new System.Windows.Forms.TextBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.dokumentyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.zamowieniaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dokumentyMagazynoweToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dokumentyHadnloweToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.generowanieToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.przygotowaneToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ustawieniaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.btnQ1 = new System.Windows.Forms.Button();
            this.btnQ3 = new System.Windows.Forms.Button();
            this.btnQ2 = new System.Windows.Forms.Button();
            this.btnQ4 = new System.Windows.Forms.Button();
            this.pnlPrzygotowane = new System.Windows.Forms.Panel();
            this.btnCallCourier = new System.Windows.Forms.Button();
            this.btnPobierzStatusy = new System.Windows.Forms.Button();
            this.btnGenerujRaport = new System.Windows.Forms.Button();
            this.cbKrajowe = new System.Windows.Forms.ComboBox();
            this.lblBrakWynikow = new System.Windows.Forms.Label();
            this.txtNadawca = new System.Windows.Forms.TextBox();
            this.txtNumerDok = new System.Windows.Forms.TextBox();
            this.lblPozycjaEtykietyP = new System.Windows.Forms.Label();
            this.chZaznaczoneL = new System.Windows.Forms.CheckBox();
            this.lblLiczbaPaczek = new System.Windows.Forms.Label();
            this.btnpQ4 = new System.Windows.Forms.Button();
            this.btnpQ2 = new System.Windows.Forms.Button();
            this.btnpQ3 = new System.Windows.Forms.Button();
            this.btnpQ1 = new System.Windows.Forms.Button();
            this.chDrukujL = new System.Windows.Forms.CheckBox();
            this.label4 = new System.Windows.Forms.Label();
            this.dpListyTo = new System.Windows.Forms.DateTimePicker();
            this.dpListyFrom = new System.Windows.Forms.DateTimePicker();
            this.label5 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.btnGenerujProtokol = new System.Windows.Forms.Button();
            this.btnGenerujEtykiety = new System.Windows.Forms.Button();
            this.btnUsun = new System.Windows.Forms.Button();
            this.lblLiczbaDok = new System.Windows.Forms.Label();
            this.txtNrProtokolu = new System.Windows.Forms.TextBox();
            this.txtFindNrListu = new System.Windows.Forms.TextBox();
            this.txtOdbiorca = new System.Windows.Forms.TextBox();
            this.dgLista = new System.Windows.Forms.DataGridView();
            this.Checked = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.colStatus = new System.Windows.Forms.DataGridViewButtonColumn();
            this.pnlGenerowanie = new System.Windows.Forms.Panel();
            this.label35 = new System.Windows.Forms.Label();
            this.txtDuty = new System.Windows.Forms.TextBox();
            this.chDuty = new System.Windows.Forms.CheckBox();
            this.chDpdExpress = new System.Windows.Forms.CheckBox();
            this.btnFindPickup = new System.Windows.Forms.Button();
            this.chPallet = new System.Windows.Forms.CheckBox();
            this.txtPudo = new System.Windows.Forms.TextBox();
            this.chPudo = new System.Windows.Forms.CheckBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtNaOkreslonaGodzine = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.cbDoreczenie = new System.Windows.Forms.ComboBox();
            this.txtImieNazwiskoNad = new System.Windows.Forms.TextBox();
            this.txtImieNazwiskoOdb = new System.Windows.Forms.TextBox();
            this.rbDrukuj = new System.Windows.Forms.RadioButton();
            this.rbZapiszEtykiete = new System.Windows.Forms.RadioButton();
            this.lblPozycjaEtykiety = new System.Windows.Forms.Label();
            this.chOpony = new System.Windows.Forms.CheckBox();
            this.label21 = new System.Windows.Forms.Label();
            this.btnPobierzAdresZUstawien = new System.Windows.Forms.Button();
            this.label28 = new System.Windows.Forms.Label();
            this.btnWyczyscFormularz = new System.Windows.Forms.Button();
            this.chTiresExport = new System.Windows.Forms.CheckBox();
            this.lblFID = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.txtMailOdb = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.txtMailNad = new System.Windows.Forms.TextBox();
            this.cbPlatnik = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.chDrukuj = new System.Windows.Forms.CheckBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.txtRef2 = new System.Windows.Forms.TextBox();
            this.txtRef1 = new System.Windows.Forms.TextBox();
            this.txtReference = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.chWniesienie = new System.Windows.Forms.CheckBox();
            this.chGenerujEtykiete = new System.Windows.Forms.CheckBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtkrajNad = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txtTelNad = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.txtNazwaNad = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.txtUlicaNad = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.txtKodNad = new System.Windows.Forms.TextBox();
            this.txtMiejscNad = new System.Windows.Forms.TextBox();
            this.pnlDokumenty = new System.Windows.Forms.Panel();
            this.pnlZamowienia = new System.Windows.Forms.Panel();
            this.label29 = new System.Windows.Forms.Label();
            this.btnPokaz = new System.Windows.Forms.Button();
            this.cbMagazynZamowienie = new System.Windows.Forms.ComboBox();
            this.cbFiltrZam = new System.Windows.Forms.ComboBox();
            this.btnGenZam = new System.Windows.Forms.Button();
            this.txtKontrahentZam = new System.Windows.Forms.TextBox();
            this.dpZamTo = new System.Windows.Forms.DateTimePicker();
            this.dpZamFrom = new System.Windows.Forms.DateTimePicker();
            this.txtNrZam = new System.Windows.Forms.TextBox();
            this.dgZamowienia = new System.Windows.Forms.DataGridView();
            this.zaznaczZ = new DPD.Forms.DataGridViewDisableCheckBoxColumn();
            this.pnlHandlowe = new System.Windows.Forms.Panel();
            this.btnPokazHand = new System.Windows.Forms.Button();
            this.cbMagazynyHan = new System.Windows.Forms.ComboBox();
            this.label30 = new System.Windows.Forms.Label();
            this.cbFiltrHan = new System.Windows.Forms.ComboBox();
            this.btnGenHan = new System.Windows.Forms.Button();
            this.txtKontrahentHan = new System.Windows.Forms.TextBox();
            this.dpHanTo = new System.Windows.Forms.DateTimePicker();
            this.dpHanFrom = new System.Windows.Forms.DateTimePicker();
            this.txtNrHan = new System.Windows.Forms.TextBox();
            this.dgHandlowe = new System.Windows.Forms.DataGridView();
            this.zaznaczH = new DPD.Forms.DataGridViewDisableCheckBoxColumn();
            this.pnlMagazynowe = new System.Windows.Forms.Panel();
            this.label24 = new System.Windows.Forms.Label();
            this.btnPokazMag = new System.Windows.Forms.Button();
            this.cbFiltrMag = new System.Windows.Forms.ComboBox();
            this.btnGenMag = new System.Windows.Forms.Button();
            this.txtKontrahentMag = new System.Windows.Forms.TextBox();
            this.dpMagTo = new System.Windows.Forms.DateTimePicker();
            this.dpMagFrom = new System.Windows.Forms.DateTimePicker();
            this.txtNrMag = new System.Windows.Forms.TextBox();
            this.cbMagazynMag = new System.Windows.Forms.ComboBox();
            this.dgMagazynowe = new System.Windows.Forms.DataGridView();
            this.zaznaczM = new DPD.Forms.DataGridViewDisableCheckBoxColumn();
            this.dataGridViewCheckBoxColumn1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewDisableCheckBoxColumn1 = new DPD.Forms.DataGridViewDisableCheckBoxColumn();
            this.dataGridViewDisableCheckBoxColumn2 = new DPD.Forms.DataGridViewDisableCheckBoxColumn();
            this.dataGridViewDisableCheckBoxColumn3 = new DPD.Forms.DataGridViewDisableCheckBoxColumn();
            this.gbDeklarowanaWartosc.SuspendLayout();
            this.gbOdbiorWlasny.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgPaczki)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.pnlPrzygotowane.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgLista)).BeginInit();
            this.pnlGenerowanie.SuspendLayout();
            this.pnlDokumenty.SuspendLayout();
            this.pnlZamowienia.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgZamowienia)).BeginInit();
            this.pnlHandlowe.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgHandlowe)).BeginInit();
            this.pnlMagazynowe.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgMagazynowe)).BeginInit();
            this.SuspendLayout();
            // 
            // chDeklWart
            // 
            this.chDeklWart.AutoSize = true;
            this.chDeklWart.ForeColor = System.Drawing.SystemColors.MenuText;
            this.chDeklWart.Location = new System.Drawing.Point(693, 129);
            this.chDeklWart.Name = "chDeklWart";
            this.chDeklWart.Size = new System.Drawing.Size(135, 17);
            this.chDeklWart.TabIndex = 136;
            this.chDeklWart.Text = "Przesyłka wartościowa";
            this.chDeklWart.UseVisualStyleBackColor = true;
            this.chDeklWart.CheckedChanged += new System.EventHandler(this.chDeklWart_CheckedChanged);
            this.chDeklWart.MouseHover += new System.EventHandler(this.chDeklWart_MouseHover);
            // 
            // gbDeklarowanaWartosc
            // 
            this.gbDeklarowanaWartosc.Controls.Add(this.txtDeklarowanaWartosc);
            this.gbDeklarowanaWartosc.Controls.Add(this.label2);
            this.gbDeklarowanaWartosc.Controls.Add(this.label1);
            this.gbDeklarowanaWartosc.ForeColor = System.Drawing.SystemColors.MenuText;
            this.gbDeklarowanaWartosc.Location = new System.Drawing.Point(711, 140);
            this.gbDeklarowanaWartosc.Name = "gbDeklarowanaWartosc";
            this.gbDeklarowanaWartosc.Size = new System.Drawing.Size(111, 32);
            this.gbDeklarowanaWartosc.TabIndex = 143;
            this.gbDeklarowanaWartosc.TabStop = false;
            // 
            // txtDeklarowanaWartosc
            // 
            this.txtDeklarowanaWartosc.Enabled = false;
            this.txtDeklarowanaWartosc.Location = new System.Drawing.Point(43, 9);
            this.txtDeklarowanaWartosc.Name = "txtDeklarowanaWartosc";
            this.txtDeklarowanaWartosc.Size = new System.Drawing.Size(40, 20);
            this.txtDeklarowanaWartosc.TabIndex = 116;
            this.txtDeklarowanaWartosc.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDeklarowanaWartosc_KeyPress);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.ForeColor = System.Drawing.SystemColors.InfoText;
            this.label2.Location = new System.Drawing.Point(89, 12);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(16, 13);
            this.label2.TabIndex = 114;
            this.label2.Text = "zł";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.ForeColor = System.Drawing.SystemColors.InfoText;
            this.label1.Location = new System.Drawing.Point(2, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(37, 13);
            this.label1.TabIndex = 115;
            this.label1.Text = "Kwota";
            // 
            // chOdbiorWlasny
            // 
            this.chOdbiorWlasny.AutoSize = true;
            this.chOdbiorWlasny.ForeColor = System.Drawing.SystemColors.MenuText;
            this.chOdbiorWlasny.Location = new System.Drawing.Point(473, 133);
            this.chOdbiorWlasny.Name = "chOdbiorWlasny";
            this.chOdbiorWlasny.Size = new System.Drawing.Size(94, 17);
            this.chOdbiorWlasny.TabIndex = 142;
            this.chOdbiorWlasny.Text = "Odbiór własny";
            this.chOdbiorWlasny.UseVisualStyleBackColor = true;
            this.chOdbiorWlasny.CheckedChanged += new System.EventHandler(this.chOdbiorWlasny_CheckedChanged);
            // 
            // chDorNaAdrPryw
            // 
            this.chDorNaAdrPryw.AutoSize = true;
            this.chDorNaAdrPryw.ForeColor = System.Drawing.SystemColors.MenuText;
            this.chDorNaAdrPryw.Location = new System.Drawing.Point(473, 92);
            this.chDorNaAdrPryw.Name = "chDorNaAdrPryw";
            this.chDorNaAdrPryw.Size = new System.Drawing.Size(169, 17);
            this.chDorNaAdrPryw.TabIndex = 140;
            this.chDorNaAdrPryw.Text = "Doręczenie na adres prywatny";
            this.chDorNaAdrPryw.UseVisualStyleBackColor = true;
            // 
            // gbOdbiorWlasny
            // 
            this.gbOdbiorWlasny.Controls.Add(this.rbOsoba);
            this.gbOdbiorWlasny.Controls.Add(this.rbFirma);
            this.gbOdbiorWlasny.Enabled = false;
            this.gbOdbiorWlasny.ForeColor = System.Drawing.SystemColors.MenuText;
            this.gbOdbiorWlasny.Location = new System.Drawing.Point(492, 144);
            this.gbOdbiorWlasny.Name = "gbOdbiorWlasny";
            this.gbOdbiorWlasny.Size = new System.Drawing.Size(135, 28);
            this.gbOdbiorWlasny.TabIndex = 145;
            this.gbOdbiorWlasny.TabStop = false;
            // 
            // rbOsoba
            // 
            this.rbOsoba.AutoSize = true;
            this.rbOsoba.Checked = true;
            this.rbOsoba.ForeColor = System.Drawing.SystemColors.InfoText;
            this.rbOsoba.Location = new System.Drawing.Point(9, 8);
            this.rbOsoba.Name = "rbOsoba";
            this.rbOsoba.Size = new System.Drawing.Size(54, 17);
            this.rbOsoba.TabIndex = 125;
            this.rbOsoba.TabStop = true;
            this.rbOsoba.Text = "osoba";
            this.rbOsoba.UseVisualStyleBackColor = true;
            // 
            // rbFirma
            // 
            this.rbFirma.AutoSize = true;
            this.rbFirma.ForeColor = System.Drawing.SystemColors.InfoText;
            this.rbFirma.Location = new System.Drawing.Point(78, 9);
            this.rbFirma.Name = "rbFirma";
            this.rbFirma.Size = new System.Drawing.Size(47, 17);
            this.rbFirma.TabIndex = 127;
            this.rbFirma.Text = "firma";
            this.rbFirma.UseVisualStyleBackColor = true;
            // 
            // chPrzesList
            // 
            this.chPrzesList.AutoSize = true;
            this.chPrzesList.ForeColor = System.Drawing.SystemColors.MenuText;
            this.chPrzesList.Location = new System.Drawing.Point(693, 49);
            this.chPrzesList.Name = "chPrzesList";
            this.chPrzesList.Size = new System.Drawing.Size(49, 17);
            this.chPrzesList.TabIndex = 137;
            this.chPrzesList.Text = "DOX";
            this.chPrzesList.UseVisualStyleBackColor = true;
            // 
            // chDokZwrot
            // 
            this.chDokZwrot.AutoSize = true;
            this.chDokZwrot.ForeColor = System.Drawing.SystemColors.MenuText;
            this.chDokZwrot.Location = new System.Drawing.Point(693, 29);
            this.chDokZwrot.Name = "chDokZwrot";
            this.chDokZwrot.Size = new System.Drawing.Size(120, 17);
            this.chDokZwrot.TabIndex = 141;
            this.chDokZwrot.Text = "Dokumenty zwrotne";
            this.chDokZwrot.UseVisualStyleBackColor = true;
            // 
            // chDorGwaran
            // 
            this.chDorGwaran.AutoSize = true;
            this.chDorGwaran.ForeColor = System.Drawing.SystemColors.MenuText;
            this.chDorGwaran.Location = new System.Drawing.Point(473, 29);
            this.chDorGwaran.Name = "chDorGwaran";
            this.chDorGwaran.Size = new System.Drawing.Size(153, 17);
            this.chDorGwaran.TabIndex = 138;
            this.chDorGwaran.Text = "Doręczenia gwarantowane";
            this.chDorGwaran.UseVisualStyleBackColor = true;
            this.chDorGwaran.CheckedChanged += new System.EventHandler(this.chDorGwaran_CheckedChanged);
            // 
            // chDorDoRakWl
            // 
            this.chDorDoRakWl.AutoSize = true;
            this.chDorDoRakWl.ForeColor = System.Drawing.SystemColors.MenuText;
            this.chDorDoRakWl.Location = new System.Drawing.Point(473, 72);
            this.chDorDoRakWl.Name = "chDorDoRakWl";
            this.chDorDoRakWl.Size = new System.Drawing.Size(162, 17);
            this.chDorDoRakWl.TabIndex = 139;
            this.chDorDoRakWl.Text = "Doręczenie do rąk własnych";
            this.chDorDoRakWl.UseVisualStyleBackColor = true;
            // 
            // chPrzesZwrot
            // 
            this.chPrzesZwrot.AutoSize = true;
            this.chPrzesZwrot.ForeColor = System.Drawing.SystemColors.MenuText;
            this.chPrzesZwrot.Location = new System.Drawing.Point(693, 69);
            this.chPrzesZwrot.Name = "chPrzesZwrot";
            this.chPrzesZwrot.Size = new System.Drawing.Size(113, 17);
            this.chPrzesZwrot.TabIndex = 135;
            this.chPrzesZwrot.Text = "Przesyłka zwrotna";
            this.chPrzesZwrot.UseVisualStyleBackColor = true;
            // 
            // chPobranie
            // 
            this.chPobranie.AutoSize = true;
            this.chPobranie.ForeColor = System.Drawing.SystemColors.MenuText;
            this.chPobranie.Location = new System.Drawing.Point(473, 113);
            this.chPobranie.Name = "chPobranie";
            this.chPobranie.Size = new System.Drawing.Size(103, 17);
            this.chPobranie.TabIndex = 134;
            this.chPobranie.Text = "Pobranie C.O.D.";
            this.chPobranie.UseVisualStyleBackColor = true;
            this.chPobranie.CheckedChanged += new System.EventHandler(this.chPobranie_CheckedChanged);
            this.chPobranie.MouseHover += new System.EventHandler(this.chPobranie_MouseHover);
            // 
            // dgPaczki
            // 
            this.dgPaczki.AllowUserToResizeColumns = false;
            this.dgPaczki.AllowUserToResizeRows = false;
            this.dgPaczki.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.dgPaczki.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgPaczki.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgPaczki.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.LP,
            this.Waga,
            this.content,
            this.cust1,
            this.Długość,
            this.Szerokość,
            this.Wysokość,
            this.Usun});
            this.dgPaczki.Location = new System.Drawing.Point(12, 214);
            this.dgPaczki.Name = "dgPaczki";
            this.dgPaczki.RowHeadersVisible = false;
            this.dgPaczki.Size = new System.Drawing.Size(444, 170);
            this.dgPaczki.TabIndex = 20;
            this.dgPaczki.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgPaczki_CellContentClick);
            this.dgPaczki.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dgPaczki_EditingControlShowing);
            this.dgPaczki.UserAddedRow += new System.Windows.Forms.DataGridViewRowEventHandler(this.dgPaczki_UserAddedRow);
            // 
            // LP
            // 
            this.LP.HeaderText = "";
            this.LP.Name = "LP";
            this.LP.ReadOnly = true;
            this.LP.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.LP.Width = 20;
            // 
            // Waga
            // 
            this.Waga.HeaderText = "Waga [kg]";
            this.Waga.Name = "Waga";
            this.Waga.Width = 40;
            // 
            // content
            // 
            this.content.HeaderText = "Zawartość";
            this.content.Name = "content";
            // 
            // cust1
            // 
            this.cust1.HeaderText = "Uwagi";
            this.cust1.Name = "cust1";
            // 
            // Długość
            // 
            this.Długość.HeaderText = "Dług [cm]";
            this.Długość.Name = "Długość";
            this.Długość.Width = 40;
            // 
            // Szerokość
            // 
            this.Szerokość.HeaderText = "Szer [cm]";
            this.Szerokość.Name = "Szerokość";
            this.Szerokość.Width = 40;
            // 
            // Wysokość
            // 
            this.Wysokość.HeaderText = "Wys [cm]";
            this.Wysokość.Name = "Wysokość";
            this.Wysokość.Width = 40;
            // 
            // Usun
            // 
            this.Usun.ActiveLinkColor = System.Drawing.Color.White;
            this.Usun.DataPropertyName = "Usuń paczkę";
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            this.Usun.DefaultCellStyle = dataGridViewCellStyle1;
            this.Usun.HeaderText = "";
            this.Usun.LinkColor = System.Drawing.Color.Red;
            this.Usun.Name = "Usun";
            this.Usun.Text = "Usuń";
            this.Usun.UseColumnTextForLinkValue = true;
            this.Usun.Width = 45;
            // 
            // txtPobranie
            // 
            this.txtPobranie.Enabled = false;
            this.txtPobranie.ForeColor = System.Drawing.SystemColors.MenuText;
            this.txtPobranie.Location = new System.Drawing.Point(571, 111);
            this.txtPobranie.Name = "txtPobranie";
            this.txtPobranie.Size = new System.Drawing.Size(52, 20);
            this.txtPobranie.TabIndex = 151;
            this.txtPobranie.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtPobranie.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPobranie_KeyPress);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.ForeColor = System.Drawing.SystemColors.MenuText;
            this.label6.Location = new System.Drawing.Point(474, 309);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(39, 13);
            this.label6.TabIndex = 149;
            this.label6.Text = "Nr listu";
            // 
            // txtNrListu
            // 
            this.txtNrListu.Location = new System.Drawing.Point(513, 306);
            this.txtNrListu.Name = "txtNrListu";
            this.txtNrListu.Size = new System.Drawing.Size(197, 20);
            this.txtNrListu.TabIndex = 148;
            // 
            // btnGenerujList
            // 
            this.btnGenerujList.BackColor = System.Drawing.Color.Silver;
            this.btnGenerujList.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGenerujList.ForeColor = System.Drawing.Color.Black;
            this.btnGenerujList.Location = new System.Drawing.Point(513, 376);
            this.btnGenerujList.Name = "btnGenerujList";
            this.btnGenerujList.Size = new System.Drawing.Size(195, 29);
            this.btnGenerujList.TabIndex = 50;
            this.btnGenerujList.Text = "Generuj numer listu";
            this.btnGenerujList.UseVisualStyleBackColor = false;
            this.btnGenerujList.Click += new System.EventHandler(this.btnGenerujList_Click);
            // 
            // cbAdresy
            // 
            this.cbAdresy.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbAdresy.DropDownWidth = 300;
            this.cbAdresy.FormattingEnabled = true;
            this.cbAdresy.Location = new System.Drawing.Point(316, 20);
            this.cbAdresy.Name = "cbAdresy";
            this.cbAdresy.Size = new System.Drawing.Size(140, 21);
            this.cbAdresy.TabIndex = 152;
            this.cbAdresy.SelectedIndexChanged += new System.EventHandler(this.cbAdresy_SelectedIndexChanged);
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.BackColor = System.Drawing.Color.Transparent;
            this.label25.ForeColor = System.Drawing.SystemColors.MenuText;
            this.label25.Location = new System.Drawing.Point(389, 113);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(25, 13);
            this.label25.TabIndex = 164;
            this.label25.Text = "Kraj";
            // 
            // txtkrajOdb
            // 
            this.txtkrajOdb.ForeColor = System.Drawing.SystemColors.MenuText;
            this.txtkrajOdb.Location = new System.Drawing.Point(421, 110);
            this.txtkrajOdb.Name = "txtkrajOdb";
            this.txtkrajOdb.Size = new System.Drawing.Size(35, 20);
            this.txtkrajOdb.TabIndex = 11;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.Color.Transparent;
            this.label17.ForeColor = System.Drawing.SystemColors.MenuText;
            this.label17.Location = new System.Drawing.Point(269, 156);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(43, 13);
            this.label17.TabIndex = 162;
            this.label17.Text = "Telefon";
            // 
            // txtTelOdb
            // 
            this.txtTelOdb.ForeColor = System.Drawing.SystemColors.MenuText;
            this.txtTelOdb.Location = new System.Drawing.Point(316, 154);
            this.txtTelOdb.Name = "txtTelOdb";
            this.txtTelOdb.Size = new System.Drawing.Size(140, 20);
            this.txtTelOdb.TabIndex = 13;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.BackColor = System.Drawing.Color.Transparent;
            this.label31.ForeColor = System.Drawing.SystemColors.MenuText;
            this.label31.Location = new System.Drawing.Point(274, 48);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(40, 13);
            this.label31.TabIndex = 154;
            this.label31.Text = "Nazwa";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.BackColor = System.Drawing.Color.Transparent;
            this.label32.ForeColor = System.Drawing.SystemColors.MenuText;
            this.label32.Location = new System.Drawing.Point(240, 113);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(74, 13);
            this.label32.TabIndex = 160;
            this.label32.Text = "Kod pocztowy";
            // 
            // txtNazwaOdb
            // 
            this.txtNazwaOdb.ForeColor = System.Drawing.SystemColors.MenuText;
            this.txtNazwaOdb.Location = new System.Drawing.Point(316, 44);
            this.txtNazwaOdb.Name = "txtNazwaOdb";
            this.txtNazwaOdb.Size = new System.Drawing.Size(140, 20);
            this.txtNazwaOdb.TabIndex = 8;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.BackColor = System.Drawing.Color.Transparent;
            this.label33.ForeColor = System.Drawing.SystemColors.MenuText;
            this.label33.Location = new System.Drawing.Point(246, 136);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(68, 13);
            this.label33.TabIndex = 159;
            this.label33.Text = "Miejscowość";
            // 
            // txtUlicaOdb
            // 
            this.txtUlicaOdb.ForeColor = System.Drawing.SystemColors.MenuText;
            this.txtUlicaOdb.Location = new System.Drawing.Point(316, 88);
            this.txtUlicaOdb.Name = "txtUlicaOdb";
            this.txtUlicaOdb.Size = new System.Drawing.Size(140, 20);
            this.txtUlicaOdb.TabIndex = 9;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.BackColor = System.Drawing.Color.Transparent;
            this.label34.ForeColor = System.Drawing.SystemColors.MenuText;
            this.label34.Location = new System.Drawing.Point(283, 92);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(31, 13);
            this.label34.TabIndex = 158;
            this.label34.Text = "Ulica";
            // 
            // txtKodOdb
            // 
            this.txtKodOdb.ForeColor = System.Drawing.SystemColors.MenuText;
            this.txtKodOdb.Location = new System.Drawing.Point(316, 110);
            this.txtKodOdb.Name = "txtKodOdb";
            this.txtKodOdb.Size = new System.Drawing.Size(50, 20);
            this.txtKodOdb.TabIndex = 10;
            // 
            // txtMiejscOdb
            // 
            this.txtMiejscOdb.ForeColor = System.Drawing.SystemColors.MenuText;
            this.txtMiejscOdb.Location = new System.Drawing.Point(316, 132);
            this.txtMiejscOdb.Name = "txtMiejscOdb";
            this.txtMiejscOdb.Size = new System.Drawing.Size(140, 20);
            this.txtMiejscOdb.TabIndex = 12;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dokumentyToolStripMenuItem,
            this.generowanieToolStripMenuItem,
            this.przygotowaneToolStripMenuItem,
            this.ustawieniaToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1042, 24);
            this.menuStrip1.TabIndex = 165;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // dokumentyToolStripMenuItem
            // 
            this.dokumentyToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.zamowieniaToolStripMenuItem,
            this.dokumentyMagazynoweToolStripMenuItem,
            this.dokumentyHadnloweToolStripMenuItem});
            this.dokumentyToolStripMenuItem.Name = "dokumentyToolStripMenuItem";
            this.dokumentyToolStripMenuItem.Size = new System.Drawing.Size(130, 20);
            this.dokumentyToolStripMenuItem.Text = "Dokumenty WF-Mag";
            this.dokumentyToolStripMenuItem.Click += new System.EventHandler(this.dokumentyToolStripMenuItem_Click);
            // 
            // zamowieniaToolStripMenuItem
            // 
            this.zamowieniaToolStripMenuItem.Name = "zamowieniaToolStripMenuItem";
            this.zamowieniaToolStripMenuItem.Size = new System.Drawing.Size(209, 22);
            this.zamowieniaToolStripMenuItem.Text = "Zamowienia";
            this.zamowieniaToolStripMenuItem.Click += new System.EventHandler(this.zamowieniaToolStripMenuItem_Click);
            // 
            // dokumentyMagazynoweToolStripMenuItem
            // 
            this.dokumentyMagazynoweToolStripMenuItem.Name = "dokumentyMagazynoweToolStripMenuItem";
            this.dokumentyMagazynoweToolStripMenuItem.Size = new System.Drawing.Size(209, 22);
            this.dokumentyMagazynoweToolStripMenuItem.Text = "Dokumenty magazynowe";
            this.dokumentyMagazynoweToolStripMenuItem.Click += new System.EventHandler(this.dokumentyMagazynoweToolStripMenuItem_Click);
            // 
            // dokumentyHadnloweToolStripMenuItem
            // 
            this.dokumentyHadnloweToolStripMenuItem.Name = "dokumentyHadnloweToolStripMenuItem";
            this.dokumentyHadnloweToolStripMenuItem.Size = new System.Drawing.Size(209, 22);
            this.dokumentyHadnloweToolStripMenuItem.Text = "Dokumenty handlowe";
            this.dokumentyHadnloweToolStripMenuItem.Click += new System.EventHandler(this.dokumentyHadnloweToolStripMenuItem_Click);
            // 
            // generowanieToolStripMenuItem
            // 
            this.generowanieToolStripMenuItem.Name = "generowanieToolStripMenuItem";
            this.generowanieToolStripMenuItem.Size = new System.Drawing.Size(139, 20);
            this.generowanieToolStripMenuItem.Text = "Generowanie przesyłek";
            this.generowanieToolStripMenuItem.Click += new System.EventHandler(this.generowanieToolStripMenuItem_Click);
            // 
            // przygotowaneToolStripMenuItem
            // 
            this.przygotowaneToolStripMenuItem.Name = "przygotowaneToolStripMenuItem";
            this.przygotowaneToolStripMenuItem.Size = new System.Drawing.Size(59, 20);
            this.przygotowaneToolStripMenuItem.Text = "Wysyłki";
            this.przygotowaneToolStripMenuItem.Click += new System.EventHandler(this.przygotowaneToolStripMenuItem_Click);
            // 
            // ustawieniaToolStripMenuItem
            // 
            this.ustawieniaToolStripMenuItem.Name = "ustawieniaToolStripMenuItem";
            this.ustawieniaToolStripMenuItem.Size = new System.Drawing.Size(76, 20);
            this.ustawieniaToolStripMenuItem.Text = "Ustawienia";
            this.ustawieniaToolStripMenuItem.Click += new System.EventHandler(this.ustawieniaToolStripMenuItem_Click);
            // 
            // btnQ1
            // 
            this.btnQ1.BackColor = System.Drawing.Color.Gray;
            this.btnQ1.Enabled = false;
            this.btnQ1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnQ1.Location = new System.Drawing.Point(750, 351);
            this.btnQ1.Name = "btnQ1";
            this.btnQ1.Size = new System.Drawing.Size(20, 30);
            this.btnQ1.TabIndex = 41;
            this.btnQ1.UseVisualStyleBackColor = false;
            this.btnQ1.Click += new System.EventHandler(this.ZmienPozycje);
            // 
            // btnQ3
            // 
            this.btnQ3.Enabled = false;
            this.btnQ3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnQ3.Location = new System.Drawing.Point(769, 351);
            this.btnQ3.Name = "btnQ3";
            this.btnQ3.Size = new System.Drawing.Size(20, 30);
            this.btnQ3.TabIndex = 42;
            this.btnQ3.UseVisualStyleBackColor = true;
            this.btnQ3.Click += new System.EventHandler(this.ZmienPozycje);
            // 
            // btnQ2
            // 
            this.btnQ2.Enabled = false;
            this.btnQ2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnQ2.Location = new System.Drawing.Point(750, 380);
            this.btnQ2.Name = "btnQ2";
            this.btnQ2.Size = new System.Drawing.Size(20, 30);
            this.btnQ2.TabIndex = 43;
            this.btnQ2.UseVisualStyleBackColor = true;
            this.btnQ2.Click += new System.EventHandler(this.ZmienPozycje);
            // 
            // btnQ4
            // 
            this.btnQ4.Enabled = false;
            this.btnQ4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnQ4.Location = new System.Drawing.Point(769, 380);
            this.btnQ4.Name = "btnQ4";
            this.btnQ4.Size = new System.Drawing.Size(20, 30);
            this.btnQ4.TabIndex = 44;
            this.btnQ4.UseVisualStyleBackColor = true;
            this.btnQ4.Click += new System.EventHandler(this.ZmienPozycje);
            // 
            // pnlPrzygotowane
            // 
            this.pnlPrzygotowane.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlPrzygotowane.Controls.Add(this.btnCallCourier);
            this.pnlPrzygotowane.Controls.Add(this.btnPobierzStatusy);
            this.pnlPrzygotowane.Controls.Add(this.btnGenerujRaport);
            this.pnlPrzygotowane.Controls.Add(this.cbKrajowe);
            this.pnlPrzygotowane.Controls.Add(this.lblBrakWynikow);
            this.pnlPrzygotowane.Controls.Add(this.txtNadawca);
            this.pnlPrzygotowane.Controls.Add(this.txtNumerDok);
            this.pnlPrzygotowane.Controls.Add(this.lblPozycjaEtykietyP);
            this.pnlPrzygotowane.Controls.Add(this.chZaznaczoneL);
            this.pnlPrzygotowane.Controls.Add(this.lblLiczbaPaczek);
            this.pnlPrzygotowane.Controls.Add(this.btnpQ4);
            this.pnlPrzygotowane.Controls.Add(this.btnpQ2);
            this.pnlPrzygotowane.Controls.Add(this.btnpQ3);
            this.pnlPrzygotowane.Controls.Add(this.btnpQ1);
            this.pnlPrzygotowane.Controls.Add(this.chDrukujL);
            this.pnlPrzygotowane.Controls.Add(this.label4);
            this.pnlPrzygotowane.Controls.Add(this.dpListyTo);
            this.pnlPrzygotowane.Controls.Add(this.dpListyFrom);
            this.pnlPrzygotowane.Controls.Add(this.label5);
            this.pnlPrzygotowane.Controls.Add(this.label9);
            this.pnlPrzygotowane.Controls.Add(this.label10);
            this.pnlPrzygotowane.Controls.Add(this.btnGenerujProtokol);
            this.pnlPrzygotowane.Controls.Add(this.btnGenerujEtykiety);
            this.pnlPrzygotowane.Controls.Add(this.btnUsun);
            this.pnlPrzygotowane.Controls.Add(this.lblLiczbaDok);
            this.pnlPrzygotowane.Controls.Add(this.txtNrProtokolu);
            this.pnlPrzygotowane.Controls.Add(this.txtFindNrListu);
            this.pnlPrzygotowane.Controls.Add(this.txtOdbiorca);
            this.pnlPrzygotowane.Controls.Add(this.dgLista);
            this.pnlPrzygotowane.Location = new System.Drawing.Point(0, 24);
            this.pnlPrzygotowane.Name = "pnlPrzygotowane";
            this.pnlPrzygotowane.Size = new System.Drawing.Size(1040, 470);
            this.pnlPrzygotowane.TabIndex = 176;
            // 
            // btnCallCourier
            // 
            this.btnCallCourier.Location = new System.Drawing.Point(746, 438);
            this.btnCallCourier.Name = "btnCallCourier";
            this.btnCallCourier.Size = new System.Drawing.Size(112, 23);
            this.btnCallCourier.TabIndex = 315;
            this.btnCallCourier.Text = "Zamów kuriera";
            this.btnCallCourier.UseVisualStyleBackColor = true;
            this.btnCallCourier.Click += new System.EventHandler(this.btnCallCourier_Click);
            // 
            // btnPobierzStatusy
            // 
            this.btnPobierzStatusy.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnPobierzStatusy.Location = new System.Drawing.Point(642, 438);
            this.btnPobierzStatusy.Name = "btnPobierzStatusy";
            this.btnPobierzStatusy.Size = new System.Drawing.Size(98, 23);
            this.btnPobierzStatusy.TabIndex = 314;
            this.btnPobierzStatusy.Text = "Pobierz statusy";
            this.btnPobierzStatusy.UseVisualStyleBackColor = true;
            this.btnPobierzStatusy.Click += new System.EventHandler(this.btnPobierzStatusy_Click);
            // 
            // btnGenerujRaport
            // 
            this.btnGenerujRaport.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnGenerujRaport.Location = new System.Drawing.Point(951, 5);
            this.btnGenerujRaport.Name = "btnGenerujRaport";
            this.btnGenerujRaport.Size = new System.Drawing.Size(82, 23);
            this.btnGenerujRaport.TabIndex = 311;
            this.btnGenerujRaport.Text = "Generuj raport";
            this.btnGenerujRaport.UseVisualStyleBackColor = true;
            this.btnGenerujRaport.Click += new System.EventHandler(this.btnGenerujRaport_Click);
            // 
            // cbKrajowe
            // 
            this.cbKrajowe.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbKrajowe.FormattingEnabled = true;
            this.cbKrajowe.Items.AddRange(new object[] {
            "Wszystkie",
            "krajowe",
            "zagraniczne"});
            this.cbKrajowe.Location = new System.Drawing.Point(472, 4);
            this.cbKrajowe.Name = "cbKrajowe";
            this.cbKrajowe.Size = new System.Drawing.Size(85, 21);
            this.cbKrajowe.TabIndex = 310;
            this.cbKrajowe.SelectedIndexChanged += new System.EventHandler(this.cbKrajowe_SelectedIndexChanged);
            // 
            // lblBrakWynikow
            // 
            this.lblBrakWynikow.AutoSize = true;
            this.lblBrakWynikow.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblBrakWynikow.Location = new System.Drawing.Point(485, 188);
            this.lblBrakWynikow.Name = "lblBrakWynikow";
            this.lblBrakWynikow.Size = new System.Drawing.Size(129, 17);
            this.lblBrakWynikow.TabIndex = 234;
            this.lblBrakWynikow.Text = "BRAK WYNIKÓW";
            this.lblBrakWynikow.Visible = false;
            // 
            // txtNadawca
            // 
            this.txtNadawca.Location = new System.Drawing.Point(88, 5);
            this.txtNadawca.Name = "txtNadawca";
            this.txtNadawca.Size = new System.Drawing.Size(110, 20);
            this.txtNadawca.TabIndex = 302;
            this.txtNadawca.TextChanged += new System.EventHandler(this.txtNadawca_TextChanged);
            // 
            // txtNumerDok
            // 
            this.txtNumerDok.Location = new System.Drawing.Point(6, 5);
            this.txtNumerDok.Name = "txtNumerDok";
            this.txtNumerDok.Size = new System.Drawing.Size(80, 20);
            this.txtNumerDok.TabIndex = 301;
            this.txtNumerDok.TextChanged += new System.EventHandler(this.txtNumerDok_TextChanged);
            // 
            // lblPozycjaEtykietyP
            // 
            this.lblPozycjaEtykietyP.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblPozycjaEtykietyP.AutoSize = true;
            this.lblPozycjaEtykietyP.BackColor = System.Drawing.Color.Transparent;
            this.lblPozycjaEtykietyP.ForeColor = System.Drawing.SystemColors.MenuText;
            this.lblPozycjaEtykietyP.Location = new System.Drawing.Point(489, 443);
            this.lblPozycjaEtykietyP.Name = "lblPozycjaEtykietyP";
            this.lblPozycjaEtykietyP.Size = new System.Drawing.Size(83, 13);
            this.lblPozycjaEtykietyP.TabIndex = 231;
            this.lblPozycjaEtykietyP.Text = "Pozycja etykiety";
            // 
            // chZaznaczoneL
            // 
            this.chZaznaczoneL.AutoSize = true;
            this.chZaznaczoneL.Location = new System.Drawing.Point(6, 27);
            this.chZaznaczoneL.Name = "chZaznaczoneL";
            this.chZaznaczoneL.Size = new System.Drawing.Size(116, 17);
            this.chZaznaczoneL.TabIndex = 306;
            this.chZaznaczoneL.Text = "Pokaż zaznaczone";
            this.chZaznaczoneL.UseVisualStyleBackColor = true;
            this.chZaznaczoneL.CheckedChanged += new System.EventHandler(this.chZaznaczoneL_CheckedChanged);
            // 
            // lblLiczbaPaczek
            // 
            this.lblLiczbaPaczek.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblLiczbaPaczek.AutoSize = true;
            this.lblLiczbaPaczek.Location = new System.Drawing.Point(2, 425);
            this.lblLiczbaPaczek.Name = "lblLiczbaPaczek";
            this.lblLiczbaPaczek.Size = new System.Drawing.Size(13, 13);
            this.lblLiczbaPaczek.TabIndex = 64;
            this.lblLiczbaPaczek.Text = "0";
            // 
            // btnpQ4
            // 
            this.btnpQ4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnpQ4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnpQ4.Location = new System.Drawing.Point(598, 434);
            this.btnpQ4.Name = "btnpQ4";
            this.btnpQ4.Size = new System.Drawing.Size(16, 24);
            this.btnpQ4.TabIndex = 62;
            this.btnpQ4.UseVisualStyleBackColor = true;
            this.btnpQ4.Click += new System.EventHandler(this.ZmienPozycje);
            // 
            // btnpQ2
            // 
            this.btnpQ2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnpQ2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnpQ2.Location = new System.Drawing.Point(583, 434);
            this.btnpQ2.Name = "btnpQ2";
            this.btnpQ2.Size = new System.Drawing.Size(16, 24);
            this.btnpQ2.TabIndex = 61;
            this.btnpQ2.UseVisualStyleBackColor = true;
            this.btnpQ2.Click += new System.EventHandler(this.ZmienPozycje);
            // 
            // btnpQ3
            // 
            this.btnpQ3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnpQ3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnpQ3.Location = new System.Drawing.Point(598, 411);
            this.btnpQ3.Name = "btnpQ3";
            this.btnpQ3.Size = new System.Drawing.Size(16, 24);
            this.btnpQ3.TabIndex = 60;
            this.btnpQ3.UseVisualStyleBackColor = true;
            this.btnpQ3.Click += new System.EventHandler(this.ZmienPozycje);
            // 
            // btnpQ1
            // 
            this.btnpQ1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnpQ1.BackColor = System.Drawing.Color.Gray;
            this.btnpQ1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnpQ1.Location = new System.Drawing.Point(583, 411);
            this.btnpQ1.Name = "btnpQ1";
            this.btnpQ1.Size = new System.Drawing.Size(16, 24);
            this.btnpQ1.TabIndex = 59;
            this.btnpQ1.UseVisualStyleBackColor = false;
            this.btnpQ1.Click += new System.EventHandler(this.ZmienPozycje);
            // 
            // chDrukujL
            // 
            this.chDrukujL.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chDrukujL.AutoSize = true;
            this.chDrukujL.Location = new System.Drawing.Point(209, 442);
            this.chDrukujL.Name = "chDrukujL";
            this.chDrukujL.Size = new System.Drawing.Size(57, 17);
            this.chDrukujL.TabIndex = 55;
            this.chDrukujL.Text = "Drukuj";
            this.chDrukujL.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label4.Location = new System.Drawing.Point(14, 8);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(0, 13);
            this.label4.TabIndex = 54;
            // 
            // dpListyTo
            // 
            this.dpListyTo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.dpListyTo.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dpListyTo.Location = new System.Drawing.Point(874, 7);
            this.dpListyTo.Name = "dpListyTo";
            this.dpListyTo.Size = new System.Drawing.Size(74, 20);
            this.dpListyTo.TabIndex = 308;
            this.dpListyTo.CloseUp += new System.EventHandler(this.dpListyTo_CloseUp);
            this.dpListyTo.ValueChanged += new System.EventHandler(this.dpListyTo_ValueChanged);
            // 
            // dpListyFrom
            // 
            this.dpListyFrom.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.dpListyFrom.CustomFormat = "";
            this.dpListyFrom.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dpListyFrom.Location = new System.Drawing.Point(784, 7);
            this.dpListyFrom.Name = "dpListyFrom";
            this.dpListyFrom.Size = new System.Drawing.Size(74, 20);
            this.dpListyFrom.TabIndex = 307;
            this.dpListyFrom.Value = new System.DateTime(2014, 1, 29, 0, 0, 0, 0);
            this.dpListyFrom.CloseUp += new System.EventHandler(this.dpListyFrom_CloseUp);
            this.dpListyFrom.ValueChanged += new System.EventHandler(this.dpListyFrom_ValueChanged);
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(856, 10);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(21, 13);
            this.label5.TabIndex = 45;
            this.label5.Text = "Do";
            // 
            // label9
            // 
            this.label9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(766, 10);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(21, 13);
            this.label9.TabIndex = 44;
            this.label9.Text = "Od";
            // 
            // label10
            // 
            this.label10.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(2, 443);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(190, 13);
            this.label10.TabIndex = 43;
            this.label10.Text = "Operacje dla zaznaczonych przesyłek:";
            // 
            // btnGenerujProtokol
            // 
            this.btnGenerujProtokol.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnGenerujProtokol.Location = new System.Drawing.Point(376, 438);
            this.btnGenerujProtokol.Name = "btnGenerujProtokol";
            this.btnGenerujProtokol.Size = new System.Drawing.Size(98, 23);
            this.btnGenerujProtokol.TabIndex = 42;
            this.btnGenerujProtokol.Text = "Generuj protokół";
            this.btnGenerujProtokol.UseVisualStyleBackColor = true;
            this.btnGenerujProtokol.Click += new System.EventHandler(this.btnGenerujProtokol_Click);
            // 
            // btnGenerujEtykiety
            // 
            this.btnGenerujEtykiety.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnGenerujEtykiety.Location = new System.Drawing.Point(279, 438);
            this.btnGenerujEtykiety.Name = "btnGenerujEtykiety";
            this.btnGenerujEtykiety.Size = new System.Drawing.Size(94, 23);
            this.btnGenerujEtykiety.TabIndex = 41;
            this.btnGenerujEtykiety.Text = "Generuj etykiety";
            this.btnGenerujEtykiety.UseVisualStyleBackColor = true;
            this.btnGenerujEtykiety.Click += new System.EventHandler(this.btnGenerujEtykiety_Click);
            // 
            // btnUsun
            // 
            this.btnUsun.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnUsun.Location = new System.Drawing.Point(957, 438);
            this.btnUsun.Name = "btnUsun";
            this.btnUsun.Size = new System.Drawing.Size(76, 23);
            this.btnUsun.TabIndex = 40;
            this.btnUsun.Text = "Usuń";
            this.btnUsun.UseVisualStyleBackColor = true;
            this.btnUsun.Click += new System.EventHandler(this.btnUsun_Click);
            // 
            // lblLiczbaDok
            // 
            this.lblLiczbaDok.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblLiczbaDok.AutoSize = true;
            this.lblLiczbaDok.Location = new System.Drawing.Point(2, 406);
            this.lblLiczbaDok.Name = "lblLiczbaDok";
            this.lblLiczbaDok.Size = new System.Drawing.Size(13, 13);
            this.lblLiczbaDok.TabIndex = 36;
            this.lblLiczbaDok.Text = "0";
            // 
            // txtNrProtokolu
            // 
            this.txtNrProtokolu.Location = new System.Drawing.Point(404, 5);
            this.txtNrProtokolu.Name = "txtNrProtokolu";
            this.txtNrProtokolu.Size = new System.Drawing.Size(66, 20);
            this.txtNrProtokolu.TabIndex = 305;
            this.txtNrProtokolu.TextChanged += new System.EventHandler(this.txtNrProtokolu_TextChanged);
            // 
            // txtFindNrListu
            // 
            this.txtFindNrListu.Location = new System.Drawing.Point(312, 5);
            this.txtFindNrListu.Name = "txtFindNrListu";
            this.txtFindNrListu.Size = new System.Drawing.Size(90, 20);
            this.txtFindNrListu.TabIndex = 304;
            this.txtFindNrListu.TextChanged += new System.EventHandler(this.txtFindNrListu_TextChanged);
            // 
            // txtOdbiorca
            // 
            this.txtOdbiorca.Location = new System.Drawing.Point(200, 5);
            this.txtOdbiorca.Name = "txtOdbiorca";
            this.txtOdbiorca.Size = new System.Drawing.Size(110, 20);
            this.txtOdbiorca.TabIndex = 303;
            this.txtOdbiorca.TextChanged += new System.EventHandler(this.txtKontrahent_TextChanged);
            // 
            // dgLista
            // 
            this.dgLista.AllowUserToAddRows = false;
            this.dgLista.AllowUserToDeleteRows = false;
            this.dgLista.AllowUserToOrderColumns = true;
            this.dgLista.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgLista.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgLista.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgLista.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgLista.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgLista.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Checked,
            this.colStatus});
            this.dgLista.Location = new System.Drawing.Point(5, 44);
            this.dgLista.MultiSelect = false;
            this.dgLista.Name = "dgLista";
            this.dgLista.RowHeadersVisible = false;
            this.dgLista.RowTemplate.Height = 32;
            this.dgLista.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dgLista.Size = new System.Drawing.Size(1028, 359);
            this.dgLista.TabIndex = 32;
            this.dgLista.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgLista_CellContentClick);
            this.dgLista.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgLista_CellMouseDoubleClick);
            this.dgLista.Sorted += new System.EventHandler(this.dgLista_Sorted);
            this.dgLista.MouseClick += new System.Windows.Forms.MouseEventHandler(this.dgLista_MouseClick);
            // 
            // Checked
            // 
            this.Checked.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.Checked.HeaderText = "";
            this.Checked.Name = "Checked";
            this.Checked.Width = 20;
            // 
            // colStatus
            // 
            this.colStatus.HeaderText = "";
            this.colStatus.Name = "colStatus";
            this.colStatus.Text = "Status";
            this.colStatus.UseColumnTextForButtonValue = true;
            // 
            // pnlGenerowanie
            // 
            this.pnlGenerowanie.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlGenerowanie.Controls.Add(this.label35);
            this.pnlGenerowanie.Controls.Add(this.txtDuty);
            this.pnlGenerowanie.Controls.Add(this.chDuty);
            this.pnlGenerowanie.Controls.Add(this.chDpdExpress);
            this.pnlGenerowanie.Controls.Add(this.btnFindPickup);
            this.pnlGenerowanie.Controls.Add(this.chPallet);
            this.pnlGenerowanie.Controls.Add(this.txtPudo);
            this.pnlGenerowanie.Controls.Add(this.chPudo);
            this.pnlGenerowanie.Controls.Add(this.txtPobranie);
            this.pnlGenerowanie.Controls.Add(this.chOdbiorWlasny);
            this.pnlGenerowanie.Controls.Add(this.chDeklWart);
            this.pnlGenerowanie.Controls.Add(this.label8);
            this.pnlGenerowanie.Controls.Add(this.chDorGwaran);
            this.pnlGenerowanie.Controls.Add(this.txtNaOkreslonaGodzine);
            this.pnlGenerowanie.Controls.Add(this.label7);
            this.pnlGenerowanie.Controls.Add(this.cbDoreczenie);
            this.pnlGenerowanie.Controls.Add(this.txtImieNazwiskoNad);
            this.pnlGenerowanie.Controls.Add(this.gbOdbiorWlasny);
            this.pnlGenerowanie.Controls.Add(this.txtImieNazwiskoOdb);
            this.pnlGenerowanie.Controls.Add(this.chPobranie);
            this.pnlGenerowanie.Controls.Add(this.rbDrukuj);
            this.pnlGenerowanie.Controls.Add(this.chPrzesZwrot);
            this.pnlGenerowanie.Controls.Add(this.rbZapiszEtykiete);
            this.pnlGenerowanie.Controls.Add(this.chDokZwrot);
            this.pnlGenerowanie.Controls.Add(this.lblPozycjaEtykiety);
            this.pnlGenerowanie.Controls.Add(this.chPrzesList);
            this.pnlGenerowanie.Controls.Add(this.btnQ4);
            this.pnlGenerowanie.Controls.Add(this.chDorNaAdrPryw);
            this.pnlGenerowanie.Controls.Add(this.btnQ2);
            this.pnlGenerowanie.Controls.Add(this.gbDeklarowanaWartosc);
            this.pnlGenerowanie.Controls.Add(this.btnQ3);
            this.pnlGenerowanie.Controls.Add(this.chOpony);
            this.pnlGenerowanie.Controls.Add(this.btnQ1);
            this.pnlGenerowanie.Controls.Add(this.label21);
            this.pnlGenerowanie.Controls.Add(this.btnPobierzAdresZUstawien);
            this.pnlGenerowanie.Controls.Add(this.label28);
            this.pnlGenerowanie.Controls.Add(this.btnWyczyscFormularz);
            this.pnlGenerowanie.Controls.Add(this.chTiresExport);
            this.pnlGenerowanie.Controls.Add(this.lblFID);
            this.pnlGenerowanie.Controls.Add(this.chDorDoRakWl);
            this.pnlGenerowanie.Controls.Add(this.label27);
            this.pnlGenerowanie.Controls.Add(this.txtMailOdb);
            this.pnlGenerowanie.Controls.Add(this.label26);
            this.pnlGenerowanie.Controls.Add(this.txtMailNad);
            this.pnlGenerowanie.Controls.Add(this.cbPlatnik);
            this.pnlGenerowanie.Controls.Add(this.label12);
            this.pnlGenerowanie.Controls.Add(this.label3);
            this.pnlGenerowanie.Controls.Add(this.chDrukuj);
            this.pnlGenerowanie.Controls.Add(this.label23);
            this.pnlGenerowanie.Controls.Add(this.label22);
            this.pnlGenerowanie.Controls.Add(this.label13);
            this.pnlGenerowanie.Controls.Add(this.txtRef2);
            this.pnlGenerowanie.Controls.Add(this.txtRef1);
            this.pnlGenerowanie.Controls.Add(this.txtReference);
            this.pnlGenerowanie.Controls.Add(this.label11);
            this.pnlGenerowanie.Controls.Add(this.chWniesienie);
            this.pnlGenerowanie.Controls.Add(this.chGenerujEtykiete);
            this.pnlGenerowanie.Controls.Add(this.label14);
            this.pnlGenerowanie.Controls.Add(this.txtkrajNad);
            this.pnlGenerowanie.Controls.Add(this.label15);
            this.pnlGenerowanie.Controls.Add(this.txtTelNad);
            this.pnlGenerowanie.Controls.Add(this.label16);
            this.pnlGenerowanie.Controls.Add(this.label18);
            this.pnlGenerowanie.Controls.Add(this.txtNazwaNad);
            this.pnlGenerowanie.Controls.Add(this.label19);
            this.pnlGenerowanie.Controls.Add(this.txtUlicaNad);
            this.pnlGenerowanie.Controls.Add(this.label20);
            this.pnlGenerowanie.Controls.Add(this.txtKodNad);
            this.pnlGenerowanie.Controls.Add(this.txtMiejscNad);
            this.pnlGenerowanie.Controls.Add(this.label25);
            this.pnlGenerowanie.Controls.Add(this.txtkrajOdb);
            this.pnlGenerowanie.Controls.Add(this.label17);
            this.pnlGenerowanie.Controls.Add(this.txtTelOdb);
            this.pnlGenerowanie.Controls.Add(this.label31);
            this.pnlGenerowanie.Controls.Add(this.label32);
            this.pnlGenerowanie.Controls.Add(this.txtNazwaOdb);
            this.pnlGenerowanie.Controls.Add(this.label33);
            this.pnlGenerowanie.Controls.Add(this.txtUlicaOdb);
            this.pnlGenerowanie.Controls.Add(this.label34);
            this.pnlGenerowanie.Controls.Add(this.txtKodOdb);
            this.pnlGenerowanie.Controls.Add(this.txtMiejscOdb);
            this.pnlGenerowanie.Controls.Add(this.cbAdresy);
            this.pnlGenerowanie.Controls.Add(this.label6);
            this.pnlGenerowanie.Controls.Add(this.txtNrListu);
            this.pnlGenerowanie.Controls.Add(this.btnGenerujList);
            this.pnlGenerowanie.Controls.Add(this.dgPaczki);
            this.pnlGenerowanie.Location = new System.Drawing.Point(0, 23);
            this.pnlGenerowanie.Name = "pnlGenerowanie";
            this.pnlGenerowanie.Size = new System.Drawing.Size(1040, 470);
            this.pnlGenerowanie.TabIndex = 177;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(625, 204);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(28, 13);
            this.label35.TabIndex = 292;
            this.label35.Text = "PLN";
            // 
            // txtDuty
            // 
            this.txtDuty.Enabled = false;
            this.txtDuty.ForeColor = System.Drawing.SystemColors.MenuText;
            this.txtDuty.Location = new System.Drawing.Point(571, 201);
            this.txtDuty.Name = "txtDuty";
            this.txtDuty.Size = new System.Drawing.Size(52, 20);
            this.txtDuty.TabIndex = 291;
            this.txtDuty.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // chDuty
            // 
            this.chDuty.AutoSize = true;
            this.chDuty.ForeColor = System.Drawing.SystemColors.MenuText;
            this.chDuty.Location = new System.Drawing.Point(473, 203);
            this.chDuty.Name = "chDuty";
            this.chDuty.Size = new System.Drawing.Size(98, 17);
            this.chDuty.TabIndex = 290;
            this.chDuty.Text = "Odprawa celna";
            this.chDuty.UseVisualStyleBackColor = true;
            this.chDuty.CheckedChanged += new System.EventHandler(this.chDuty_CheckedChanged);
            // 
            // chDpdExpress
            // 
            this.chDpdExpress.AutoSize = true;
            this.chDpdExpress.Location = new System.Drawing.Point(694, 196);
            this.chDpdExpress.Name = "chDpdExpress";
            this.chDpdExpress.Size = new System.Drawing.Size(102, 17);
            this.chDpdExpress.TabIndex = 289;
            this.chDpdExpress.Text = "DPD EXPRESS";
            this.chDpdExpress.UseVisualStyleBackColor = true;
            // 
            // btnFindPickup
            // 
            this.btnFindPickup.Location = new System.Drawing.Point(627, 173);
            this.btnFindPickup.Name = "btnFindPickup";
            this.btnFindPickup.Size = new System.Drawing.Size(47, 23);
            this.btnFindPickup.TabIndex = 282;
            this.btnFindPickup.Text = "Znajdź";
            this.btnFindPickup.UseVisualStyleBackColor = true;
            this.btnFindPickup.Click += new System.EventHandler(this.btnFindPickup_Click);
            // 
            // chPallet
            // 
            this.chPallet.AutoSize = true;
            this.chPallet.Location = new System.Drawing.Point(694, 175);
            this.chPallet.Name = "chPallet";
            this.chPallet.Size = new System.Drawing.Size(119, 17);
            this.chPallet.TabIndex = 244;
            this.chPallet.Text = "Przesyłka paletowa";
            this.chPallet.UseVisualStyleBackColor = true;
            // 
            // txtPudo
            // 
            this.txtPudo.Enabled = false;
            this.txtPudo.Location = new System.Drawing.Point(533, 175);
            this.txtPudo.Name = "txtPudo";
            this.txtPudo.Size = new System.Drawing.Size(91, 20);
            this.txtPudo.TabIndex = 243;
            // 
            // chPudo
            // 
            this.chPudo.AutoSize = true;
            this.chPudo.ForeColor = System.Drawing.SystemColors.MenuText;
            this.chPudo.Location = new System.Drawing.Point(472, 177);
            this.chPudo.Name = "chPudo";
            this.chPudo.Size = new System.Drawing.Size(59, 17);
            this.chPudo.TabIndex = 242;
            this.chPudo.Text = "Pickup";
            this.chPudo.UseVisualStyleBackColor = true;
            this.chPudo.CheckedChanged += new System.EventHandler(this.chPudo_CheckedChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.ForeColor = System.Drawing.SystemColors.MenuText;
            this.label8.Location = new System.Drawing.Point(236, 69);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(78, 13);
            this.label8.TabIndex = 241;
            this.label8.Text = "Imię i nazwisko";
            // 
            // txtNaOkreslonaGodzine
            // 
            this.txtNaOkreslonaGodzine.Location = new System.Drawing.Point(624, 47);
            this.txtNaOkreslonaGodzine.Name = "txtNaOkreslonaGodzine";
            this.txtNaOkreslonaGodzine.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtNaOkreslonaGodzine.Size = new System.Drawing.Size(45, 20);
            this.txtNaOkreslonaGodzine.TabIndex = 227;
            this.txtNaOkreslonaGodzine.Visible = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.ForeColor = System.Drawing.SystemColors.MenuText;
            this.label7.Location = new System.Drawing.Point(10, 70);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(78, 13);
            this.label7.TabIndex = 240;
            this.label7.Text = "Imię i nazwisko";
            // 
            // cbDoreczenie
            // 
            this.cbDoreczenie.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbDoreczenie.FormattingEnabled = true;
            this.cbDoreczenie.Location = new System.Drawing.Point(492, 47);
            this.cbDoreczenie.Name = "cbDoreczenie";
            this.cbDoreczenie.Size = new System.Drawing.Size(126, 21);
            this.cbDoreczenie.TabIndex = 226;
            this.cbDoreczenie.Visible = false;
            this.cbDoreczenie.SelectedIndexChanged += new System.EventHandler(this.cbDoreczenie_SelectedIndexChanged);
            // 
            // txtImieNazwiskoNad
            // 
            this.txtImieNazwiskoNad.ForeColor = System.Drawing.SystemColors.MenuText;
            this.txtImieNazwiskoNad.Location = new System.Drawing.Point(89, 66);
            this.txtImieNazwiskoNad.Name = "txtImieNazwiskoNad";
            this.txtImieNazwiskoNad.Size = new System.Drawing.Size(140, 20);
            this.txtImieNazwiskoNad.TabIndex = 238;
            // 
            // txtImieNazwiskoOdb
            // 
            this.txtImieNazwiskoOdb.ForeColor = System.Drawing.SystemColors.MenuText;
            this.txtImieNazwiskoOdb.Location = new System.Drawing.Point(316, 66);
            this.txtImieNazwiskoOdb.Name = "txtImieNazwiskoOdb";
            this.txtImieNazwiskoOdb.Size = new System.Drawing.Size(140, 20);
            this.txtImieNazwiskoOdb.TabIndex = 239;
            // 
            // rbDrukuj
            // 
            this.rbDrukuj.AutoSize = true;
            this.rbDrukuj.Location = new System.Drawing.Point(616, 351);
            this.rbDrukuj.Name = "rbDrukuj";
            this.rbDrukuj.Size = new System.Drawing.Size(96, 17);
            this.rbDrukuj.TabIndex = 232;
            this.rbDrukuj.TabStop = true;
            this.rbDrukuj.Text = "Drukuj etykietę";
            this.rbDrukuj.UseVisualStyleBackColor = true;
            this.rbDrukuj.Visible = false;
            // 
            // rbZapiszEtykiete
            // 
            this.rbZapiszEtykiete.AutoSize = true;
            this.rbZapiszEtykiete.Location = new System.Drawing.Point(616, 332);
            this.rbZapiszEtykiete.Name = "rbZapiszEtykiete";
            this.rbZapiszEtykiete.Size = new System.Drawing.Size(96, 17);
            this.rbZapiszEtykiete.TabIndex = 231;
            this.rbZapiszEtykiete.TabStop = true;
            this.rbZapiszEtykiete.Text = "Zapisz etykietę";
            this.rbZapiszEtykiete.UseVisualStyleBackColor = true;
            this.rbZapiszEtykiete.Visible = false;
            // 
            // lblPozycjaEtykiety
            // 
            this.lblPozycjaEtykiety.AutoSize = true;
            this.lblPozycjaEtykiety.BackColor = System.Drawing.Color.Transparent;
            this.lblPozycjaEtykiety.ForeColor = System.Drawing.SystemColors.MenuText;
            this.lblPozycjaEtykiety.Location = new System.Drawing.Point(726, 336);
            this.lblPozycjaEtykiety.Name = "lblPozycjaEtykiety";
            this.lblPozycjaEtykiety.Size = new System.Drawing.Size(83, 13);
            this.lblPozycjaEtykiety.TabIndex = 230;
            this.lblPozycjaEtykiety.Text = "Pozycja etykiety";
            // 
            // chOpony
            // 
            this.chOpony.AutoSize = true;
            this.chOpony.ForeColor = System.Drawing.SystemColors.MenuText;
            this.chOpony.Location = new System.Drawing.Point(693, 89);
            this.chOpony.Name = "chOpony";
            this.chOpony.Size = new System.Drawing.Size(57, 17);
            this.chOpony.TabIndex = 225;
            this.chOpony.Text = "Opony";
            this.chOpony.UseVisualStyleBackColor = true;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.BackColor = System.Drawing.Color.Transparent;
            this.label21.ForeColor = System.Drawing.SystemColors.MenuText;
            this.label21.Location = new System.Drawing.Point(624, 115);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(16, 13);
            this.label21.TabIndex = 189;
            this.label21.Text = "zł";
            // 
            // btnPobierzAdresZUstawien
            // 
            this.btnPobierzAdresZUstawien.BackColor = System.Drawing.Color.Silver;
            this.btnPobierzAdresZUstawien.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPobierzAdresZUstawien.ForeColor = System.Drawing.Color.Black;
            this.btnPobierzAdresZUstawien.Location = new System.Drawing.Point(90, 20);
            this.btnPobierzAdresZUstawien.Name = "btnPobierzAdresZUstawien";
            this.btnPobierzAdresZUstawien.Size = new System.Drawing.Size(138, 21);
            this.btnPobierzAdresZUstawien.TabIndex = 229;
            this.btnPobierzAdresZUstawien.Text = "Pobierz adres z ustawień";
            this.btnPobierzAdresZUstawien.UseVisualStyleBackColor = false;
            this.btnPobierzAdresZUstawien.Click += new System.EventHandler(this.btnPobierzAdresZUstawien_Click);
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.BackColor = System.Drawing.Color.Transparent;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label28.ForeColor = System.Drawing.SystemColors.MenuText;
            this.label28.Location = new System.Drawing.Point(489, 4);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(54, 13);
            this.label28.TabIndex = 224;
            this.label28.Text = "USŁUGI";
            // 
            // btnWyczyscFormularz
            // 
            this.btnWyczyscFormularz.BackColor = System.Drawing.Color.Silver;
            this.btnWyczyscFormularz.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnWyczyscFormularz.ForeColor = System.Drawing.Color.Black;
            this.btnWyczyscFormularz.Location = new System.Drawing.Point(728, 305);
            this.btnWyczyscFormularz.Name = "btnWyczyscFormularz";
            this.btnWyczyscFormularz.Size = new System.Drawing.Size(107, 23);
            this.btnWyczyscFormularz.TabIndex = 228;
            this.btnWyczyscFormularz.Text = "Wyczyść formularz";
            this.btnWyczyscFormularz.UseVisualStyleBackColor = false;
            this.btnWyczyscFormularz.Click += new System.EventHandler(this.btnWyczyscFormularz_Click);
            // 
            // chTiresExport
            // 
            this.chTiresExport.AutoSize = true;
            this.chTiresExport.ForeColor = System.Drawing.SystemColors.MenuText;
            this.chTiresExport.Location = new System.Drawing.Point(693, 109);
            this.chTiresExport.Name = "chTiresExport";
            this.chTiresExport.Size = new System.Drawing.Size(139, 17);
            this.chTiresExport.TabIndex = 196;
            this.chTiresExport.Text = "Opony międzynarodowe";
            this.chTiresExport.UseVisualStyleBackColor = true;
            // 
            // lblFID
            // 
            this.lblFID.AutoSize = true;
            this.lblFID.BackColor = System.Drawing.Color.Transparent;
            this.lblFID.ForeColor = System.Drawing.SystemColors.MenuText;
            this.lblFID.Location = new System.Drawing.Point(648, 237);
            this.lblFID.Name = "lblFID";
            this.lblFID.Size = new System.Drawing.Size(24, 13);
            this.lblFID.TabIndex = 223;
            this.lblFID.Text = "Ref";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.BackColor = System.Drawing.Color.Transparent;
            this.label27.ForeColor = System.Drawing.SystemColors.MenuText;
            this.label27.Location = new System.Drawing.Point(276, 179);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(35, 13);
            this.label27.TabIndex = 222;
            this.label27.Text = "E-mail";
            // 
            // txtMailOdb
            // 
            this.txtMailOdb.ForeColor = System.Drawing.SystemColors.MenuText;
            this.txtMailOdb.Location = new System.Drawing.Point(316, 176);
            this.txtMailOdb.Name = "txtMailOdb";
            this.txtMailOdb.Size = new System.Drawing.Size(140, 20);
            this.txtMailOdb.TabIndex = 14;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.BackColor = System.Drawing.Color.Transparent;
            this.label26.ForeColor = System.Drawing.SystemColors.MenuText;
            this.label26.Location = new System.Drawing.Point(49, 179);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(35, 13);
            this.label26.TabIndex = 220;
            this.label26.Text = "E-mail";
            // 
            // txtMailNad
            // 
            this.txtMailNad.ForeColor = System.Drawing.SystemColors.MenuText;
            this.txtMailNad.Location = new System.Drawing.Point(89, 176);
            this.txtMailNad.Name = "txtMailNad";
            this.txtMailNad.Size = new System.Drawing.Size(140, 20);
            this.txtMailNad.TabIndex = 7;
            // 
            // cbPlatnik
            // 
            this.cbPlatnik.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbPlatnik.FormattingEnabled = true;
            this.cbPlatnik.Location = new System.Drawing.Point(515, 233);
            this.cbPlatnik.Name = "cbPlatnik";
            this.cbPlatnik.Size = new System.Drawing.Size(121, 21);
            this.cbPlatnik.TabIndex = 25;
            this.cbPlatnik.SelectedIndexChanged += new System.EventHandler(this.cbPlatnik_SelectedIndexChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label12.ForeColor = System.Drawing.SystemColors.MenuText;
            this.label12.Location = new System.Drawing.Point(314, 4);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(117, 13);
            this.label12.TabIndex = 217;
            this.label12.Text = "ADRES ODBIORCY";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label3.ForeColor = System.Drawing.SystemColors.MenuText;
            this.label3.Location = new System.Drawing.Point(87, 4);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(115, 13);
            this.label3.TabIndex = 216;
            this.label3.Text = "ADRES NADAWCY";
            // 
            // chDrukuj
            // 
            this.chDrukuj.AutoSize = true;
            this.chDrukuj.Enabled = false;
            this.chDrukuj.ForeColor = System.Drawing.SystemColors.ControlText;
            this.chDrukuj.Location = new System.Drawing.Point(513, 353);
            this.chDrukuj.Name = "chDrukuj";
            this.chDrukuj.Size = new System.Drawing.Size(97, 17);
            this.chDrukuj.TabIndex = 29;
            this.chDrukuj.Text = "Drukuj etykietę";
            this.chDrukuj.UseVisualStyleBackColor = true;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.BackColor = System.Drawing.Color.Transparent;
            this.label23.ForeColor = System.Drawing.SystemColors.MenuText;
            this.label23.Location = new System.Drawing.Point(648, 263);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(30, 13);
            this.label23.TabIndex = 213;
            this.label23.Text = "Ref2";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.BackColor = System.Drawing.Color.Transparent;
            this.label22.ForeColor = System.Drawing.SystemColors.MenuText;
            this.label22.Location = new System.Drawing.Point(485, 263);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(30, 13);
            this.label22.TabIndex = 212;
            this.label22.Text = "Ref1";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.ForeColor = System.Drawing.SystemColors.MenuText;
            this.label13.Location = new System.Drawing.Point(10, 404);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(24, 13);
            this.label13.TabIndex = 211;
            this.label13.Text = "Ref";
            this.label13.Visible = false;
            // 
            // txtRef2
            // 
            this.txtRef2.ForeColor = System.Drawing.SystemColors.MenuText;
            this.txtRef2.Location = new System.Drawing.Point(678, 260);
            this.txtRef2.Name = "txtRef2";
            this.txtRef2.Size = new System.Drawing.Size(120, 20);
            this.txtRef2.TabIndex = 27;
            // 
            // txtRef1
            // 
            this.txtRef1.ForeColor = System.Drawing.SystemColors.MenuText;
            this.txtRef1.Location = new System.Drawing.Point(515, 260);
            this.txtRef1.Name = "txtRef1";
            this.txtRef1.Size = new System.Drawing.Size(120, 20);
            this.txtRef1.TabIndex = 26;
            // 
            // txtReference
            // 
            this.txtReference.ForeColor = System.Drawing.SystemColors.MenuText;
            this.txtReference.Location = new System.Drawing.Point(33, 401);
            this.txtReference.Name = "txtReference";
            this.txtReference.Size = new System.Drawing.Size(80, 20);
            this.txtReference.TabIndex = 205;
            this.txtReference.Visible = false;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(474, 237);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(41, 13);
            this.label11.TabIndex = 197;
            this.label11.Text = "Płatnik";
            // 
            // chWniesienie
            // 
            this.chWniesienie.AutoSize = true;
            this.chWniesienie.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.chWniesienie.Location = new System.Drawing.Point(121, 410);
            this.chWniesienie.Name = "chWniesienie";
            this.chWniesienie.Size = new System.Drawing.Size(78, 17);
            this.chWniesienie.TabIndex = 195;
            this.chWniesienie.Text = "Wniesienie";
            this.chWniesienie.UseVisualStyleBackColor = true;
            this.chWniesienie.Visible = false;
            // 
            // chGenerujEtykiete
            // 
            this.chGenerujEtykiete.AutoSize = true;
            this.chGenerujEtykiete.Location = new System.Drawing.Point(513, 332);
            this.chGenerujEtykiete.Name = "chGenerujEtykiete";
            this.chGenerujEtykiete.Size = new System.Drawing.Size(97, 17);
            this.chGenerujEtykiete.TabIndex = 28;
            this.chGenerujEtykiete.Text = "Zapisz etykietę";
            this.chGenerujEtykiete.UseVisualStyleBackColor = true;
            this.chGenerujEtykiete.CheckedChanged += new System.EventHandler(this.chGenerujEtykiete_CheckedChanged);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.ForeColor = System.Drawing.SystemColors.MenuText;
            this.label14.Location = new System.Drawing.Point(161, 113);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(25, 13);
            this.label14.TabIndex = 188;
            this.label14.Text = "Kraj";
            // 
            // txtkrajNad
            // 
            this.txtkrajNad.ForeColor = System.Drawing.SystemColors.MenuText;
            this.txtkrajNad.Location = new System.Drawing.Point(191, 110);
            this.txtkrajNad.Name = "txtkrajNad";
            this.txtkrajNad.Size = new System.Drawing.Size(38, 20);
            this.txtkrajNad.TabIndex = 4;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.ForeColor = System.Drawing.SystemColors.MenuText;
            this.label15.Location = new System.Drawing.Point(41, 156);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(43, 13);
            this.label15.TabIndex = 186;
            this.label15.Text = "Telefon";
            // 
            // txtTelNad
            // 
            this.txtTelNad.ForeColor = System.Drawing.SystemColors.MenuText;
            this.txtTelNad.Location = new System.Drawing.Point(89, 154);
            this.txtTelNad.Name = "txtTelNad";
            this.txtTelNad.Size = new System.Drawing.Size(140, 20);
            this.txtTelNad.TabIndex = 6;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.Color.Transparent;
            this.label16.ForeColor = System.Drawing.SystemColors.MenuText;
            this.label16.Location = new System.Drawing.Point(47, 48);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(40, 13);
            this.label16.TabIndex = 178;
            this.label16.Text = "Nazwa";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.Color.Transparent;
            this.label18.ForeColor = System.Drawing.SystemColors.MenuText;
            this.label18.Location = new System.Drawing.Point(13, 113);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(74, 13);
            this.label18.TabIndex = 184;
            this.label18.Text = "Kod pocztowy";
            // 
            // txtNazwaNad
            // 
            this.txtNazwaNad.ForeColor = System.Drawing.SystemColors.MenuText;
            this.txtNazwaNad.Location = new System.Drawing.Point(89, 44);
            this.txtNazwaNad.Name = "txtNazwaNad";
            this.txtNazwaNad.Size = new System.Drawing.Size(140, 20);
            this.txtNazwaNad.TabIndex = 1;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.BackColor = System.Drawing.Color.Transparent;
            this.label19.ForeColor = System.Drawing.SystemColors.MenuText;
            this.label19.Location = new System.Drawing.Point(19, 136);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(68, 13);
            this.label19.TabIndex = 183;
            this.label19.Text = "Miejscowość";
            // 
            // txtUlicaNad
            // 
            this.txtUlicaNad.ForeColor = System.Drawing.SystemColors.MenuText;
            this.txtUlicaNad.Location = new System.Drawing.Point(89, 88);
            this.txtUlicaNad.Name = "txtUlicaNad";
            this.txtUlicaNad.Size = new System.Drawing.Size(140, 20);
            this.txtUlicaNad.TabIndex = 2;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.BackColor = System.Drawing.Color.Transparent;
            this.label20.ForeColor = System.Drawing.SystemColors.MenuText;
            this.label20.Location = new System.Drawing.Point(57, 92);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(31, 13);
            this.label20.TabIndex = 182;
            this.label20.Text = "Ulica";
            // 
            // txtKodNad
            // 
            this.txtKodNad.ForeColor = System.Drawing.SystemColors.MenuText;
            this.txtKodNad.Location = new System.Drawing.Point(89, 110);
            this.txtKodNad.Name = "txtKodNad";
            this.txtKodNad.Size = new System.Drawing.Size(50, 20);
            this.txtKodNad.TabIndex = 3;
            // 
            // txtMiejscNad
            // 
            this.txtMiejscNad.ForeColor = System.Drawing.SystemColors.MenuText;
            this.txtMiejscNad.Location = new System.Drawing.Point(89, 132);
            this.txtMiejscNad.Name = "txtMiejscNad";
            this.txtMiejscNad.Size = new System.Drawing.Size(140, 20);
            this.txtMiejscNad.TabIndex = 5;
            // 
            // pnlDokumenty
            // 
            this.pnlDokumenty.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlDokumenty.Controls.Add(this.pnlHandlowe);
            this.pnlDokumenty.Controls.Add(this.pnlMagazynowe);
            this.pnlDokumenty.Controls.Add(this.pnlZamowienia);
            this.pnlDokumenty.Location = new System.Drawing.Point(0, 23);
            this.pnlDokumenty.Name = "pnlDokumenty";
            this.pnlDokumenty.Size = new System.Drawing.Size(1040, 470);
            this.pnlDokumenty.TabIndex = 178;
            // 
            // pnlZamowienia
            // 
            this.pnlZamowienia.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlZamowienia.Controls.Add(this.label29);
            this.pnlZamowienia.Controls.Add(this.btnPokaz);
            this.pnlZamowienia.Controls.Add(this.cbMagazynZamowienie);
            this.pnlZamowienia.Controls.Add(this.cbFiltrZam);
            this.pnlZamowienia.Controls.Add(this.btnGenZam);
            this.pnlZamowienia.Controls.Add(this.txtKontrahentZam);
            this.pnlZamowienia.Controls.Add(this.dpZamTo);
            this.pnlZamowienia.Controls.Add(this.dpZamFrom);
            this.pnlZamowienia.Controls.Add(this.txtNrZam);
            this.pnlZamowienia.Controls.Add(this.dgZamowienia);
            this.pnlZamowienia.Location = new System.Drawing.Point(0, 0);
            this.pnlZamowienia.Name = "pnlZamowienia";
            this.pnlZamowienia.Size = new System.Drawing.Size(1040, 470);
            this.pnlZamowienia.TabIndex = 4;
            // 
            // label29
            // 
            this.label29.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(3, 448);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(64, 13);
            this.label29.TabIndex = 20;
            this.label29.Text = "Zamówienie";
            // 
            // btnPokaz
            // 
            this.btnPokaz.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPokaz.Location = new System.Drawing.Point(760, 4);
            this.btnPokaz.Name = "btnPokaz";
            this.btnPokaz.Size = new System.Drawing.Size(75, 23);
            this.btnPokaz.TabIndex = 19;
            this.btnPokaz.Text = "Pokaż";
            this.btnPokaz.UseVisualStyleBackColor = true;
            this.btnPokaz.Visible = false;
            this.btnPokaz.Click += new System.EventHandler(this.btnPokaz_Click);
            // 
            // cbMagazynZamowienie
            // 
            this.cbMagazynZamowienie.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbMagazynZamowienie.FormattingEnabled = true;
            this.cbMagazynZamowienie.Location = new System.Drawing.Point(421, 5);
            this.cbMagazynZamowienie.Name = "cbMagazynZamowienie";
            this.cbMagazynZamowienie.Size = new System.Drawing.Size(148, 21);
            this.cbMagazynZamowienie.TabIndex = 18;
            this.cbMagazynZamowienie.SelectedValueChanged += new System.EventHandler(this.cbMagazynZamowienie_SelectedValueChanged);
            // 
            // cbFiltrZam
            // 
            this.cbFiltrZam.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbFiltrZam.FormattingEnabled = true;
            this.cbFiltrZam.Location = new System.Drawing.Point(320, 5);
            this.cbFiltrZam.Name = "cbFiltrZam";
            this.cbFiltrZam.Size = new System.Drawing.Size(98, 21);
            this.cbFiltrZam.TabIndex = 17;
            this.cbFiltrZam.SelectedIndexChanged += new System.EventHandler(this.cbFiltrZam_SelectedIndexChanged);
            // 
            // btnGenZam
            // 
            this.btnGenZam.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnGenZam.Location = new System.Drawing.Point(897, 442);
            this.btnGenZam.Name = "btnGenZam";
            this.btnGenZam.Size = new System.Drawing.Size(137, 23);
            this.btnGenZam.TabIndex = 15;
            this.btnGenZam.Text = "Przygotuj wysyłkę";
            this.btnGenZam.UseVisualStyleBackColor = true;
            this.btnGenZam.Click += new System.EventHandler(this.btnGenZam_Click);
            // 
            // txtKontrahentZam
            // 
            this.txtKontrahentZam.Location = new System.Drawing.Point(115, 5);
            this.txtKontrahentZam.Name = "txtKontrahentZam";
            this.txtKontrahentZam.Size = new System.Drawing.Size(200, 20);
            this.txtKontrahentZam.TabIndex = 14;
            this.txtKontrahentZam.TextChanged += new System.EventHandler(this.txtKontrahentZam_TextChanged);
            // 
            // dpZamTo
            // 
            this.dpZamTo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.dpZamTo.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dpZamTo.Location = new System.Drawing.Point(939, 6);
            this.dpZamTo.Name = "dpZamTo";
            this.dpZamTo.Size = new System.Drawing.Size(94, 20);
            this.dpZamTo.TabIndex = 9;
            this.dpZamTo.CloseUp += new System.EventHandler(this.PobierzZam);
            // 
            // dpZamFrom
            // 
            this.dpZamFrom.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.dpZamFrom.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dpZamFrom.Location = new System.Drawing.Point(841, 6);
            this.dpZamFrom.Name = "dpZamFrom";
            this.dpZamFrom.Size = new System.Drawing.Size(92, 20);
            this.dpZamFrom.TabIndex = 8;
            this.dpZamFrom.CloseUp += new System.EventHandler(this.PobierzZam);
            // 
            // txtNrZam
            // 
            this.txtNrZam.Location = new System.Drawing.Point(10, 5);
            this.txtNrZam.Name = "txtNrZam";
            this.txtNrZam.Size = new System.Drawing.Size(100, 20);
            this.txtNrZam.TabIndex = 7;
            this.txtNrZam.TextChanged += new System.EventHandler(this.PobierzZam);
            // 
            // dgZamowienia
            // 
            this.dgZamowienia.AllowUserToAddRows = false;
            this.dgZamowienia.AllowUserToDeleteRows = false;
            this.dgZamowienia.AllowUserToResizeRows = false;
            this.dgZamowienia.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgZamowienia.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgZamowienia.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgZamowienia.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.zaznaczZ});
            this.dgZamowienia.Location = new System.Drawing.Point(10, 30);
            this.dgZamowienia.MultiSelect = false;
            this.dgZamowienia.Name = "dgZamowienia";
            this.dgZamowienia.RowHeadersVisible = false;
            this.dgZamowienia.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgZamowienia.Size = new System.Drawing.Size(1023, 407);
            this.dgZamowienia.TabIndex = 6;
            this.dgZamowienia.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgZamowienia_CellContentClick);
            this.dgZamowienia.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.PrzejdzDoWidokuGenerowania);
            this.dgZamowienia.Sorted += new System.EventHandler(this.dgZamowienia_Sorted);
            // 
            // zaznaczZ
            // 
            this.zaznaczZ.HeaderText = "";
            this.zaznaczZ.MinimumWidth = 30;
            this.zaznaczZ.Name = "zaznaczZ";
            // 
            // pnlHandlowe
            // 
            this.pnlHandlowe.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlHandlowe.Controls.Add(this.btnPokazHand);
            this.pnlHandlowe.Controls.Add(this.cbMagazynyHan);
            this.pnlHandlowe.Controls.Add(this.label30);
            this.pnlHandlowe.Controls.Add(this.cbFiltrHan);
            this.pnlHandlowe.Controls.Add(this.btnGenHan);
            this.pnlHandlowe.Controls.Add(this.txtKontrahentHan);
            this.pnlHandlowe.Controls.Add(this.dpHanTo);
            this.pnlHandlowe.Controls.Add(this.dpHanFrom);
            this.pnlHandlowe.Controls.Add(this.txtNrHan);
            this.pnlHandlowe.Controls.Add(this.dgHandlowe);
            this.pnlHandlowe.Location = new System.Drawing.Point(0, 0);
            this.pnlHandlowe.Name = "pnlHandlowe";
            this.pnlHandlowe.Size = new System.Drawing.Size(1040, 470);
            this.pnlHandlowe.TabIndex = 6;
            // 
            // btnPokazHand
            // 
            this.btnPokazHand.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPokazHand.Location = new System.Drawing.Point(760, 4);
            this.btnPokazHand.Name = "btnPokazHand";
            this.btnPokazHand.Size = new System.Drawing.Size(75, 23);
            this.btnPokazHand.TabIndex = 21;
            this.btnPokazHand.Text = "Pokaż";
            this.btnPokazHand.UseVisualStyleBackColor = true;
            this.btnPokazHand.Visible = false;
            this.btnPokazHand.Click += new System.EventHandler(this.btnPokazHand_Click);
            // 
            // cbMagazynyHan
            // 
            this.cbMagazynyHan.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbMagazynyHan.FormattingEnabled = true;
            this.cbMagazynyHan.Location = new System.Drawing.Point(421, 6);
            this.cbMagazynyHan.Name = "cbMagazynyHan";
            this.cbMagazynyHan.Size = new System.Drawing.Size(148, 21);
            this.cbMagazynyHan.TabIndex = 20;
            this.cbMagazynyHan.SelectedIndexChanged += new System.EventHandler(this.cbMagazynyHan_SelectedIndexChanged);
            // 
            // label30
            // 
            this.label30.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(3, 447);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(112, 13);
            this.label30.TabIndex = 19;
            this.label30.Text = "Dokumenty Handlowe";
            // 
            // cbFiltrHan
            // 
            this.cbFiltrHan.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbFiltrHan.FormattingEnabled = true;
            this.cbFiltrHan.Location = new System.Drawing.Point(319, 6);
            this.cbFiltrHan.Name = "cbFiltrHan";
            this.cbFiltrHan.Size = new System.Drawing.Size(98, 21);
            this.cbFiltrHan.TabIndex = 18;
            this.cbFiltrHan.SelectedIndexChanged += new System.EventHandler(this.cbFiltrHan_SelectedIndexChanged);
            // 
            // btnGenHan
            // 
            this.btnGenHan.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnGenHan.Location = new System.Drawing.Point(897, 442);
            this.btnGenHan.Name = "btnGenHan";
            this.btnGenHan.Size = new System.Drawing.Size(137, 23);
            this.btnGenHan.TabIndex = 16;
            this.btnGenHan.Text = "Przygotuj wysyłkę";
            this.btnGenHan.UseVisualStyleBackColor = true;
            this.btnGenHan.Click += new System.EventHandler(this.btnGenHan_Click);
            // 
            // txtKontrahentHan
            // 
            this.txtKontrahentHan.Location = new System.Drawing.Point(115, 7);
            this.txtKontrahentHan.Name = "txtKontrahentHan";
            this.txtKontrahentHan.Size = new System.Drawing.Size(200, 20);
            this.txtKontrahentHan.TabIndex = 13;
            this.txtKontrahentHan.TextChanged += new System.EventHandler(this.txtKontrahentHan_TextChanged);
            // 
            // dpHanTo
            // 
            this.dpHanTo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.dpHanTo.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dpHanTo.Location = new System.Drawing.Point(939, 6);
            this.dpHanTo.Name = "dpHanTo";
            this.dpHanTo.Size = new System.Drawing.Size(94, 20);
            this.dpHanTo.TabIndex = 11;
            this.dpHanTo.CloseUp += new System.EventHandler(this.PobierzHan);
            // 
            // dpHanFrom
            // 
            this.dpHanFrom.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.dpHanFrom.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dpHanFrom.Location = new System.Drawing.Point(841, 6);
            this.dpHanFrom.Name = "dpHanFrom";
            this.dpHanFrom.Size = new System.Drawing.Size(92, 20);
            this.dpHanFrom.TabIndex = 10;
            this.dpHanFrom.CloseUp += new System.EventHandler(this.PobierzHan);
            // 
            // txtNrHan
            // 
            this.txtNrHan.Location = new System.Drawing.Point(10, 7);
            this.txtNrHan.Name = "txtNrHan";
            this.txtNrHan.Size = new System.Drawing.Size(100, 20);
            this.txtNrHan.TabIndex = 9;
            this.txtNrHan.TextChanged += new System.EventHandler(this.PobierzHan);
            // 
            // dgHandlowe
            // 
            this.dgHandlowe.AllowUserToAddRows = false;
            this.dgHandlowe.AllowUserToDeleteRows = false;
            this.dgHandlowe.AllowUserToResizeRows = false;
            this.dgHandlowe.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgHandlowe.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgHandlowe.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgHandlowe.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.zaznaczH});
            this.dgHandlowe.Location = new System.Drawing.Point(10, 30);
            this.dgHandlowe.Name = "dgHandlowe";
            this.dgHandlowe.RowHeadersVisible = false;
            this.dgHandlowe.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgHandlowe.Size = new System.Drawing.Size(1023, 407);
            this.dgHandlowe.TabIndex = 8;
            this.dgHandlowe.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.PrzejdzDoWidokuGenerowania);
            this.dgHandlowe.Sorted += new System.EventHandler(this.dgHandlowe_Sorted);
            // 
            // zaznaczH
            // 
            this.zaznaczH.HeaderText = "";
            this.zaznaczH.Name = "zaznaczH";
            // 
            // pnlMagazynowe
            // 
            this.pnlMagazynowe.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlMagazynowe.Controls.Add(this.label24);
            this.pnlMagazynowe.Controls.Add(this.btnPokazMag);
            this.pnlMagazynowe.Controls.Add(this.cbFiltrMag);
            this.pnlMagazynowe.Controls.Add(this.btnGenMag);
            this.pnlMagazynowe.Controls.Add(this.txtKontrahentMag);
            this.pnlMagazynowe.Controls.Add(this.dpMagTo);
            this.pnlMagazynowe.Controls.Add(this.dpMagFrom);
            this.pnlMagazynowe.Controls.Add(this.txtNrMag);
            this.pnlMagazynowe.Controls.Add(this.cbMagazynMag);
            this.pnlMagazynowe.Controls.Add(this.dgMagazynowe);
            this.pnlMagazynowe.Location = new System.Drawing.Point(0, 0);
            this.pnlMagazynowe.Name = "pnlMagazynowe";
            this.pnlMagazynowe.Size = new System.Drawing.Size(1042, 470);
            this.pnlMagazynowe.TabIndex = 5;
            // 
            // label24
            // 
            this.label24.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(3, 447);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(121, 13);
            this.label24.TabIndex = 22;
            this.label24.Text = "Dokument Magazynowy";
            // 
            // btnPokazMag
            // 
            this.btnPokazMag.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPokazMag.Location = new System.Drawing.Point(760, 4);
            this.btnPokazMag.Name = "btnPokazMag";
            this.btnPokazMag.Size = new System.Drawing.Size(75, 23);
            this.btnPokazMag.TabIndex = 21;
            this.btnPokazMag.Text = "Pokaż";
            this.btnPokazMag.UseVisualStyleBackColor = true;
            this.btnPokazMag.Visible = false;
            this.btnPokazMag.Click += new System.EventHandler(this.btnPokazHandlowy_Click);
            // 
            // cbFiltrMag
            // 
            this.cbFiltrMag.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbFiltrMag.FormattingEnabled = true;
            this.cbFiltrMag.Location = new System.Drawing.Point(318, 6);
            this.cbFiltrMag.Name = "cbFiltrMag";
            this.cbFiltrMag.Size = new System.Drawing.Size(98, 21);
            this.cbFiltrMag.TabIndex = 19;
            this.cbFiltrMag.SelectedIndexChanged += new System.EventHandler(this.cbFiltrMag_SelectedIndexChanged);
            // 
            // btnGenMag
            // 
            this.btnGenMag.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnGenMag.Location = new System.Drawing.Point(899, 442);
            this.btnGenMag.Name = "btnGenMag";
            this.btnGenMag.Size = new System.Drawing.Size(137, 23);
            this.btnGenMag.TabIndex = 16;
            this.btnGenMag.Text = "Przygotuj wysyłkę";
            this.btnGenMag.UseVisualStyleBackColor = true;
            this.btnGenMag.Click += new System.EventHandler(this.btnGenMag_Click);
            // 
            // txtKontrahentMag
            // 
            this.txtKontrahentMag.Location = new System.Drawing.Point(115, 7);
            this.txtKontrahentMag.Name = "txtKontrahentMag";
            this.txtKontrahentMag.Size = new System.Drawing.Size(200, 20);
            this.txtKontrahentMag.TabIndex = 12;
            this.txtKontrahentMag.TextChanged += new System.EventHandler(this.txtKontrahentMag_TextChanged);
            // 
            // dpMagTo
            // 
            this.dpMagTo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.dpMagTo.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dpMagTo.Location = new System.Drawing.Point(939, 6);
            this.dpMagTo.Name = "dpMagTo";
            this.dpMagTo.Size = new System.Drawing.Size(96, 20);
            this.dpMagTo.TabIndex = 11;
            this.dpMagTo.CloseUp += new System.EventHandler(this.PobierzMag);
            // 
            // dpMagFrom
            // 
            this.dpMagFrom.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.dpMagFrom.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dpMagFrom.Location = new System.Drawing.Point(841, 6);
            this.dpMagFrom.Name = "dpMagFrom";
            this.dpMagFrom.Size = new System.Drawing.Size(92, 20);
            this.dpMagFrom.TabIndex = 10;
            this.dpMagFrom.CloseUp += new System.EventHandler(this.PobierzMag);
            // 
            // txtNrMag
            // 
            this.txtNrMag.Location = new System.Drawing.Point(10, 7);
            this.txtNrMag.Name = "txtNrMag";
            this.txtNrMag.Size = new System.Drawing.Size(100, 20);
            this.txtNrMag.TabIndex = 9;
            this.txtNrMag.TextChanged += new System.EventHandler(this.PobierzMag);
            // 
            // cbMagazynMag
            // 
            this.cbMagazynMag.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbMagazynMag.FormattingEnabled = true;
            this.cbMagazynMag.Location = new System.Drawing.Point(420, 6);
            this.cbMagazynMag.Name = "cbMagazynMag";
            this.cbMagazynMag.Size = new System.Drawing.Size(148, 21);
            this.cbMagazynMag.TabIndex = 20;
            this.cbMagazynMag.SelectedValueChanged += new System.EventHandler(this.cbMagazynMag_SelectedValueChanged);
            // 
            // dgMagazynowe
            // 
            this.dgMagazynowe.AllowUserToAddRows = false;
            this.dgMagazynowe.AllowUserToDeleteRows = false;
            this.dgMagazynowe.AllowUserToResizeRows = false;
            this.dgMagazynowe.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgMagazynowe.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgMagazynowe.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgMagazynowe.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.zaznaczM});
            this.dgMagazynowe.Location = new System.Drawing.Point(10, 30);
            this.dgMagazynowe.Name = "dgMagazynowe";
            this.dgMagazynowe.RowHeadersVisible = false;
            this.dgMagazynowe.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgMagazynowe.Size = new System.Drawing.Size(1025, 407);
            this.dgMagazynowe.TabIndex = 8;
            this.dgMagazynowe.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.PrzejdzDoWidokuGenerowania);
            this.dgMagazynowe.Sorted += new System.EventHandler(this.dgMagazynowe_Sorted);
            // 
            // zaznaczM
            // 
            this.zaznaczM.HeaderText = "";
            this.zaznaczM.Name = "zaznaczM";
            // 
            // dataGridViewCheckBoxColumn1
            // 
            this.dataGridViewCheckBoxColumn1.HeaderText = "";
            this.dataGridViewCheckBoxColumn1.Name = "dataGridViewCheckBoxColumn1";
            this.dataGridViewCheckBoxColumn1.Width = 25;
            // 
            // dataGridViewDisableCheckBoxColumn1
            // 
            this.dataGridViewDisableCheckBoxColumn1.HeaderText = "";
            this.dataGridViewDisableCheckBoxColumn1.MinimumWidth = 30;
            this.dataGridViewDisableCheckBoxColumn1.Name = "dataGridViewDisableCheckBoxColumn1";
            this.dataGridViewDisableCheckBoxColumn1.Width = 799;
            // 
            // dataGridViewDisableCheckBoxColumn2
            // 
            this.dataGridViewDisableCheckBoxColumn2.HeaderText = "";
            this.dataGridViewDisableCheckBoxColumn2.Name = "dataGridViewDisableCheckBoxColumn2";
            this.dataGridViewDisableCheckBoxColumn2.Width = 799;
            // 
            // dataGridViewDisableCheckBoxColumn3
            // 
            this.dataGridViewDisableCheckBoxColumn3.HeaderText = "";
            this.dataGridViewDisableCheckBoxColumn3.Name = "dataGridViewDisableCheckBoxColumn3";
            this.dataGridViewDisableCheckBoxColumn3.Width = 799;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1042, 493);
            this.Controls.Add(this.pnlDokumenty);
            this.Controls.Add(this.pnlPrzygotowane);
            this.Controls.Add(this.pnlGenerowanie);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.MinimumSize = new System.Drawing.Size(1000, 490);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "DPD Polska - Moduł wysyłkowy dla systemu WF-Mag";
            this.SizeChanged += new System.EventHandler(this.Form1_SizeChanged);
            this.gbDeklarowanaWartosc.ResumeLayout(false);
            this.gbDeklarowanaWartosc.PerformLayout();
            this.gbOdbiorWlasny.ResumeLayout(false);
            this.gbOdbiorWlasny.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgPaczki)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.pnlPrzygotowane.ResumeLayout(false);
            this.pnlPrzygotowane.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgLista)).EndInit();
            this.pnlGenerowanie.ResumeLayout(false);
            this.pnlGenerowanie.PerformLayout();
            this.pnlDokumenty.ResumeLayout(false);
            this.pnlZamowienia.ResumeLayout(false);
            this.pnlZamowienia.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgZamowienia)).EndInit();
            this.pnlHandlowe.ResumeLayout(false);
            this.pnlHandlowe.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgHandlowe)).EndInit();
            this.pnlMagazynowe.ResumeLayout(false);
            this.pnlMagazynowe.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgMagazynowe)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox chDeklWart;
        private System.Windows.Forms.GroupBox gbDeklarowanaWartosc;
        private System.Windows.Forms.TextBox txtDeklarowanaWartosc;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox chOdbiorWlasny;
        private System.Windows.Forms.CheckBox chDorNaAdrPryw;
        private System.Windows.Forms.GroupBox gbOdbiorWlasny;
        private System.Windows.Forms.RadioButton rbOsoba;
        private System.Windows.Forms.RadioButton rbFirma;
        private System.Windows.Forms.CheckBox chPrzesList;
        private System.Windows.Forms.CheckBox chDokZwrot;
        private System.Windows.Forms.CheckBox chDorGwaran;
        private System.Windows.Forms.CheckBox chDorDoRakWl;
        private System.Windows.Forms.CheckBox chPrzesZwrot;
        private System.Windows.Forms.CheckBox chPobranie;
        private System.Windows.Forms.DataGridView dgPaczki;
        private System.Windows.Forms.TextBox txtPobranie;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtNrListu;
        private System.Windows.Forms.Button btnGenerujList;
        private System.Windows.Forms.ComboBox cbAdresy;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox txtkrajOdb;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtTelOdb;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.TextBox txtNazwaOdb;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.TextBox txtUlicaOdb;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.TextBox txtKodOdb;
        private System.Windows.Forms.TextBox txtMiejscOdb;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem ustawieniaToolStripMenuItem;
        private System.Windows.Forms.Button btnQ1;
        private System.Windows.Forms.Button btnQ3;
        private System.Windows.Forms.Button btnQ2;
        private System.Windows.Forms.Button btnQ4;
        private System.Windows.Forms.Panel pnlPrzygotowane;
        private System.Windows.Forms.CheckBox chDrukujL;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DateTimePicker dpListyTo;
        private System.Windows.Forms.DateTimePicker dpListyFrom;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button btnGenerujProtokol;
        private System.Windows.Forms.Button btnGenerujEtykiety;
        private System.Windows.Forms.Button btnUsun;
        private System.Windows.Forms.Label lblLiczbaDok;
        private System.Windows.Forms.TextBox txtNrProtokolu;
        private System.Windows.Forms.TextBox txtFindNrListu;
        private System.Windows.Forms.TextBox txtOdbiorca;
        private System.Windows.Forms.DataGridView dgLista;
        private System.Windows.Forms.Panel pnlGenerowanie;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtkrajNad;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtTelNad;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox txtNazwaNad;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txtUlicaNad;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox txtKodNad;
        private System.Windows.Forms.TextBox txtMiejscNad;
        private System.Windows.Forms.Panel pnlDokumenty;
        private System.Windows.Forms.ToolStripMenuItem dokumentyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem generowanieToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem przygotowaneToolStripMenuItem;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.CheckBox chGenerujEtykiete;
        private System.Windows.Forms.ToolStripMenuItem zamowieniaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dokumentyMagazynoweToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dokumentyHadnloweToolStripMenuItem;
        private System.Windows.Forms.Panel pnlZamowienia;
        private System.Windows.Forms.Panel pnlHandlowe;
        private System.Windows.Forms.DateTimePicker dpHanTo;
        private System.Windows.Forms.DateTimePicker dpHanFrom;
        private System.Windows.Forms.TextBox txtNrHan;
        private System.Windows.Forms.DataGridView dgHandlowe;
        private System.Windows.Forms.DateTimePicker dpZamTo;
        private System.Windows.Forms.DateTimePicker dpZamFrom;
        private System.Windows.Forms.TextBox txtNrZam;
        private System.Windows.Forms.DataGridView dgZamowienia;
        private System.Windows.Forms.Panel pnlMagazynowe;
        private System.Windows.Forms.DateTimePicker dpMagTo;
        private System.Windows.Forms.DateTimePicker dpMagFrom;
        private System.Windows.Forms.TextBox txtNrMag;
        private System.Windows.Forms.CheckBox chTiresExport;
        private System.Windows.Forms.CheckBox chWniesienie;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtKontrahentZam;
        private System.Windows.Forms.TextBox txtKontrahentMag;
        private System.Windows.Forms.TextBox txtKontrahentHan;
        private System.Windows.Forms.TextBox txtRef2;
        private System.Windows.Forms.TextBox txtRef1;
        private System.Windows.Forms.TextBox txtReference;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.CheckBox chDrukuj;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox cbPlatnik;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox txtMailOdb;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox txtMailNad;
        private System.Windows.Forms.Label lblFID;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.CheckBox chOpony;
        private System.Windows.Forms.ComboBox cbDoreczenie;
        private System.Windows.Forms.TextBox txtNaOkreslonaGodzine;
        private DataGridViewDisableCheckBoxColumn zaznaczH;
        private DataGridViewDisableCheckBoxColumn zaznaczM;
        private System.Windows.Forms.Button btnGenZam;
        private System.Windows.Forms.Button btnGenHan;
        private System.Windows.Forms.Button btnGenMag;
        private System.Windows.Forms.Button btnpQ4;
        private System.Windows.Forms.Button btnpQ2;
        private System.Windows.Forms.Button btnpQ3;
        private System.Windows.Forms.Button btnpQ1;
        private System.Windows.Forms.Label lblLiczbaPaczek;
        private System.Windows.Forms.Button btnWyczyscFormularz;
        private System.Windows.Forms.DataGridViewTextBoxColumn LP;
        private System.Windows.Forms.DataGridViewTextBoxColumn Waga;
        private System.Windows.Forms.DataGridViewTextBoxColumn content;
        private System.Windows.Forms.DataGridViewTextBoxColumn cust1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Długość;
        private System.Windows.Forms.DataGridViewTextBoxColumn Szerokość;
        private System.Windows.Forms.DataGridViewTextBoxColumn Wysokość;
        private System.Windows.Forms.DataGridViewLinkColumn Usun;
        private System.Windows.Forms.CheckBox chZaznaczoneL;
        private System.Windows.Forms.Button btnPobierzAdresZUstawien;
        private System.Windows.Forms.Label lblPozycjaEtykiety;
        private System.Windows.Forms.Label lblPozycjaEtykietyP;
        private System.Windows.Forms.TextBox txtNumerDok;
        private DataGridViewDisableCheckBoxColumn zaznaczZ;
        private System.Windows.Forms.ComboBox cbFiltrZam;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn1;
        private DataGridViewDisableCheckBoxColumn dataGridViewDisableCheckBoxColumn1;
        private DataGridViewDisableCheckBoxColumn dataGridViewDisableCheckBoxColumn2;
        private DataGridViewDisableCheckBoxColumn dataGridViewDisableCheckBoxColumn3;
        private System.Windows.Forms.ComboBox cbFiltrMag;
        private System.Windows.Forms.ComboBox cbFiltrHan;
        private System.Windows.Forms.RadioButton rbDrukuj;
        private System.Windows.Forms.RadioButton rbZapiszEtykiete;
        private System.Windows.Forms.TextBox txtNadawca;
        private System.Windows.Forms.Label lblBrakWynikow;
        private System.Windows.Forms.ComboBox cbKrajowe;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtImieNazwiskoNad;
        private System.Windows.Forms.TextBox txtImieNazwiskoOdb;
        private System.Windows.Forms.Button btnGenerujRaport;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Checked;
        private System.Windows.Forms.DataGridViewButtonColumn colStatus;
        private System.Windows.Forms.Button btnPobierzStatusy;
        private System.Windows.Forms.ComboBox cbMagazynZamowienie;
        private System.Windows.Forms.Button btnPokaz;
        private System.Windows.Forms.Button btnPokazMag;
        private System.Windows.Forms.ComboBox cbMagazynMag;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Button btnPokazHand;
        private System.Windows.Forms.ComboBox cbMagazynyHan;
        private System.Windows.Forms.TextBox txtPudo;
        private System.Windows.Forms.CheckBox chPudo;
        private System.Windows.Forms.CheckBox chPallet;
        private System.Windows.Forms.Button btnFindPickup;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.TextBox txtDuty;
        private System.Windows.Forms.CheckBox chDuty;
        private System.Windows.Forms.CheckBox chDpdExpress;
        private System.Windows.Forms.Button btnCallCourier;
        private System.Windows.Forms.DataGridView dgMagazynowe;
    }
}

