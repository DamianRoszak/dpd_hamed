﻿using System;
using DPD.DpdApi;

namespace DPD.Forms
{
    public class PickupCallDto
    {
        public DateTime Date { get; set; }
        public DateTime TimeFrom { get; set; }
        public DateTime TimeTo { get; set; }
        public pickupCallOperationTypeDPPEnumV1 OperationType { get; set; }
        public string OrderName { get; set; }
        public int Checksum { get; set; }
        public pickupCallOrderTypeDPPEnumV1 OrderType { get; set; }
        public bool WaybillReady { get; set; }
        
        public string CustomerFullName { get; set; }
        public string CustomerName { get; set; }
        public string Phone { get; set; }
        public string PayerName { get; set; }
        public int PayerNumber { get; set; }
        public string PayerCostCenter { get; set; } = "";

        public string AddressName { get; set; }
        public string Address { get; set; }
        public string PostCode { get; set; }
        public string City { get; set; }
        public string AddressPhone { get; set; }
        
        public int PackagesDox { get; set; }
        public int Packages { get; set; }
        public int Pallets { get; set; }
    }
}