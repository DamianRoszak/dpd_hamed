﻿namespace DPD.Forms
{
    partial class Ustawienia2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Ustawienia2));
            this.lbKategorie = new System.Windows.Forms.ListBox();
            this.pnlGlowny = new System.Windows.Forms.Panel();
            this.lWersjaAplikacji = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.pnlUstawieniaPolDostawy = new System.Windows.Forms.Panel();
            this.cbAktywnyAdresDostawy = new System.Windows.Forms.CheckBox();
            this.pnlInne = new System.Windows.Forms.Panel();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.rbNazwaPelna = new System.Windows.Forms.RadioButton();
            this.rbNazwa = new System.Windows.Forms.RadioButton();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.rbWagaDomyslna = new System.Windows.Forms.RadioButton();
            this.rbWagaZMaga = new System.Windows.Forms.RadioButton();
            this.txtWaga = new System.Windows.Forms.TextBox();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.btnOpHan = new System.Windows.Forms.Button();
            this.btnOpMag = new System.Windows.Forms.Button();
            this.btnOpZam = new System.Windows.Forms.Button();
            this.pnlDaneDod = new System.Windows.Forms.Panel();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.chPole10 = new System.Windows.Forms.CheckBox();
            this.cbNrZamKlienta = new System.Windows.Forms.ComboBox();
            this.cbPole10 = new System.Windows.Forms.ComboBox();
            this.chNrZamKlienta = new System.Windows.Forms.CheckBox();
            this.cbWartoscBrutto = new System.Windows.Forms.ComboBox();
            this.chWartoscBrutto = new System.Windows.Forms.CheckBox();
            this.cbPole9 = new System.Windows.Forms.ComboBox();
            this.cbPole8 = new System.Windows.Forms.ComboBox();
            this.cbPole7 = new System.Windows.Forms.ComboBox();
            this.cbPole6 = new System.Windows.Forms.ComboBox();
            this.cbPole5 = new System.Windows.Forms.ComboBox();
            this.cbPole4 = new System.Windows.Forms.ComboBox();
            this.cbPole3 = new System.Windows.Forms.ComboBox();
            this.cbPole2 = new System.Windows.Forms.ComboBox();
            this.cbPole1 = new System.Windows.Forms.ComboBox();
            this.cbUwagi = new System.Windows.Forms.ComboBox();
            this.cbNrDok = new System.Windows.Forms.ComboBox();
            this.chPole2 = new System.Windows.Forms.CheckBox();
            this.chPole3 = new System.Windows.Forms.CheckBox();
            this.chPole4 = new System.Windows.Forms.CheckBox();
            this.chPole5 = new System.Windows.Forms.CheckBox();
            this.chPole6 = new System.Windows.Forms.CheckBox();
            this.chPole7 = new System.Windows.Forms.CheckBox();
            this.chPole8 = new System.Windows.Forms.CheckBox();
            this.chPole9 = new System.Windows.Forms.CheckBox();
            this.chPole1 = new System.Windows.Forms.CheckBox();
            this.chUwagi = new System.Windows.Forms.CheckBox();
            this.chNrDok = new System.Windows.Forms.CheckBox();
            this.pnlBlokujGenerowanieListu = new System.Windows.Forms.Panel();
            this.gbBlokujGenerowanieDokumentow = new System.Windows.Forms.GroupBox();
            this.btnPobierzFormy_Blokuj = new System.Windows.Forms.Button();
            this.clbBlokowanieFormDostaw = new System.Windows.Forms.CheckedListBox();
            this.pnlAdres = new System.Windows.Forms.Panel();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.txtImieNazwiskoNad = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.txtMailNad = new System.Windows.Forms.TextBox();
            this.chDomyslnyAdresNadawcy = new System.Windows.Forms.CheckBox();
            this.label25 = new System.Windows.Forms.Label();
            this.txtkrajNad = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.txtTelNad = new System.Windows.Forms.TextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.txtNazwaNad = new System.Windows.Forms.TextBox();
            this.label33 = new System.Windows.Forms.Label();
            this.txtUlicaNad = new System.Windows.Forms.TextBox();
            this.label34 = new System.Windows.Forms.Label();
            this.txtKodNad = new System.Windows.Forms.TextBox();
            this.txtMiejscNad = new System.Windows.Forms.TextBox();
            this.pnlGenerujZwrotDokumetnow = new System.Windows.Forms.Panel();
            this.gbGenerujZwrotDokumentow = new System.Windows.Forms.GroupBox();
            this.btnPobierzForm_Generuj = new System.Windows.Forms.Button();
            this.clbGenerujZwrotDokumetnow = new System.Windows.Forms.CheckedListBox();
            this.pnlDPD = new System.Windows.Forms.Panel();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.btnSprawdzDPD = new System.Windows.Forms.Button();
            this.chSprawdzanieStatusow = new System.Windows.Forms.CheckBox();
            this.btnUsunNumkat = new System.Windows.Forms.Button();
            this.dgNumkat = new System.Windows.Forms.DataGridView();
            this.btnDodajNumkat = new System.Windows.Forms.Button();
            this.txtNumkatNazwa = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtNumkatNumer = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtWSDL = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtHasloDPD = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtLoginDPD = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.pnlSzablon = new System.Windows.Forms.Panel();
            this.chWlaczSzablon = new System.Windows.Forms.CheckBox();
            this.grSzablon = new System.Windows.Forms.GroupBox();
            this.label35 = new System.Windows.Forms.Label();
            this.txtDuty = new System.Windows.Forms.TextBox();
            this.chDuty = new System.Windows.Forms.CheckBox();
            this.chDpdExpress = new System.Windows.Forms.CheckBox();
            this.btnFindPickup = new System.Windows.Forms.Button();
            this.chPallet = new System.Windows.Forms.CheckBox();
            this.txtPudo = new System.Windows.Forms.TextBox();
            this.chPudo = new System.Windows.Forms.CheckBox();
            this.chDeklWart = new System.Windows.Forms.CheckBox();
            this.chPobierzKwoteZWfMaga = new System.Windows.Forms.CheckBox();
            this.chOpony = new System.Windows.Forms.CheckBox();
            this.chTiresExport = new System.Windows.Forms.CheckBox();
            this.label28 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.txtPobranie = new System.Windows.Forms.TextBox();
            this.gbDeklarowanaWartosc = new System.Windows.Forms.GroupBox();
            this.txtDeklarowanaWartosc = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.chOdbiorWlasny = new System.Windows.Forms.CheckBox();
            this.chDorNaAdrPryw = new System.Windows.Forms.CheckBox();
            this.chPrzesList = new System.Windows.Forms.CheckBox();
            this.chDokZwrot = new System.Windows.Forms.CheckBox();
            this.chDorGwaran = new System.Windows.Forms.CheckBox();
            this.chPrzesZwrot = new System.Windows.Forms.CheckBox();
            this.chPobranie = new System.Windows.Forms.CheckBox();
            this.gbOdbiorWlasny = new System.Windows.Forms.GroupBox();
            this.rbOsoba = new System.Windows.Forms.RadioButton();
            this.rbFirma = new System.Windows.Forms.RadioButton();
            this.cbDoreczenie = new System.Windows.Forms.ComboBox();
            this.txtNaOkreslonaGodzine = new System.Windows.Forms.TextBox();
            this.chDorDoRakWl = new System.Windows.Forms.CheckBox();
            this.chPrzesylkaWartosciowaZWfMaga = new System.Windows.Forms.CheckBox();
            this.pnlPolaczenie = new System.Windows.Forms.Panel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lblInfo = new System.Windows.Forms.Label();
            this.btnTest = new System.Windows.Forms.Button();
            this.txtDb = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.cbUwierz = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtHasloDb = new System.Windows.Forms.TextBox();
            this.txtLoginDb = new System.Windows.Forms.TextBox();
            this.txtSerwer = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.pnlEP = new System.Windows.Forms.Panel();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.groupBox13 = new System.Windows.Forms.GroupBox();
            this.chAutoWydrukPoZapisie = new System.Windows.Forms.CheckBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.chEtykieciarka = new System.Windows.Forms.CheckBox();
            this.label13 = new System.Windows.Forms.Label();
            this.rbZpl = new System.Windows.Forms.RadioButton();
            this.rbEpl = new System.Windows.Forms.RadioButton();
            this.rbPdf = new System.Windows.Forms.RadioButton();
            this.cbDrukarkaEtykieta = new System.Windows.Forms.ComboBox();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this.cbDrukarkaProtokol = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnRaporty = new System.Windows.Forms.Button();
            this.txtRaporty = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.btnProtokol = new System.Windows.Forms.Button();
            this.btnEtykieta = new System.Windows.Forms.Button();
            this.txtProtokol = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtEtykieta = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.pnlPobranieCOD = new System.Windows.Forms.Panel();
            this.groupBox14 = new System.Windows.Forms.GroupBox();
            this.btnPobierzPlatnosci = new System.Windows.Forms.Button();
            this.clbPobranieCOD = new System.Windows.Forms.CheckedListBox();
            this.pnlZapis = new System.Windows.Forms.Panel();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.cbZmienStatus = new System.Windows.Forms.ComboBox();
            this.chZmienStatus = new System.Windows.Forms.CheckBox();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.chZamZapiszNrListuV2 = new System.Windows.Forms.CheckBox();
            this.chPoleDodZam = new System.Windows.Forms.CheckBox();
            this.cbZam = new System.Windows.Forms.ComboBox();
            this.chUwagiZam = new System.Windows.Forms.CheckBox();
            this.chInfDodZam = new System.Windows.Forms.CheckBox();
            this.chKomentarz = new System.Windows.Forms.CheckBox();
            this.grMag = new System.Windows.Forms.GroupBox();
            this.chPolDodMag = new System.Windows.Forms.CheckBox();
            this.cbMag = new System.Windows.Forms.ComboBox();
            this.chUwagiMag = new System.Windows.Forms.CheckBox();
            this.grHandl = new System.Windows.Forms.GroupBox();
            this.chPolDodHand = new System.Windows.Forms.CheckBox();
            this.cbHand = new System.Windows.Forms.ComboBox();
            this.chUwagiHand = new System.Windows.Forms.CheckBox();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.pnlZwrotDokumentow = new System.Windows.Forms.Panel();
            this.gbZwrotDokumentow = new System.Windows.Forms.GroupBox();
            this.checkedListBox1 = new System.Windows.Forms.CheckedListBox();
            this.pnlGlowny.SuspendLayout();
            this.pnlUstawieniaPolDostawy.SuspendLayout();
            this.pnlInne.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox11.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.pnlDaneDod.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.pnlBlokujGenerowanieListu.SuspendLayout();
            this.gbBlokujGenerowanieDokumentow.SuspendLayout();
            this.pnlAdres.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.pnlGenerujZwrotDokumetnow.SuspendLayout();
            this.gbGenerujZwrotDokumentow.SuspendLayout();
            this.pnlDPD.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgNumkat)).BeginInit();
            this.pnlSzablon.SuspendLayout();
            this.grSzablon.SuspendLayout();
            this.gbDeklarowanaWartosc.SuspendLayout();
            this.gbOdbiorWlasny.SuspendLayout();
            this.pnlPolaczenie.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.pnlEP.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox13.SuspendLayout();
            this.groupBox12.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.pnlPobranieCOD.SuspendLayout();
            this.groupBox14.SuspendLayout();
            this.pnlZapis.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.grMag.SuspendLayout();
            this.grHandl.SuspendLayout();
            this.pnlZwrotDokumentow.SuspendLayout();
            this.gbZwrotDokumentow.SuspendLayout();
            this.SuspendLayout();
            // 
            // lbKategorie
            // 
            this.lbKategorie.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.lbKategorie.FormattingEnabled = true;
            this.lbKategorie.Items.AddRange(new object[] {
            "Połączenie z Wf-Magiem",
            "Etykiety i protokoły",
            "Dane konta webserwisu DPD",
            "Adres nadawcy",
            "Miejsce zapisu nr listu",
            "Dane dodatkowe z Wf-Maga",
            "Szablon przesyłki",
            "Ustawienia pobierania danych",
            "Inne",
            "Pobranie C.O.D.",
            "Blokuj generowanie listu",
            "Generuj zwrot dokumentow"});
            this.lbKategorie.Location = new System.Drawing.Point(0, 0);
            this.lbKategorie.Name = "lbKategorie";
            this.lbKategorie.Size = new System.Drawing.Size(159, 394);
            this.lbKategorie.TabIndex = 0;
            this.lbKategorie.SelectedIndexChanged += new System.EventHandler(this.lbKategorie_SelectedIndexChanged);
            // 
            // pnlGlowny
            // 
            this.pnlGlowny.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.pnlGlowny.Controls.Add(this.lWersjaAplikacji);
            this.pnlGlowny.Controls.Add(this.button1);
            this.pnlGlowny.Controls.Add(this.button2);
            this.pnlGlowny.Controls.Add(this.pnlSzablon);
            this.pnlGlowny.Controls.Add(this.pnlPolaczenie);
            this.pnlGlowny.Controls.Add(this.pnlEP);
            this.pnlGlowny.Controls.Add(this.pnlPobranieCOD);
            this.pnlGlowny.Controls.Add(this.pnlZapis);
            this.pnlGlowny.Controls.Add(this.pnlUstawieniaPolDostawy);
            this.pnlGlowny.Controls.Add(this.pnlInne);
            this.pnlGlowny.Controls.Add(this.pnlDaneDod);
            this.pnlGlowny.Controls.Add(this.pnlBlokujGenerowanieListu);
            this.pnlGlowny.Controls.Add(this.pnlAdres);
            this.pnlGlowny.Controls.Add(this.pnlGenerujZwrotDokumetnow);
            this.pnlGlowny.Controls.Add(this.pnlDPD);
            this.pnlGlowny.Location = new System.Drawing.Point(159, 0);
            this.pnlGlowny.Name = "pnlGlowny";
            this.pnlGlowny.Size = new System.Drawing.Size(369, 392);
            this.pnlGlowny.TabIndex = 1;
            // 
            // lWersjaAplikacji
            // 
            this.lWersjaAplikacji.AutoSize = true;
            this.lWersjaAplikacji.Location = new System.Drawing.Point(6, 371);
            this.lWersjaAplikacji.Name = "lWersjaAplikacji";
            this.lWersjaAplikacji.Size = new System.Drawing.Size(13, 13);
            this.lWersjaAplikacji.TabIndex = 53;
            this.lWersjaAplikacji.Text = "--";
            // 
            // button1
            // 
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Location = new System.Drawing.Point(249, 366);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(114, 23);
            this.button1.TabIndex = 47;
            this.button1.Text = "Zapisz ustawienia";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Location = new System.Drawing.Point(127, 366);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(114, 23);
            this.button2.TabIndex = 48;
            this.button2.Text = "config";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Visible = false;
            this.button2.Click += new System.EventHandler(this.button2_Click_1);
            // 
            // pnlUstawieniaPolDostawy
            // 
            this.pnlUstawieniaPolDostawy.Controls.Add(this.cbAktywnyAdresDostawy);
            this.pnlUstawieniaPolDostawy.Location = new System.Drawing.Point(1, 1);
            this.pnlUstawieniaPolDostawy.Name = "pnlUstawieniaPolDostawy";
            this.pnlUstawieniaPolDostawy.Size = new System.Drawing.Size(365, 359);
            this.pnlUstawieniaPolDostawy.TabIndex = 52;
            // 
            // cbAktywnyAdresDostawy
            // 
            this.cbAktywnyAdresDostawy.AutoSize = true;
            this.cbAktywnyAdresDostawy.Location = new System.Drawing.Point(12, 12);
            this.cbAktywnyAdresDostawy.Name = "cbAktywnyAdresDostawy";
            this.cbAktywnyAdresDostawy.Size = new System.Drawing.Size(303, 17);
            this.cbAktywnyAdresDostawy.TabIndex = 0;
            this.cbAktywnyAdresDostawy.Text = "Aktywuj modyfikację adresu dostawy (dokument handlowy)";
            this.cbAktywnyAdresDostawy.UseVisualStyleBackColor = true;
            // 
            // pnlInne
            // 
            this.pnlInne.Controls.Add(this.groupBox4);
            this.pnlInne.Controls.Add(this.groupBox11);
            this.pnlInne.Controls.Add(this.groupBox10);
            this.pnlInne.Location = new System.Drawing.Point(5, 5);
            this.pnlInne.Name = "pnlInne";
            this.pnlInne.Size = new System.Drawing.Size(364, 355);
            this.pnlInne.TabIndex = 42;
            // 
            // groupBox4
            // 
            this.groupBox4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox4.Controls.Add(this.rbNazwaPelna);
            this.groupBox4.Controls.Add(this.rbNazwa);
            this.groupBox4.Location = new System.Drawing.Point(8, 224);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(332, 66);
            this.groupBox4.TabIndex = 51;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Nazwa kontrahenta";
            // 
            // rbNazwaPelna
            // 
            this.rbNazwaPelna.AutoSize = true;
            this.rbNazwaPelna.Location = new System.Drawing.Point(17, 41);
            this.rbNazwaPelna.Name = "rbNazwaPelna";
            this.rbNazwaPelna.Size = new System.Drawing.Size(159, 17);
            this.rbNazwaPelna.TabIndex = 2;
            this.rbNazwaPelna.Text = "pobieraj z pola Pełna nazwa";
            this.rbNazwaPelna.UseVisualStyleBackColor = true;
            // 
            // rbNazwa
            // 
            this.rbNazwa.AutoSize = true;
            this.rbNazwa.Checked = true;
            this.rbNazwa.Location = new System.Drawing.Point(17, 20);
            this.rbNazwa.Name = "rbNazwa";
            this.rbNazwa.Size = new System.Drawing.Size(189, 17);
            this.rbNazwa.TabIndex = 1;
            this.rbNazwa.TabStop = true;
            this.rbNazwa.Text = "pobieraj z pola Nazwa kontrahenta";
            this.rbNazwa.UseVisualStyleBackColor = true;
            // 
            // groupBox11
            // 
            this.groupBox11.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox11.Controls.Add(this.rbWagaDomyslna);
            this.groupBox11.Controls.Add(this.rbWagaZMaga);
            this.groupBox11.Controls.Add(this.txtWaga);
            this.groupBox11.Location = new System.Drawing.Point(8, 156);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(332, 66);
            this.groupBox11.TabIndex = 50;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "Waga domyślna dla pierwszej paczki";
            // 
            // rbWagaDomyslna
            // 
            this.rbWagaDomyslna.AutoSize = true;
            this.rbWagaDomyslna.Location = new System.Drawing.Point(17, 43);
            this.rbWagaDomyslna.Name = "rbWagaDomyslna";
            this.rbWagaDomyslna.Size = new System.Drawing.Size(84, 17);
            this.rbWagaDomyslna.TabIndex = 2;
            this.rbWagaDomyslna.Text = "wskaż wagę";
            this.rbWagaDomyslna.UseVisualStyleBackColor = true;
            // 
            // rbWagaZMaga
            // 
            this.rbWagaZMaga.AutoSize = true;
            this.rbWagaZMaga.Checked = true;
            this.rbWagaZMaga.Location = new System.Drawing.Point(17, 20);
            this.rbWagaZMaga.Name = "rbWagaZMaga";
            this.rbWagaZMaga.Size = new System.Drawing.Size(120, 17);
            this.rbWagaZMaga.TabIndex = 1;
            this.rbWagaZMaga.TabStop = true;
            this.rbWagaZMaga.Text = "pobieraj z WF-Maga";
            this.rbWagaZMaga.UseVisualStyleBackColor = true;
            // 
            // txtWaga
            // 
            this.txtWaga.Location = new System.Drawing.Point(110, 42);
            this.txtWaga.Name = "txtWaga";
            this.txtWaga.Size = new System.Drawing.Size(62, 20);
            this.txtWaga.TabIndex = 0;
            this.txtWaga.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtWaga_KeyPress);
            // 
            // groupBox10
            // 
            this.groupBox10.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox10.Controls.Add(this.btnOpHan);
            this.groupBox10.Controls.Add(this.btnOpMag);
            this.groupBox10.Controls.Add(this.btnOpZam);
            this.groupBox10.Location = new System.Drawing.Point(8, 8);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(332, 147);
            this.groupBox10.TabIndex = 49;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "Operacje dodatkowe w Wf-Magu";
            // 
            // btnOpHan
            // 
            this.btnOpHan.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnOpHan.Location = new System.Drawing.Point(27, 97);
            this.btnOpHan.Name = "btnOpHan";
            this.btnOpHan.Size = new System.Drawing.Size(278, 23);
            this.btnOpHan.TabIndex = 50;
            this.btnOpHan.Text = "Dodaj operację do dokumentów handlowych";
            this.btnOpHan.UseVisualStyleBackColor = true;
            this.btnOpHan.Click += new System.EventHandler(this.CreateOpDod);
            // 
            // btnOpMag
            // 
            this.btnOpMag.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnOpMag.Location = new System.Drawing.Point(27, 65);
            this.btnOpMag.Name = "btnOpMag";
            this.btnOpMag.Size = new System.Drawing.Size(278, 23);
            this.btnOpMag.TabIndex = 49;
            this.btnOpMag.Text = "Dodaj operację do dokumentów magazynowych";
            this.btnOpMag.UseVisualStyleBackColor = true;
            this.btnOpMag.Click += new System.EventHandler(this.CreateOpDod);
            // 
            // btnOpZam
            // 
            this.btnOpZam.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnOpZam.Location = new System.Drawing.Point(27, 33);
            this.btnOpZam.Name = "btnOpZam";
            this.btnOpZam.Size = new System.Drawing.Size(278, 23);
            this.btnOpZam.TabIndex = 48;
            this.btnOpZam.Text = "Dodaj operację do zamówień";
            this.btnOpZam.UseVisualStyleBackColor = true;
            this.btnOpZam.Click += new System.EventHandler(this.CreateOpDod);
            // 
            // pnlDaneDod
            // 
            this.pnlDaneDod.Controls.Add(this.groupBox6);
            this.pnlDaneDod.Location = new System.Drawing.Point(5, 5);
            this.pnlDaneDod.Name = "pnlDaneDod";
            this.pnlDaneDod.Size = new System.Drawing.Size(364, 355);
            this.pnlDaneDod.TabIndex = 5;
            // 
            // groupBox6
            // 
            this.groupBox6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox6.Controls.Add(this.chPole10);
            this.groupBox6.Controls.Add(this.cbNrZamKlienta);
            this.groupBox6.Controls.Add(this.cbPole10);
            this.groupBox6.Controls.Add(this.chNrZamKlienta);
            this.groupBox6.Controls.Add(this.cbWartoscBrutto);
            this.groupBox6.Controls.Add(this.chWartoscBrutto);
            this.groupBox6.Controls.Add(this.cbPole9);
            this.groupBox6.Controls.Add(this.cbPole8);
            this.groupBox6.Controls.Add(this.cbPole7);
            this.groupBox6.Controls.Add(this.cbPole6);
            this.groupBox6.Controls.Add(this.cbPole5);
            this.groupBox6.Controls.Add(this.cbPole4);
            this.groupBox6.Controls.Add(this.cbPole3);
            this.groupBox6.Controls.Add(this.cbPole2);
            this.groupBox6.Controls.Add(this.cbPole1);
            this.groupBox6.Controls.Add(this.cbUwagi);
            this.groupBox6.Controls.Add(this.cbNrDok);
            this.groupBox6.Controls.Add(this.chPole2);
            this.groupBox6.Controls.Add(this.chPole3);
            this.groupBox6.Controls.Add(this.chPole4);
            this.groupBox6.Controls.Add(this.chPole5);
            this.groupBox6.Controls.Add(this.chPole6);
            this.groupBox6.Controls.Add(this.chPole7);
            this.groupBox6.Controls.Add(this.chPole8);
            this.groupBox6.Controls.Add(this.chPole9);
            this.groupBox6.Controls.Add(this.chPole1);
            this.groupBox6.Controls.Add(this.chUwagi);
            this.groupBox6.Controls.Add(this.chNrDok);
            this.groupBox6.Location = new System.Drawing.Point(8, 8);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(332, 347);
            this.groupBox6.TabIndex = 37;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Pobranie dodatkowych pól z Wf-Maga";
            // 
            // chPole10
            // 
            this.chPole10.AutoSize = true;
            this.chPole10.Location = new System.Drawing.Point(42, 316);
            this.chPole10.Name = "chPole10";
            this.chPole10.Size = new System.Drawing.Size(59, 17);
            this.chPole10.TabIndex = 195;
            this.chPole10.Text = "Pole10";
            this.chPole10.UseVisualStyleBackColor = true;
            // 
            // cbNrZamKlienta
            // 
            this.cbNrZamKlienta.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbNrZamKlienta.FormattingEnabled = true;
            this.cbNrZamKlienta.Location = new System.Drawing.Point(199, 50);
            this.cbNrZamKlienta.Name = "cbNrZamKlienta";
            this.cbNrZamKlienta.Size = new System.Drawing.Size(121, 21);
            this.cbNrZamKlienta.TabIndex = 211;
            // 
            // cbPole10
            // 
            this.cbPole10.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbPole10.FormattingEnabled = true;
            this.cbPole10.Location = new System.Drawing.Point(199, 315);
            this.cbPole10.Name = "cbPole10";
            this.cbPole10.Size = new System.Drawing.Size(121, 21);
            this.cbPole10.TabIndex = 207;
            // 
            // chNrZamKlienta
            // 
            this.chNrZamKlienta.AutoSize = true;
            this.chNrZamKlienta.Location = new System.Drawing.Point(42, 52);
            this.chNrZamKlienta.Name = "chNrZamKlienta";
            this.chNrZamKlienta.Size = new System.Drawing.Size(149, 17);
            this.chNrZamKlienta.TabIndex = 210;
            this.chNrZamKlienta.Text = "Numer zamówienia klienta";
            this.chNrZamKlienta.UseVisualStyleBackColor = true;
            // 
            // cbWartoscBrutto
            // 
            this.cbWartoscBrutto.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbWartoscBrutto.Enabled = false;
            this.cbWartoscBrutto.FormattingEnabled = true;
            this.cbWartoscBrutto.Location = new System.Drawing.Point(199, 94);
            this.cbWartoscBrutto.Name = "cbWartoscBrutto";
            this.cbWartoscBrutto.Size = new System.Drawing.Size(121, 21);
            this.cbWartoscBrutto.TabIndex = 209;
            // 
            // chWartoscBrutto
            // 
            this.chWartoscBrutto.AutoSize = true;
            this.chWartoscBrutto.Location = new System.Drawing.Point(42, 95);
            this.chWartoscBrutto.Name = "chWartoscBrutto";
            this.chWartoscBrutto.Size = new System.Drawing.Size(152, 17);
            this.chWartoscBrutto.TabIndex = 208;
            this.chWartoscBrutto.Text = "Wartość brutto dokumentu";
            this.chWartoscBrutto.UseVisualStyleBackColor = true;
            // 
            // cbPole9
            // 
            this.cbPole9.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbPole9.FormattingEnabled = true;
            this.cbPole9.Location = new System.Drawing.Point(199, 292);
            this.cbPole9.Name = "cbPole9";
            this.cbPole9.Size = new System.Drawing.Size(121, 21);
            this.cbPole9.TabIndex = 206;
            // 
            // cbPole8
            // 
            this.cbPole8.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbPole8.FormattingEnabled = true;
            this.cbPole8.Location = new System.Drawing.Point(199, 270);
            this.cbPole8.Name = "cbPole8";
            this.cbPole8.Size = new System.Drawing.Size(121, 21);
            this.cbPole8.TabIndex = 205;
            // 
            // cbPole7
            // 
            this.cbPole7.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbPole7.FormattingEnabled = true;
            this.cbPole7.Location = new System.Drawing.Point(199, 248);
            this.cbPole7.Name = "cbPole7";
            this.cbPole7.Size = new System.Drawing.Size(121, 21);
            this.cbPole7.TabIndex = 204;
            // 
            // cbPole6
            // 
            this.cbPole6.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbPole6.FormattingEnabled = true;
            this.cbPole6.Location = new System.Drawing.Point(199, 226);
            this.cbPole6.Name = "cbPole6";
            this.cbPole6.Size = new System.Drawing.Size(121, 21);
            this.cbPole6.TabIndex = 203;
            // 
            // cbPole5
            // 
            this.cbPole5.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbPole5.FormattingEnabled = true;
            this.cbPole5.Location = new System.Drawing.Point(199, 204);
            this.cbPole5.Name = "cbPole5";
            this.cbPole5.Size = new System.Drawing.Size(121, 21);
            this.cbPole5.TabIndex = 202;
            // 
            // cbPole4
            // 
            this.cbPole4.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbPole4.FormattingEnabled = true;
            this.cbPole4.Location = new System.Drawing.Point(199, 182);
            this.cbPole4.Name = "cbPole4";
            this.cbPole4.Size = new System.Drawing.Size(121, 21);
            this.cbPole4.TabIndex = 201;
            // 
            // cbPole3
            // 
            this.cbPole3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbPole3.FormattingEnabled = true;
            this.cbPole3.Location = new System.Drawing.Point(199, 160);
            this.cbPole3.Name = "cbPole3";
            this.cbPole3.Size = new System.Drawing.Size(121, 21);
            this.cbPole3.TabIndex = 200;
            // 
            // cbPole2
            // 
            this.cbPole2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbPole2.FormattingEnabled = true;
            this.cbPole2.Location = new System.Drawing.Point(199, 138);
            this.cbPole2.Name = "cbPole2";
            this.cbPole2.Size = new System.Drawing.Size(121, 21);
            this.cbPole2.TabIndex = 199;
            // 
            // cbPole1
            // 
            this.cbPole1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbPole1.FormattingEnabled = true;
            this.cbPole1.Location = new System.Drawing.Point(199, 116);
            this.cbPole1.Name = "cbPole1";
            this.cbPole1.Size = new System.Drawing.Size(121, 21);
            this.cbPole1.TabIndex = 198;
            // 
            // cbUwagi
            // 
            this.cbUwagi.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbUwagi.FormattingEnabled = true;
            this.cbUwagi.Location = new System.Drawing.Point(199, 72);
            this.cbUwagi.Name = "cbUwagi";
            this.cbUwagi.Size = new System.Drawing.Size(121, 21);
            this.cbUwagi.TabIndex = 197;
            // 
            // cbNrDok
            // 
            this.cbNrDok.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbNrDok.FormattingEnabled = true;
            this.cbNrDok.Location = new System.Drawing.Point(199, 28);
            this.cbNrDok.Name = "cbNrDok";
            this.cbNrDok.Size = new System.Drawing.Size(121, 21);
            this.cbNrDok.TabIndex = 196;
            // 
            // chPole2
            // 
            this.chPole2.AutoSize = true;
            this.chPole2.Location = new System.Drawing.Point(42, 139);
            this.chPole2.Name = "chPole2";
            this.chPole2.Size = new System.Drawing.Size(53, 17);
            this.chPole2.TabIndex = 194;
            this.chPole2.Text = "Pole2";
            this.chPole2.UseVisualStyleBackColor = true;
            // 
            // chPole3
            // 
            this.chPole3.AutoSize = true;
            this.chPole3.Location = new System.Drawing.Point(42, 161);
            this.chPole3.Name = "chPole3";
            this.chPole3.Size = new System.Drawing.Size(53, 17);
            this.chPole3.TabIndex = 193;
            this.chPole3.Text = "Pole3";
            this.chPole3.UseVisualStyleBackColor = true;
            // 
            // chPole4
            // 
            this.chPole4.AutoSize = true;
            this.chPole4.Location = new System.Drawing.Point(42, 183);
            this.chPole4.Name = "chPole4";
            this.chPole4.Size = new System.Drawing.Size(53, 17);
            this.chPole4.TabIndex = 192;
            this.chPole4.Text = "Pole4";
            this.chPole4.UseVisualStyleBackColor = true;
            // 
            // chPole5
            // 
            this.chPole5.AutoSize = true;
            this.chPole5.Location = new System.Drawing.Point(42, 205);
            this.chPole5.Name = "chPole5";
            this.chPole5.Size = new System.Drawing.Size(53, 17);
            this.chPole5.TabIndex = 191;
            this.chPole5.Text = "Pole5";
            this.chPole5.UseVisualStyleBackColor = true;
            // 
            // chPole6
            // 
            this.chPole6.AutoSize = true;
            this.chPole6.Location = new System.Drawing.Point(42, 227);
            this.chPole6.Name = "chPole6";
            this.chPole6.Size = new System.Drawing.Size(53, 17);
            this.chPole6.TabIndex = 190;
            this.chPole6.Text = "Pole6";
            this.chPole6.UseVisualStyleBackColor = true;
            // 
            // chPole7
            // 
            this.chPole7.AutoSize = true;
            this.chPole7.Location = new System.Drawing.Point(42, 249);
            this.chPole7.Name = "chPole7";
            this.chPole7.Size = new System.Drawing.Size(53, 17);
            this.chPole7.TabIndex = 189;
            this.chPole7.Text = "Pole7";
            this.chPole7.UseVisualStyleBackColor = true;
            // 
            // chPole8
            // 
            this.chPole8.AutoSize = true;
            this.chPole8.Location = new System.Drawing.Point(42, 271);
            this.chPole8.Name = "chPole8";
            this.chPole8.Size = new System.Drawing.Size(53, 17);
            this.chPole8.TabIndex = 188;
            this.chPole8.Text = "Pole8";
            this.chPole8.UseVisualStyleBackColor = true;
            // 
            // chPole9
            // 
            this.chPole9.AutoSize = true;
            this.chPole9.Location = new System.Drawing.Point(42, 293);
            this.chPole9.Name = "chPole9";
            this.chPole9.Size = new System.Drawing.Size(53, 17);
            this.chPole9.TabIndex = 187;
            this.chPole9.Text = "Pole9";
            this.chPole9.UseVisualStyleBackColor = true;
            // 
            // chPole1
            // 
            this.chPole1.AutoSize = true;
            this.chPole1.Location = new System.Drawing.Point(42, 117);
            this.chPole1.Name = "chPole1";
            this.chPole1.Size = new System.Drawing.Size(53, 17);
            this.chPole1.TabIndex = 186;
            this.chPole1.Text = "Pole1";
            this.chPole1.UseVisualStyleBackColor = true;
            // 
            // chUwagi
            // 
            this.chUwagi.AutoSize = true;
            this.chUwagi.Location = new System.Drawing.Point(42, 74);
            this.chUwagi.Name = "chUwagi";
            this.chUwagi.Size = new System.Drawing.Size(56, 17);
            this.chUwagi.TabIndex = 185;
            this.chUwagi.Text = "Uwagi";
            this.chUwagi.UseVisualStyleBackColor = true;
            // 
            // chNrDok
            // 
            this.chNrDok.AutoSize = true;
            this.chNrDok.Location = new System.Drawing.Point(42, 30);
            this.chNrDok.Name = "chNrDok";
            this.chNrDok.Size = new System.Drawing.Size(113, 17);
            this.chNrDok.TabIndex = 184;
            this.chNrDok.Text = "Numer dokumentu";
            this.chNrDok.UseVisualStyleBackColor = true;
            // 
            // pnlBlokujGenerowanieListu
            // 
            this.pnlBlokujGenerowanieListu.Controls.Add(this.gbBlokujGenerowanieDokumentow);
            this.pnlBlokujGenerowanieListu.Location = new System.Drawing.Point(0, 5);
            this.pnlBlokujGenerowanieListu.Name = "pnlBlokujGenerowanieListu";
            this.pnlBlokujGenerowanieListu.Size = new System.Drawing.Size(365, 354);
            this.pnlBlokujGenerowanieListu.TabIndex = 1;
            // 
            // gbBlokujGenerowanieDokumentow
            // 
            this.gbBlokujGenerowanieDokumentow.Controls.Add(this.btnPobierzFormy_Blokuj);
            this.gbBlokujGenerowanieDokumentow.Controls.Add(this.clbBlokowanieFormDostaw);
            this.gbBlokujGenerowanieDokumentow.Location = new System.Drawing.Point(7, 6);
            this.gbBlokujGenerowanieDokumentow.Name = "gbBlokujGenerowanieDokumentow";
            this.gbBlokujGenerowanieDokumentow.Size = new System.Drawing.Size(352, 227);
            this.gbBlokujGenerowanieDokumentow.TabIndex = 0;
            this.gbBlokujGenerowanieDokumentow.TabStop = false;
            this.gbBlokujGenerowanieDokumentow.Text = "Blokuj Generowanie Dokumentów dla form dostaw:";
            // 
            // btnPobierzFormy_Blokuj
            // 
            this.btnPobierzFormy_Blokuj.Location = new System.Drawing.Point(201, 178);
            this.btnPobierzFormy_Blokuj.Name = "btnPobierzFormy_Blokuj";
            this.btnPobierzFormy_Blokuj.Size = new System.Drawing.Size(139, 23);
            this.btnPobierzFormy_Blokuj.TabIndex = 1;
            this.btnPobierzFormy_Blokuj.Text = "PobierzFormyDostaw";
            this.btnPobierzFormy_Blokuj.UseVisualStyleBackColor = true;
            this.btnPobierzFormy_Blokuj.Click += new System.EventHandler(this.PobierzWszystkieFormyDostawy);
            // 
            // clbBlokowanieFormDostaw
            // 
            this.clbBlokowanieFormDostaw.FormattingEnabled = true;
            this.clbBlokowanieFormDostaw.Location = new System.Drawing.Point(9, 16);
            this.clbBlokowanieFormDostaw.Name = "clbBlokowanieFormDostaw";
            this.clbBlokowanieFormDostaw.Size = new System.Drawing.Size(331, 154);
            this.clbBlokowanieFormDostaw.TabIndex = 0;
            // 
            // pnlAdres
            // 
            this.pnlAdres.Controls.Add(this.groupBox8);
            this.pnlAdres.Location = new System.Drawing.Point(5, 5);
            this.pnlAdres.Name = "pnlAdres";
            this.pnlAdres.Size = new System.Drawing.Size(361, 355);
            this.pnlAdres.TabIndex = 3;
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.txtImieNazwiskoNad);
            this.groupBox8.Controls.Add(this.label18);
            this.groupBox8.Controls.Add(this.label12);
            this.groupBox8.Controls.Add(this.txtMailNad);
            this.groupBox8.Controls.Add(this.chDomyslnyAdresNadawcy);
            this.groupBox8.Controls.Add(this.label25);
            this.groupBox8.Controls.Add(this.txtkrajNad);
            this.groupBox8.Controls.Add(this.label17);
            this.groupBox8.Controls.Add(this.txtTelNad);
            this.groupBox8.Controls.Add(this.label31);
            this.groupBox8.Controls.Add(this.label32);
            this.groupBox8.Controls.Add(this.txtNazwaNad);
            this.groupBox8.Controls.Add(this.label33);
            this.groupBox8.Controls.Add(this.txtUlicaNad);
            this.groupBox8.Controls.Add(this.label34);
            this.groupBox8.Controls.Add(this.txtKodNad);
            this.groupBox8.Controls.Add(this.txtMiejscNad);
            this.groupBox8.Location = new System.Drawing.Point(8, 8);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(239, 202);
            this.groupBox8.TabIndex = 178;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Adres nadawcy";
            // 
            // txtImieNazwiskoNad
            // 
            this.txtImieNazwiskoNad.ForeColor = System.Drawing.SystemColors.MenuText;
            this.txtImieNazwiskoNad.Location = new System.Drawing.Point(86, 59);
            this.txtImieNazwiskoNad.Name = "txtImieNazwiskoNad";
            this.txtImieNazwiskoNad.Size = new System.Drawing.Size(140, 20);
            this.txtImieNazwiskoNad.TabIndex = 32;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.Color.Transparent;
            this.label18.ForeColor = System.Drawing.SystemColors.MenuText;
            this.label18.Location = new System.Drawing.Point(2, 63);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(78, 13);
            this.label18.TabIndex = 181;
            this.label18.Text = "Imię i nazwisko";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.ForeColor = System.Drawing.SystemColors.MenuText;
            this.label12.Location = new System.Drawing.Point(45, 172);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(35, 13);
            this.label12.TabIndex = 179;
            this.label12.Text = "E-mail";
            // 
            // txtMailNad
            // 
            this.txtMailNad.ForeColor = System.Drawing.SystemColors.MenuText;
            this.txtMailNad.Location = new System.Drawing.Point(86, 169);
            this.txtMailNad.Name = "txtMailNad";
            this.txtMailNad.Size = new System.Drawing.Size(140, 20);
            this.txtMailNad.TabIndex = 38;
            // 
            // chDomyslnyAdresNadawcy
            // 
            this.chDomyslnyAdresNadawcy.AutoSize = true;
            this.chDomyslnyAdresNadawcy.Location = new System.Drawing.Point(82, 19);
            this.chDomyslnyAdresNadawcy.Name = "chDomyslnyAdresNadawcy";
            this.chDomyslnyAdresNadawcy.Size = new System.Drawing.Size(146, 17);
            this.chDomyslnyAdresNadawcy.TabIndex = 30;
            this.chDomyslnyAdresNadawcy.Text = "Domyślny adres nadawcy";
            this.chDomyslnyAdresNadawcy.UseVisualStyleBackColor = true;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.BackColor = System.Drawing.Color.Transparent;
            this.label25.ForeColor = System.Drawing.SystemColors.MenuText;
            this.label25.Location = new System.Drawing.Point(162, 106);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(25, 13);
            this.label25.TabIndex = 176;
            this.label25.Text = "Kraj";
            // 
            // txtkrajNad
            // 
            this.txtkrajNad.ForeColor = System.Drawing.SystemColors.MenuText;
            this.txtkrajNad.Location = new System.Drawing.Point(191, 103);
            this.txtkrajNad.Name = "txtkrajNad";
            this.txtkrajNad.Size = new System.Drawing.Size(35, 20);
            this.txtkrajNad.TabIndex = 35;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.Color.Transparent;
            this.label17.ForeColor = System.Drawing.SystemColors.MenuText;
            this.label17.Location = new System.Drawing.Point(37, 150);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(43, 13);
            this.label17.TabIndex = 174;
            this.label17.Text = "Telefon";
            // 
            // txtTelNad
            // 
            this.txtTelNad.ForeColor = System.Drawing.SystemColors.MenuText;
            this.txtTelNad.Location = new System.Drawing.Point(86, 147);
            this.txtTelNad.Name = "txtTelNad";
            this.txtTelNad.Size = new System.Drawing.Size(140, 20);
            this.txtTelNad.TabIndex = 37;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.BackColor = System.Drawing.Color.Transparent;
            this.label31.ForeColor = System.Drawing.SystemColors.MenuText;
            this.label31.Location = new System.Drawing.Point(40, 41);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(40, 13);
            this.label31.TabIndex = 166;
            this.label31.Text = "Nazwa";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.BackColor = System.Drawing.Color.Transparent;
            this.label32.ForeColor = System.Drawing.SystemColors.MenuText;
            this.label32.Location = new System.Drawing.Point(6, 106);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(74, 13);
            this.label32.TabIndex = 172;
            this.label32.Text = "Kod pocztowy";
            // 
            // txtNazwaNad
            // 
            this.txtNazwaNad.ForeColor = System.Drawing.SystemColors.MenuText;
            this.txtNazwaNad.Location = new System.Drawing.Point(86, 37);
            this.txtNazwaNad.Name = "txtNazwaNad";
            this.txtNazwaNad.Size = new System.Drawing.Size(140, 20);
            this.txtNazwaNad.TabIndex = 31;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.BackColor = System.Drawing.Color.Transparent;
            this.label33.ForeColor = System.Drawing.SystemColors.MenuText;
            this.label33.Location = new System.Drawing.Point(12, 129);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(68, 13);
            this.label33.TabIndex = 171;
            this.label33.Text = "Miejscowość";
            // 
            // txtUlicaNad
            // 
            this.txtUlicaNad.ForeColor = System.Drawing.SystemColors.MenuText;
            this.txtUlicaNad.Location = new System.Drawing.Point(86, 81);
            this.txtUlicaNad.Name = "txtUlicaNad";
            this.txtUlicaNad.Size = new System.Drawing.Size(140, 20);
            this.txtUlicaNad.TabIndex = 33;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.BackColor = System.Drawing.Color.Transparent;
            this.label34.ForeColor = System.Drawing.SystemColors.MenuText;
            this.label34.Location = new System.Drawing.Point(49, 85);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(31, 13);
            this.label34.TabIndex = 170;
            this.label34.Text = "Ulica";
            // 
            // txtKodNad
            // 
            this.txtKodNad.ForeColor = System.Drawing.SystemColors.MenuText;
            this.txtKodNad.Location = new System.Drawing.Point(86, 103);
            this.txtKodNad.Name = "txtKodNad";
            this.txtKodNad.Size = new System.Drawing.Size(50, 20);
            this.txtKodNad.TabIndex = 34;
            // 
            // txtMiejscNad
            // 
            this.txtMiejscNad.ForeColor = System.Drawing.SystemColors.MenuText;
            this.txtMiejscNad.Location = new System.Drawing.Point(86, 125);
            this.txtMiejscNad.Name = "txtMiejscNad";
            this.txtMiejscNad.Size = new System.Drawing.Size(140, 20);
            this.txtMiejscNad.TabIndex = 36;
            // 
            // pnlGenerujZwrotDokumetnow
            // 
            this.pnlGenerujZwrotDokumetnow.Controls.Add(this.gbGenerujZwrotDokumentow);
            this.pnlGenerujZwrotDokumetnow.Location = new System.Drawing.Point(1, -3);
            this.pnlGenerujZwrotDokumetnow.Name = "pnlGenerujZwrotDokumetnow";
            this.pnlGenerujZwrotDokumetnow.Size = new System.Drawing.Size(368, 355);
            this.pnlGenerujZwrotDokumetnow.TabIndex = 1;
            // 
            // gbGenerujZwrotDokumentow
            // 
            this.gbGenerujZwrotDokumentow.Controls.Add(this.btnPobierzForm_Generuj);
            this.gbGenerujZwrotDokumentow.Controls.Add(this.clbGenerujZwrotDokumetnow);
            this.gbGenerujZwrotDokumentow.Location = new System.Drawing.Point(7, 6);
            this.gbGenerujZwrotDokumentow.Name = "gbGenerujZwrotDokumentow";
            this.gbGenerujZwrotDokumentow.Size = new System.Drawing.Size(352, 227);
            this.gbGenerujZwrotDokumentow.TabIndex = 0;
            this.gbGenerujZwrotDokumentow.TabStop = false;
            this.gbGenerujZwrotDokumentow.Text = "Generuj Zwrot Dokumentów dla form dostaw:";
            // 
            // btnPobierzForm_Generuj
            // 
            this.btnPobierzForm_Generuj.Location = new System.Drawing.Point(208, 196);
            this.btnPobierzForm_Generuj.Name = "btnPobierzForm_Generuj";
            this.btnPobierzForm_Generuj.Size = new System.Drawing.Size(138, 23);
            this.btnPobierzForm_Generuj.TabIndex = 1;
            this.btnPobierzForm_Generuj.Text = "Pobierz formy dostaw";
            this.btnPobierzForm_Generuj.UseVisualStyleBackColor = true;
            this.btnPobierzForm_Generuj.Click += new System.EventHandler(this.PobierzWszystkieFormyDostawy);
            // 
            // clbGenerujZwrotDokumetnow
            // 
            this.clbGenerujZwrotDokumetnow.FormattingEnabled = true;
            this.clbGenerujZwrotDokumetnow.Location = new System.Drawing.Point(6, 18);
            this.clbGenerujZwrotDokumetnow.Name = "clbGenerujZwrotDokumetnow";
            this.clbGenerujZwrotDokumetnow.Size = new System.Drawing.Size(340, 169);
            this.clbGenerujZwrotDokumetnow.TabIndex = 0;
            // 
            // pnlDPD
            // 
            this.pnlDPD.Controls.Add(this.groupBox3);
            this.pnlDPD.Location = new System.Drawing.Point(5, 5);
            this.pnlDPD.Name = "pnlDPD";
            this.pnlDPD.Size = new System.Drawing.Size(364, 355);
            this.pnlDPD.TabIndex = 2;
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox3.Controls.Add(this.btnSprawdzDPD);
            this.groupBox3.Controls.Add(this.chSprawdzanieStatusow);
            this.groupBox3.Controls.Add(this.btnUsunNumkat);
            this.groupBox3.Controls.Add(this.dgNumkat);
            this.groupBox3.Controls.Add(this.btnDodajNumkat);
            this.groupBox3.Controls.Add(this.txtNumkatNazwa);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Controls.Add(this.txtNumkatNumer);
            this.groupBox3.Controls.Add(this.label11);
            this.groupBox3.Controls.Add(this.txtWSDL);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Controls.Add(this.txtHasloDPD);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Controls.Add(this.txtLoginDPD);
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Location = new System.Drawing.Point(8, 8);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(346, 341);
            this.groupBox3.TabIndex = 41;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Dane konta webserwisu DPD";
            // 
            // btnSprawdzDPD
            // 
            this.btnSprawdzDPD.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSprawdzDPD.Location = new System.Drawing.Point(216, 281);
            this.btnSprawdzDPD.Name = "btnSprawdzDPD";
            this.btnSprawdzDPD.Size = new System.Drawing.Size(121, 23);
            this.btnSprawdzDPD.TabIndex = 50;
            this.btnSprawdzDPD.Text = "Sprawdź poprawność";
            this.btnSprawdzDPD.UseVisualStyleBackColor = true;
            this.btnSprawdzDPD.Click += new System.EventHandler(this.btnSprawdzDPD_Click);
            // 
            // chSprawdzanieStatusow
            // 
            this.chSprawdzanieStatusow.AutoSize = true;
            this.chSprawdzanieStatusow.Location = new System.Drawing.Point(58, 81);
            this.chSprawdzanieStatusow.Name = "chSprawdzanieStatusow";
            this.chSprawdzanieStatusow.Size = new System.Drawing.Size(181, 17);
            this.chSprawdzanieStatusow.TabIndex = 52;
            this.chSprawdzanieStatusow.Text = "Sprawdzanie statusów przesyłek";
            this.chSprawdzanieStatusow.UseVisualStyleBackColor = true;
            this.chSprawdzanieStatusow.Click += new System.EventHandler(this.chSprawdzanieStatusow_Click);
            // 
            // btnUsunNumkat
            // 
            this.btnUsunNumkat.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnUsunNumkat.Location = new System.Drawing.Point(7, 281);
            this.btnUsunNumkat.Name = "btnUsunNumkat";
            this.btnUsunNumkat.Size = new System.Drawing.Size(60, 23);
            this.btnUsunNumkat.TabIndex = 48;
            this.btnUsunNumkat.Text = "Usuń";
            this.btnUsunNumkat.UseVisualStyleBackColor = true;
            this.btnUsunNumkat.Click += new System.EventHandler(this.btnUsunNumkat_Click);
            // 
            // dgNumkat
            // 
            this.dgNumkat.AllowUserToAddRows = false;
            this.dgNumkat.AllowUserToResizeColumns = false;
            this.dgNumkat.AllowUserToResizeRows = false;
            this.dgNumkat.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgNumkat.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgNumkat.Location = new System.Drawing.Point(7, 145);
            this.dgNumkat.MultiSelect = false;
            this.dgNumkat.Name = "dgNumkat";
            this.dgNumkat.ReadOnly = true;
            this.dgNumkat.RowHeadersVisible = false;
            this.dgNumkat.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgNumkat.Size = new System.Drawing.Size(331, 133);
            this.dgNumkat.TabIndex = 47;
            this.dgNumkat.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgNumkat_CellContentClick);
            // 
            // btnDodajNumkat
            // 
            this.btnDodajNumkat.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDodajNumkat.Location = new System.Drawing.Point(278, 98);
            this.btnDodajNumkat.Name = "btnDodajNumkat";
            this.btnDodajNumkat.Size = new System.Drawing.Size(60, 23);
            this.btnDodajNumkat.TabIndex = 46;
            this.btnDodajNumkat.Text = "Dodaj";
            this.btnDodajNumkat.UseVisualStyleBackColor = true;
            this.btnDodajNumkat.Click += new System.EventHandler(this.btnDodajNumkat_Click);
            // 
            // txtNumkatNazwa
            // 
            this.txtNumkatNazwa.Location = new System.Drawing.Point(108, 121);
            this.txtNumkatNazwa.Name = "txtNumkatNazwa";
            this.txtNumkatNazwa.Size = new System.Drawing.Size(155, 20);
            this.txtNumkatNazwa.TabIndex = 45;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(4, 124);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(100, 13);
            this.label6.TabIndex = 41;
            this.label6.Text = "Nazwa klienta DPD";
            // 
            // txtNumkatNumer
            // 
            this.txtNumkatNumer.Location = new System.Drawing.Point(108, 100);
            this.txtNumkatNumer.Name = "txtNumkatNumer";
            this.txtNumkatNumer.Size = new System.Drawing.Size(155, 20);
            this.txtNumkatNumer.TabIndex = 44;
            this.txtNumkatNumer.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNumkatNumer_KeyPress);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(4, 103);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(98, 13);
            this.label11.TabIndex = 39;
            this.label11.Text = "Numer klienta DPD";
            // 
            // txtWSDL
            // 
            this.txtWSDL.Location = new System.Drawing.Point(58, 59);
            this.txtWSDL.Name = "txtWSDL";
            this.txtWSDL.Size = new System.Drawing.Size(281, 20);
            this.txtWSDL.TabIndex = 43;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(4, 62);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(39, 13);
            this.label5.TabIndex = 37;
            this.label5.Text = "WSDL";
            // 
            // txtHasloDPD
            // 
            this.txtHasloDPD.Location = new System.Drawing.Point(58, 37);
            this.txtHasloDPD.Name = "txtHasloDPD";
            this.txtHasloDPD.PasswordChar = '*';
            this.txtHasloDPD.Size = new System.Drawing.Size(205, 20);
            this.txtHasloDPD.TabIndex = 42;
            this.txtHasloDPD.UseSystemPasswordChar = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(4, 40);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(36, 13);
            this.label3.TabIndex = 35;
            this.label3.Text = "Hasło";
            // 
            // txtLoginDPD
            // 
            this.txtLoginDPD.Location = new System.Drawing.Point(58, 15);
            this.txtLoginDPD.Name = "txtLoginDPD";
            this.txtLoginDPD.Size = new System.Drawing.Size(205, 20);
            this.txtLoginDPD.TabIndex = 41;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(4, 18);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(33, 13);
            this.label4.TabIndex = 33;
            this.label4.Text = "Login";
            // 
            // pnlSzablon
            // 
            this.pnlSzablon.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlSzablon.Controls.Add(this.chWlaczSzablon);
            this.pnlSzablon.Controls.Add(this.grSzablon);
            this.pnlSzablon.Location = new System.Drawing.Point(5, 5);
            this.pnlSzablon.Name = "pnlSzablon";
            this.pnlSzablon.Size = new System.Drawing.Size(364, 355);
            this.pnlSzablon.TabIndex = 0;
            // 
            // chWlaczSzablon
            // 
            this.chWlaczSzablon.AutoSize = true;
            this.chWlaczSzablon.Location = new System.Drawing.Point(18, 6);
            this.chWlaczSzablon.Name = "chWlaczSzablon";
            this.chWlaczSzablon.Size = new System.Drawing.Size(58, 17);
            this.chWlaczSzablon.TabIndex = 246;
            this.chWlaczSzablon.Text = "Włącz";
            this.chWlaczSzablon.UseVisualStyleBackColor = true;
            this.chWlaczSzablon.CheckedChanged += new System.EventHandler(this.chWlaczSzablon_CheckedChanged);
            // 
            // grSzablon
            // 
            this.grSzablon.Controls.Add(this.label35);
            this.grSzablon.Controls.Add(this.txtDuty);
            this.grSzablon.Controls.Add(this.chDuty);
            this.grSzablon.Controls.Add(this.chDpdExpress);
            this.grSzablon.Controls.Add(this.btnFindPickup);
            this.grSzablon.Controls.Add(this.chPallet);
            this.grSzablon.Controls.Add(this.txtPudo);
            this.grSzablon.Controls.Add(this.chPudo);
            this.grSzablon.Controls.Add(this.chDeklWart);
            this.grSzablon.Controls.Add(this.chPobierzKwoteZWfMaga);
            this.grSzablon.Controls.Add(this.chOpony);
            this.grSzablon.Controls.Add(this.chTiresExport);
            this.grSzablon.Controls.Add(this.label28);
            this.grSzablon.Controls.Add(this.label21);
            this.grSzablon.Controls.Add(this.txtPobranie);
            this.grSzablon.Controls.Add(this.gbDeklarowanaWartosc);
            this.grSzablon.Controls.Add(this.chOdbiorWlasny);
            this.grSzablon.Controls.Add(this.chDorNaAdrPryw);
            this.grSzablon.Controls.Add(this.chPrzesList);
            this.grSzablon.Controls.Add(this.chDokZwrot);
            this.grSzablon.Controls.Add(this.chDorGwaran);
            this.grSzablon.Controls.Add(this.chPrzesZwrot);
            this.grSzablon.Controls.Add(this.chPobranie);
            this.grSzablon.Controls.Add(this.gbOdbiorWlasny);
            this.grSzablon.Controls.Add(this.cbDoreczenie);
            this.grSzablon.Controls.Add(this.txtNaOkreslonaGodzine);
            this.grSzablon.Controls.Add(this.chDorDoRakWl);
            this.grSzablon.Controls.Add(this.chPrzesylkaWartosciowaZWfMaga);
            this.grSzablon.Location = new System.Drawing.Point(7, 22);
            this.grSzablon.Name = "grSzablon";
            this.grSzablon.Size = new System.Drawing.Size(351, 319);
            this.grSzablon.TabIndex = 247;
            this.grSzablon.TabStop = false;
            this.grSzablon.Enter += new System.EventHandler(this.grSzablon_Enter);
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(159, 233);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(28, 13);
            this.label35.TabIndex = 296;
            this.label35.Text = "PLN";
            // 
            // txtDuty
            // 
            this.txtDuty.Enabled = false;
            this.txtDuty.ForeColor = System.Drawing.SystemColors.MenuText;
            this.txtDuty.Location = new System.Drawing.Point(105, 230);
            this.txtDuty.Name = "txtDuty";
            this.txtDuty.Size = new System.Drawing.Size(52, 20);
            this.txtDuty.TabIndex = 295;
            this.txtDuty.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // chDuty
            // 
            this.chDuty.AutoSize = true;
            this.chDuty.ForeColor = System.Drawing.SystemColors.MenuText;
            this.chDuty.Location = new System.Drawing.Point(7, 232);
            this.chDuty.Name = "chDuty";
            this.chDuty.Size = new System.Drawing.Size(98, 17);
            this.chDuty.TabIndex = 294;
            this.chDuty.Text = "Odprawa celna";
            this.chDuty.UseVisualStyleBackColor = true;
            this.chDuty.CheckedChanged += new System.EventHandler(this.chDuty_CheckedChanged);
            // 
            // chDpdExpress
            // 
            this.chDpdExpress.AutoSize = true;
            this.chDpdExpress.Location = new System.Drawing.Point(198, 229);
            this.chDpdExpress.Name = "chDpdExpress";
            this.chDpdExpress.Size = new System.Drawing.Size(102, 17);
            this.chDpdExpress.TabIndex = 293;
            this.chDpdExpress.Text = "DPD EXPRESS";
            this.chDpdExpress.UseVisualStyleBackColor = true;
            // 
            // btnFindPickup
            // 
            this.btnFindPickup.Location = new System.Drawing.Point(146, 202);
            this.btnFindPickup.Name = "btnFindPickup";
            this.btnFindPickup.Size = new System.Drawing.Size(47, 23);
            this.btnFindPickup.TabIndex = 283;
            this.btnFindPickup.Text = "Znajdź";
            this.btnFindPickup.UseVisualStyleBackColor = true;
            this.btnFindPickup.Click += new System.EventHandler(this.btnFindPickup_Click);
            // 
            // chPallet
            // 
            this.chPallet.AutoSize = true;
            this.chPallet.Location = new System.Drawing.Point(198, 206);
            this.chPallet.Name = "chPallet";
            this.chPallet.Size = new System.Drawing.Size(119, 17);
            this.chPallet.TabIndex = 250;
            this.chPallet.Text = "Przesyłka paletowa";
            this.chPallet.UseVisualStyleBackColor = true;
            // 
            // txtPudo
            // 
            this.txtPudo.Enabled = false;
            this.txtPudo.Location = new System.Drawing.Point(65, 204);
            this.txtPudo.Name = "txtPudo";
            this.txtPudo.Size = new System.Drawing.Size(80, 20);
            this.txtPudo.TabIndex = 249;
            // 
            // chPudo
            // 
            this.chPudo.AutoSize = true;
            this.chPudo.ForeColor = System.Drawing.SystemColors.MenuText;
            this.chPudo.Location = new System.Drawing.Point(7, 206);
            this.chPudo.Name = "chPudo";
            this.chPudo.Size = new System.Drawing.Size(59, 17);
            this.chPudo.TabIndex = 248;
            this.chPudo.Text = "Pickup";
            this.chPudo.UseVisualStyleBackColor = true;
            this.chPudo.CheckedChanged += new System.EventHandler(this.chPudo_CheckedChanged);
            // 
            // chDeklWart
            // 
            this.chDeklWart.AutoSize = true;
            this.chDeklWart.ForeColor = System.Drawing.SystemColors.MenuText;
            this.chDeklWart.Location = new System.Drawing.Point(198, 133);
            this.chDeklWart.Name = "chDeklWart";
            this.chDeklWart.Size = new System.Drawing.Size(135, 17);
            this.chDeklWart.TabIndex = 230;
            this.chDeklWart.Text = "Przesyłka wartościowa";
            this.chDeklWart.UseVisualStyleBackColor = true;
            this.chDeklWart.CheckedChanged += new System.EventHandler(this.chDeklWart_CheckedChanged);
            // 
            // chPobierzKwoteZWfMaga
            // 
            this.chPobierzKwoteZWfMaga.AutoSize = true;
            this.chPobierzKwoteZWfMaga.ForeColor = System.Drawing.SystemColors.MenuText;
            this.chPobierzKwoteZWfMaga.Location = new System.Drawing.Point(26, 138);
            this.chPobierzKwoteZWfMaga.Name = "chPobierzKwoteZWfMaga";
            this.chPobierzKwoteZWfMaga.Size = new System.Drawing.Size(148, 17);
            this.chPobierzKwoteZWfMaga.TabIndex = 246;
            this.chPobierzKwoteZWfMaga.Text = "Pobierz kwotę z Wf-Maga";
            this.chPobierzKwoteZWfMaga.UseVisualStyleBackColor = true;
            this.chPobierzKwoteZWfMaga.CheckedChanged += new System.EventHandler(this.chPobierzKwoteZWfMaga_CheckedChanged);
            // 
            // chOpony
            // 
            this.chOpony.AutoSize = true;
            this.chOpony.ForeColor = System.Drawing.SystemColors.MenuText;
            this.chOpony.Location = new System.Drawing.Point(198, 95);
            this.chOpony.Name = "chOpony";
            this.chOpony.Size = new System.Drawing.Size(57, 17);
            this.chOpony.TabIndex = 243;
            this.chOpony.Text = "Opony";
            this.chOpony.UseVisualStyleBackColor = true;
            // 
            // chTiresExport
            // 
            this.chTiresExport.AutoSize = true;
            this.chTiresExport.ForeColor = System.Drawing.SystemColors.MenuText;
            this.chTiresExport.Location = new System.Drawing.Point(198, 115);
            this.chTiresExport.Name = "chTiresExport";
            this.chTiresExport.Size = new System.Drawing.Size(139, 17);
            this.chTiresExport.TabIndex = 241;
            this.chTiresExport.Text = "Opony międzynarodowe";
            this.chTiresExport.UseVisualStyleBackColor = true;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.BackColor = System.Drawing.Color.Transparent;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label28.ForeColor = System.Drawing.SystemColors.MenuText;
            this.label28.Location = new System.Drawing.Point(23, 17);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(54, 13);
            this.label28.TabIndex = 242;
            this.label28.Text = "USŁUGI";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.BackColor = System.Drawing.Color.Transparent;
            this.label21.ForeColor = System.Drawing.SystemColors.MenuText;
            this.label21.Location = new System.Drawing.Point(158, 121);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(16, 13);
            this.label21.TabIndex = 240;
            this.label21.Text = "zł";
            // 
            // txtPobranie
            // 
            this.txtPobranie.Enabled = false;
            this.txtPobranie.ForeColor = System.Drawing.SystemColors.MenuText;
            this.txtPobranie.Location = new System.Drawing.Point(105, 117);
            this.txtPobranie.Name = "txtPobranie";
            this.txtPobranie.Size = new System.Drawing.Size(52, 20);
            this.txtPobranie.TabIndex = 239;
            this.txtPobranie.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // gbDeklarowanaWartosc
            // 
            this.gbDeklarowanaWartosc.Controls.Add(this.txtDeklarowanaWartosc);
            this.gbDeklarowanaWartosc.Controls.Add(this.label19);
            this.gbDeklarowanaWartosc.Controls.Add(this.label20);
            this.gbDeklarowanaWartosc.Enabled = false;
            this.gbDeklarowanaWartosc.ForeColor = System.Drawing.SystemColors.MenuText;
            this.gbDeklarowanaWartosc.Location = new System.Drawing.Point(204, 149);
            this.gbDeklarowanaWartosc.Name = "gbDeklarowanaWartosc";
            this.gbDeklarowanaWartosc.Size = new System.Drawing.Size(111, 32);
            this.gbDeklarowanaWartosc.TabIndex = 237;
            this.gbDeklarowanaWartosc.TabStop = false;
            // 
            // txtDeklarowanaWartosc
            // 
            this.txtDeklarowanaWartosc.Location = new System.Drawing.Point(43, 9);
            this.txtDeklarowanaWartosc.Name = "txtDeklarowanaWartosc";
            this.txtDeklarowanaWartosc.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtDeklarowanaWartosc.Size = new System.Drawing.Size(40, 20);
            this.txtDeklarowanaWartosc.TabIndex = 116;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.BackColor = System.Drawing.Color.Transparent;
            this.label19.ForeColor = System.Drawing.SystemColors.InfoText;
            this.label19.Location = new System.Drawing.Point(89, 12);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(16, 13);
            this.label19.TabIndex = 114;
            this.label19.Text = "zł";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.BackColor = System.Drawing.Color.Transparent;
            this.label20.ForeColor = System.Drawing.SystemColors.InfoText;
            this.label20.Location = new System.Drawing.Point(2, 12);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(37, 13);
            this.label20.TabIndex = 115;
            this.label20.Text = "Kwota";
            // 
            // chOdbiorWlasny
            // 
            this.chOdbiorWlasny.AutoSize = true;
            this.chOdbiorWlasny.ForeColor = System.Drawing.SystemColors.MenuText;
            this.chOdbiorWlasny.Location = new System.Drawing.Point(7, 159);
            this.chOdbiorWlasny.Name = "chOdbiorWlasny";
            this.chOdbiorWlasny.Size = new System.Drawing.Size(94, 17);
            this.chOdbiorWlasny.TabIndex = 236;
            this.chOdbiorWlasny.Text = "Odbiór własny";
            this.chOdbiorWlasny.UseVisualStyleBackColor = true;
            this.chOdbiorWlasny.CheckedChanged += new System.EventHandler(this.chOdbiorWlasny_CheckedChanged);
            // 
            // chDorNaAdrPryw
            // 
            this.chDorNaAdrPryw.AutoSize = true;
            this.chDorNaAdrPryw.ForeColor = System.Drawing.SystemColors.MenuText;
            this.chDorNaAdrPryw.Location = new System.Drawing.Point(7, 98);
            this.chDorNaAdrPryw.Name = "chDorNaAdrPryw";
            this.chDorNaAdrPryw.Size = new System.Drawing.Size(169, 17);
            this.chDorNaAdrPryw.TabIndex = 234;
            this.chDorNaAdrPryw.Text = "Doręczenie na adres prywatny";
            this.chDorNaAdrPryw.UseVisualStyleBackColor = true;
            // 
            // chPrzesList
            // 
            this.chPrzesList.AutoSize = true;
            this.chPrzesList.ForeColor = System.Drawing.SystemColors.MenuText;
            this.chPrzesList.Location = new System.Drawing.Point(198, 55);
            this.chPrzesList.Name = "chPrzesList";
            this.chPrzesList.Size = new System.Drawing.Size(49, 17);
            this.chPrzesList.TabIndex = 231;
            this.chPrzesList.Text = "DOX";
            this.chPrzesList.UseVisualStyleBackColor = true;
            // 
            // chDokZwrot
            // 
            this.chDokZwrot.AutoSize = true;
            this.chDokZwrot.ForeColor = System.Drawing.SystemColors.MenuText;
            this.chDokZwrot.Location = new System.Drawing.Point(198, 35);
            this.chDokZwrot.Name = "chDokZwrot";
            this.chDokZwrot.Size = new System.Drawing.Size(120, 17);
            this.chDokZwrot.TabIndex = 235;
            this.chDokZwrot.Text = "Dokumenty zwrotne";
            this.chDokZwrot.UseVisualStyleBackColor = true;
            // 
            // chDorGwaran
            // 
            this.chDorGwaran.AutoSize = true;
            this.chDorGwaran.ForeColor = System.Drawing.SystemColors.MenuText;
            this.chDorGwaran.Location = new System.Drawing.Point(7, 35);
            this.chDorGwaran.Name = "chDorGwaran";
            this.chDorGwaran.Size = new System.Drawing.Size(153, 17);
            this.chDorGwaran.TabIndex = 232;
            this.chDorGwaran.Text = "Doręczenia gwarantowane";
            this.chDorGwaran.UseVisualStyleBackColor = true;
            this.chDorGwaran.CheckedChanged += new System.EventHandler(this.chDorGwaran_CheckedChanged);
            // 
            // chPrzesZwrot
            // 
            this.chPrzesZwrot.AutoSize = true;
            this.chPrzesZwrot.ForeColor = System.Drawing.SystemColors.MenuText;
            this.chPrzesZwrot.Location = new System.Drawing.Point(198, 75);
            this.chPrzesZwrot.Name = "chPrzesZwrot";
            this.chPrzesZwrot.Size = new System.Drawing.Size(113, 17);
            this.chPrzesZwrot.TabIndex = 229;
            this.chPrzesZwrot.Text = "Przesyłka zwrotna";
            this.chPrzesZwrot.UseVisualStyleBackColor = true;
            // 
            // chPobranie
            // 
            this.chPobranie.AutoSize = true;
            this.chPobranie.ForeColor = System.Drawing.SystemColors.MenuText;
            this.chPobranie.Location = new System.Drawing.Point(7, 119);
            this.chPobranie.Name = "chPobranie";
            this.chPobranie.Size = new System.Drawing.Size(103, 17);
            this.chPobranie.TabIndex = 228;
            this.chPobranie.Text = "Pobranie C.O.D.";
            this.chPobranie.UseVisualStyleBackColor = true;
            this.chPobranie.CheckedChanged += new System.EventHandler(this.chPobranie_CheckedChanged);
            // 
            // gbOdbiorWlasny
            // 
            this.gbOdbiorWlasny.Controls.Add(this.rbOsoba);
            this.gbOdbiorWlasny.Controls.Add(this.rbFirma);
            this.gbOdbiorWlasny.Enabled = false;
            this.gbOdbiorWlasny.ForeColor = System.Drawing.SystemColors.MenuText;
            this.gbOdbiorWlasny.Location = new System.Drawing.Point(26, 170);
            this.gbOdbiorWlasny.Name = "gbOdbiorWlasny";
            this.gbOdbiorWlasny.Size = new System.Drawing.Size(135, 28);
            this.gbOdbiorWlasny.TabIndex = 238;
            this.gbOdbiorWlasny.TabStop = false;
            // 
            // rbOsoba
            // 
            this.rbOsoba.AutoSize = true;
            this.rbOsoba.Checked = true;
            this.rbOsoba.ForeColor = System.Drawing.SystemColors.InfoText;
            this.rbOsoba.Location = new System.Drawing.Point(9, 8);
            this.rbOsoba.Name = "rbOsoba";
            this.rbOsoba.Size = new System.Drawing.Size(54, 17);
            this.rbOsoba.TabIndex = 125;
            this.rbOsoba.TabStop = true;
            this.rbOsoba.Text = "osoba";
            this.rbOsoba.UseVisualStyleBackColor = true;
            // 
            // rbFirma
            // 
            this.rbFirma.AutoSize = true;
            this.rbFirma.ForeColor = System.Drawing.SystemColors.InfoText;
            this.rbFirma.Location = new System.Drawing.Point(78, 9);
            this.rbFirma.Name = "rbFirma";
            this.rbFirma.Size = new System.Drawing.Size(47, 17);
            this.rbFirma.TabIndex = 127;
            this.rbFirma.Text = "firma";
            this.rbFirma.UseVisualStyleBackColor = true;
            // 
            // cbDoreczenie
            // 
            this.cbDoreczenie.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbDoreczenie.FormattingEnabled = true;
            this.cbDoreczenie.Location = new System.Drawing.Point(26, 53);
            this.cbDoreczenie.Name = "cbDoreczenie";
            this.cbDoreczenie.Size = new System.Drawing.Size(115, 21);
            this.cbDoreczenie.TabIndex = 244;
            this.cbDoreczenie.Visible = false;
            this.cbDoreczenie.SelectedIndexChanged += new System.EventHandler(this.cbDoreczenie_SelectedIndexChanged);
            // 
            // txtNaOkreslonaGodzine
            // 
            this.txtNaOkreslonaGodzine.Location = new System.Drawing.Point(144, 54);
            this.txtNaOkreslonaGodzine.Name = "txtNaOkreslonaGodzine";
            this.txtNaOkreslonaGodzine.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtNaOkreslonaGodzine.Size = new System.Drawing.Size(45, 20);
            this.txtNaOkreslonaGodzine.TabIndex = 245;
            this.txtNaOkreslonaGodzine.Visible = false;
            // 
            // chDorDoRakWl
            // 
            this.chDorDoRakWl.AutoSize = true;
            this.chDorDoRakWl.ForeColor = System.Drawing.SystemColors.MenuText;
            this.chDorDoRakWl.Location = new System.Drawing.Point(7, 78);
            this.chDorDoRakWl.Name = "chDorDoRakWl";
            this.chDorDoRakWl.Size = new System.Drawing.Size(162, 17);
            this.chDorDoRakWl.TabIndex = 233;
            this.chDorDoRakWl.Text = "Doręczenie do rąk własnych";
            this.chDorDoRakWl.UseVisualStyleBackColor = true;
            // 
            // chPrzesylkaWartosciowaZWfMaga
            // 
            this.chPrzesylkaWartosciowaZWfMaga.AutoSize = true;
            this.chPrzesylkaWartosciowaZWfMaga.ForeColor = System.Drawing.SystemColors.MenuText;
            this.chPrzesylkaWartosciowaZWfMaga.Location = new System.Drawing.Point(202, 183);
            this.chPrzesylkaWartosciowaZWfMaga.Name = "chPrzesylkaWartosciowaZWfMaga";
            this.chPrzesylkaWartosciowaZWfMaga.Size = new System.Drawing.Size(148, 17);
            this.chPrzesylkaWartosciowaZWfMaga.TabIndex = 247;
            this.chPrzesylkaWartosciowaZWfMaga.Text = "Pobierz kwotę z Wf-Maga";
            this.chPrzesylkaWartosciowaZWfMaga.UseVisualStyleBackColor = true;
            this.chPrzesylkaWartosciowaZWfMaga.CheckedChanged += new System.EventHandler(this.chPrzesylkaWartosciowaZWfMaga_CheckedChanged);
            // 
            // pnlPolaczenie
            // 
            this.pnlPolaczenie.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlPolaczenie.Controls.Add(this.groupBox2);
            this.pnlPolaczenie.Location = new System.Drawing.Point(5, 5);
            this.pnlPolaczenie.Name = "pnlPolaczenie";
            this.pnlPolaczenie.Size = new System.Drawing.Size(364, 355);
            this.pnlPolaczenie.TabIndex = 0;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.lblInfo);
            this.groupBox2.Controls.Add(this.btnTest);
            this.groupBox2.Controls.Add(this.txtDb);
            this.groupBox2.Controls.Add(this.label15);
            this.groupBox2.Controls.Add(this.cbUwierz);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.txtHasloDb);
            this.groupBox2.Controls.Add(this.txtLoginDb);
            this.groupBox2.Controls.Add(this.txtSerwer);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Location = new System.Drawing.Point(8, 8);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(309, 161);
            this.groupBox2.TabIndex = 39;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Połącznie z WF-Magiem";
            // 
            // lblInfo
            // 
            this.lblInfo.AutoSize = true;
            this.lblInfo.Location = new System.Drawing.Point(10, 137);
            this.lblInfo.Name = "lblInfo";
            this.lblInfo.Size = new System.Drawing.Size(0, 13);
            this.lblInfo.TabIndex = 41;
            // 
            // btnTest
            // 
            this.btnTest.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTest.Location = new System.Drawing.Point(145, 133);
            this.btnTest.Name = "btnTest";
            this.btnTest.Size = new System.Drawing.Size(155, 23);
            this.btnTest.TabIndex = 6;
            this.btnTest.Text = "Testuj połączenie";
            this.btnTest.UseVisualStyleBackColor = true;
            this.btnTest.Click += new System.EventHandler(this.btnTest_Click);
            // 
            // txtDb
            // 
            this.txtDb.Location = new System.Drawing.Point(145, 41);
            this.txtDb.Name = "txtDb";
            this.txtDb.Size = new System.Drawing.Size(155, 20);
            this.txtDb.TabIndex = 2;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(10, 44);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(84, 13);
            this.label15.TabIndex = 31;
            this.label15.Text = "Nazwa katalogu";
            // 
            // cbUwierz
            // 
            this.cbUwierz.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbUwierz.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbUwierz.FormattingEnabled = true;
            this.cbUwierz.Location = new System.Drawing.Point(146, 64);
            this.cbUwierz.Name = "cbUwierz";
            this.cbUwierz.Size = new System.Drawing.Size(155, 21);
            this.cbUwierz.TabIndex = 3;
            this.cbUwierz.SelectedIndexChanged += new System.EventHandler(this.cbUwierz_SelectedIndexChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(10, 113);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(36, 13);
            this.label7.TabIndex = 16;
            this.label7.Text = "Hasło";
            // 
            // txtHasloDb
            // 
            this.txtHasloDb.Enabled = false;
            this.txtHasloDb.Location = new System.Drawing.Point(145, 110);
            this.txtHasloDb.Name = "txtHasloDb";
            this.txtHasloDb.PasswordChar = '*';
            this.txtHasloDb.Size = new System.Drawing.Size(155, 20);
            this.txtHasloDb.TabIndex = 5;
            this.txtHasloDb.UseSystemPasswordChar = true;
            // 
            // txtLoginDb
            // 
            this.txtLoginDb.Enabled = false;
            this.txtLoginDb.Location = new System.Drawing.Point(145, 88);
            this.txtLoginDb.Name = "txtLoginDb";
            this.txtLoginDb.Size = new System.Drawing.Size(155, 20);
            this.txtLoginDb.TabIndex = 4;
            // 
            // txtSerwer
            // 
            this.txtSerwer.Location = new System.Drawing.Point(145, 19);
            this.txtSerwer.Name = "txtSerwer";
            this.txtSerwer.Size = new System.Drawing.Size(155, 20);
            this.txtSerwer.TabIndex = 1;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(10, 91);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(33, 13);
            this.label8.TabIndex = 12;
            this.label8.Text = "Login";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(10, 69);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(83, 13);
            this.label9.TabIndex = 11;
            this.label9.Text = "Uwierzytelnianie";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(10, 22);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(80, 13);
            this.label10.TabIndex = 10;
            this.label10.Text = "Nazwa serwera";
            // 
            // pnlEP
            // 
            this.pnlEP.Controls.Add(this.groupBox5);
            this.pnlEP.Controls.Add(this.groupBox1);
            this.pnlEP.Location = new System.Drawing.Point(5, 5);
            this.pnlEP.Name = "pnlEP";
            this.pnlEP.Size = new System.Drawing.Size(364, 355);
            this.pnlEP.TabIndex = 1;
            // 
            // groupBox5
            // 
            this.groupBox5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox5.Controls.Add(this.groupBox13);
            this.groupBox5.Controls.Add(this.groupBox12);
            this.groupBox5.Location = new System.Drawing.Point(8, 112);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(353, 200);
            this.groupBox5.TabIndex = 46;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Drukarki";
            // 
            // groupBox13
            // 
            this.groupBox13.Controls.Add(this.chAutoWydrukPoZapisie);
            this.groupBox13.Controls.Add(this.label16);
            this.groupBox13.Controls.Add(this.label14);
            this.groupBox13.Controls.Add(this.chEtykieciarka);
            this.groupBox13.Controls.Add(this.label13);
            this.groupBox13.Controls.Add(this.rbZpl);
            this.groupBox13.Controls.Add(this.rbEpl);
            this.groupBox13.Controls.Add(this.rbPdf);
            this.groupBox13.Controls.Add(this.cbDrukarkaEtykieta);
            this.groupBox13.Location = new System.Drawing.Point(12, 75);
            this.groupBox13.Name = "groupBox13";
            this.groupBox13.Size = new System.Drawing.Size(325, 112);
            this.groupBox13.TabIndex = 182;
            this.groupBox13.TabStop = false;
            this.groupBox13.Text = "Etykieta";
            // 
            // chAutoWydrukPoZapisie
            // 
            this.chAutoWydrukPoZapisie.AutoSize = true;
            this.chAutoWydrukPoZapisie.Location = new System.Drawing.Point(235, 87);
            this.chAutoWydrukPoZapisie.Name = "chAutoWydrukPoZapisie";
            this.chAutoWydrukPoZapisie.Size = new System.Drawing.Size(47, 17);
            this.chAutoWydrukPoZapisie.TabIndex = 190;
            this.chAutoWydrukPoZapisie.Text = "TAK";
            this.chAutoWydrukPoZapisie.UseVisualStyleBackColor = true;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.Color.Transparent;
            this.label16.ForeColor = System.Drawing.SystemColors.MenuText;
            this.label16.Location = new System.Drawing.Point(30, 88);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(163, 13);
            this.label16.TabIndex = 189;
            this.label16.Text = "Automatyczny wydruk po zapisie:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Enabled = false;
            this.label14.ForeColor = System.Drawing.SystemColors.MenuText;
            this.label14.Location = new System.Drawing.Point(30, 68);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(149, 13);
            this.label14.TabIndex = 188;
            this.label14.Text = "Wybierz format zapisu etykiety";
            // 
            // chEtykieciarka
            // 
            this.chEtykieciarka.AutoSize = true;
            this.chEtykieciarka.Location = new System.Drawing.Point(235, 47);
            this.chEtykieciarka.Name = "chEtykieciarka";
            this.chEtykieciarka.Size = new System.Drawing.Size(47, 17);
            this.chEtykieciarka.TabIndex = 187;
            this.chEtykieciarka.Text = "TAK";
            this.chEtykieciarka.UseVisualStyleBackColor = true;
            this.chEtykieciarka.CheckedChanged += new System.EventHandler(this.chEtykieciarka_CheckedChanged);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(30, 48);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(197, 13);
            this.label13.TabIndex = 186;
            this.label13.Text = "Czy wybrana drukarka jest etykieciarką?";
            // 
            // rbZpl
            // 
            this.rbZpl.AutoSize = true;
            this.rbZpl.Enabled = false;
            this.rbZpl.Location = new System.Drawing.Point(278, 66);
            this.rbZpl.Name = "rbZpl";
            this.rbZpl.Size = new System.Drawing.Size(45, 17);
            this.rbZpl.TabIndex = 183;
            this.rbZpl.Text = "ZPL";
            this.rbZpl.UseVisualStyleBackColor = true;
            // 
            // rbEpl
            // 
            this.rbEpl.AutoSize = true;
            this.rbEpl.Enabled = false;
            this.rbEpl.Location = new System.Drawing.Point(235, 66);
            this.rbEpl.Name = "rbEpl";
            this.rbEpl.Size = new System.Drawing.Size(45, 17);
            this.rbEpl.TabIndex = 182;
            this.rbEpl.Text = "EPL";
            this.rbEpl.UseVisualStyleBackColor = true;
            // 
            // rbPdf
            // 
            this.rbPdf.AutoSize = true;
            this.rbPdf.Checked = true;
            this.rbPdf.Enabled = false;
            this.rbPdf.Location = new System.Drawing.Point(190, 66);
            this.rbPdf.Name = "rbPdf";
            this.rbPdf.Size = new System.Drawing.Size(46, 17);
            this.rbPdf.TabIndex = 181;
            this.rbPdf.TabStop = true;
            this.rbPdf.Text = "PDF";
            this.rbPdf.UseVisualStyleBackColor = true;
            // 
            // cbDrukarkaEtykieta
            // 
            this.cbDrukarkaEtykieta.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbDrukarkaEtykieta.FormattingEnabled = true;
            this.cbDrukarkaEtykieta.Location = new System.Drawing.Point(32, 17);
            this.cbDrukarkaEtykieta.Name = "cbDrukarkaEtykieta";
            this.cbDrukarkaEtykieta.Size = new System.Drawing.Size(284, 21);
            this.cbDrukarkaEtykieta.TabIndex = 184;
            // 
            // groupBox12
            // 
            this.groupBox12.Controls.Add(this.cbDrukarkaProtokol);
            this.groupBox12.Location = new System.Drawing.Point(13, 20);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Size = new System.Drawing.Size(324, 50);
            this.groupBox12.TabIndex = 181;
            this.groupBox12.TabStop = false;
            this.groupBox12.Text = "Protokół";
            // 
            // cbDrukarkaProtokol
            // 
            this.cbDrukarkaProtokol.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbDrukarkaProtokol.FormattingEnabled = true;
            this.cbDrukarkaProtokol.Location = new System.Drawing.Point(32, 19);
            this.cbDrukarkaProtokol.Name = "cbDrukarkaProtokol";
            this.cbDrukarkaProtokol.Size = new System.Drawing.Size(283, 21);
            this.cbDrukarkaProtokol.TabIndex = 175;
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.btnRaporty);
            this.groupBox1.Controls.Add(this.txtRaporty);
            this.groupBox1.Controls.Add(this.label22);
            this.groupBox1.Controls.Add(this.btnProtokol);
            this.groupBox1.Controls.Add(this.btnEtykieta);
            this.groupBox1.Controls.Add(this.txtProtokol);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.txtEtykieta);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(8, 8);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(353, 98);
            this.groupBox1.TabIndex = 44;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Miejsce zapisu";
            // 
            // btnRaporty
            // 
            this.btnRaporty.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRaporty.Location = new System.Drawing.Point(278, 66);
            this.btnRaporty.Name = "btnRaporty";
            this.btnRaporty.Size = new System.Drawing.Size(60, 23);
            this.btnRaporty.TabIndex = 42;
            this.btnRaporty.Text = "Wybierz";
            this.btnRaporty.UseVisualStyleBackColor = true;
            this.btnRaporty.Click += new System.EventHandler(this.btnRaporty_Click);
            // 
            // txtRaporty
            // 
            this.txtRaporty.Location = new System.Drawing.Point(79, 68);
            this.txtRaporty.Name = "txtRaporty";
            this.txtRaporty.Size = new System.Drawing.Size(193, 20);
            this.txtRaporty.TabIndex = 40;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(12, 71);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(48, 13);
            this.label22.TabIndex = 41;
            this.label22.Text = "raportów";
            // 
            // btnProtokol
            // 
            this.btnProtokol.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnProtokol.Location = new System.Drawing.Point(278, 40);
            this.btnProtokol.Name = "btnProtokol";
            this.btnProtokol.Size = new System.Drawing.Size(60, 23);
            this.btnProtokol.TabIndex = 39;
            this.btnProtokol.Text = "Wybierz";
            this.btnProtokol.UseVisualStyleBackColor = true;
            this.btnProtokol.Click += new System.EventHandler(this.btnProtokol_Click);
            // 
            // btnEtykieta
            // 
            this.btnEtykieta.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEtykieta.Location = new System.Drawing.Point(278, 14);
            this.btnEtykieta.Name = "btnEtykieta";
            this.btnEtykieta.Size = new System.Drawing.Size(60, 23);
            this.btnEtykieta.TabIndex = 38;
            this.btnEtykieta.Text = "Wybierz";
            this.btnEtykieta.UseVisualStyleBackColor = true;
            this.btnEtykieta.Click += new System.EventHandler(this.btnEtykieta_Click);
            // 
            // txtProtokol
            // 
            this.txtProtokol.Location = new System.Drawing.Point(79, 42);
            this.txtProtokol.Name = "txtProtokol";
            this.txtProtokol.Size = new System.Drawing.Size(193, 20);
            this.txtProtokol.TabIndex = 22;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 45);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(61, 13);
            this.label2.TabIndex = 37;
            this.label2.Text = "protokołów";
            // 
            // txtEtykieta
            // 
            this.txtEtykieta.Location = new System.Drawing.Point(79, 16);
            this.txtEtykieta.Name = "txtEtykieta";
            this.txtEtykieta.Size = new System.Drawing.Size(193, 20);
            this.txtEtykieta.TabIndex = 21;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 35;
            this.label1.Text = "etykiet";
            // 
            // pnlPobranieCOD
            // 
            this.pnlPobranieCOD.Controls.Add(this.groupBox14);
            this.pnlPobranieCOD.Location = new System.Drawing.Point(0, 2);
            this.pnlPobranieCOD.Name = "pnlPobranieCOD";
            this.pnlPobranieCOD.Size = new System.Drawing.Size(369, 358);
            this.pnlPobranieCOD.TabIndex = 52;
            // 
            // groupBox14
            // 
            this.groupBox14.Controls.Add(this.btnPobierzPlatnosci);
            this.groupBox14.Controls.Add(this.clbPobranieCOD);
            this.groupBox14.Location = new System.Drawing.Point(7, 6);
            this.groupBox14.Name = "groupBox14";
            this.groupBox14.Size = new System.Drawing.Size(353, 217);
            this.groupBox14.TabIndex = 0;
            this.groupBox14.TabStop = false;
            this.groupBox14.Text = "Pobieraj kwotę pobrania C.O.D. dla:";
            // 
            // btnPobierzPlatnosci
            // 
            this.btnPobierzPlatnosci.Location = new System.Drawing.Point(234, 188);
            this.btnPobierzPlatnosci.Name = "btnPobierzPlatnosci";
            this.btnPobierzPlatnosci.Size = new System.Drawing.Size(113, 23);
            this.btnPobierzPlatnosci.TabIndex = 1;
            this.btnPobierzPlatnosci.Text = "Pobierz płatności";
            this.btnPobierzPlatnosci.UseVisualStyleBackColor = true;
            this.btnPobierzPlatnosci.Click += new System.EventHandler(this.btnPobierzPlatnosci_Click);
            // 
            // clbPobranieCOD
            // 
            this.clbPobranieCOD.FormattingEnabled = true;
            this.clbPobranieCOD.Location = new System.Drawing.Point(7, 16);
            this.clbPobranieCOD.Name = "clbPobranieCOD";
            this.clbPobranieCOD.Size = new System.Drawing.Size(340, 169);
            this.clbPobranieCOD.TabIndex = 0;
            // 
            // pnlZapis
            // 
            this.pnlZapis.Controls.Add(this.groupBox9);
            this.pnlZapis.Controls.Add(this.groupBox7);
            this.pnlZapis.Controls.Add(this.grMag);
            this.pnlZapis.Controls.Add(this.grHandl);
            this.pnlZapis.Location = new System.Drawing.Point(5, 5);
            this.pnlZapis.Name = "pnlZapis";
            this.pnlZapis.Size = new System.Drawing.Size(364, 355);
            this.pnlZapis.TabIndex = 4;
            // 
            // groupBox9
            // 
            this.groupBox9.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox9.Controls.Add(this.cbZmienStatus);
            this.groupBox9.Controls.Add(this.chZmienStatus);
            this.groupBox9.Location = new System.Drawing.Point(7, 217);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(353, 42);
            this.groupBox9.TabIndex = 69;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Zmiana statusu zamowienia";
            this.groupBox9.Visible = false;
            // 
            // cbZmienStatus
            // 
            this.cbZmienStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbZmienStatus.Enabled = false;
            this.cbZmienStatus.ForeColor = System.Drawing.SystemColors.MenuText;
            this.cbZmienStatus.FormattingEnabled = true;
            this.cbZmienStatus.Location = new System.Drawing.Point(119, 16);
            this.cbZmienStatus.Name = "cbZmienStatus";
            this.cbZmienStatus.Size = new System.Drawing.Size(107, 21);
            this.cbZmienStatus.TabIndex = 60;
            // 
            // chZmienStatus
            // 
            this.chZmienStatus.AutoSize = true;
            this.chZmienStatus.ForeColor = System.Drawing.SystemColors.MenuText;
            this.chZmienStatus.Location = new System.Drawing.Point(6, 19);
            this.chZmienStatus.Name = "chZmienStatus";
            this.chZmienStatus.Size = new System.Drawing.Size(86, 17);
            this.chZmienStatus.TabIndex = 58;
            this.chZmienStatus.Text = "Zmień status";
            this.chZmienStatus.UseVisualStyleBackColor = true;
            // 
            // groupBox7
            // 
            this.groupBox7.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox7.Controls.Add(this.chZamZapiszNrListuV2);
            this.groupBox7.Controls.Add(this.chPoleDodZam);
            this.groupBox7.Controls.Add(this.cbZam);
            this.groupBox7.Controls.Add(this.chUwagiZam);
            this.groupBox7.Controls.Add(this.chInfDodZam);
            this.groupBox7.Controls.Add(this.chKomentarz);
            this.groupBox7.Location = new System.Drawing.Point(8, 107);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(353, 107);
            this.groupBox7.TabIndex = 68;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Zamówienia";
            // 
            // chZamZapiszNrListuV2
            // 
            this.chZamZapiszNrListuV2.AutoSize = true;
            this.chZamZapiszNrListuV2.Location = new System.Drawing.Point(5, 84);
            this.chZamZapiszNrListuV2.Name = "chZamZapiszNrListuV2";
            this.chZamZapiszNrListuV2.Size = new System.Drawing.Size(274, 17);
            this.chZamZapiszNrListuV2.TabIndex = 61;
            this.chZamZapiszNrListuV2.Text = "Kolumna numer przesyłki (od wersji WF-MAG 8.11.2)";
            this.chZamZapiszNrListuV2.UseVisualStyleBackColor = true;
            // 
            // chPoleDodZam
            // 
            this.chPoleDodZam.AutoSize = true;
            this.chPoleDodZam.ForeColor = System.Drawing.SystemColors.MenuText;
            this.chPoleDodZam.Location = new System.Drawing.Point(6, 63);
            this.chPoleDodZam.Name = "chPoleDodZam";
            this.chPoleDodZam.Size = new System.Drawing.Size(103, 17);
            this.chPoleDodZam.TabIndex = 60;
            this.chPoleDodZam.Text = "Pole dodatkowe";
            this.chPoleDodZam.UseVisualStyleBackColor = true;
            this.chPoleDodZam.CheckedChanged += new System.EventHandler(this.chPoleDodZam_CheckedChanged);
            // 
            // cbZam
            // 
            this.cbZam.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbZam.Enabled = false;
            this.cbZam.ForeColor = System.Drawing.SystemColors.MenuText;
            this.cbZam.FormattingEnabled = true;
            this.cbZam.Location = new System.Drawing.Point(125, 61);
            this.cbZam.Name = "cbZam";
            this.cbZam.Size = new System.Drawing.Size(68, 21);
            this.cbZam.TabIndex = 59;
            // 
            // chUwagiZam
            // 
            this.chUwagiZam.AutoSize = true;
            this.chUwagiZam.ForeColor = System.Drawing.SystemColors.MenuText;
            this.chUwagiZam.Location = new System.Drawing.Point(152, 42);
            this.chUwagiZam.Name = "chUwagiZam";
            this.chUwagiZam.Size = new System.Drawing.Size(56, 17);
            this.chUwagiZam.TabIndex = 56;
            this.chUwagiZam.Text = "Uwagi";
            this.chUwagiZam.UseVisualStyleBackColor = true;
            // 
            // chInfDodZam
            // 
            this.chInfDodZam.AutoSize = true;
            this.chInfDodZam.ForeColor = System.Drawing.SystemColors.MenuText;
            this.chInfDodZam.Location = new System.Drawing.Point(6, 42);
            this.chInfDodZam.Name = "chInfDodZam";
            this.chInfDodZam.Size = new System.Drawing.Size(131, 17);
            this.chInfDodZam.TabIndex = 58;
            this.chInfDodZam.Text = "Informacje dodatkowe";
            this.chInfDodZam.UseVisualStyleBackColor = true;
            // 
            // chKomentarz
            // 
            this.chKomentarz.AutoSize = true;
            this.chKomentarz.ForeColor = System.Drawing.SystemColors.MenuText;
            this.chKomentarz.Location = new System.Drawing.Point(6, 19);
            this.chKomentarz.Name = "chKomentarz";
            this.chKomentarz.Size = new System.Drawing.Size(163, 17);
            this.chKomentarz.TabIndex = 57;
            this.chKomentarz.Text = "Komentarz do zmiany statusu";
            this.chKomentarz.UseVisualStyleBackColor = true;
            // 
            // grMag
            // 
            this.grMag.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grMag.Controls.Add(this.chPolDodMag);
            this.grMag.Controls.Add(this.cbMag);
            this.grMag.Controls.Add(this.chUwagiMag);
            this.grMag.ForeColor = System.Drawing.SystemColors.MenuText;
            this.grMag.Location = new System.Drawing.Point(8, 56);
            this.grMag.Name = "grMag";
            this.grMag.Size = new System.Drawing.Size(353, 47);
            this.grMag.TabIndex = 67;
            this.grMag.TabStop = false;
            this.grMag.Text = "Dokumenty magazynowe";
            // 
            // chPolDodMag
            // 
            this.chPolDodMag.AutoSize = true;
            this.chPolDodMag.Location = new System.Drawing.Point(93, 20);
            this.chPolDodMag.Name = "chPolDodMag";
            this.chPolDodMag.Size = new System.Drawing.Size(103, 17);
            this.chPolDodMag.TabIndex = 56;
            this.chPolDodMag.Text = "Pole dodatkowe";
            this.chPolDodMag.UseVisualStyleBackColor = true;
            this.chPolDodMag.CheckedChanged += new System.EventHandler(this.chPolDodMag_CheckedChanged);
            // 
            // cbMag
            // 
            this.cbMag.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbMag.Enabled = false;
            this.cbMag.FormattingEnabled = true;
            this.cbMag.Location = new System.Drawing.Point(200, 17);
            this.cbMag.Name = "cbMag";
            this.cbMag.Size = new System.Drawing.Size(68, 21);
            this.cbMag.TabIndex = 55;
            // 
            // chUwagiMag
            // 
            this.chUwagiMag.AutoSize = true;
            this.chUwagiMag.Location = new System.Drawing.Point(6, 19);
            this.chUwagiMag.Name = "chUwagiMag";
            this.chUwagiMag.Size = new System.Drawing.Size(56, 17);
            this.chUwagiMag.TabIndex = 56;
            this.chUwagiMag.Text = "Uwagi";
            this.chUwagiMag.UseVisualStyleBackColor = true;
            // 
            // grHandl
            // 
            this.grHandl.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grHandl.Controls.Add(this.chPolDodHand);
            this.grHandl.Controls.Add(this.cbHand);
            this.grHandl.Controls.Add(this.chUwagiHand);
            this.grHandl.ForeColor = System.Drawing.SystemColors.MenuText;
            this.grHandl.Location = new System.Drawing.Point(8, 8);
            this.grHandl.Name = "grHandl";
            this.grHandl.Size = new System.Drawing.Size(353, 41);
            this.grHandl.TabIndex = 66;
            this.grHandl.TabStop = false;
            this.grHandl.Text = "Dokumenty handlowe";
            // 
            // chPolDodHand
            // 
            this.chPolDodHand.AutoSize = true;
            this.chPolDodHand.Location = new System.Drawing.Point(93, 20);
            this.chPolDodHand.Name = "chPolDodHand";
            this.chPolDodHand.Size = new System.Drawing.Size(103, 17);
            this.chPolDodHand.TabIndex = 56;
            this.chPolDodHand.Text = "Pole dodatkowe";
            this.chPolDodHand.UseVisualStyleBackColor = true;
            this.chPolDodHand.CheckedChanged += new System.EventHandler(this.chPolDodHand_CheckedChanged);
            // 
            // cbHand
            // 
            this.cbHand.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbHand.Enabled = false;
            this.cbHand.FormattingEnabled = true;
            this.cbHand.Location = new System.Drawing.Point(200, 17);
            this.cbHand.Name = "cbHand";
            this.cbHand.Size = new System.Drawing.Size(68, 21);
            this.cbHand.TabIndex = 55;
            // 
            // chUwagiHand
            // 
            this.chUwagiHand.AutoSize = true;
            this.chUwagiHand.Location = new System.Drawing.Point(6, 19);
            this.chUwagiHand.Name = "chUwagiHand";
            this.chUwagiHand.Size = new System.Drawing.Size(56, 17);
            this.chUwagiHand.TabIndex = 56;
            this.chUwagiHand.Text = "Uwagi";
            this.chUwagiHand.UseVisualStyleBackColor = true;
            // 
            // pnlZwrotDokumentow
            // 
            this.pnlZwrotDokumentow.Controls.Add(this.gbZwrotDokumentow);
            this.pnlZwrotDokumentow.Location = new System.Drawing.Point(0, 0);
            this.pnlZwrotDokumentow.Name = "pnlZwrotDokumentow";
            this.pnlZwrotDokumentow.Size = new System.Drawing.Size(366, 354);
            this.pnlZwrotDokumentow.TabIndex = 1;
            // 
            // gbZwrotDokumentow
            // 
            this.gbZwrotDokumentow.Controls.Add(this.checkedListBox1);
            this.gbZwrotDokumentow.Location = new System.Drawing.Point(9, 9);
            this.gbZwrotDokumentow.Name = "gbZwrotDokumentow";
            this.gbZwrotDokumentow.Size = new System.Drawing.Size(350, 211);
            this.gbZwrotDokumentow.TabIndex = 0;
            this.gbZwrotDokumentow.TabStop = false;
            this.gbZwrotDokumentow.Text = "Generuj Zwrot Dokumentów dla form dostaw:";
            // 
            // checkedListBox1
            // 
            this.checkedListBox1.FormattingEnabled = true;
            this.checkedListBox1.Location = new System.Drawing.Point(6, 21);
            this.checkedListBox1.Name = "checkedListBox1";
            this.checkedListBox1.Size = new System.Drawing.Size(336, 154);
            this.checkedListBox1.TabIndex = 0;
            // 
            // Ustawienia2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(532, 391);
            this.Controls.Add(this.lbKategorie);
            this.Controls.Add(this.pnlGlowny);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximumSize = new System.Drawing.Size(548, 430);
            this.MinimumSize = new System.Drawing.Size(548, 390);
            this.Name = "Ustawienia2";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Ustawienia";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Ustawienia2_FormClosed);
            this.Load += new System.EventHandler(this.Ustawienia2_Load);
            this.pnlGlowny.ResumeLayout(false);
            this.pnlGlowny.PerformLayout();
            this.pnlUstawieniaPolDostawy.ResumeLayout(false);
            this.pnlUstawieniaPolDostawy.PerformLayout();
            this.pnlInne.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox11.ResumeLayout(false);
            this.groupBox11.PerformLayout();
            this.groupBox10.ResumeLayout(false);
            this.pnlDaneDod.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.pnlBlokujGenerowanieListu.ResumeLayout(false);
            this.gbBlokujGenerowanieDokumentow.ResumeLayout(false);
            this.pnlAdres.ResumeLayout(false);
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.pnlGenerujZwrotDokumetnow.ResumeLayout(false);
            this.gbGenerujZwrotDokumentow.ResumeLayout(false);
            this.pnlDPD.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgNumkat)).EndInit();
            this.pnlSzablon.ResumeLayout(false);
            this.pnlSzablon.PerformLayout();
            this.grSzablon.ResumeLayout(false);
            this.grSzablon.PerformLayout();
            this.gbDeklarowanaWartosc.ResumeLayout(false);
            this.gbDeklarowanaWartosc.PerformLayout();
            this.gbOdbiorWlasny.ResumeLayout(false);
            this.gbOdbiorWlasny.PerformLayout();
            this.pnlPolaczenie.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.pnlEP.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox13.ResumeLayout(false);
            this.groupBox13.PerformLayout();
            this.groupBox12.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.pnlPobranieCOD.ResumeLayout(false);
            this.groupBox14.ResumeLayout(false);
            this.pnlZapis.ResumeLayout(false);
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.grMag.ResumeLayout(false);
            this.grMag.PerformLayout();
            this.grHandl.ResumeLayout(false);
            this.grHandl.PerformLayout();
            this.pnlZwrotDokumentow.ResumeLayout(false);
            this.gbZwrotDokumentow.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListBox lbKategorie;
        private System.Windows.Forms.Panel pnlGlowny;
        private System.Windows.Forms.Panel pnlSzablon;
        private System.Windows.Forms.Panel pnlPolaczenie; 
        private System.Windows.Forms.Panel pnlEP;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.ComboBox cbDrukarkaProtokol;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnProtokol;
        private System.Windows.Forms.Button btnEtykieta;
        private System.Windows.Forms.TextBox txtProtokol;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtEtykieta;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel pnlDPD;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label lblInfo;
        private System.Windows.Forms.Button btnTest;
        private System.Windows.Forms.TextBox txtDb;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.ComboBox cbUwierz;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtHasloDb;
        private System.Windows.Forms.TextBox txtLoginDb;
        private System.Windows.Forms.TextBox txtSerwer;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button btnUsunNumkat;
        private System.Windows.Forms.DataGridView dgNumkat;
        private System.Windows.Forms.Button btnDodajNumkat;
        private System.Windows.Forms.TextBox txtNumkatNazwa;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtNumkatNumer;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtWSDL;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtHasloDPD;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtLoginDPD;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel pnlZapis;
        private System.Windows.Forms.Panel pnlAdres;
        private System.Windows.Forms.Panel pnlDaneDod;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.CheckBox chPoleDodZam;
        private System.Windows.Forms.ComboBox cbZam;
        private System.Windows.Forms.CheckBox chUwagiZam;
        private System.Windows.Forms.CheckBox chInfDodZam;
        private System.Windows.Forms.CheckBox chKomentarz;
        private System.Windows.Forms.GroupBox grMag;
        private System.Windows.Forms.CheckBox chPolDodMag;
        private System.Windows.Forms.ComboBox cbMag;
        private System.Windows.Forms.CheckBox chUwagiMag;
        private System.Windows.Forms.GroupBox grHandl;
        private System.Windows.Forms.CheckBox chPolDodHand;
        private System.Windows.Forms.ComboBox cbHand;
        private System.Windows.Forms.CheckBox chUwagiHand;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtMailNad;
        private System.Windows.Forms.CheckBox chDomyslnyAdresNadawcy;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox txtkrajNad;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtTelNad;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.TextBox txtNazwaNad;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.TextBox txtUlicaNad;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.TextBox txtKodNad;
        private System.Windows.Forms.TextBox txtMiejscNad;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.CheckBox chPole10;
        private System.Windows.Forms.CheckBox chPole2;
        private System.Windows.Forms.CheckBox chPole3;
        private System.Windows.Forms.CheckBox chPole4;
        private System.Windows.Forms.CheckBox chPole5;
        private System.Windows.Forms.CheckBox chPole6;
        private System.Windows.Forms.CheckBox chPole7;
        private System.Windows.Forms.CheckBox chPole8;
        private System.Windows.Forms.CheckBox chPole9;
        private System.Windows.Forms.CheckBox chPole1;
        private System.Windows.Forms.CheckBox chUwagi;
        private System.Windows.Forms.CheckBox chNrDok;
        private System.Windows.Forms.ComboBox cbPole10;
        private System.Windows.Forms.ComboBox cbPole9;
        private System.Windows.Forms.ComboBox cbPole8;
        private System.Windows.Forms.ComboBox cbPole7;
        private System.Windows.Forms.ComboBox cbPole6;
        private System.Windows.Forms.ComboBox cbPole5;
        private System.Windows.Forms.ComboBox cbPole4;
        private System.Windows.Forms.ComboBox cbPole3;
        private System.Windows.Forms.ComboBox cbPole2;
        private System.Windows.Forms.ComboBox cbPole1;
        private System.Windows.Forms.ComboBox cbUwagi;
        private System.Windows.Forms.ComboBox cbNrDok;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.CheckBox chZmienStatus;
        private System.Windows.Forms.ComboBox cbZmienStatus;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Panel pnlInne;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.Button btnOpHan;
        private System.Windows.Forms.Button btnOpMag;
        private System.Windows.Forms.Button btnOpZam;
        private System.Windows.Forms.Button btnSprawdzDPD;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.TextBox txtWaga;
        private System.Windows.Forms.RadioButton rbWagaDomyslna;
        private System.Windows.Forms.RadioButton rbWagaZMaga;
        private System.Windows.Forms.GroupBox groupBox13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.CheckBox chEtykieciarka;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.RadioButton rbZpl;
        private System.Windows.Forms.RadioButton rbEpl;
        private System.Windows.Forms.RadioButton rbPdf;
        private System.Windows.Forms.ComboBox cbDrukarkaEtykieta;
        private System.Windows.Forms.GroupBox groupBox12;
        private System.Windows.Forms.CheckBox chAutoWydrukPoZapisie;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.ComboBox cbWartoscBrutto;
        private System.Windows.Forms.CheckBox chWartoscBrutto;
        private System.Windows.Forms.TextBox txtImieNazwiskoNad;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.CheckBox chOpony;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.CheckBox chTiresExport;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox txtPobranie;
        private System.Windows.Forms.CheckBox chDeklWart;
        private System.Windows.Forms.GroupBox gbDeklarowanaWartosc;
        private System.Windows.Forms.TextBox txtDeklarowanaWartosc;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.CheckBox chOdbiorWlasny;
        private System.Windows.Forms.CheckBox chDorNaAdrPryw;
        private System.Windows.Forms.CheckBox chPrzesList;
        private System.Windows.Forms.CheckBox chDokZwrot;
        private System.Windows.Forms.CheckBox chDorGwaran;
        private System.Windows.Forms.CheckBox chPrzesZwrot;
        private System.Windows.Forms.CheckBox chPobranie;
        private System.Windows.Forms.GroupBox gbOdbiorWlasny;
        private System.Windows.Forms.RadioButton rbOsoba;
        private System.Windows.Forms.RadioButton rbFirma;
        private System.Windows.Forms.ComboBox cbDoreczenie;
        private System.Windows.Forms.TextBox txtNaOkreslonaGodzine;
        private System.Windows.Forms.CheckBox chDorDoRakWl;
        private System.Windows.Forms.CheckBox chWlaczSzablon;
        private System.Windows.Forms.GroupBox grSzablon;
        private System.Windows.Forms.CheckBox chPobierzKwoteZWfMaga;
        private System.Windows.Forms.CheckBox chPrzesylkaWartosciowaZWfMaga;
        private System.Windows.Forms.Button btnRaporty;
        private System.Windows.Forms.TextBox txtRaporty;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.CheckBox chSprawdzanieStatusow;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.RadioButton rbNazwaPelna;
        private System.Windows.Forms.RadioButton rbNazwa;
        private System.Windows.Forms.Panel pnlPobranieCOD;
        private System.Windows.Forms.GroupBox groupBox14;
        private System.Windows.Forms.CheckedListBox clbPobranieCOD;
        private System.Windows.Forms.Button btnPobierzPlatnosci;
        private System.Windows.Forms.ComboBox cbNrZamKlienta;
        private System.Windows.Forms.CheckBox chNrZamKlienta;
        private System.Windows.Forms.Label lWersjaAplikacji;
        private System.Windows.Forms.TextBox txtPudo;
        private System.Windows.Forms.CheckBox chPudo;
        private System.Windows.Forms.CheckBox chZamZapiszNrListuV2;
        private System.Windows.Forms.CheckBox chPallet;
        private System.Windows.Forms.Button btnFindPickup;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.TextBox txtDuty;
        private System.Windows.Forms.CheckBox chDuty;
        private System.Windows.Forms.CheckBox chDpdExpress;
        private System.Windows.Forms.Panel pnlUstawieniaPolDostawy;
        private System.Windows.Forms.CheckBox cbAktywnyAdresDostawy;
        private System.Windows.Forms.Panel pnlBlokujGenerowanieListu;
        private System.Windows.Forms.GroupBox gbBlokujGenerowanieDokumentow;
        private System.Windows.Forms.CheckedListBox clbBlokowanieFormDostaw;
        private System.Windows.Forms.Button btnPobierzFormy_Blokuj;
        private System.Windows.Forms.Panel pnlZwrotDokumentow;
        private System.Windows.Forms.GroupBox gbZwrotDokumentow;
        private System.Windows.Forms.CheckedListBox checkedListBox1;
        private System.Windows.Forms.Panel pnlGenerujZwrotDokumetnow;
        private System.Windows.Forms.GroupBox gbGenerujZwrotDokumentow;
        private System.Windows.Forms.CheckedListBox clbGenerujZwrotDokumetnow;
        private System.Windows.Forms.Button btnPobierzForm_Generuj;
    }
}