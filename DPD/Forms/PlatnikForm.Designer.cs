﻿namespace DPD.Forms
{
    partial class PlatnikForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            var resources = new System.ComponentModel.ComponentResourceManager(typeof(PlatnikForm));
            this.dgNumkat = new System.Windows.Forms.DataGridView();
            this.btnOk = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txtNumer = new System.Windows.Forms.TextBox();
            this.txtNazwa = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgNumkat)).BeginInit();
            this.SuspendLayout();
            // 
            // dgNumkat
            // 
            this.dgNumkat.AllowUserToAddRows = false;
            this.dgNumkat.AllowUserToResizeColumns = false;
            this.dgNumkat.AllowUserToResizeRows = false;
            this.dgNumkat.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgNumkat.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgNumkat.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgNumkat.Location = new System.Drawing.Point(0, 49);
            this.dgNumkat.MultiSelect = false;
            this.dgNumkat.Name = "dgNumkat";
            this.dgNumkat.ReadOnly = true;
            this.dgNumkat.RowHeadersVisible = false;
            this.dgNumkat.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgNumkat.Size = new System.Drawing.Size(331, 207);
            this.dgNumkat.TabIndex = 45;
            this.dgNumkat.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgNumkat_CellMouseDoubleClick);
            // 
            // btnOk
            // 
            this.btnOk.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOk.Location = new System.Drawing.Point(0, 256);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(331, 23);
            this.btnOk.TabIndex = 46;
            this.btnOk.Text = "Wybierz";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 47;
            this.label1.Text = "label1";
            // 
            // txtNumer
            // 
            this.txtNumer.Location = new System.Drawing.Point(0, 25);
            this.txtNumer.Name = "txtNumer";
            this.txtNumer.Size = new System.Drawing.Size(140, 20);
            this.txtNumer.TabIndex = 48;
            this.txtNumer.TextChanged += new System.EventHandler(this.txtNumer_TextChanged);
            this.txtNumer.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNumer_KeyPress);
            // 
            // txtNazwa
            // 
            this.txtNazwa.Location = new System.Drawing.Point(146, 25);
            this.txtNazwa.Name = "txtNazwa";
            this.txtNazwa.Size = new System.Drawing.Size(144, 20);
            this.txtNazwa.TabIndex = 49;
            this.txtNazwa.TextChanged += new System.EventHandler(this.txtNazwa_TextChanged);
            // 
            // PlatnikForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(331, 279);
            this.Controls.Add(this.txtNazwa);
            this.Controls.Add(this.txtNumer);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.dgNumkat);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximumSize = new System.Drawing.Size(347, 317);
            this.MinimumSize = new System.Drawing.Size(347, 317);
            this.Name = "PlatnikForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Płatnik";
            ((System.ComponentModel.ISupportInitialize)(this.dgNumkat)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgNumkat;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtNumer;
        private System.Windows.Forms.TextBox txtNazwa;

    }
}