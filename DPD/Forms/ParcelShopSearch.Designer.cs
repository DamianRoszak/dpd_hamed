﻿namespace DPD.Forms
{
    partial class ParcelShopSearch
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnFindParcelShop = new System.Windows.Forms.Button();
            this.rbFindByAddress = new System.Windows.Forms.RadioButton();
            this.rbFindByGeo = new System.Windows.Forms.RadioButton();
            this.gbAddress = new System.Windows.Forms.GroupBox();
            this.txtFindByAddressPostCode = new System.Windows.Forms.TextBox();
            this.txtFindByAddressCity = new System.Windows.Forms.TextBox();
            this.cbFindByAddressCountry = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.gbGeo = new System.Windows.Forms.GroupBox();
            this.numDistance = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            this.txtLatiude = new System.Windows.Forms.TextBox();
            this.txtLongitude = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.dgParcelShopList = new System.Windows.Forms.DataGridView();
            this.lError = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.Ulica = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.lPickupLatitude = new System.Windows.Forms.Label();
            this.lPickupLongitude = new System.Windows.Forms.Label();
            this.lPickupCity = new System.Windows.Forms.Label();
            this.lPickupPostCode = new System.Windows.Forms.Label();
            this.lPickupAddress = new System.Windows.Forms.Label();
            this.lPickupNumber = new System.Windows.Forms.Label();
            this.btnConfirm = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.lSatFrom = new System.Windows.Forms.Label();
            this.lFriFrom = new System.Windows.Forms.Label();
            this.lTheFrom = new System.Windows.Forms.Label();
            this.lSatTo = new System.Windows.Forms.Label();
            this.lFriTo = new System.Windows.Forms.Label();
            this.lTheTo = new System.Windows.Forms.Label();
            this.lWenTo = new System.Windows.Forms.Label();
            this.lThuTo = new System.Windows.Forms.Label();
            this.lMondayTo = new System.Windows.Forms.Label();
            this.lWenFrom = new System.Windows.Forms.Label();
            this.lThuFrom = new System.Windows.Forms.Label();
            this.lMondayFrom = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.dtDateFrom = new System.Windows.Forms.DateTimePicker();
            this.gbAddress.SuspendLayout();
            this.gbGeo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numDistance)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgParcelShopList)).BeginInit();
            this.SuspendLayout();
            // 
            // btnFindParcelShop
            // 
            this.btnFindParcelShop.Location = new System.Drawing.Point(822, 10);
            this.btnFindParcelShop.Name = "btnFindParcelShop";
            this.btnFindParcelShop.Size = new System.Drawing.Size(75, 28);
            this.btnFindParcelShop.TabIndex = 0;
            this.btnFindParcelShop.Text = "Szukaj";
            this.btnFindParcelShop.UseVisualStyleBackColor = true;
            this.btnFindParcelShop.Click += new System.EventHandler(this.btnFindParcelShop_Click);
            // 
            // rbFindByAddress
            // 
            this.rbFindByAddress.AutoSize = true;
            this.rbFindByAddress.Checked = true;
            this.rbFindByAddress.Location = new System.Drawing.Point(8, 19);
            this.rbFindByAddress.Name = "rbFindByAddress";
            this.rbFindByAddress.Size = new System.Drawing.Size(52, 17);
            this.rbFindByAddress.TabIndex = 1;
            this.rbFindByAddress.TabStop = true;
            this.rbFindByAddress.Text = "Adres";
            this.rbFindByAddress.UseVisualStyleBackColor = true;
            this.rbFindByAddress.CheckedChanged += new System.EventHandler(this.rbFindByAddress_CheckedChanged);
            // 
            // rbFindByGeo
            // 
            this.rbFindByGeo.AutoSize = true;
            this.rbFindByGeo.Location = new System.Drawing.Point(8, 57);
            this.rbFindByGeo.Name = "rbFindByGeo";
            this.rbFindByGeo.Size = new System.Drawing.Size(153, 17);
            this.rbFindByGeo.TabIndex = 2;
            this.rbFindByGeo.Text = "Współrzędne geograficzne";
            this.rbFindByGeo.UseVisualStyleBackColor = true;
            this.rbFindByGeo.CheckedChanged += new System.EventHandler(this.rbFindByGeo_CheckedChanged);
            // 
            // gbAddress
            // 
            this.gbAddress.Controls.Add(this.txtFindByAddressPostCode);
            this.gbAddress.Controls.Add(this.txtFindByAddressCity);
            this.gbAddress.Controls.Add(this.cbFindByAddressCountry);
            this.gbAddress.Controls.Add(this.label3);
            this.gbAddress.Controls.Add(this.label2);
            this.gbAddress.Controls.Add(this.label1);
            this.gbAddress.Location = new System.Drawing.Point(167, 6);
            this.gbAddress.Name = "gbAddress";
            this.gbAddress.Size = new System.Drawing.Size(564, 39);
            this.gbAddress.TabIndex = 3;
            this.gbAddress.TabStop = false;
            // 
            // txtFindByAddressPostCode
            // 
            this.txtFindByAddressPostCode.Location = new System.Drawing.Point(408, 12);
            this.txtFindByAddressPostCode.Name = "txtFindByAddressPostCode";
            this.txtFindByAddressPostCode.Size = new System.Drawing.Size(142, 20);
            this.txtFindByAddressPostCode.TabIndex = 5;
            // 
            // txtFindByAddressCity
            // 
            this.txtFindByAddressCity.Location = new System.Drawing.Point(180, 12);
            this.txtFindByAddressCity.Name = "txtFindByAddressCity";
            this.txtFindByAddressCity.Size = new System.Drawing.Size(142, 20);
            this.txtFindByAddressCity.TabIndex = 4;
            // 
            // cbFindByAddressCountry
            // 
            this.cbFindByAddressCountry.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbFindByAddressCountry.FormattingEnabled = true;
            this.cbFindByAddressCountry.Items.AddRange(new object[] {
            "POL",
            "ABW",
            "AFG",
            "AGO",
            "AIA",
            "ALA",
            "ALB",
            "AND",
            "ARE",
            "ARG",
            "ARM",
            "ASM",
            "ATA",
            "ATF",
            "ATG",
            "AUS",
            "AUT",
            "AZE",
            "BDI",
            "BEL",
            "BEN",
            "BES",
            "BFA",
            "BGD",
            "BGR",
            "BHR",
            "BHS",
            "BIH",
            "BLM",
            "BLR",
            "BLZ",
            "BMU",
            "BOL",
            "BRA",
            "BRB",
            "BRN",
            "BTN",
            "BVT",
            "BWA",
            "CAF",
            "CAN",
            "CCK",
            "CHE",
            "CHL",
            "CHN",
            "CIV",
            "CMR",
            "COD",
            "COG",
            "COK",
            "COL",
            "COM",
            "CPV",
            "CRI",
            "CUB",
            "CUW",
            "CXR",
            "CYM",
            "CYP",
            "CZE",
            "DEU",
            "DJI",
            "DMA",
            "DNK",
            "DOM",
            "DZA",
            "ECU",
            "EGY",
            "ERI",
            "ESH",
            "ESP",
            "EST",
            "ETH",
            "FIN",
            "FJI",
            "FLK",
            "FRA",
            "FRO",
            "FSM",
            "GAB",
            "GBR",
            "GEO",
            "GGY",
            "GHA",
            "GIB",
            "GIN",
            "GLP",
            "GMB",
            "GNB",
            "GNQ",
            "GRC",
            "GRD",
            "GRL",
            "GTM",
            "GUF",
            "GUM",
            "GUY",
            "HKG",
            "HMD",
            "HND",
            "HRV",
            "HTI",
            "HUN",
            "IDN",
            "IMN",
            "IND",
            "IOT",
            "IRL",
            "IRN",
            "IRQ",
            "ISL",
            "ISR",
            "ITA",
            "JAM",
            "JEY",
            "JOR",
            "JPN",
            "KAZ",
            "KEN",
            "KGZ",
            "KHM",
            "KIR",
            "KNA",
            "KOR",
            "KWT",
            "LAO",
            "LBN",
            "LBR",
            "LBY",
            "LCA",
            "LIE",
            "LKA",
            "LSO",
            "LTU",
            "LUX",
            "LVA",
            "MAC",
            "MAF",
            "MAR",
            "MCO",
            "MDA",
            "MDG",
            "MDV",
            "MEX",
            "MHL",
            "MKD",
            "MLI",
            "MLT",
            "MMR",
            "MNE",
            "MNG",
            "MNP",
            "MOZ",
            "MRT",
            "MSR",
            "MTQ",
            "MUS",
            "MWI",
            "MYS",
            "MYT",
            "NAM",
            "NCL",
            "NER",
            "NFK",
            "NGA",
            "NIC",
            "NIU",
            "NLD",
            "NOR",
            "NPL",
            "NRU",
            "NZL",
            "OMN",
            "PAK",
            "PAN",
            "PCN",
            "PER",
            "PHL",
            "PLW",
            "PNG",
            "POL",
            "PRI",
            "PRK",
            "PRT",
            "PRY",
            "PSE",
            "PYF",
            "QAT",
            "REU",
            "ROU",
            "RUS",
            "RWA",
            "SAU",
            "SDN",
            "SEN",
            "SGP",
            "SGS",
            "SHN",
            "SJM",
            "SLB",
            "SLE",
            "SLV",
            "SMR",
            "SOM",
            "SPM",
            "SRB",
            "SSD",
            "STP",
            "SUR",
            "SVK",
            "SVN",
            "SWE",
            "SWZ",
            "SXM",
            "SYC",
            "SYR",
            "TCA",
            "TCD",
            "TGO",
            "THA",
            "TJK",
            "TKL",
            "TKM",
            "TLS",
            "TON",
            "TTO",
            "TUN",
            "TUR",
            "TUV",
            "TWN",
            "TZA",
            "UGA",
            "UKR",
            "UMI",
            "URY",
            "USA",
            "UZB",
            "VAT",
            "VCT",
            "VEN",
            "VGB",
            "VIR",
            "VNM",
            "VUT",
            "WLF",
            "WSM",
            "YEM",
            "ZAF",
            "ZMB",
            "ZWE"});
            this.cbFindByAddressCountry.Location = new System.Drawing.Point(37, 12);
            this.cbFindByAddressCountry.Name = "cbFindByAddressCountry";
            this.cbFindByAddressCountry.Size = new System.Drawing.Size(63, 21);
            this.cbFindByAddressCountry.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(328, 16);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(74, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Kod pocztowy";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(106, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(68, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Miejscowość";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(25, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Kraj";
            // 
            // gbGeo
            // 
            this.gbGeo.Controls.Add(this.numDistance);
            this.gbGeo.Controls.Add(this.label6);
            this.gbGeo.Controls.Add(this.txtLatiude);
            this.gbGeo.Controls.Add(this.txtLongitude);
            this.gbGeo.Controls.Add(this.label4);
            this.gbGeo.Controls.Add(this.label5);
            this.gbGeo.Enabled = false;
            this.gbGeo.Location = new System.Drawing.Point(167, 44);
            this.gbGeo.Name = "gbGeo";
            this.gbGeo.Size = new System.Drawing.Size(712, 39);
            this.gbGeo.TabIndex = 6;
            this.gbGeo.TabStop = false;
            // 
            // numDistance
            // 
            this.numDistance.Location = new System.Drawing.Point(627, 13);
            this.numDistance.Maximum = new decimal(new int[] {
            50000,
            0,
            0,
            0});
            this.numDistance.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.numDistance.Name = "numDistance";
            this.numDistance.Size = new System.Drawing.Size(79, 20);
            this.numDistance.TabIndex = 7;
            this.numDistance.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(547, 16);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(73, 13);
            this.label6.TabIndex = 6;
            this.label6.Text = "Odległość (m)";
            // 
            // txtLatiude
            // 
            this.txtLatiude.Location = new System.Drawing.Point(399, 12);
            this.txtLatiude.Name = "txtLatiude";
            this.txtLatiude.Size = new System.Drawing.Size(142, 20);
            this.txtLatiude.TabIndex = 5;
            // 
            // txtLongitude
            // 
            this.txtLongitude.Location = new System.Drawing.Point(124, 13);
            this.txtLongitude.Name = "txtLongitude";
            this.txtLongitude.Size = new System.Drawing.Size(142, 20);
            this.txtLongitude.TabIndex = 4;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(272, 15);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(121, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = "Szerokość geograficzna";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 15);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(112, 13);
            this.label5.TabIndex = 1;
            this.label5.Text = "Długość geograficzna";
            // 
            // dgParcelShopList
            // 
            this.dgParcelShopList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgParcelShopList.Location = new System.Drawing.Point(11, 119);
            this.dgParcelShopList.Margin = new System.Windows.Forms.Padding(1);
            this.dgParcelShopList.MultiSelect = false;
            this.dgParcelShopList.Name = "dgParcelShopList";
            this.dgParcelShopList.ReadOnly = true;
            this.dgParcelShopList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgParcelShopList.Size = new System.Drawing.Size(885, 204);
            this.dgParcelShopList.TabIndex = 7;
            this.dgParcelShopList.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgParcelShopList_CellMouseClick);
            this.dgParcelShopList.SelectionChanged += new System.EventHandler(this.dgParcelShopList_SelectionChanged);
            // 
            // lError
            // 
            this.lError.AutoSize = true;
            this.lError.Dock = System.Windows.Forms.DockStyle.Top;
            this.lError.ForeColor = System.Drawing.Color.Red;
            this.lError.Location = new System.Drawing.Point(0, 0);
            this.lError.Name = "lError";
            this.lError.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lError.Size = new System.Drawing.Size(22, 13);
            this.lError.TabIndex = 8;
            this.lError.Text = "OK";
            this.lError.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lError.Visible = false;
            this.lError.TextChanged += new System.EventHandler(this.lError_TextChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(95, 336);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(38, 13);
            this.label7.TabIndex = 9;
            this.label7.Text = "Numer";
            this.label7.Click += new System.EventHandler(this.label7_Click);
            // 
            // Ulica
            // 
            this.Ulica.AutoSize = true;
            this.Ulica.Location = new System.Drawing.Point(102, 358);
            this.Ulica.Name = "Ulica";
            this.Ulica.Size = new System.Drawing.Size(31, 13);
            this.Ulica.TabIndex = 10;
            this.Ulica.Text = "Ulica";
            this.Ulica.Click += new System.EventHandler(this.Ulica_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(62, 380);
            this.label9.Name = "label9";
            this.label9.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label9.Size = new System.Drawing.Size(74, 13);
            this.label9.TabIndex = 11;
            this.label9.Text = "Kod pocztowy";
            this.label9.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.label9.Click += new System.EventHandler(this.label9_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(12, 446);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(121, 13);
            this.label10.TabIndex = 14;
            this.label10.Text = "Szerokość geograficzna";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(21, 424);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(112, 13);
            this.label11.TabIndex = 13;
            this.label11.Text = "Długość geograficzna";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(95, 402);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(38, 13);
            this.label12.TabIndex = 12;
            this.label12.Text = "Miasto";
            this.label12.Click += new System.EventHandler(this.label12_Click);
            // 
            // lPickupLatitude
            // 
            this.lPickupLatitude.AutoSize = true;
            this.lPickupLatitude.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lPickupLatitude.Location = new System.Drawing.Point(143, 446);
            this.lPickupLatitude.Name = "lPickupLatitude";
            this.lPickupLatitude.Size = new System.Drawing.Size(11, 13);
            this.lPickupLatitude.TabIndex = 20;
            this.lPickupLatitude.Text = "-";
            // 
            // lPickupLongitude
            // 
            this.lPickupLongitude.AutoSize = true;
            this.lPickupLongitude.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lPickupLongitude.Location = new System.Drawing.Point(143, 424);
            this.lPickupLongitude.Name = "lPickupLongitude";
            this.lPickupLongitude.Size = new System.Drawing.Size(11, 13);
            this.lPickupLongitude.TabIndex = 19;
            this.lPickupLongitude.Text = "-";
            // 
            // lPickupCity
            // 
            this.lPickupCity.AutoSize = true;
            this.lPickupCity.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lPickupCity.Location = new System.Drawing.Point(143, 402);
            this.lPickupCity.Name = "lPickupCity";
            this.lPickupCity.Size = new System.Drawing.Size(11, 13);
            this.lPickupCity.TabIndex = 18;
            this.lPickupCity.Text = "-";
            // 
            // lPickupPostCode
            // 
            this.lPickupPostCode.AutoSize = true;
            this.lPickupPostCode.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lPickupPostCode.Location = new System.Drawing.Point(143, 380);
            this.lPickupPostCode.Name = "lPickupPostCode";
            this.lPickupPostCode.Size = new System.Drawing.Size(11, 13);
            this.lPickupPostCode.TabIndex = 17;
            this.lPickupPostCode.Text = "-";
            // 
            // lPickupAddress
            // 
            this.lPickupAddress.AutoSize = true;
            this.lPickupAddress.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lPickupAddress.Location = new System.Drawing.Point(143, 358);
            this.lPickupAddress.Name = "lPickupAddress";
            this.lPickupAddress.Size = new System.Drawing.Size(11, 13);
            this.lPickupAddress.TabIndex = 16;
            this.lPickupAddress.Text = "-";
            // 
            // lPickupNumber
            // 
            this.lPickupNumber.AutoSize = true;
            this.lPickupNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lPickupNumber.Location = new System.Drawing.Point(143, 336);
            this.lPickupNumber.Name = "lPickupNumber";
            this.lPickupNumber.Size = new System.Drawing.Size(11, 13);
            this.lPickupNumber.TabIndex = 15;
            this.lPickupNumber.Text = "-";
            // 
            // btnConfirm
            // 
            this.btnConfirm.Location = new System.Drawing.Point(825, 438);
            this.btnConfirm.Name = "btnConfirm";
            this.btnConfirm.Size = new System.Drawing.Size(75, 28);
            this.btnConfirm.TabIndex = 21;
            this.btnConfirm.Text = "Wybierz";
            this.btnConfirm.UseVisualStyleBackColor = true;
            this.btnConfirm.Click += new System.EventHandler(this.btnConfirm_Click);
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(744, 438);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 28);
            this.btnClose.TabIndex = 22;
            this.btnClose.Text = "Anuluj";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label8.Location = new System.Drawing.Point(524, 336);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(104, 13);
            this.label8.TabIndex = 23;
            this.label8.Text = "Godziny otwarcia";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(535, 358);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(69, 13);
            this.label13.TabIndex = 24;
            this.label13.Text = "Poniedziałek";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(562, 380);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(42, 13);
            this.label14.TabIndex = 25;
            this.label14.Text = "Wtorek";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(562, 402);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(35, 13);
            this.label15.TabIndex = 26;
            this.label15.Text = "Środa";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(722, 402);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(41, 13);
            this.label16.TabIndex = 29;
            this.label16.Text = "Sobota";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(722, 380);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(37, 13);
            this.label17.TabIndex = 28;
            this.label17.Text = "Piątek";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(722, 358);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(51, 13);
            this.label18.TabIndex = 27;
            this.label18.Text = "Czwartek";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label22.Location = new System.Drawing.Point(832, 402);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(11, 13);
            this.label22.TabIndex = 35;
            this.label22.Text = "-";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label23.Location = new System.Drawing.Point(832, 380);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(11, 13);
            this.label23.TabIndex = 34;
            this.label23.Text = "-";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label24.Location = new System.Drawing.Point(832, 358);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(11, 13);
            this.label24.TabIndex = 33;
            this.label24.Text = "-";
            // 
            // lSatFrom
            // 
            this.lSatFrom.AutoSize = true;
            this.lSatFrom.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lSatFrom.Location = new System.Drawing.Point(790, 402);
            this.lSatFrom.Name = "lSatFrom";
            this.lSatFrom.Size = new System.Drawing.Size(0, 13);
            this.lSatFrom.TabIndex = 38;
            // 
            // lFriFrom
            // 
            this.lFriFrom.AutoSize = true;
            this.lFriFrom.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lFriFrom.Location = new System.Drawing.Point(790, 380);
            this.lFriFrom.Name = "lFriFrom";
            this.lFriFrom.Size = new System.Drawing.Size(0, 13);
            this.lFriFrom.TabIndex = 37;
            // 
            // lTheFrom
            // 
            this.lTheFrom.AutoSize = true;
            this.lTheFrom.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lTheFrom.Location = new System.Drawing.Point(790, 358);
            this.lTheFrom.Name = "lTheFrom";
            this.lTheFrom.Size = new System.Drawing.Size(0, 13);
            this.lTheFrom.TabIndex = 36;
            // 
            // lSatTo
            // 
            this.lSatTo.AutoSize = true;
            this.lSatTo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lSatTo.Location = new System.Drawing.Point(846, 402);
            this.lSatTo.Name = "lSatTo";
            this.lSatTo.Size = new System.Drawing.Size(0, 13);
            this.lSatTo.TabIndex = 41;
            // 
            // lFriTo
            // 
            this.lFriTo.AutoSize = true;
            this.lFriTo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lFriTo.Location = new System.Drawing.Point(846, 380);
            this.lFriTo.Name = "lFriTo";
            this.lFriTo.Size = new System.Drawing.Size(0, 13);
            this.lFriTo.TabIndex = 40;
            // 
            // lTheTo
            // 
            this.lTheTo.AutoSize = true;
            this.lTheTo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lTheTo.Location = new System.Drawing.Point(846, 358);
            this.lTheTo.Name = "lTheTo";
            this.lTheTo.Size = new System.Drawing.Size(0, 13);
            this.lTheTo.TabIndex = 39;
            // 
            // lWenTo
            // 
            this.lWenTo.AutoSize = true;
            this.lWenTo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lWenTo.Location = new System.Drawing.Point(674, 402);
            this.lWenTo.Name = "lWenTo";
            this.lWenTo.Size = new System.Drawing.Size(0, 13);
            this.lWenTo.TabIndex = 50;
            // 
            // lThuTo
            // 
            this.lThuTo.AutoSize = true;
            this.lThuTo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lThuTo.Location = new System.Drawing.Point(674, 380);
            this.lThuTo.Name = "lThuTo";
            this.lThuTo.Size = new System.Drawing.Size(0, 13);
            this.lThuTo.TabIndex = 49;
            // 
            // lMondayTo
            // 
            this.lMondayTo.AutoSize = true;
            this.lMondayTo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lMondayTo.Location = new System.Drawing.Point(674, 358);
            this.lMondayTo.Name = "lMondayTo";
            this.lMondayTo.Size = new System.Drawing.Size(0, 13);
            this.lMondayTo.TabIndex = 48;
            // 
            // lWenFrom
            // 
            this.lWenFrom.AutoSize = true;
            this.lWenFrom.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lWenFrom.Location = new System.Drawing.Point(617, 402);
            this.lWenFrom.Name = "lWenFrom";
            this.lWenFrom.Size = new System.Drawing.Size(0, 13);
            this.lWenFrom.TabIndex = 47;
            // 
            // lThuFrom
            // 
            this.lThuFrom.AutoSize = true;
            this.lThuFrom.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lThuFrom.Location = new System.Drawing.Point(617, 380);
            this.lThuFrom.Name = "lThuFrom";
            this.lThuFrom.Size = new System.Drawing.Size(0, 13);
            this.lThuFrom.TabIndex = 46;
            // 
            // lMondayFrom
            // 
            this.lMondayFrom.AutoSize = true;
            this.lMondayFrom.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lMondayFrom.Location = new System.Drawing.Point(617, 358);
            this.lMondayFrom.Name = "lMondayFrom";
            this.lMondayFrom.Size = new System.Drawing.Size(0, 13);
            this.lMondayFrom.TabIndex = 45;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label34.Location = new System.Drawing.Point(660, 402);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(11, 13);
            this.label34.TabIndex = 44;
            this.label34.Text = "-";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label35.Location = new System.Drawing.Point(660, 380);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(11, 13);
            this.label35.TabIndex = 43;
            this.label35.Text = "-";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label36.Location = new System.Drawing.Point(660, 358);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(11, 13);
            this.label36.TabIndex = 42;
            this.label36.Text = "-";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(21, 90);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(85, 13);
            this.label19.TabIndex = 8;
            this.label19.Text = "Data doręczenia";
            // 
            // dtDateFrom
            // 
            this.dtDateFrom.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtDateFrom.Location = new System.Drawing.Point(112, 87);
            this.dtDateFrom.Name = "dtDateFrom";
            this.dtDateFrom.Size = new System.Drawing.Size(86, 20);
            this.dtDateFrom.TabIndex = 51;
            // 
            // ParcelShopSearch
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.ClientSize = new System.Drawing.Size(912, 478);
            this.Controls.Add(this.dtDateFrom);
            this.Controls.Add(this.lWenTo);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.lThuTo);
            this.Controls.Add(this.lMondayTo);
            this.Controls.Add(this.lWenFrom);
            this.Controls.Add(this.lThuFrom);
            this.Controls.Add(this.lMondayFrom);
            this.Controls.Add(this.label34);
            this.Controls.Add(this.label35);
            this.Controls.Add(this.label36);
            this.Controls.Add(this.lSatTo);
            this.Controls.Add(this.lFriTo);
            this.Controls.Add(this.lTheTo);
            this.Controls.Add(this.lSatFrom);
            this.Controls.Add(this.lFriFrom);
            this.Controls.Add(this.lTheFrom);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnConfirm);
            this.Controls.Add(this.lPickupLatitude);
            this.Controls.Add(this.lPickupLongitude);
            this.Controls.Add(this.lPickupCity);
            this.Controls.Add(this.lPickupPostCode);
            this.Controls.Add(this.lPickupAddress);
            this.Controls.Add(this.lPickupNumber);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.Ulica);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.lError);
            this.Controls.Add(this.dgParcelShopList);
            this.Controls.Add(this.gbGeo);
            this.Controls.Add(this.gbAddress);
            this.Controls.Add(this.rbFindByGeo);
            this.Controls.Add(this.rbFindByAddress);
            this.Controls.Add(this.btnFindParcelShop);
            this.Name = "ParcelShopSearch";
            this.Text = "Znajdź punkt Pickup";
            this.gbAddress.ResumeLayout(false);
            this.gbAddress.PerformLayout();
            this.gbGeo.ResumeLayout(false);
            this.gbGeo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numDistance)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgParcelShopList)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnFindParcelShop;
        private System.Windows.Forms.RadioButton rbFindByAddress;
        private System.Windows.Forms.RadioButton rbFindByGeo;
        private System.Windows.Forms.GroupBox gbAddress;
        private System.Windows.Forms.TextBox txtFindByAddressPostCode;
        private System.Windows.Forms.TextBox txtFindByAddressCity;
        private System.Windows.Forms.ComboBox cbFindByAddressCountry;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox gbGeo;
        private System.Windows.Forms.TextBox txtLatiude;
        private System.Windows.Forms.TextBox txtLongitude;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DataGridView dgParcelShopList;
        private System.Windows.Forms.Label lError;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.NumericUpDown numDistance;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label Ulica;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label lPickupLatitude;
        private System.Windows.Forms.Label lPickupLongitude;
        private System.Windows.Forms.Label lPickupCity;
        private System.Windows.Forms.Label lPickupPostCode;
        private System.Windows.Forms.Label lPickupAddress;
        private System.Windows.Forms.Label lPickupNumber;
        private System.Windows.Forms.Button btnConfirm;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label lSatFrom;
        private System.Windows.Forms.Label lFriFrom;
        private System.Windows.Forms.Label lTheFrom;
        private System.Windows.Forms.Label lSatTo;
        private System.Windows.Forms.Label lFriTo;
        private System.Windows.Forms.Label lTheTo;
        private System.Windows.Forms.Label lWenTo;
        private System.Windows.Forms.Label lThuTo;
        private System.Windows.Forms.Label lMondayTo;
        private System.Windows.Forms.Label lWenFrom;
        private System.Windows.Forms.Label lThuFrom;
        private System.Windows.Forms.Label lMondayFrom;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.DateTimePicker dtDateFrom;
    }
}