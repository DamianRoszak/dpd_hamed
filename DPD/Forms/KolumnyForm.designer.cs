﻿namespace DPD.Forms
{
    partial class KolumnyForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.chNrDokumentu = new System.Windows.Forms.CheckBox();
            this.chAdresNadawcy = new System.Windows.Forms.CheckBox();
            this.chAdresOdbiorcy = new System.Windows.Forms.CheckBox();
            this.chNrListu = new System.Windows.Forms.CheckBox();
            this.chDataListu = new System.Windows.Forms.CheckBox();
            this.chDataProtokolu = new System.Windows.Forms.CheckBox();
            this.chEtykieta = new System.Windows.Forms.CheckBox();
            this.chPobranieCOD = new System.Windows.Forms.CheckBox();
            this.chPrzesylkaWartosciowa = new System.Windows.Forms.CheckBox();
            this.chStatus = new System.Windows.Forms.CheckBox();
            this.chWaga = new System.Windows.Forms.CheckBox();
            this.chUslugi = new System.Windows.Forms.CheckBox();
            this.chDataOstatniegoSprawdzenia = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.chNrProtokolu = new System.Windows.Forms.CheckBox();
            this.chPaczki = new System.Windows.Forms.CheckBox();
            this.chStatusPrzycisk = new System.Windows.Forms.CheckBox();
            this.chId_mag = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // chNrDokumentu
            // 
            this.chNrDokumentu.AutoSize = true;
            this.chNrDokumentu.Location = new System.Drawing.Point(27, 30);
            this.chNrDokumentu.Name = "chNrDokumentu";
            this.chNrDokumentu.Size = new System.Drawing.Size(113, 17);
            this.chNrDokumentu.TabIndex = 0;
            this.chNrDokumentu.Text = "Numer dokumentu";
            this.chNrDokumentu.UseVisualStyleBackColor = true;
            // 
            // chAdresNadawcy
            // 
            this.chAdresNadawcy.AutoSize = true;
            this.chAdresNadawcy.Location = new System.Drawing.Point(27, 53);
            this.chAdresNadawcy.Name = "chAdresNadawcy";
            this.chAdresNadawcy.Size = new System.Drawing.Size(99, 17);
            this.chAdresNadawcy.TabIndex = 1;
            this.chAdresNadawcy.Text = "Adres nadawcy";
            this.chAdresNadawcy.UseVisualStyleBackColor = true;
            // 
            // chAdresOdbiorcy
            // 
            this.chAdresOdbiorcy.AutoSize = true;
            this.chAdresOdbiorcy.Location = new System.Drawing.Point(27, 76);
            this.chAdresOdbiorcy.Name = "chAdresOdbiorcy";
            this.chAdresOdbiorcy.Size = new System.Drawing.Size(96, 17);
            this.chAdresOdbiorcy.TabIndex = 2;
            this.chAdresOdbiorcy.Text = "Adres odbiorcy";
            this.chAdresOdbiorcy.UseVisualStyleBackColor = true;
            // 
            // chNrListu
            // 
            this.chNrListu.AutoSize = true;
            this.chNrListu.Location = new System.Drawing.Point(27, 99);
            this.chNrListu.Name = "chNrListu";
            this.chNrListu.Size = new System.Drawing.Size(58, 17);
            this.chNrListu.TabIndex = 3;
            this.chNrListu.Text = "Nr listu";
            this.chNrListu.UseVisualStyleBackColor = true;
            // 
            // chDataListu
            // 
            this.chDataListu.AutoSize = true;
            this.chDataListu.Location = new System.Drawing.Point(27, 145);
            this.chDataListu.Name = "chDataListu";
            this.chDataListu.Size = new System.Drawing.Size(70, 17);
            this.chDataListu.TabIndex = 4;
            this.chDataListu.Text = "Data listu";
            this.chDataListu.UseVisualStyleBackColor = true;
            // 
            // chDataProtokolu
            // 
            this.chDataProtokolu.AutoSize = true;
            this.chDataProtokolu.Location = new System.Drawing.Point(27, 168);
            this.chDataProtokolu.Name = "chDataProtokolu";
            this.chDataProtokolu.Size = new System.Drawing.Size(98, 17);
            this.chDataProtokolu.TabIndex = 5;
            this.chDataProtokolu.Text = "Data protokołu";
            this.chDataProtokolu.UseVisualStyleBackColor = true;
            // 
            // chEtykieta
            // 
            this.chEtykieta.AutoSize = true;
            this.chEtykieta.Location = new System.Drawing.Point(27, 191);
            this.chEtykieta.Name = "chEtykieta";
            this.chEtykieta.Size = new System.Drawing.Size(64, 17);
            this.chEtykieta.TabIndex = 6;
            this.chEtykieta.Text = "Etykieta";
            this.chEtykieta.UseVisualStyleBackColor = true;
            // 
            // chPobranieCOD
            // 
            this.chPobranieCOD.AutoSize = true;
            this.chPobranieCOD.Location = new System.Drawing.Point(27, 237);
            this.chPobranieCOD.Name = "chPobranieCOD";
            this.chPobranieCOD.Size = new System.Drawing.Size(94, 17);
            this.chPobranieCOD.TabIndex = 7;
            this.chPobranieCOD.Text = "Pobranie COD";
            this.chPobranieCOD.UseVisualStyleBackColor = true;
            // 
            // chPrzesylkaWartosciowa
            // 
            this.chPrzesylkaWartosciowa.AutoSize = true;
            this.chPrzesylkaWartosciowa.Location = new System.Drawing.Point(27, 260);
            this.chPrzesylkaWartosciowa.Name = "chPrzesylkaWartosciowa";
            this.chPrzesylkaWartosciowa.Size = new System.Drawing.Size(135, 17);
            this.chPrzesylkaWartosciowa.TabIndex = 8;
            this.chPrzesylkaWartosciowa.Text = "Przesyłka wartościowa";
            this.chPrzesylkaWartosciowa.UseVisualStyleBackColor = true;
            // 
            // chStatus
            // 
            this.chStatus.AutoSize = true;
            this.chStatus.Location = new System.Drawing.Point(27, 329);
            this.chStatus.Name = "chStatus";
            this.chStatus.Size = new System.Drawing.Size(56, 17);
            this.chStatus.TabIndex = 11;
            this.chStatus.Text = "Status";
            this.chStatus.UseVisualStyleBackColor = true;
            // 
            // chWaga
            // 
            this.chWaga.AutoSize = true;
            this.chWaga.Location = new System.Drawing.Point(27, 306);
            this.chWaga.Name = "chWaga";
            this.chWaga.Size = new System.Drawing.Size(55, 17);
            this.chWaga.TabIndex = 10;
            this.chWaga.Text = "Waga";
            this.chWaga.UseVisualStyleBackColor = true;
            // 
            // chUslugi
            // 
            this.chUslugi.AutoSize = true;
            this.chUslugi.Location = new System.Drawing.Point(27, 283);
            this.chUslugi.Name = "chUslugi";
            this.chUslugi.Size = new System.Drawing.Size(57, 17);
            this.chUslugi.TabIndex = 9;
            this.chUslugi.Text = "Usługi";
            this.chUslugi.UseVisualStyleBackColor = true;
            // 
            // chDataOstatniegoSprawdzenia
            // 
            this.chDataOstatniegoSprawdzenia.AutoSize = true;
            this.chDataOstatniegoSprawdzenia.Location = new System.Drawing.Point(27, 375);
            this.chDataOstatniegoSprawdzenia.Name = "chDataOstatniegoSprawdzenia";
            this.chDataOstatniegoSprawdzenia.Size = new System.Drawing.Size(163, 17);
            this.chDataOstatniegoSprawdzenia.TabIndex = 12;
            this.chDataOstatniegoSprawdzenia.Text = "Data ostatniego sprawdzenia";
            this.chDataOstatniegoSprawdzenia.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 13);
            this.label1.TabIndex = 13;
            this.label1.Text = "Widoczne kolumny:";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(75, 430);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 14;
            this.button1.Text = "Zapisz";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // chNrProtokolu
            // 
            this.chNrProtokolu.AutoSize = true;
            this.chNrProtokolu.Location = new System.Drawing.Point(27, 122);
            this.chNrProtokolu.Name = "chNrProtokolu";
            this.chNrProtokolu.Size = new System.Drawing.Size(86, 17);
            this.chNrProtokolu.TabIndex = 15;
            this.chNrProtokolu.Text = "Nr protokołu";
            this.chNrProtokolu.UseVisualStyleBackColor = true;
            // 
            // chPaczki
            // 
            this.chPaczki.AutoSize = true;
            this.chPaczki.Location = new System.Drawing.Point(27, 214);
            this.chPaczki.Name = "chPaczki";
            this.chPaczki.Size = new System.Drawing.Size(58, 17);
            this.chPaczki.TabIndex = 16;
            this.chPaczki.Text = "Paczki";
            this.chPaczki.UseVisualStyleBackColor = true;
            // 
            // chStatusPrzycisk
            // 
            this.chStatusPrzycisk.AutoSize = true;
            this.chStatusPrzycisk.Location = new System.Drawing.Point(27, 352);
            this.chStatusPrzycisk.Name = "chStatusPrzycisk";
            this.chStatusPrzycisk.Size = new System.Drawing.Size(103, 17);
            this.chStatusPrzycisk.TabIndex = 17;
            this.chStatusPrzycisk.Text = "Status (przycisk)";
            this.chStatusPrzycisk.UseVisualStyleBackColor = true;
            // 
            // chId_mag
            // 
            this.chId_mag.AutoSize = true;
            this.chId_mag.Location = new System.Drawing.Point(27, 398);
            this.chId_mag.Name = "chId_mag";
            this.chId_mag.Size = new System.Drawing.Size(86, 17);
            this.chId_mag.TabIndex = 18;
            this.chId_mag.Text = "Id magazynu";
            this.chId_mag.UseVisualStyleBackColor = true;
            // 
            // KolumnyForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(220, 470);
            this.Controls.Add(this.chId_mag);
            this.Controls.Add(this.chStatusPrzycisk);
            this.Controls.Add(this.chPaczki);
            this.Controls.Add(this.chNrProtokolu);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.chDataOstatniegoSprawdzenia);
            this.Controls.Add(this.chStatus);
            this.Controls.Add(this.chWaga);
            this.Controls.Add(this.chUslugi);
            this.Controls.Add(this.chPrzesylkaWartosciowa);
            this.Controls.Add(this.chPobranieCOD);
            this.Controls.Add(this.chEtykieta);
            this.Controls.Add(this.chDataProtokolu);
            this.Controls.Add(this.chDataListu);
            this.Controls.Add(this.chNrListu);
            this.Controls.Add(this.chAdresOdbiorcy);
            this.Controls.Add(this.chAdresNadawcy);
            this.Controls.Add(this.chNrDokumentu);
            this.Name = "KolumnyForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Konfiguracja kolumn";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox chNrDokumentu;
        private System.Windows.Forms.CheckBox chAdresNadawcy;
        private System.Windows.Forms.CheckBox chAdresOdbiorcy;
        private System.Windows.Forms.CheckBox chNrListu;
        private System.Windows.Forms.CheckBox chDataListu;
        private System.Windows.Forms.CheckBox chDataProtokolu;
        private System.Windows.Forms.CheckBox chEtykieta;
        private System.Windows.Forms.CheckBox chPobranieCOD;
        private System.Windows.Forms.CheckBox chPrzesylkaWartosciowa;
        private System.Windows.Forms.CheckBox chStatus;
        private System.Windows.Forms.CheckBox chWaga;
        private System.Windows.Forms.CheckBox chUslugi;
        private System.Windows.Forms.CheckBox chDataOstatniegoSprawdzenia;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.CheckBox chNrProtokolu;
        private System.Windows.Forms.CheckBox chPaczki;
        private System.Windows.Forms.CheckBox chStatusPrzycisk;
        private System.Windows.Forms.CheckBox chId_mag;
    }
}