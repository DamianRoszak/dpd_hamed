﻿namespace DPD.Forms
{
    partial class SzablonForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SzablonForm));
            this.txtPobranie = new System.Windows.Forms.TextBox();
            this.chOdbiorWlasny = new System.Windows.Forms.CheckBox();
            this.chDeklWart = new System.Windows.Forms.CheckBox();
            this.chDorGwaran = new System.Windows.Forms.CheckBox();
            this.txtNaOkreslonaGodzine = new System.Windows.Forms.TextBox();
            this.cbDoreczenie = new System.Windows.Forms.ComboBox();
            this.gbOdbiorWlasny = new System.Windows.Forms.GroupBox();
            this.rbOsoba = new System.Windows.Forms.RadioButton();
            this.rbFirma = new System.Windows.Forms.RadioButton();
            this.chPobranie = new System.Windows.Forms.CheckBox();
            this.chPrzesZwrot = new System.Windows.Forms.CheckBox();
            this.chDokZwrot = new System.Windows.Forms.CheckBox();
            this.chPrzesList = new System.Windows.Forms.CheckBox();
            this.chDorNaAdrPryw = new System.Windows.Forms.CheckBox();
            this.gbDeklarowanaWartosc = new System.Windows.Forms.GroupBox();
            this.txtDeklarowanaWartosc = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.chOpony = new System.Windows.Forms.CheckBox();
            this.label21 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.chTiresExport = new System.Windows.Forms.CheckBox();
            this.chDorDoRakWl = new System.Windows.Forms.CheckBox();
            this.btnZatwierdz = new System.Windows.Forms.Button();
            this.btnAnuluj = new System.Windows.Forms.Button();
            this.chPobierzKwoteZWfMaga = new System.Windows.Forms.CheckBox();
            this.chPrzesylkaWartosciowaZWfMaga = new System.Windows.Forms.CheckBox();
            this.chPallet = new System.Windows.Forms.CheckBox();
            this.txtPudo = new System.Windows.Forms.TextBox();
            this.chPudo = new System.Windows.Forms.CheckBox();
            this.btnFindPickup = new System.Windows.Forms.Button();
            this.chDpdExpress = new System.Windows.Forms.CheckBox();
            this.txtDuty = new System.Windows.Forms.TextBox();
            this.chDuty = new System.Windows.Forms.CheckBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.gbOdbiorWlasny.SuspendLayout();
            this.gbDeklarowanaWartosc.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtPobranie
            // 
            this.txtPobranie.Enabled = false;
            this.txtPobranie.ForeColor = System.Drawing.SystemColors.MenuText;
            this.txtPobranie.Location = new System.Drawing.Point(106, 115);
            this.txtPobranie.Name = "txtPobranie";
            this.txtPobranie.Size = new System.Drawing.Size(52, 20);
            this.txtPobranie.TabIndex = 239;
            this.txtPobranie.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // chOdbiorWlasny
            // 
            this.chOdbiorWlasny.AutoSize = true;
            this.chOdbiorWlasny.ForeColor = System.Drawing.SystemColors.MenuText;
            this.chOdbiorWlasny.Location = new System.Drawing.Point(8, 158);
            this.chOdbiorWlasny.Name = "chOdbiorWlasny";
            this.chOdbiorWlasny.Size = new System.Drawing.Size(94, 17);
            this.chOdbiorWlasny.TabIndex = 236;
            this.chOdbiorWlasny.Text = "Odbiór własny";
            this.chOdbiorWlasny.UseVisualStyleBackColor = true;
            this.chOdbiorWlasny.CheckedChanged += new System.EventHandler(this.chOdbiorWlasny_CheckedChanged);
            // 
            // chDeklWart
            // 
            this.chDeklWart.AutoSize = true;
            this.chDeklWart.ForeColor = System.Drawing.SystemColors.MenuText;
            this.chDeklWart.Location = new System.Drawing.Point(224, 134);
            this.chDeklWart.Name = "chDeklWart";
            this.chDeklWart.Size = new System.Drawing.Size(135, 17);
            this.chDeklWart.TabIndex = 230;
            this.chDeklWart.Text = "Przesyłka wartościowa";
            this.chDeklWart.UseVisualStyleBackColor = true;
            this.chDeklWart.CheckedChanged += new System.EventHandler(this.chDeklWart_CheckedChanged);
            // 
            // chDorGwaran
            // 
            this.chDorGwaran.AutoSize = true;
            this.chDorGwaran.ForeColor = System.Drawing.SystemColors.MenuText;
            this.chDorGwaran.Location = new System.Drawing.Point(8, 34);
            this.chDorGwaran.Name = "chDorGwaran";
            this.chDorGwaran.Size = new System.Drawing.Size(153, 17);
            this.chDorGwaran.TabIndex = 232;
            this.chDorGwaran.Text = "Doręczenia gwarantowane";
            this.chDorGwaran.UseVisualStyleBackColor = true;
            this.chDorGwaran.CheckedChanged += new System.EventHandler(this.chDorGwaran_CheckedChanged);
            // 
            // txtNaOkreslonaGodzine
            // 
            this.txtNaOkreslonaGodzine.Location = new System.Drawing.Point(164, 53);
            this.txtNaOkreslonaGodzine.Name = "txtNaOkreslonaGodzine";
            this.txtNaOkreslonaGodzine.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtNaOkreslonaGodzine.Size = new System.Drawing.Size(45, 20);
            this.txtNaOkreslonaGodzine.TabIndex = 245;
            this.txtNaOkreslonaGodzine.Visible = false;
            // 
            // cbDoreczenie
            // 
            this.cbDoreczenie.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbDoreczenie.FormattingEnabled = true;
            this.cbDoreczenie.Location = new System.Drawing.Point(27, 52);
            this.cbDoreczenie.Name = "cbDoreczenie";
            this.cbDoreczenie.Size = new System.Drawing.Size(131, 21);
            this.cbDoreczenie.TabIndex = 244;
            this.cbDoreczenie.Visible = false;
            this.cbDoreczenie.SelectedIndexChanged += new System.EventHandler(this.cbDoreczenie_SelectedIndexChanged);
            // 
            // gbOdbiorWlasny
            // 
            this.gbOdbiorWlasny.Controls.Add(this.rbOsoba);
            this.gbOdbiorWlasny.Controls.Add(this.rbFirma);
            this.gbOdbiorWlasny.Enabled = false;
            this.gbOdbiorWlasny.ForeColor = System.Drawing.SystemColors.MenuText;
            this.gbOdbiorWlasny.Location = new System.Drawing.Point(27, 169);
            this.gbOdbiorWlasny.Name = "gbOdbiorWlasny";
            this.gbOdbiorWlasny.Size = new System.Drawing.Size(135, 28);
            this.gbOdbiorWlasny.TabIndex = 238;
            this.gbOdbiorWlasny.TabStop = false;
            // 
            // rbOsoba
            // 
            this.rbOsoba.AutoSize = true;
            this.rbOsoba.Checked = true;
            this.rbOsoba.ForeColor = System.Drawing.SystemColors.InfoText;
            this.rbOsoba.Location = new System.Drawing.Point(9, 8);
            this.rbOsoba.Name = "rbOsoba";
            this.rbOsoba.Size = new System.Drawing.Size(54, 17);
            this.rbOsoba.TabIndex = 125;
            this.rbOsoba.TabStop = true;
            this.rbOsoba.Text = "osoba";
            this.rbOsoba.UseVisualStyleBackColor = true;
            // 
            // rbFirma
            // 
            this.rbFirma.AutoSize = true;
            this.rbFirma.ForeColor = System.Drawing.SystemColors.InfoText;
            this.rbFirma.Location = new System.Drawing.Point(78, 9);
            this.rbFirma.Name = "rbFirma";
            this.rbFirma.Size = new System.Drawing.Size(47, 17);
            this.rbFirma.TabIndex = 127;
            this.rbFirma.Text = "firma";
            this.rbFirma.UseVisualStyleBackColor = true;
            // 
            // chPobranie
            // 
            this.chPobranie.AutoSize = true;
            this.chPobranie.ForeColor = System.Drawing.SystemColors.MenuText;
            this.chPobranie.Location = new System.Drawing.Point(8, 118);
            this.chPobranie.Name = "chPobranie";
            this.chPobranie.Size = new System.Drawing.Size(103, 17);
            this.chPobranie.TabIndex = 228;
            this.chPobranie.Text = "Pobranie C.O.D.";
            this.chPobranie.UseVisualStyleBackColor = true;
            this.chPobranie.CheckedChanged += new System.EventHandler(this.chPobranie_CheckedChanged);
            // 
            // chPrzesZwrot
            // 
            this.chPrzesZwrot.AutoSize = true;
            this.chPrzesZwrot.ForeColor = System.Drawing.SystemColors.MenuText;
            this.chPrzesZwrot.Location = new System.Drawing.Point(224, 74);
            this.chPrzesZwrot.Name = "chPrzesZwrot";
            this.chPrzesZwrot.Size = new System.Drawing.Size(113, 17);
            this.chPrzesZwrot.TabIndex = 229;
            this.chPrzesZwrot.Text = "Przesyłka zwrotna";
            this.chPrzesZwrot.UseVisualStyleBackColor = true;
            // 
            // chDokZwrot
            // 
            this.chDokZwrot.AutoSize = true;
            this.chDokZwrot.ForeColor = System.Drawing.SystemColors.MenuText;
            this.chDokZwrot.Location = new System.Drawing.Point(224, 34);
            this.chDokZwrot.Name = "chDokZwrot";
            this.chDokZwrot.Size = new System.Drawing.Size(120, 17);
            this.chDokZwrot.TabIndex = 235;
            this.chDokZwrot.Text = "Dokumenty zwrotne";
            this.chDokZwrot.UseVisualStyleBackColor = true;
            // 
            // chPrzesList
            // 
            this.chPrzesList.AutoSize = true;
            this.chPrzesList.ForeColor = System.Drawing.SystemColors.MenuText;
            this.chPrzesList.Location = new System.Drawing.Point(224, 54);
            this.chPrzesList.Name = "chPrzesList";
            this.chPrzesList.Size = new System.Drawing.Size(49, 17);
            this.chPrzesList.TabIndex = 231;
            this.chPrzesList.Text = "DOX";
            this.chPrzesList.UseVisualStyleBackColor = true;
            // 
            // chDorNaAdrPryw
            // 
            this.chDorNaAdrPryw.AutoSize = true;
            this.chDorNaAdrPryw.ForeColor = System.Drawing.SystemColors.MenuText;
            this.chDorNaAdrPryw.Location = new System.Drawing.Point(8, 97);
            this.chDorNaAdrPryw.Name = "chDorNaAdrPryw";
            this.chDorNaAdrPryw.Size = new System.Drawing.Size(169, 17);
            this.chDorNaAdrPryw.TabIndex = 234;
            this.chDorNaAdrPryw.Text = "Doręczenie na adres prywatny";
            this.chDorNaAdrPryw.UseVisualStyleBackColor = true;
            // 
            // gbDeklarowanaWartosc
            // 
            this.gbDeklarowanaWartosc.Controls.Add(this.txtDeklarowanaWartosc);
            this.gbDeklarowanaWartosc.Controls.Add(this.label2);
            this.gbDeklarowanaWartosc.Controls.Add(this.label1);
            this.gbDeklarowanaWartosc.Enabled = false;
            this.gbDeklarowanaWartosc.ForeColor = System.Drawing.SystemColors.MenuText;
            this.gbDeklarowanaWartosc.Location = new System.Drawing.Point(242, 145);
            this.gbDeklarowanaWartosc.Name = "gbDeklarowanaWartosc";
            this.gbDeklarowanaWartosc.Size = new System.Drawing.Size(111, 32);
            this.gbDeklarowanaWartosc.TabIndex = 237;
            this.gbDeklarowanaWartosc.TabStop = false;
            // 
            // txtDeklarowanaWartosc
            // 
            this.txtDeklarowanaWartosc.Location = new System.Drawing.Point(43, 9);
            this.txtDeklarowanaWartosc.Name = "txtDeklarowanaWartosc";
            this.txtDeklarowanaWartosc.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtDeklarowanaWartosc.Size = new System.Drawing.Size(40, 20);
            this.txtDeklarowanaWartosc.TabIndex = 116;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.ForeColor = System.Drawing.SystemColors.InfoText;
            this.label2.Location = new System.Drawing.Point(89, 12);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(16, 13);
            this.label2.TabIndex = 114;
            this.label2.Text = "zł";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.ForeColor = System.Drawing.SystemColors.InfoText;
            this.label1.Location = new System.Drawing.Point(2, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(37, 13);
            this.label1.TabIndex = 115;
            this.label1.Text = "Kwota";
            // 
            // chOpony
            // 
            this.chOpony.AutoSize = true;
            this.chOpony.ForeColor = System.Drawing.SystemColors.MenuText;
            this.chOpony.Location = new System.Drawing.Point(224, 94);
            this.chOpony.Name = "chOpony";
            this.chOpony.Size = new System.Drawing.Size(57, 17);
            this.chOpony.TabIndex = 243;
            this.chOpony.Text = "Opony";
            this.chOpony.UseVisualStyleBackColor = true;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.BackColor = System.Drawing.Color.Transparent;
            this.label21.ForeColor = System.Drawing.SystemColors.MenuText;
            this.label21.Location = new System.Drawing.Point(159, 97);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(16, 13);
            this.label21.TabIndex = 240;
            this.label21.Text = "zł";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.BackColor = System.Drawing.Color.Transparent;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label28.ForeColor = System.Drawing.SystemColors.MenuText;
            this.label28.Location = new System.Drawing.Point(24, 9);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(54, 13);
            this.label28.TabIndex = 242;
            this.label28.Text = "USŁUGI";
            // 
            // chTiresExport
            // 
            this.chTiresExport.AutoSize = true;
            this.chTiresExport.ForeColor = System.Drawing.SystemColors.MenuText;
            this.chTiresExport.Location = new System.Drawing.Point(224, 114);
            this.chTiresExport.Name = "chTiresExport";
            this.chTiresExport.Size = new System.Drawing.Size(139, 17);
            this.chTiresExport.TabIndex = 241;
            this.chTiresExport.Text = "Opony międzynarodowe";
            this.chTiresExport.UseVisualStyleBackColor = true;
            // 
            // chDorDoRakWl
            // 
            this.chDorDoRakWl.AutoSize = true;
            this.chDorDoRakWl.ForeColor = System.Drawing.SystemColors.MenuText;
            this.chDorDoRakWl.Location = new System.Drawing.Point(8, 77);
            this.chDorDoRakWl.Name = "chDorDoRakWl";
            this.chDorDoRakWl.Size = new System.Drawing.Size(162, 17);
            this.chDorDoRakWl.TabIndex = 233;
            this.chDorDoRakWl.Text = "Doręczenie do rąk własnych";
            this.chDorDoRakWl.UseVisualStyleBackColor = true;
            // 
            // btnZatwierdz
            // 
            this.btnZatwierdz.Location = new System.Drawing.Point(224, 264);
            this.btnZatwierdz.Name = "btnZatwierdz";
            this.btnZatwierdz.Size = new System.Drawing.Size(139, 27);
            this.btnZatwierdz.TabIndex = 246;
            this.btnZatwierdz.Text = "Zatwierdź";
            this.btnZatwierdz.UseVisualStyleBackColor = true;
            this.btnZatwierdz.Click += new System.EventHandler(this.btnZatwierdz_Click);
            // 
            // btnAnuluj
            // 
            this.btnAnuluj.Location = new System.Drawing.Point(25, 264);
            this.btnAnuluj.Name = "btnAnuluj";
            this.btnAnuluj.Size = new System.Drawing.Size(150, 27);
            this.btnAnuluj.TabIndex = 247;
            this.btnAnuluj.Text = "Anuluj";
            this.btnAnuluj.UseVisualStyleBackColor = true;
            this.btnAnuluj.Click += new System.EventHandler(this.btnAnuluj_Click);
            // 
            // chPobierzKwoteZWfMaga
            // 
            this.chPobierzKwoteZWfMaga.AutoSize = true;
            this.chPobierzKwoteZWfMaga.ForeColor = System.Drawing.SystemColors.MenuText;
            this.chPobierzKwoteZWfMaga.Location = new System.Drawing.Point(27, 137);
            this.chPobierzKwoteZWfMaga.Name = "chPobierzKwoteZWfMaga";
            this.chPobierzKwoteZWfMaga.Size = new System.Drawing.Size(148, 17);
            this.chPobierzKwoteZWfMaga.TabIndex = 248;
            this.chPobierzKwoteZWfMaga.Text = "Pobierz kwotę z Wf-Maga";
            this.chPobierzKwoteZWfMaga.UseVisualStyleBackColor = true;
            this.chPobierzKwoteZWfMaga.CheckedChanged += new System.EventHandler(this.chPobierzKwoteZWfMaga_CheckedChanged);
            // 
            // chPrzesylkaWartosciowaZWfMaga
            // 
            this.chPrzesylkaWartosciowaZWfMaga.AutoSize = true;
            this.chPrzesylkaWartosciowaZWfMaga.ForeColor = System.Drawing.SystemColors.MenuText;
            this.chPrzesylkaWartosciowaZWfMaga.Location = new System.Drawing.Point(242, 179);
            this.chPrzesylkaWartosciowaZWfMaga.Name = "chPrzesylkaWartosciowaZWfMaga";
            this.chPrzesylkaWartosciowaZWfMaga.Size = new System.Drawing.Size(148, 17);
            this.chPrzesylkaWartosciowaZWfMaga.TabIndex = 249;
            this.chPrzesylkaWartosciowaZWfMaga.Text = "Pobierz kwotę z Wf-Maga";
            this.chPrzesylkaWartosciowaZWfMaga.UseVisualStyleBackColor = true;
            // 
            // chPallet
            // 
            this.chPallet.AutoSize = true;
            this.chPallet.Location = new System.Drawing.Point(224, 202);
            this.chPallet.Name = "chPallet";
            this.chPallet.Size = new System.Drawing.Size(56, 17);
            this.chPallet.TabIndex = 250;
            this.chPallet.Text = "Paleta";
            this.chPallet.UseVisualStyleBackColor = true;
            // 
            // txtPudo
            // 
            this.txtPudo.Enabled = false;
            this.txtPudo.Location = new System.Drawing.Point(65, 203);
            this.txtPudo.Name = "txtPudo";
            this.txtPudo.Size = new System.Drawing.Size(87, 20);
            this.txtPudo.TabIndex = 252;
            // 
            // chPudo
            // 
            this.chPudo.AutoSize = true;
            this.chPudo.ForeColor = System.Drawing.SystemColors.MenuText;
            this.chPudo.Location = new System.Drawing.Point(8, 205);
            this.chPudo.Name = "chPudo";
            this.chPudo.Size = new System.Drawing.Size(59, 17);
            this.chPudo.TabIndex = 251;
            this.chPudo.Text = "Pickup";
            this.chPudo.UseVisualStyleBackColor = true;
            this.chPudo.CheckedChanged += new System.EventHandler(this.chPudo_CheckedChanged);
            // 
            // btnFindPickup
            // 
            this.btnFindPickup.Location = new System.Drawing.Point(155, 201);
            this.btnFindPickup.Name = "btnFindPickup";
            this.btnFindPickup.Size = new System.Drawing.Size(47, 23);
            this.btnFindPickup.TabIndex = 284;
            this.btnFindPickup.Text = "Znajdź";
            this.btnFindPickup.UseVisualStyleBackColor = true;
            this.btnFindPickup.Click += new System.EventHandler(this.btnFindPickup_Click);
            // 
            // chDpdExpress
            // 
            this.chDpdExpress.AutoSize = true;
            this.chDpdExpress.Location = new System.Drawing.Point(224, 221);
            this.chDpdExpress.Name = "chDpdExpress";
            this.chDpdExpress.Size = new System.Drawing.Size(102, 17);
            this.chDpdExpress.TabIndex = 285;
            this.chDpdExpress.Text = "DPD EXPRESS";
            this.chDpdExpress.UseVisualStyleBackColor = true;
            // 
            // txtDuty
            // 
            this.txtDuty.Enabled = false;
            this.txtDuty.ForeColor = System.Drawing.SystemColors.MenuText;
            this.txtDuty.Location = new System.Drawing.Point(106, 228);
            this.txtDuty.Name = "txtDuty";
            this.txtDuty.Size = new System.Drawing.Size(52, 20);
            this.txtDuty.TabIndex = 287;
            this.txtDuty.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // chDuty
            // 
            this.chDuty.AutoSize = true;
            this.chDuty.ForeColor = System.Drawing.SystemColors.MenuText;
            this.chDuty.Location = new System.Drawing.Point(8, 230);
            this.chDuty.Name = "chDuty";
            this.chDuty.Size = new System.Drawing.Size(98, 17);
            this.chDuty.TabIndex = 286;
            this.chDuty.Text = "Odprawa celna";
            this.chDuty.UseVisualStyleBackColor = true;
            this.chDuty.CheckedChanged += new System.EventHandler(this.chDuty_CheckedChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(160, 231);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(28, 13);
            this.label3.TabIndex = 288;
            this.label3.Text = "PLN";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(161, 118);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(28, 13);
            this.label4.TabIndex = 289;
            this.label4.Text = "PLN";
            // 
            // SzablonForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(391, 303);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtDuty);
            this.Controls.Add(this.chDuty);
            this.Controls.Add(this.chDpdExpress);
            this.Controls.Add(this.btnFindPickup);
            this.Controls.Add(this.txtPudo);
            this.Controls.Add(this.chPudo);
            this.Controls.Add(this.chPallet);
            this.Controls.Add(this.chPrzesylkaWartosciowaZWfMaga);
            this.Controls.Add(this.chPobierzKwoteZWfMaga);
            this.Controls.Add(this.btnAnuluj);
            this.Controls.Add(this.btnZatwierdz);
            this.Controls.Add(this.txtPobranie);
            this.Controls.Add(this.chOdbiorWlasny);
            this.Controls.Add(this.chDeklWart);
            this.Controls.Add(this.chDorGwaran);
            this.Controls.Add(this.txtNaOkreslonaGodzine);
            this.Controls.Add(this.cbDoreczenie);
            this.Controls.Add(this.gbOdbiorWlasny);
            this.Controls.Add(this.chPobranie);
            this.Controls.Add(this.chPrzesZwrot);
            this.Controls.Add(this.chDokZwrot);
            this.Controls.Add(this.chPrzesList);
            this.Controls.Add(this.chDorNaAdrPryw);
            this.Controls.Add(this.gbDeklarowanaWartosc);
            this.Controls.Add(this.chOpony);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.label28);
            this.Controls.Add(this.chTiresExport);
            this.Controls.Add(this.chDorDoRakWl);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(407, 342);
            this.MinimumSize = new System.Drawing.Size(407, 342);
            this.Name = "SzablonForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Wybór usług";
            this.Load += new System.EventHandler(this.SzablonForm_Load);
            this.gbOdbiorWlasny.ResumeLayout(false);
            this.gbOdbiorWlasny.PerformLayout();
            this.gbDeklarowanaWartosc.ResumeLayout(false);
            this.gbDeklarowanaWartosc.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtPobranie;
        private System.Windows.Forms.CheckBox chOdbiorWlasny;
        private System.Windows.Forms.CheckBox chDeklWart;
        private System.Windows.Forms.CheckBox chDorGwaran;
        private System.Windows.Forms.TextBox txtNaOkreslonaGodzine;
        private System.Windows.Forms.ComboBox cbDoreczenie;
        private System.Windows.Forms.GroupBox gbOdbiorWlasny;
        private System.Windows.Forms.RadioButton rbOsoba;
        private System.Windows.Forms.RadioButton rbFirma;
        private System.Windows.Forms.CheckBox chPobranie;
        private System.Windows.Forms.CheckBox chPrzesZwrot;
        private System.Windows.Forms.CheckBox chDokZwrot;
        private System.Windows.Forms.CheckBox chPrzesList;
        private System.Windows.Forms.CheckBox chDorNaAdrPryw;
        private System.Windows.Forms.GroupBox gbDeklarowanaWartosc;
        private System.Windows.Forms.TextBox txtDeklarowanaWartosc;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox chOpony;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.CheckBox chTiresExport;
        private System.Windows.Forms.CheckBox chDorDoRakWl;
        private System.Windows.Forms.Button btnZatwierdz;
        private System.Windows.Forms.Button btnAnuluj;
        private System.Windows.Forms.CheckBox chPobierzKwoteZWfMaga;
        private System.Windows.Forms.CheckBox chPrzesylkaWartosciowaZWfMaga;
        private System.Windows.Forms.CheckBox chPallet;
        private System.Windows.Forms.TextBox txtPudo;
        private System.Windows.Forms.CheckBox chPudo;
        private System.Windows.Forms.Button btnFindPickup;
        private System.Windows.Forms.CheckBox chDpdExpress;
        private System.Windows.Forms.TextBox txtDuty;
        private System.Windows.Forms.CheckBox chDuty;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
    }
}