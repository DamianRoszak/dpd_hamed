﻿using System;
using System.Windows.Forms;
using static DPD.Properties.Settings;

namespace DPD.Forms
{
    public partial class KolumnyForm : Form
    {
        public bool IsSaved = false;
        private readonly bool _isStatusServiceEnabled;
        public KolumnyForm(bool isStatusServiceEnabled)
        {
            InitializeComponent();
            _isStatusServiceEnabled = isStatusServiceEnabled;
            PobierzZapisaneUstawieniaKolumn();
        }

        private void ZapiszUstawieniaKolumn()
        {
          
            Default.bColNrDokumentu = chNrDokumentu.Checked;
            Default.bColAdresNadawcy = chAdresNadawcy.Checked;
            Default.bColAdresOdbiorcy = chAdresOdbiorcy.Checked;
            Default.bColNrListu = chNrListu.Checked;
            Default.bColNrProtokolu = chNrProtokolu.Checked;
            Default.bColDataListu = chDataListu.Checked;
            Default.bColDataProtokolu = chDataProtokolu.Checked;
            Default.bColEtykieta = chEtykieta.Checked;
            Default.bColPobranieCOD = chPobranieCOD.Checked;
            Default.bColPrzesylkaWartosciowa = chPrzesylkaWartosciowa.Checked;
            Default.bColStatus = chStatus.Checked;
            Default.bColWaga = chWaga.Checked;
            Default.bColUslugi = chUslugi.Checked;
            Default.bColDataOstatniegoSprawdzenia = chDataOstatniegoSprawdzenia.Checked;
            Default.bColPaczki = chPaczki.Checked;
            Default.Id_mag = chId_mag.Checked;
            Default.bColStatusPrzycisk = _isStatusServiceEnabled && chStatusPrzycisk.Checked;

            Default.Save();
        }

        private void PobierzZapisaneUstawieniaKolumn()
        {
           
            chNrDokumentu.Checked = Default.bColNrDokumentu;
            chAdresNadawcy.Checked = Default.bColAdresNadawcy;
            chAdresOdbiorcy.Checked = Default.bColAdresOdbiorcy;
            chNrListu.Checked = Default.bColNrListu;
            chNrProtokolu.Checked = Default.bColNrProtokolu;
            chDataListu.Checked = Default.bColDataListu;
            chDataProtokolu.Checked = Default.bColDataProtokolu;
            chEtykieta.Checked = Default.bColEtykieta;
            chPaczki.Checked = Default.bColPaczki;
            chPobranieCOD.Checked = Default.bColPobranieCOD;
            chPrzesylkaWartosciowa.Checked = Default.bColPrzesylkaWartosciowa;
            chStatus.Checked = Default.bColStatus;
            chWaga.Checked = Default.bColWaga;
            chUslugi.Checked = Default.bColUslugi;
            chDataOstatniegoSprawdzenia.Checked = Default.bColDataOstatniegoSprawdzenia;
            chId_mag.Checked = Default.Id_mag;
            if (!_isStatusServiceEnabled)
            {
                chStatusPrzycisk.Enabled = false;
                chStatusPrzycisk.Checked = false;
            }
            else
            {
                chStatusPrzycisk.Checked = Default.bColStatusPrzycisk;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ZapiszUstawieniaKolumn();
            IsSaved = true;
            Close();
        }
    }
}
