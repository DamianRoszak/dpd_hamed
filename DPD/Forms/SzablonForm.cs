﻿using System;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using DPD.Helpers;
using static DPD.Properties.Settings;

namespace DPD.Forms
{
    public partial class SzablonForm : Form
    {
        public UslugiHelper Uslugi;

        public bool JestZatwierdzone = false;

        public SzablonForm()
        {
            InitializeComponent();

            cbDoreczenie.DataSource = Form1.DoreczeniaTyp.Clone();

            LoadSzablon();
        }

        private void LoadSzablon()
        {
            chPallet.Checked = Default.bServicePallet;
            chDorGwaran.Checked = Default.bDoreczenieGwarantowane;
            if (chDorGwaran.Checked)
            {
                cbDoreczenie.SelectedItem = Default.vDoreczenieGwarantowane;
                if (cbDoreczenie.SelectedItem.ToString() == "DPD na godzinę")
                    txtNaOkreslonaGodzine.Text = Default.vDoreczenieGwarantowaneGodzina;
            }
            chDorNaAdrPryw.Checked = Default.bDoreczenieNaAdresPrywatny;
            chDorDoRakWl.Checked = Default.bDoreczenieDoRakWlasnych;

            chPobranie.Checked = Default.bPobranieCOD;
            chPobierzKwoteZWfMaga.Checked = Default.bPobranieZWfMaga;

            txtPobranie.Text = Default.vPobranieCOD;

            chOdbiorWlasny.Checked = Default.bOdbiorWlasny;
            if (chOdbiorWlasny.Checked)
                if (Default.vOdbiorWlasny == "Osoba") rbOsoba.Checked = true; else rbFirma.Checked = true;

            chDokZwrot.Checked = Default.bDokumentyZwrotne;
            chPrzesList.Checked = Default.bDOX;
            chPrzesZwrot.Checked = Default.bPrzesylkaZwrotna;
            chOpony.Checked = Default.bOpony;
            chTiresExport.Checked = Default.bOponyMiedzynarodowe;

            chDeklWart.Checked = Default.bPrzesylkaWartosciowa;
            chPrzesylkaWartosciowaZWfMaga.Checked = Default.bPrzesylkaWartosciowaZWfMaga;
            if (chDeklWart.Checked)
                txtDeklarowanaWartosc.Text = Default.vPrzesylkaWartosciowa;


            chPudo.Checked = Default.bPudo;
            if (chPudo.Checked)
                txtPudo.Text = Default.vPudo;
        }

        private void chDorGwaran_CheckedChanged(object sender, EventArgs e)
        {
            cbDoreczenie.Visible = chDorGwaran.Checked;
            if (!chDorGwaran.Checked)
                cbDoreczenie.SelectedIndex = 0;
        }

        private void cbDoreczenie_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtNaOkreslonaGodzine.Visible = cbDoreczenie.SelectedValue.ToString() == "DPD na godzinę";
        }

        private void chPobranie_CheckedChanged(object sender, EventArgs e)
        {
            txtPobranie.Enabled = chPobranie.Checked;
        }

        private void chOdbiorWlasny_CheckedChanged(object sender, EventArgs e)
        {
            gbOdbiorWlasny.Enabled = chOdbiorWlasny.Checked;
        }

        private void chDeklWart_CheckedChanged(object sender, EventArgs e)
        {
            gbDeklarowanaWartosc.Enabled = chDeklWart.Checked;
            txtDeklarowanaWartosc.Enabled = chDeklWart.Checked;
        }

        private const int EmSetcuebanner = 0x1501;
        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        private static extern int SendMessage(IntPtr hWnd, int msg, int wParam, [MarshalAs(UnmanagedType.LPWStr)]string lParam);

        private void btnZatwierdz_Click(object sender, EventArgs e)
        {
            JestZatwierdzone = true;
            Uslugi = PobierzUslugiZkontrolek();
            Close();
        }

        private UslugiHelper PobierzUslugiZkontrolek()
        {
            var u = new UslugiHelper
            {
                DoreczenieGwarantowane = chDorGwaran.Checked,
                DoreczenieDoRakWlasnych = chDorDoRakWl.Checked,
                DoreczenieNaAdersPrywatny = chDorNaAdrPryw.Checked,
                Pobranie = chPobranie.Checked,
                PobranieZWfMaga = chPobierzKwoteZWfMaga.Checked,
                OdbiorWlasny = chOdbiorWlasny.Checked,
                DokumentyZwrotne = chDokZwrot.Checked,
                Dox = chPrzesList.Checked,
                PrzesylkaZwrotna = chPrzesZwrot.Checked,
                Opony = chOpony.Checked,
                OponyMiedzynarodowe = chTiresExport.Checked,
                PrzesylkaWartosciowa = chDeklWart.Checked,
                PrzesylkaWartosciowaZWfMaga = chPrzesylkaWartosciowaZWfMaga.Checked,
                Pudo = chPudo.Checked,
                PrzesylkaPaletowa = chPallet.Checked
            };

            if (chDorGwaran.Checked)
            {
                u.DoreczenieGwarantowaneWartosc = cbDoreczenie.SelectedItem.ToString();
                if (cbDoreczenie.SelectedItem.ToString() == "DPD na godzinę")
                    u.DoreczenieGwarantowaneNaGodzine = txtNaOkreslonaGodzine.Text;
            }
            if (chPobranie.Checked && !chPobierzKwoteZWfMaga.Checked)
                u.PobranieWartosc = txtPobranie.Text;
            if (chOdbiorWlasny.Checked)
                u.OdbiorWlasnyWartosc = rbFirma.Checked ? "Firma" : "Osoba";
            if (chDeklWart.Checked)
                u.PrzesylkaWartosciowaWartosc = txtDeklarowanaWartosc.Text;

            if (chPudo.Checked)
                u.PudoWartosc = txtPudo.Text;
            return u;
        }

        private void btnAnuluj_Click(object sender, EventArgs e)
        {
            JestZatwierdzone = false;
            Close();
        }

        private void chPobierzKwoteZWfMaga_CheckedChanged(object sender, EventArgs e)
        {
            txtPobranie.Enabled = !chPobierzKwoteZWfMaga.Checked;
        }

        private void SzablonForm_Load(object sender, EventArgs e)
        {

        }

        private void chPudo_CheckedChanged(object sender, EventArgs e)
        {
            txtPudo.Enabled = chPudo.Checked;
        }

        private void btnFindPickup_Click(object sender, EventArgs e)
        {
            var parcelShopSearch = new ParcelShopSearch();
            parcelShopSearch.ShowDialog();

            if (parcelShopSearch.DialogResult != DialogResult.OK ||
                (string.IsNullOrEmpty(parcelShopSearch.ReturnVal) || parcelShopSearch.ReturnVal.Equals("-"))) return;
            chPudo.Checked = true;
            txtPudo.Text = parcelShopSearch.ReturnVal;
        }

        private void chDuty_CheckedChanged(object sender, EventArgs e)
        {
            txtDuty.Enabled = chDuty.Checked;
        }
    }
}
