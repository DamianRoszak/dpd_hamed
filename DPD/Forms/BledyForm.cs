﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using DPD.DpdApi;

namespace DPD.Forms
{
    public partial class BledyForm : Form
    {
        private readonly List<validationInfoPGRV2> _errors = new List<validationInfoPGRV2>();

        public BledyForm(List<validationInfoPGRV2> _errors)
        {
            InitializeComponent();
            this._errors = _errors;

            numericUpDown1.Maximum = this._errors.Count;
            lblLiczbaBledow.Text = this._errors.Count.ToString();

            lblId.Text = this._errors[0].ErrorId.ToString();
            lblKod.Text = this._errors[0].ErrorCode;
            lblNazwy.Text = this._errors[0].FieldNames;
            lblInfo.Text = this._errors[0].Info;
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            lblId.Text = _errors[(int)numericUpDown1.Value - 1].ErrorId.ToString();
            lblKod.Text = _errors[(int)numericUpDown1.Value - 1].ErrorCode;
            lblNazwy.Text = _errors[(int)numericUpDown1.Value - 1].FieldNames;
            lblInfo.Text = _errors[(int)numericUpDown1.Value - 1].Info;
        }
    }
}
