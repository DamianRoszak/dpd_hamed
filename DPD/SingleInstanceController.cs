﻿using System.Linq;
using DPD.Forms;
using Microsoft.VisualBasic.ApplicationServices;

namespace DPD
{
    public class SingleInstanceController : WindowsFormsApplicationBase
    {
        public SingleInstanceController()
        {
            IsSingleInstance = true;

            StartupNextInstance += this_StartupNextInstance;
        }

        private void this_StartupNextInstance(object sender, StartupNextInstanceEventArgs e)
        {
            var form = MainForm as Form1; 
            form.PobierzDane(e.CommandLine.ToArray());
        }

        protected override void OnCreateMainForm()
        {
            MainForm = new Form1();
        }
    }
}
