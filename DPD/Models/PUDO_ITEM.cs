﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RestSharp.Deserializers;

namespace DPD.Models
{
    // ReSharper disable once InconsistentNaming
    public class PUDO_ITEM
    {
        [DeserializeAs(Name = "PUDO_ID")]
        public string PudoId { get; set; }

        [DeserializeAs(Name = "ORDER")]
        public string Order { get; set; }

        [DeserializeAs(Name = "PUDO_TYPE")]
        public string PudoType { get; set; }

        [DeserializeAs(Name = "LANGUAGE")]
        public string Language { get; set; }

        [DeserializeAs(Name = "ADDRESS1")]
        public string Address1 { get; set; }

        [DeserializeAs(Name = "LOCATION_HINT")]
        public string LocationHint { get; set; }

        [DeserializeAs(Name = "ZIPCODE")]
        public string Zipcode { get; set; }

        [DeserializeAs(Name = "CITY")]
        public string City { get; set; }

        [DeserializeAs(Name = "COUNTRY")]
        public string Country { get; set; }

        [DeserializeAs(Name = "LONGITUDE")]
        public string Longitude { get; set; }

        [DeserializeAs(Name = "LATITUDE")]
        public string Latitude { get; set; }

        [DeserializeAs(Name = "PARKING")]
        public string Parking { get; set; }

        [DeserializeAs(Name = "HANDICAPE")]
        public string Handicape { get; set; }


        // ReSharper disable once InconsistentNaming
        public List<OPENING_HOURS_ITEM> OPENING_HOURS_ITEMS { get; set; }
    }
}
