﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RestSharp.Deserializers;

namespace DPD.Models
{
    public class ParcelShopModel
    {
        [DeserializeAs(Name = "REQUEST_ID")]
        public string RequestId { get; set; }

        // ReSharper disable once InconsistentNaming
        public List<PUDO_ITEM> PUDO_ITEMS { get; set; }
    }
}
