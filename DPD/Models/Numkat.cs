﻿namespace DPD.Models
{
    public class Numkat
    {
        public int Numer { get; set; }
        public string Nazwa { get; set; }
        public bool Domyslny { get; set; }
    }
}
