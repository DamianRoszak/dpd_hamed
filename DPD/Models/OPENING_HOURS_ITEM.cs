﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RestSharp.Deserializers;

namespace DPD.Models
{
    // ReSharper disable once InconsistentNaming
    public class OPENING_HOURS_ITEM
    {
        [DeserializeAs(Name = "DAY_ID")]
        public string DayId { get; set; }

        [DeserializeAs(Name = "START_TM")]
        public string StartTm { get; set; }

        [DeserializeAs(Name = "END_TM")]
        public string EndTm { get; set; }

    }
}
