﻿namespace DPD.Models
{
    public class Adres
    {
        public string Nazwa { get; set; }
        public string ImieNazwisko { get; set; }
        public string Ulica { get; set; }
        public string Kod { get; set; }
        public string Miejscowosc { get; set; }
        public string Kraj { get; set; }
        public string Telefon { get; set; }
        public string Mail { get; set; }
        public bool JestDomyslny { get; set; }

        public override bool Equals(object obj)
        {
            var tmp = obj as Adres;
            if (tmp == null) return false;

            return Nazwa == tmp.Nazwa &&
                ImieNazwisko == tmp.ImieNazwisko &&
                 Ulica == tmp.Ulica &&
                  Kod == tmp.Kod &&
                   Miejscowosc == tmp.Miejscowosc &&
                    Kraj == tmp.Kraj &&
                     Telefon == tmp.Telefon;
        }
    }
}
