﻿namespace DPD.Models
{
    public class Pola
    {
        public string Numer;
        public string NumerZamowieniaKlienta;
        public string Uwagi;
        public string Pole1;
        public string Pole2;
        public string Pole3;
        public string Pole4;
        public string Pole5;
        public string Pole6;
        public string Pole7;
        public string Pole8;
        public string Pole9;
        public string Pole10;

        public string ReturnValue(string field)
        {
            switch (field)
            {
                case "bnrDok": return Numer;
                case "bNrZamKlienta": return NumerZamowieniaKlienta;
                case "bUwagi": return Uwagi;
                case "bPole1": return Pole1;
                case "bPole2": return Pole2;
                case "bPole3": return Pole3;
                case "bPole4": return Pole4;
                case "bPole5": return Pole5;
                case "bPole6": return Pole6;
                case "bPole7": return Pole7;
                case "bPole8": return Pole8;
                case "bPole9": return Pole9;
                case "bPole10": return Pole10;
            }
            return null;
        }
    }
}
