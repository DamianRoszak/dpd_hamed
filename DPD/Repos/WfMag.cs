﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using DPD.Helpers;
using DPD.Models;
using static DPD.Properties.Settings;

namespace DPD.Repos
{
    public class WfMag
    {
        private DataTable _data = new DataTable();
        public string IdDokumentu;
        private readonly TypDokumentu _typDokumentu;

        private readonly string _idKontrahenta;
        public Adres AdresNadawcy = null;
        public List<Adres> AdresyOdbiorcy = new List<Adres>();
        public decimal KwotaDokumentu = 0;
        public decimal Waga = 1;
        private bool _posiadaTabeleAdresDostawy = true;
        private bool _posiadaTelefonWKartoteceKontrahenta = true;
        private static string _wersjaWfMaga = null;
        public string NumerDokumentu = null;
        public string AdresEmail = null;

        public WfMag()
        {
            PobierzMiejsceZapisuNrListu();

            SprawdzWersjeWfMaga();

            _isFormatYmd = Sql.RunQuery("select cast('2014-05-25' as datetime)",false);
        }

        public WfMag(string id, TypDokumentu typ)
            : this()
        {
            IdDokumentu = id;
            _typDokumentu = typ;
            _idKontrahenta = PobierzIdKontrahenta();

            PobierzAdresNadawcy();
            PobierzAdresyOdbiorcy();
            PobierzKwoteINumer();
            PobierzWage();
            PobierzMailKontrahenta();
        }

        public WfMag(string _idKontrahenta)
            : this()
        {
            this._idKontrahenta = _idKontrahenta;
            _typDokumentu = TypDokumentu.Brak;
            IdDokumentu = null;
        }

        private void PobierzMailKontrahenta()
        {
            try
            {
                _data = PobierzDane(Query.MailKontrahenta(_idKontrahenta));

                if (_data != null && _data.Rows.Count > 0)
                    AdresEmail = _data.Rows[0]["adres_email"].ToString();

                if (string.IsNullOrEmpty(AdresEmail))
                {
                    _data = PobierzDane(Query.MailKontrahentaBezDomyslnego(_idKontrahenta));

                    if (_data != null && _data.Rows.Count > 0)
                        AdresEmail = _data.Rows[0]["adres_email"].ToString();
                }
            }
            catch (Exception ex)
            {
                Log.AddLog("Nie można pobrać adresu kontrahenta: " + ex.Message);
            }
        }

        private void SprawdzWersjeWfMaga()
        {
            try
            {
                _data = PobierzDane(Query.WersjaWfMag());

                if (_data.Rows.Count > 0)
                {
                    _wersjaWfMaga = _data.Rows[0]["PRGWER"].ToString();
                    var tmp = _wersjaWfMaga.Split('.');
                    if (tmp[0] == "7")
                    {
                        int v2;
                        int.TryParse(tmp[1], out v2);
                        if (v2 < 70)
                            _posiadaTabeleAdresDostawy = false;

                        if (v2 < 81)
                            _posiadaTelefonWKartoteceKontrahenta = false;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.AddLog(ex.ToString());
            }
        }

        public DataTable PobierzListy(string nrDok, string nadawca, string odbiorca, string nrListu, string nrProtokolu,
            DateTime from, DateTime to, string zrodlo)
        {
            string dateFrom, dateTo;
            if (!Sql.RunQuery("select cast('2014-05-25' as datetime)",false))
            {
                dateFrom = from.Year + "-" + from.Day + "-" + from.Month;
                dateTo = to.Year + "-" + to.Day + "-" + to.Month;
            }
            else
            {
                dateFrom = from.ToShortDateString();
                dateTo = to.ToShortDateString();
            }

            var zap = @"
select l.id, numer_dok numer, adres_odbiorcy_nazwa + ' ' + isnull(adres_odbiorcy_imieNazwisko,'') adres_odbiorcy_nazwa, nr_listu, nr_protokolu, 
            adres_odbiorcy_ulica + ' ' + adres_odbiorcy_kod + ' ' +adres_odbiorcy_kraj + ' ' + adres_odbiorcy_miejscowosc as adres,
            data_listu, data_protokolu, etykieta, sessionType, 1 as paczki , adres_nadawcy_nazwa, 
            adres_nadawcy_ulica + ' ' + adres_nadawcy_kod + ' ' + adres_nadawcy_miejscowosc adresNadania,
            adres_nadawcy_ulica, adres_nadawcy_kod, adres_nadawcy_miejscowosc, adres_nadawcy_kraj, adres_nadawcy_tel,id_magazynu,
			u.pobranieKwota COD, u.przesylkaWartosciowaKwota as 'Przesyłka Wartościowa', (select sum(waga) from _dpd_paczki where id = l.id) Waga,
			(select (case when dox = 1 then 'DOX,' else '' end) +
					(case when dokumentyZwrotne = 1 then 'DZ, ' else '' end)+
					(case when przesylkaZwrotna = 1 then 'PZ, ' else '' end)+
					(case when doreczenieDoRakWlasnych = 1 then 'DRW, ' else '' end)+
					(case when opony = 1 then 'Opony, ' else '' end)+
					(case when oponyExport = 1 then 'Opony int, ' else '' end)+
                    (case when przesylkaPaletowa = 1 then 'Palety, ' else '' end)+
					(case when doreczenieNaAdresPryw = 1 then 'PRYW, ' else '' end)+
                    (case when duty = 1 then 'Odprawa celna, ' else '' end) +
					(case when dpdExpress = 1 then 'DPD EXPRESS, ' else '' end) +
					(case when pudo = 1 then 'Pickup, ' else '' end) +
					(case when odbiorWlasny = 1 then 
								 (case when odbiorWlasnyTyp = 'Osoba' then 'ODB PRYW, '
									   when odbiorWlasnyTyp = 'Firma' then 'ODB  FIRMA, ' 
								  end)
						  else '' end) +
					(case when doreczenieGwarantowane = 1 then 
							 (case when doreczenieGwarantowaneTyp = 'DPD 9:30' then 'DOR 9:30, '
								   when doreczenieGwarantowaneTyp = 'DPD 12:00' then 'DOR 12:00, ' 
								   when doreczenieGwarantowaneTyp = 'Sobota' then 'SOB, '  
								   when doreczenieGwarantowaneTyp = 'DPD NEXTDAY' then 'DPD NEXT DAY, '
								   when doreczenieGwarantowaneTyp = 'DPD na godzinę' then 'DOR ' + doreczenieGwarantowaneWartosc   
							  end)
						  else '' end) 

			 from _dpd_uslugi where id = l.id)Usługi, adres_odbiorcy_kraj, l.status, l.data_ostatniego_sprawdzenia
from _DPD_LISTY l	
	inner join _dpd_uslugi u on u.id = l.id
	where  l.numer_dok like '%" + nrDok + @"%'
            and (l.adres_odbiorcy_nazwa like '%" + odbiorca + @"%' or l.adres_odbiorcy_imieNazwisko like '%" + odbiorca +
                      @"%' or l.adres_odbiorcy_ulica like '%" + odbiorca +
                      @"%' or l.adres_odbiorcy_kod like '%" + odbiorca + @"%' or l.adres_odbiorcy_miejscowosc like '%" +
                      odbiorca + @"%' ) 

            and (l.adres_nadawcy_nazwa like '%" + nadawca + @"%' or l.adres_nadawcy_imieNazwisko like '%" + nadawca +
                      @"%' or l.adres_nadawcy_ulica like '%" + nadawca +
                      @"%' or l.adres_nadawcy_kod like '%" + nadawca + @"%' or l.adres_nadawcy_miejscowosc like '%" +
                      nadawca + @"%' )
 
            and l.nr_listu like '%" + nrListu + @"%'
            and data_listu >= '" + dateFrom + @"'
            and data_listu <= '" + dateTo + @" 23:59:59'";

            var zrodloWartosc = "";
            switch (zrodlo)
            {
                case "krajowe":
                {
                    zrodloWartosc = "and adres_odbiorcy_kraj = 'PL'";
                    break;
                }
                case "zagraniczne":
                {
                    zrodloWartosc = "and adres_odbiorcy_kraj != 'PL'";
                    break;
                }
            }

            zap += zrodloWartosc;

            if (string.IsNullOrEmpty(nrProtokolu))
                zap += " and (l.nr_protokolu like '%" + nrProtokolu + @"%' or l.nr_protokolu is null) ";
            else
                zap += " and l.nr_protokolu like '%" + nrProtokolu + @"%'";
            
            zap += " order by l.id ";

            return _data = PobierzDane(zap);
        }

        public List<string[]> PobierzWszystkieListy()
        {
            var list = new List<string[]>();

            var data = ConvertDateTimeToProperFormat(DateTime.Now.AddDays(-30));

            var query = $@"select id, nr_listu from _DPD_LISTY where data_listu > '{data}'";

            var dt = PobierzDane(query);

            if (dt != null && dt.Rows != null && dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    var pos = new[] {row.ItemArray[0].ToString(), row.ItemArray[1].ToString()};
                    list.Add(pos);
                }
            }

            return list;
        }

        public void UpdateStatusList(List<string[]> list, DateTime date)
        {
            var data = ConvertDateTimeToProperFormat(date);

            foreach (var item in list)
            {
                var query =
                    $@"update _DPD_LISTY set status = '{item[1]}', data_ostatniego_sprawdzenia = '{data}' where id = {item
                        [0]}";

                var resp = Sql.RunQuery(query);
            }
        }

        private string ConvertDateTimeToProperFormat(DateTime dateTime)
        {
            string data;

            if (!_isFormatYmd)
            {
                data = dateTime.Year + "-" + dateTime.Day + "-" + dateTime.Month + " " +
                       dateTime.Hour + ":" + dateTime.Minute + ":" + dateTime.Second;
            }
            else
            {
                data = dateTime.Year + "-" + dateTime.Month + "-" + dateTime.Day + " " +
                       dateTime.Hour + ":" + dateTime.Minute + ":" + dateTime.Second;
            }

            return data;
        }

        public DataTable PobierzZmapowanePlatnosci()
        {
            var zap = string.Format(@"select * from _DPD_MAPOWANIEPOBRANIA");

            return PobierzDane(zap);
        }
        public DataTable PobierzFormyDostawy_Blokowanie()
        {
            var zap = string.Format(@"select * from _DPD_MAPOWANIEFORMYDOSTAWY_BLOKOWANIE");
            return PobierzDane(zap);
        }
        public DataTable PobierzFormyDostawy_DokumentZwrotny()
        {
            var zap = string.Format(@"select * from _DPD_MAPOWANIEFORMYDOSTAWY_DOKUMENTZWROTNY");
            return PobierzDane(zap);
        }

        public string PobierzRodzajPlatnosciDlaWybranegoDokumentu(string id, TypDokumentu typ)
        {
            var dataTable = new DataTable();
            var rodzajPlatnosci = "";
            var zap = "";
            if (typ == TypDokumentu.Zamowienie)
            {
                zap =
                    $@"select FORMA_PLATNOSCI
                                    from zamowienie
                                    where id_zamowienia={id}";

            }
            else if (typ == TypDokumentu.Dokument_Handlowy)
            {
                zap =
                    $@"select FORMA_PLATNOSCI
                                    from DOKUMENT_HANDLOWY
                                    where ID_DOKUMENTU_HANDLOWEGO={id}";
            }

            if (string.IsNullOrEmpty(zap))
                return "";

            dataTable = PobierzDane(zap);

            if (dataTable != null && dataTable.Rows.Count > 0)
            {
                try
                {
                    rodzajPlatnosci = (string) (dataTable.Rows[0]["FORMA_PLATNOSCI"].ToString() ?? "").Trim();
                }
                catch (Exception ex)
                {
                    Log.AddLog("PobierzRodzajPlatnosciDlaZamowienia" + ex);
                }
            }

            return rodzajPlatnosci;
        }

        public DataTable PobierzPlatnosci()
        {
            var zap = string.Format(@"select * from FORMA_PLATNOSCI");

            return PobierzDane(zap);
        }

        internal static void InsertPlatnoscCod(int isChecked, string nazwa)
        {
            Sql.RunQuery($@"insert into _DPD_MAPOWANIEPOBRANIA ([id],[nazwa],[is_enabled]) values (0,'{nazwa}',{isChecked})");

        }

        internal static void DeletePlatnoscCod()
        {
            Sql.RunQuery(string.Format(@"delete FROM _DPD_MAPOWANIEPOBRANIA"));
        }

        internal static void InsertFormyPobrania_Blokowanie(int isChecked, string nazwa)
        {
            Sql.RunQuery($@"INSERT into _DPD_MAPOWANIEFORMYDOSTAWY_BLOKOWANIE([id],[nazwa],[is_enabled]) values (0,'{nazwa}',{isChecked})");
        }
        internal static void DeleteFormyPobrania_Blokowanie()
        {
            Sql.RunQuery(string.Format(@"delete FROM _DPD_MAPOWANIEFORMYDOSTAWY_BLOKOWANIE"));
        }

        internal static void InsertFormyPobrania_DokumentZwrotny(int isChecked, string nazwa)
        {
            Sql.RunQuery($@"INSERT into _DPD_MAPOWANIEFORMYDOSTAWY_DOKUMENTZWROTNY([id],[nazwa],[is_enabled]) values (0,'{nazwa}',{isChecked})");
        }
        internal static void DeleteFormyPobrania_DokumentZwrotny()
        {
            Sql.RunQuery(string.Format(@"delete FROM _DPD_MAPOWANIEFORMYDOSTAWY_DOKUMENTZWROTNY"));
        }

        public DataTable PobierzSzczegolyListu(string id)
        {
            var zap = $@"select * from _DPD_LISTY where id = {id}";

            return _data = PobierzDane(zap);
        }

        public DataTable PobierzUslugiListu(string id)
        {
            var zap = $@"select * from _DPD_USLUGI where id = {id}";

            return _data = PobierzDane(zap);
        }

        public DataTable PobierzPaczkiListu(string id)
        {
            var zap = $@"select * from _DPD_PACZKI where id = {id}";

            return _data = PobierzDane(zap);
        }

        private void PobierzWage()
        {
            var tmp = Query.Waga(IdDokumentu, _typDokumentu);

            if (tmp == null)
                return;

            _data = PobierzDane(tmp);

            try
            {

                Waga = _data.Rows[0][0].ToString().ToDecimal();
                if (Waga == 0)
                    Waga = 1;
            }
            catch (Exception ex)
            {
                Log.AddLog(ex.ToString());
            }
        }

        private void PobierzKwoteINumer()
        {
            _data = PobierzDane(Query.KwotaNumer(IdDokumentu, _typDokumentu));

            try
            {
                NumerDokumentu = _data.Rows[0][1].ToString();
                KwotaDokumentu = _data.Rows[0][0].ToString().ToDecimal();
            }
            catch (Exception ex)
            {
                Log.AddLog(ex.ToString());
            }
        }

        private string PobierzIdKontrahenta()
        {
            _data = PobierzDane(Query.IdKontrahenta(IdDokumentu, _typDokumentu));

            if (_data != null && _data.Rows.Count > 0)
                return _data.Rows[0][0].ToString();
            return null;
        }

        private DataTable PobierzDane(string query)
        {
            try
            {
                using (var conn = new SqlConnection(Sql.ConnStr))
                {
                    conn.Open();
                    using (var cmd = new SqlCommand(query, conn))
                    {
                        var rdr = cmd.ExecuteReader();
                         
                        if (rdr.HasRows)
                        {
                            var dt = new DataTable();
                            dt.Load(rdr);

                            foreach (DataColumn col in dt.Columns)
                                col.ReadOnly = false;
                            return dt;
                        }
                        return null;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.AddLog(ex.ToString());
                return null;
            }
        }

        public void PobierzAdresNadawcy()
        {
            AdresNadawcy = new Adres();

            //switch (typDokumentu)
            //{
            //    case TypDokumentu.ZAMOWIENIE: { data = FillGrid(QUERY.AdresNadawcyZam(idDokumentu)); break; }
            //    case TypDokumentu.DOKUMENT_MAGAZYNOWY: { data = FillGrid(QUERY.AdresNadawcyMag(idDokumentu)); break; }
            //    case TypDokumentu.DOKUMENT_HANDLOWY: { data = FillGrid(QUERY.AdresNadawcyHan(idDokumentu)); break; }
            //    case TypDokumentu.BRAK: { break; }
            //}

            _data = PobierzDane(Query.AdresNadawcy(_idKontrahenta));

            if (_data != null && _data.Rows.Count > 0)
            {
                if (_typDokumentu != TypDokumentu.Brak && _data.Rows.Count > 0)
                {
                    try
                    {
                        AdresNadawcy.Nazwa = (string) (_data.Rows[0]["nazwa"].ToString() ?? "").Trim();
                        AdresNadawcy.Ulica = (string) (_data.Rows[0]["ulica"].ToString() ?? "").Trim();
                        AdresNadawcy.Kod = (string) (_data.Rows[0]["KOD_POCZTOWY"].ToString() ?? "").Trim();
                        AdresNadawcy.Miejscowosc = (string) (_data.Rows[0]["miejscowosc"].ToString() ?? "").Trim();
                        AdresNadawcy.Kraj = (string) (_data.Rows[0]["kraj"].ToString() ?? "").Trim();
                        AdresNadawcy.Telefon = (string) (_data.Rows[0]["tel"].ToString() ?? "").Trim();
                        AdresNadawcy.Mail = (string) (_data.Rows[0]["mail"].ToString() ?? "").Trim();
                    }
                    catch (Exception ex)
                    {
                        Log.AddLog("PobierzAdresNadawcy#" + ex);
                    }
                }
                else AdresNadawcy = null;
            }
            else AdresNadawcy = null;
        }

        private void PobierzAdresDostawy()
        {
            if (!_posiadaTabeleAdresDostawy)
                return;
            if (_typDokumentu == TypDokumentu.Dokument_Magazynowy)
                _data = PobierzDane(Query.AdresDostawyMag(IdDokumentu));
            else if (_typDokumentu == TypDokumentu.Zamowienie)
                _data = PobierzDane(Query.AdresDostawyZam(IdDokumentu));
            else if (_typDokumentu == TypDokumentu.Dokument_Handlowy)
                _data = PobierzDane(Query.AdresDostawyZDokumentuMagazynowego(IdDokumentu));
            else
                _data = null;

            if (_data != null && _data.Rows.Count > 0)
            {
                try
                {
                    var adresDostawy = new Adres()
                    {
                        Nazwa = (string) (_data.Rows[0]["firma"].ToString() ?? "").Trim(),
                        ImieNazwisko = (string) (_data.Rows[0]["ODBIORCA"].ToString() ?? "").Trim(),
                        Ulica = (string) (_data.Rows[0]["ulica"].ToString() ?? "").Trim(),
                        Kod = (string) (_data.Rows[0]["kod"].ToString() ?? "").Trim(),
                        Miejscowosc = (string) (_data.Rows[0]["miejscowosc"].ToString() ?? "").Trim(),
                        Kraj = (string) (_data.Rows[0]["kraj"].ToString() ?? "").Trim(),
                        Telefon = (string) (_data.Rows[0]["tel"].ToString() ?? "").Trim(),
                        JestDomyslny = true
                    };

                    if (!AdresyOdbiorcy.Contains(adresDostawy))
                        AdresyOdbiorcy.Add(adresDostawy);
                }
                catch (Exception ex)
                {
                    Log.AddLog("PobierzAdresDostawy#" + ex);
                }
            }
        }

        private void PobierzMiejscaDostawyKontrahenta()
        {
            if (!_posiadaTabeleAdresDostawy)
                return;

            _data = PobierzDane(Query.MiejscaDostawyKontrahenta(_idKontrahenta));

            if (_data != null && _data.Rows.Count > 0)
            {
                foreach (DataRow row in _data.Rows)
                {
                    try
                    {
                        var a = new Adres()
                        {
                            Nazwa = (string) (row["firma"].ToString() ?? "").Trim(),
                            ImieNazwisko = (string) (row["odbiorca"].ToString() ?? "").Trim(),
                            Ulica = (string) (row["ulica"].ToString() ?? "").Trim(),
                            Kod = (string) (row["kod"].ToString() ?? "").Trim(),
                            Miejscowosc = (string) (row["miejscowosc"].ToString() ?? "").Trim(),
                            Kraj = (string) (row["kraj"].ToString() ?? "").Trim(),
                            Telefon = (string) (row["tel"].ToString() ?? "").Trim(),
                            JestDomyslny = row["domyslny"].ToString() == "1" ? true : false
                        };
                        if (!AdresyOdbiorcy.Contains(a))
                            AdresyOdbiorcy.Add(a);
                    }
                    catch (Exception ex)
                    {
                        Log.AddLog("PobierzMiejscaDostawyKontrahenta#" + ex);
                    }
                }
            }
        }

        private void PobierzAdresKontrahenta()
        {
            _data = PobierzDane(Query.AdresKontrahenta(_idKontrahenta));

            if (_data != null && _data.Rows.Count > 0)
            {
                try
                {
                    var a = new Adres()
                    {
                        Nazwa = (string) (_data.Rows[0]["nazwa"].ToString() ?? "").Trim(),
                        Ulica = (string) (_data.Rows[0]["ulica"].ToString() ?? "").Trim(),
                        Kod = (string) (_data.Rows[0]["kod"].ToString() ?? "").Trim(),
                        Miejscowosc = (string) (_data.Rows[0]["miejscowosc"].ToString() ?? "").Trim(),
                        Kraj = (string) (_data.Rows[0]["kraj"].ToString() ?? "").Trim(),
                        Telefon = (string) (_data.Rows[0]["tel"].ToString() ?? "").Trim(),
                        JestDomyslny = false
                    };
                    if (!AdresyOdbiorcy.Contains(a))
                        AdresyOdbiorcy.Add(a);
                }
                catch (Exception ex)
                {
                    Log.AddLog("PobierzAdresKontrahenta1#" + ex);
                }
            }
            else
            {
                _data = PobierzDane(Query.AdresKontrahentaV78(_idKontrahenta));

                try
                {
                    if (_data != null && _data.Rows.Count > 0)
                    {
                        var a = new Adres()
                        {
                            Nazwa = (string) (_data.Rows[0]["nazwa"].ToString() ?? "").Trim(),
                            Ulica = (string) (_data.Rows[0]["ulica"].ToString() ?? "").Trim(),
                            Kod = (string) (_data.Rows[0]["kod"].ToString() ?? "").Trim(),
                            Miejscowosc = (string) (_data.Rows[0]["miejscowosc"].ToString() ?? "").Trim(),
                            Kraj = (string) (_data.Rows[0]["kraj"].ToString() ?? "").Trim(),
                            //telefon = (string)(data.Rows[0]["tel"].ToString() ?? "").Trim(),
                            JestDomyslny = false
                        };
                        if (!AdresyOdbiorcy.Contains(a))
                            AdresyOdbiorcy.Add(a);
                    }
                }
                catch (Exception ex)
                {
                    Log.AddLog("PobierzAdresKontrahenta2#" + ex);
                }
            }
        }

        public void PobierzAdresyOdbiorcy()
        {
            PobierzAdresDostawy();

            PobierzMiejscaDostawyKontrahenta();

            PobierzAdresKontrahenta();
        }

        public DataTable PobierzZamowienia(string nr, DateTime from, DateTime to, string kontrahent, string magazyn)
        {
            var dateFrom = DateToInt(from);
            var dateTo = DateToInt(to);

            return PobierzDane(Query.Zamowienia(nr, dateFrom, dateTo, kontrahent, magazyn));
        }

        public DataTable PobierzDokumentyMagazynowe(string nr, DateTime from, DateTime to, string kontrahent,
            string magazyn)
        {
            var dateFrom = DateToInt(from);
            var dateTo = DateToInt(to);

            return PobierzDane(Query.DokumentyMagazynowe(nr, dateFrom, dateTo, kontrahent, magazyn));
        }

        public DataTable PobierzDokumentyHandlowe(string nr, DateTime from, DateTime to, string kontrahent,
            string magazyn)
        {
            var dateFrom = DateToInt(from);
            var dateTo = DateToInt(to);

            return PobierzDane(Query.DokumentyHandlowe(nr, dateFrom, dateTo, kontrahent, magazyn));
        }

        public int DateToInt(DateTime data)
        {
            var start = new DateTime(1900, 1, 1);
            var dif = (data - start).TotalDays;
            dif = dif + 36163;
            return Convert.ToInt32(dif);
        }

        public Pola PobierzPola()
        {
            Pola pola = null;

            _data = PobierzDane(Query.Pola(IdDokumentu, _typDokumentu));

            if (_data == null || _data.Rows.Count <= 0) return null;

            pola = new Pola
            {
                Numer = _data.Rows[0]["numer"].ToString(),
                Uwagi = _data.Rows[0]["uwagi"].ToString(),
                Pole1 = _data.Rows[0]["pole1"].ToString(),
                Pole2 = _data.Rows[0]["pole2"].ToString(),
                Pole3 = _data.Rows[0]["pole3"].ToString(),
                Pole4 = _data.Rows[0]["pole4"].ToString(),
                Pole5 = _data.Rows[0]["pole5"].ToString(),
                Pole6 = _data.Rows[0]["pole6"].ToString(),
                Pole7 = _data.Rows[0]["pole7"].ToString(),
                Pole8 = _data.Rows[0]["pole8"].ToString(),
                Pole9 = _data.Rows[0]["pole9"].ToString(),
                Pole10 = _data.Rows[0]["pole10"].ToString()
            };

            if (_typDokumentu == TypDokumentu.Zamowienie)
                pola.NumerZamowieniaKlienta = _data.Rows[0]["nr_zamowienia_klienta"].ToString();

            return pola;
        }

        public static List<Numkat> PobierzNumkaty(string numer, string nazwa)
        {
            var numkaty = new List<Numkat>();
            var query = "";
            if (string.IsNullOrEmpty(numer))
                query = string.Format("select * from _DPD_NUMKAT where nazwa like '%{1}%'", numer, nazwa);
            else
                query =
                    $"select * from _DPD_NUMKAT where cast(numer as varchar(20)) like '%{numer}%' and nazwa like '%{nazwa}%'";

            try
            {
                using (var conn = new SqlConnection(Sql.ConnStr))
                {
                    conn.Open();
                    using (var cmd = new SqlCommand(query, conn))
                    {
                        var rdr = cmd.ExecuteReader();

                        if (rdr.HasRows)
                        {
                            while (rdr.Read())
                            {
                                numkaty.Add(new Numkat()
                                {
                                    Numer = Convert.ToInt32(rdr[0].ToString()),
                                    Nazwa = rdr[1].ToString(),
                                    Domyslny = rdr[2].ToString() == "True" ? true : false
                                });
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.AddLog("QUERY: pobierz numkaty;" + ex);
            }
            return numkaty;
        }

        public static void CreateTableNumkat()
        {
            Sql.RunQuery(Query.CreateDpdNumkat());
        }

        public static bool CreateOpDod(string kodGrupy, string nazwa, string kodOperacji)
        {
            string query = null;

            query = _wersjaWfMaga == "CreateOpDodV7608" ? Query.CreateOpDodV792(kodGrupy, nazwa, kodOperacji, Application.ExecutablePath) : Query.CreateOpDodV792(kodGrupy, nazwa, kodOperacji, Application.ExecutablePath);

            return Sql.RunQuery(query);
        }

        public static void CreateTablesDpd()
        {
            Sql.RunQuery(Query.CreateDpdListy());
            Sql.RunQuery(Query.CreateDpdPaczki());
            Sql.RunQuery(Query.CreateDpdUslugi());
            Sql.RunQuery(Query.CreateMapowaniePobrania());
            Sql.RunQuery(Query.CreateMapowanieFormDostawy_Blokowanie());
            Sql.RunQuery(Query.CreateMapowanieFormDostawy_DokumentZwrotny());
        }

        private bool _zamUwagi;
        private bool _magUwagi;
        private bool _handUwagi;
        private bool _zamPd;
        private bool _magPd;
        private bool _hanPd;
        private bool _zamInfDod;
        private bool _zamKomentarz;
        private string _zamPDnr;
        private string _magPDnr;
        private string _hanPDnr;
        private readonly bool _isFormatYmd;
        private bool _zamKolumnaNrListu;

        private void PobierzMiejsceZapisuNrListu()
        {
            _handUwagi = Default.handUwagi;
            _hanPd = Default.handPD;
            _zamKolumnaNrListu = Default.zamKolumnaNrListu;
            if (_hanPd)
                _hanPDnr = Default.handPDnr;

            _magUwagi = Default.magUwagi;
            _magPd = Default.magPD;
            if (_magPd)
                _magPDnr = Default.magPDnr;

            _zamUwagi = Default.zamUwagi;
            _zamInfDod = Default.zamInfDod;
            _zamKomentarz = Default.zamKomentarz;
            _zamPd = Default.zamPD;
            if (_zamPd)
                _zamPDnr = Default.zamPDnr;

        }

        public void ZapiszNumerPrzesylki(string nrListu)
        {
            try
            {
                if (_zamKolumnaNrListu && _typDokumentu == TypDokumentu.Zamowienie)
                {
                    var zap = @"update ZAMOWIENIE set NUMER_PRZESYLKI = '" + nrListu + @"' , ID_OPERATORA_PRZESYLKI = 8 where ID_ZAMOWIENIA = " + IdDokumentu;
                    Sql.RunQuery(zap);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(@"Nie udało się zapisać numeru listu przewozowego do kolumny Numer przesyłki");
                Log.AddLog(ex.ToString());
            }
        }

        public void ZapiszNrListuNaDokumencie(string nrListu)
        {
            try
            {

          
            if (_typDokumentu == TypDokumentu.Dokument_Magazynowy)
            {
                if (_magUwagi)
                {
                    var zap = @"UPDATE DOKUMENT_MAGAZYNOWY
                                       SET UWAGI = '" + nrListu + @"'                   
                                       WHERE ID_DOK_MAGAZYNOWEGO=" + IdDokumentu;
                    Sql.RunQuery(zap);
                }

                if (_magPd)
                {
                    var zap = @"update DOKUMENT_MAGAZYNOWY
            	                    set " + _magPDnr + @" =  '" + nrListu + @"'     
                                where ID_DOK_MAGAZYNOWEGO = " + IdDokumentu;
                    Sql.RunQuery(zap);
                }
            }
            else if (_typDokumentu == TypDokumentu.Dokument_Handlowy)
            {
                if (_handUwagi)
                {
                    var zap = @"UPDATE DOKUMENT_HANDLOWY
                                       SET UWAGI = '" + nrListu + @"'                   
                                       WHERE ID_DOKUMENTU_HANDLOWEGO=" + IdDokumentu;
                    Sql.RunQuery(zap);
                }
                if (_hanPd)
                {
                    var zap = @"update DOKUMENT_HANDLOWY
            	                    set " + _hanPDnr + @" = '" + nrListu + @"'     
                                where ID_DOKUMENTU_HANDLOWEGO = " + IdDokumentu;
                    Sql.RunQuery(zap);
                }
            }
            else if(_typDokumentu == TypDokumentu.Zamowienie)
            {
                if (_zamPd)
                {
                    var zap = @"update zamowienie
	                     set " + _zamPDnr + @" = '" + nrListu + @"'     
                    where id_zamowienia = " + IdDokumentu;
                    Sql.RunQuery(zap);
                }
                if (_zamUwagi)
                {
                    var zap = @"UPDATE ZAMOWIENIE
                           SET UWAGI = '" + nrListu + @"'                   
                           WHERE ID_ZAMOWIENIA=" + IdDokumentu;
                    Sql.RunQuery(zap);
                }
                if (_zamKomentarz)
                {
                    _data = PobierzDane(Query.CheckRejestrZmianStatusZamOpis(IdDokumentu));

                    if (_data != null && _data.Rows.Count > 0 && (int) (_data.Rows?[0]?[0] ?? 0) > 0)
                        Sql.RunQuery(Query.UpdateRejestrZmianStatusZamOpis(IdDokumentu, nrListu));
                    else
                        Sql.RunQuery(Query.InsertRejestrZmianStatusZamOpis(IdDokumentu, nrListu));
                    
                }
                if (_zamInfDod)
                {
                    var zap = @"UPDATE ZAMOWIENIE
                           SET INFORMACJE_DODATKOWE= '" + nrListu + @"'                   
                           WHERE ID_ZAMOWIENIA=" + IdDokumentu;
                    Sql.RunQuery(zap);
                }

             
            }
            }
            catch (Exception ex )
            {
                MessageBox.Show(@"Nie udało się zapisać numeru listu przewozowego na dokumencie");
                Log.AddLog(ex.ToString());           
            }
        }

        public void ZmienStatusZamowienia()
        {
            var zap = @"UPDATE ZAMOWIENIE
                           SET STATUS_ZAM = 'S'         
                           WHERE ID_ZAMOWIENIA=" + IdDokumentu;
            Sql.RunQuery(zap);
        }

    }
}
