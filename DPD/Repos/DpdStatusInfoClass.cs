﻿using System;
using System.Collections.Generic;
using System.Linq;
using DPD.DpdStatusInfo;

namespace DPD.Repos
{
    internal class DpdStatusInfoClass
    {
        private readonly DPDInfoServicesObjEventsService _srv = new DPDInfoServicesObjEventsService();

        private readonly authDataV1 _loginData = new authDataV1();
        private readonly string _language = "PL";

        public DpdStatusInfoClass(string login, string password)
        {
            _loginData.channel = "ITC";
            _loginData.login = login;
            _loginData.password = password;
        }

        public DpdStatusInfoClass()
        {
            _loginData.channel = "ITC";
            _loginData.login = "12411802";
            _loginData.password = "ugfnQxrQuUJMyqhk";
        }

        public bool CheckAvailability()
        {
            try
            {
                var resp = _srv.getEventsForWaybillV1("1111111111", eventsSelectTypeEnum.ONLY_LAST, true, _language, _loginData);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private List<DpdStatusResponse> _list = new List<DpdStatusResponse>();

        public List<string[]> GetLastStatusList(List<string[]> list)
        {
            var statusList = new List<string[]>();

            foreach (var item in list)
            {
                var singleStatus = new string[2];
                singleStatus[0] = item[0];

                string mainWaybill;

                if (item[1].Contains("\r\n"))
                    mainWaybill = item[1].Split(new[] { "\r\n" }, StringSplitOptions.None)[0];
                else if (item[1].Contains(";"))
                    mainWaybill = item[1].Split(new[] { ";" }, StringSplitOptions.None)[0];
                else
                    mainWaybill = item[1];

                try
                {
                    var resp = _srv.getEventsForWaybillV1(mainWaybill, eventsSelectTypeEnum.ONLY_LAST, true, _language, _loginData);

                    if (resp != null && resp.eventsList != null && resp.eventsList.Count() > 0)
                    {
                        singleStatus[1] = resp.eventsList[0].description;
                        statusList.Add(singleStatus);
                    }
                    else
                    {
                        singleStatus[1] = null;
                        statusList.Add(singleStatus);
                    }
                }
                catch (Exception)
                {
                    return null;
                }
            }

            return statusList;
        }

        public string GetLastStatus(string waybill)
        {
            try
            {
                var resp = _srv.getEventsForWaybillV1(waybill, eventsSelectTypeEnum.ONLY_LAST, true, _language, _loginData);

                return resp.eventsList[0].description;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public DpdStatusResponse GetEvents(string waybill)
        {
            string mainWaybill;

            if (waybill.Contains("\r\n"))
                mainWaybill = waybill.Split(new[] { "\r\n" }, StringSplitOptions.None)[0];
            else if (waybill.Contains(";"))
                mainWaybill = waybill.Split(new[] { ";" }, StringSplitOptions.None)[0];
            else
                mainWaybill = waybill;
            try
            {

                var resp = _srv.getEventsForWaybillV1(mainWaybill, eventsSelectTypeEnum.ALL, true, _language, _loginData);

                var eventArray = resp.eventsList;

                var dpd = new DpdStatusResponse();
                dpd.Waybill = eventArray[0].waybill;
                dpd.Events = new List<DpdEvent>();

                foreach (var e in eventArray)
                {
                    var ev = new DpdEvent();
                    ev.Depot = e.depotName;
                    ev.Description = e.description;
                    ev.PackageRef = e.packageReference;
                    ev.ParcelRef = e.parcelReference;
                    //ev.time = e.eventTime.Replace("T", " ");
                    //"2015-09-30T09:00:12.278";
                    var tab = e.eventTime.Split('T');
                    var tabDate = tab[0].Split('-');
                    var tabTime = tab[1].Split(':');

                    var date = new DateTime(Convert.ToInt32(tabDate[0]), Convert.ToInt32(tabDate[1]), Convert.ToInt32(tabDate[2]),
                       Convert.ToInt32(tabTime[0]), Convert.ToInt32(tabTime[1]), Convert.ToInt32(tabTime[2].Split('.')[0]));

                    ev.Date = date;

                    dpd.Events.Add(ev);
                }

                return dpd;
            }
            catch (Exception)
            {
                return null;
            }
        }

    }

    public class DpdStatusResponse
    {
        public string Waybill { get; set; }
        public List<DpdEvent> Events { get; set; }
    }

    public class DpdEvent
    {
        public string Description { get; set; }
        public string Depot { get; set; }
        public DateTime Date { get; set; }
        public string PackageRef { get; set; }
        public string ParcelRef { get; set; }
    }
}
