﻿using System;
using System.Windows.Forms;
using NLog;

namespace DPD.Repos
{
    public static class Log
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public static void AddLog(string msg)
        {
            try
            {
                Logger.Error(msg);
#if DEBUG
                 MessageboxShowError(@"DEBUG MODE ERROR: " +Environment.NewLine+ msg);
#endif
            }
            catch (Exception)
            {
                // ignored
            }
        }

        public static void AddLogWarning(string msg)
        {
            try
            {
                Logger.Warn(msg);
#if DEBUG
                 MessageboxShowError(@"DEBUG MODE WARNING: " + Environment.NewLine + msg);
#endif
            }
            catch (Exception)
            {
                // ignored
            }
        }

        public static void AddLogImportCsvInfo(string msg)
        {
            try
            {
                Logger.Info(msg);
#if DEBUG
                 MessageboxShowError(@"DEBUG MODE CSV INFO: " + Environment.NewLine + msg);
#endif
            }
            catch (Exception)
            {
                // ignored
            }
        }

        public static void MessageboxShowError(string msg)
        {
            MessageBox.Show(msg, @"Administrator", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        public static void MessageboxShowWarning(string msg)
        {
            MessageBox.Show(msg, @"Administrator", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }

        public static void MessageboxShowInfo(string msg)
        {
            MessageBox.Show(msg, @"Administrator", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
        }

        public static DialogResult MessageboxShowQuestionYesNo(string msg)
        {
            return MessageBox.Show(msg, @"Administrator", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
        }

    }
}
