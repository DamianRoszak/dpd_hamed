﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DPD.Models;
using RestSharp;

namespace DPD.Classess
{
    internal class ParcelShopClass
    {
        private string ClientUrl { get; }
        private string Key { get; }
        private string RequestId { get; }

        public ParcelShopClass(string clientUrl, string key, string requestId)
        {
            Key = key;
            RequestId = requestId;
            ClientUrl = clientUrl;
        }

        public IRestResponse<ParcelShopModel> GetParcelShopByAddress(string countryCode, string zipcode, string city, DateTime dateFrom)
        {
            var client = new RestClient(ClientUrl);
            var request = new RestRequest("api/pudo/list/byaddress", Method.GET);
            request.AddParameter("Key", Key);
            request.AddParameter("requestID", RequestId);

            request.AddParameter("countryCode", countryCode);
            request.AddParameter("zipCode", zipcode);
            request.AddParameter("city", city);
            request.AddParameter("holiday_tolerant", "3");
            request.AddParameter("date_from", dateFrom.ToString("dd/MM/yyyy"));

            return client.Execute<ParcelShopModel>(request);
        }

        public IRestResponse<ParcelShopModel> GetParcelShopByLondLat(string longitude, string latitude, string maxDistanceSearch, DateTime dateFrom)
        {
            var client = new RestClient(ClientUrl);
            var request = new RestRequest("api/pudo/list/bylonglat", Method.GET);
            request.AddParameter("Key", Key);
            request.AddParameter("requestID", RequestId);

            request.AddParameter("longitude", longitude);
            request.AddParameter("latitude", latitude);
            request.AddParameter("max_distance_search", maxDistanceSearch);
            request.AddParameter("holiday_tolerant", "3");
            request.AddParameter("date_from", dateFrom.ToString("dd/MM/yyyy"));

            return client.Execute<ParcelShopModel>(request);
        }
    }
}
