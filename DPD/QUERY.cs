﻿using System;
using System.Windows.Forms;
using DPD.Models;
using DPD.Properties;

namespace DPD
{
    public static class Query
    {
        public static string WersjaWfMag()
        {
            return @"SELECT TOP 1 [IDTAB]
                      ,[PRGKOD]
                      ,[PRGNAZWA]
                      ,[PRGWER]
                      ,[DBWER]
                      ,[START]
                      ,[ConfigInfo]
                      ,[WORKSTATIONS]
                  FROM [WAPRODBSTATE]
                  where prgkod = 3";
        }

        public static string AdresNadawcy(string idKontrahenta)
        {
            return
                $@"select f.NAZWA_PELNA nazwa, (a.ULICA +' '+ isnull(a.NR_DOMU,'') +' '+isnull(a.NR_LOKALU,'')) AS 'ULICA', 
            a.KOD_POCZTOWY, a.MIEJSCOWOSC, a.SYM_KRAJU as 'kraj', a.TELEFON as 'tel',a.E_MAIL as 'mail',
			case when (f.id_adresu_domyslnego =a.ID_ADRESY_FIRMY) then 1 else 0 end domyslny
from kontrahent k, Firma f, ADRESY_FIRMY a
where k.ID_FIRMY = f.ID_FIRMY and 
    a.ID_FIRMY = f.ID_FIRMY and 
    k.id_kontrahenta= {idKontrahenta}
order by domyslny desc";

            //            return string.Format(@"select f.NAZWA_PELNA nazwa, (a.ULICA +' '+ isnull(a.NR_DOMU,'') +' '+isnull(a.NR_LOKALU,'')) AS 'ULICA', 
            //                                    a.KOD_POCZTOWY, a.MIEJSCOWOSC, a.SYM_KRAJU as 'kraj', a.TELEFON as 'tel',a.E_MAIL as 'mail'
            //                        from kontrahent k, Firma f, ADRESY_FIRMY a
            //                        where k.ID_FIRMY = f.ID_FIRMY and 
            //                            a.ID_FIRMY = f.ID_FIRMY and 
            //                            k.id_kontrahenta= {0}", idKontrahenta);
        }
        
        public static string AdresDostawyZam(string idZamowienia)
        {
            return
                $@"select md.ID_MIEJSCA_DOSTAWY, md.SYM_KRAJU kraj, md.KOD_POCZTOWY kod, md.MIEJSCOWOSC, md.ULICA_LOKAL ulica, 
			md.FIRMA, md.ODBIORCA, md.TEL   
            from DOSTAWA d inner join MIEJSCE_DOSTAWY md on md.ID_MIEJSCA_DOSTAWY = d.ID_MIEJSCA_DOSTAWY
            where d.ID_ZAMOWIENIA = {idZamowienia}";
        }

        public static string FormaDostawyDokumentHandlowy(string idDokumentu)
        {
            return $@"select fd.NAZWA from DOKUMENT_HANDLOWY dh
            left join POZYCJA_DOKUMENTU_MAGAZYNOWEGO pdm on dh.ID_DOKUMENTU_HANDLOWEGO = pdm.ID_DOK_HANDLOWEGO
            left join DOSTAWA d on d.ID_DOK_MAGAZYNOWEGO = pdm.ID_DOK_MAGAZYNOWEGO
            left join FORMA_DOSTAWY fd on d.ID_FORMY_DOSTAWY = fd.ID_FORMY_DOSTAWY
            where dh.ID_DOKUMENTU_HANDLOWEGO = {idDokumentu}";
        }

        public static string AdresFirmy(string idZamowienia)
        {
            return $@"Select top 1 f.NAZWA as 'Nazwa',af.TELEFON as 'NumerTelefonu',af.MIEJSCOWOSC as 'Miejscowosc',
            af.ULICA as 'Ulica',af.KOD_POCZTOWY as 'KodPocztowy' from FIRMA f
            left join ZAMOWIENIE z on z.ID_FIRMY = f.ID_FIRMY
            left join ADRESY_FIRMY af on af.ID_FIRMY = f.ID_FIRMY
            where z.ID_ZAMOWIENIA = {idZamowienia}";
        }

        public static string PobierzWszystkieFormyDostawy()
        {
            return $@"Select NAZWA from FORMA_DOSTAWY";
        }
       
        public static string AdresDostawyZDokumentuMagazynowego(string idDokHandlowy)
        {
            return $@"select TOP 1 fd.NAZWA as 'nazwa', md.ID_MIEJSCA_DOSTAWY as 'MiejsceDostawy', md.SYM_KRAJU as 'kraj', md.KOD_POCZTOWY as 'kod', md.MIEJSCOWOSC as 'miejscowosc',
            md.ULICA_LOKAL as 'ulica', md.FIRMA , md.ODBIORCA as 'ODBIORCA', md.TEL as 'tel' from DOKUMENT_HANDLOWY dh
            left join POZYCJA_DOKUMENTU_MAGAZYNOWEGO pdm on dh.ID_DOKUMENTU_HANDLOWEGO = pdm.ID_DOK_HANDLOWEGO
            left join DOKUMENT_MAGAZYNOWY dm on pdm.ID_DOK_MAGAZYNOWEGO = dm.ID_DOK_MAGAZYNOWEGO
            left join DOSTAWA d on d.ID_DOK_MAGAZYNOWEGO = dm.ID_DOK_MAGAZYNOWEGO
            left join FORMA_DOSTAWY fd on d.ID_FORMY_DOSTAWY = fd.ID_FORMY_DOSTAWY
            left join MIEJSCE_DOSTAWY md on md.ID_MIEJSCA_DOSTAWY = d.ID_MIEJSCA_DOSTAWY
            where dh.ID_DOKUMENTU_HANDLOWEGO = {idDokHandlowy}";
        }

        public static string AdresDostawyMag(string idDokMag)
        {
            return
                $@"select md.ID_MIEJSCA_DOSTAWY, md.SYM_KRAJU kraj, md.KOD_POCZTOWY kod, md.MIEJSCOWOSC, md.ULICA_LOKAL ulica, 
			md.FIRMA, md.ODBIORCA, md.TEL 
from DOSTAWA d inner join 
	MIEJSCE_DOSTAWY md on md.ID_MIEJSCA_DOSTAWY = d.ID_MIEJSCA_DOSTAWY
where d.ID_DOK_MAGAZYNOWEGO =  {idDokMag}";
        }

        public static string MiejscaDostawyKontrahenta(string idKontrahenta)
        {
            return
                $@"select md.ID_MIEJSCA_DOSTAWY, md.SYM_KRAJU kraj, md.KOD_POCZTOWY kod, md.MIEJSCOWOSC, md.ULICA_LOKAL ulica, 
	md.FIRMA, md.ODBIORCA, md.TEL, (case when(md.id_miejsca_dostawy = k.ID_MIEJSCA_DOSTAWY_DOM) then '1' else '0' end) as domyslny from miejsce_dostawy md inner join 
	KONTRAHENT k on k.ID_KONTRAHENTA = md.ID_KONTRAHENTA
where k.ID_KONTRAHENTA =  {idKontrahenta}
order by domyslny desc";
        }

        public static string AdresKontrahenta(string idKontrahenta)
        {
            return string.Format(@"select {1} as nazwa, KOD_POCZTOWY kod, MIEJSCOWOSC, ULICA_LOKAL ulica, SYM_KRAJU kraj,TELEFON_FIRMOWY tel, ADRES_EMAIL 
from KONTRAHENT
where ID_KONTRAHENTA = {0}", idKontrahenta, Properties.Settings.Default.poleNazwa);
        }

        public static string AdresKontrahentaV78(string idKontrahenta)
        {
            return string.Format(@"select {1} as nazwa, KOD_POCZTOWY kod, MIEJSCOWOSC, ULICA_LOKAL ulica, SYM_KRAJU kraj, ADRES_EMAIL 
from KONTRAHENT
where ID_KONTRAHENTA = {0}", idKontrahenta, Properties.Settings.Default.poleNazwa);
        }

        public static string KwotaNumer(string idDokumentu, TypDokumentu typDokumentu)
        {
            var tmp = "";
            switch (typDokumentu)
            {
                case TypDokumentu.Zamowienie: { tmp = "id_zamowienia"; break; }
                case TypDokumentu.Dokument_Magazynowy: { tmp = "id_dok_magazynowego"; break; }
                case TypDokumentu.Dokument_Handlowy: { tmp = "id_dokumentu_handlowego"; break; }
            }

            return $@"select wartosc_brutto, numer from {typDokumentu}
where {tmp} =  {idDokumentu}";
        }

        public static string Waga(string idDokumentu, TypDokumentu typDokumentu)
        {
            switch (typDokumentu)
            {
                case TypDokumentu.Zamowienie:
                {
                    return @"select sum(d.masa*d.ilosc) from
                            (select 
                            case when a.JED_WAGI = 'g' then a.WAGA / 1000 
                             when a.JED_WAGI = 'dkg' then a.WAGA / 100 
                             when a.JED_WAGI = 't' then a.WAGA * 1000 
                             when a.JED_WAGI= 'kg' then a.WAGA
                             end as 'masa', pz.ZAMOWIONO as 'ilosc'
                            from ARTYKUL a
                            join POZYCJA_ZAMOWIENIA pz on pz.ID_ARTYKULU = a.ID_ARTYKULU
                            join ZAMOWIENIE z on pz.ID_ZAMOWIENIA = z.ID_ZAMOWIENIA
                            where z.ID_ZAMOWIENIA = " + idDokumentu + ") as d";
                }
                case TypDokumentu.Dokument_Magazynowy:
                {
                    return @"select sum (d.masa*d.ilosc) from
                            (select 
                            case when a.JED_WAGI = 'g' then a.WAGA / 1000 
                             when a.JED_WAGI = 'dkg' then a.WAGA / 100 
                             when a.JED_WAGI = 't' then a.WAGA * 1000 
                             when a.JED_WAGI= 'kg' then a.WAGA
                             end as 'masa', pdm.ILOSC as 'ilosc'
                            from ARTYKUL a
                            join POZYCJA_DOKUMENTU_MAGAZYNOWEGO pdm on pdm.ID_ARTYKULU = a.ID_ARTYKULU
                            join DOKUMENT_MAGAZYNOWY dm on pdm.ID_DOK_MAGAZYNOWEGO = dm.ID_DOK_MAGAZYNOWEGO
                            where dm.ID_DOK_MAGAZYNOWEGO = " + idDokumentu + ") as d";
                }
                case TypDokumentu.Dokument_Handlowy:
                {
                    return @"select sum (d.masa*d.ilosc) from
                            (select 
                            case when a.JED_WAGI = 'g' then a.WAGA / 1000 
                             when a.JED_WAGI = 'dkg' then a.WAGA / 100 
                             when a.JED_WAGI = 't' then a.WAGA * 1000 
                             when a.JED_WAGI= 'kg' then a.WAGA
                             end as 'masa', pdm.ILOSC as 'ilosc'
                            from ARTYKUL a
                            join POZYCJA_DOKUMENTU_MAGAZYNOWEGO pdm on pdm.ID_ARTYKULU = a.ID_ARTYKULU
                            join DOKUMENT_MAGAZYNOWY dm on dm.ID_DOK_MAGAZYNOWEGO = pdm.ID_DOK_MAGAZYNOWEGO
                            join DOKUMENT_HANDLOWY dh on dh.ID_DOKUMENTU_HANDLOWEGO = dm.ID_DOKUMENTU_HANDLOWEGO
                            where dh.ID_DOKUMENTU_HANDLOWEGO = " + idDokumentu + ") as d";
                }
                default:
                    return null;
            }
        }
        
        public static string IdKontrahenta(string idDokumentu, TypDokumentu typDokumentu)
        {
            var docType = string.Empty;
            var tmp = string.Empty;
            
            switch (typDokumentu)
            {
                case TypDokumentu.Zamowienie:
                {
                    tmp = "id_zamowienia";
                    docType = "ZAMOWIENIE";
                    break;
                }
                case TypDokumentu.Dokument_Magazynowy:
                {
                    tmp = "id_dok_magazynowego";
                    docType = "DOKUMENT_MAGAZYNOWY";
                    break;
                }
                case TypDokumentu.Dokument_Handlowy:
                {
                    tmp = "id_dokumentu_handlowego";
                    docType = "DOKUMENT_HANDLOWY";
                    break;
                }
            }

            return $@"select ID_KONTRAHENTA from {docType} where {tmp} =  {idDokumentu}";
        }

        public static string Pola(string idDokumentu, TypDokumentu typDokumentu)
        {
            var tmp = "";
            switch (typDokumentu)
            {
                case TypDokumentu.Zamowienie:
                {
                    tmp = "id_zamowienia";
                    break;
                }
                case TypDokumentu.Dokument_Magazynowy:
                {
                    tmp = "id_dok_magazynowego";
                    break;
                }
                case TypDokumentu.Dokument_Handlowy:
                {
                    tmp = "id_dokumentu_handlowego";
                    break;
                }
            }
            if (typDokumentu == TypDokumentu.Zamowienie)
            {
                return
                    $@"select numer,nr_zamowienia_klienta, uwagi, pole1, pole2, pole3, pole4, pole5, pole6, pole7, pole8, pole9, pole10 from {typDokumentu} 
                                        where {tmp} = {idDokumentu}";
            }
            return
                $@"select numer, uwagi, pole1, pole2, pole3, pole4, pole5, pole6, pole7, pole8, pole9, pole10 from {typDokumentu} 
                                        where {tmp} = {idDokumentu}";
        }
        
        public static string Zamowienia(string nr, int from, int to, string kontrahent,string magazyn)
        {
            return
                $@"select z.id_zamowienia id, z.id_kontrahenta, z.numer ,k.nazwa, cast(z.data-36163 as datetime) data,z.wartosc_brutto,z.FORMA_PLATNOSCI, case when(d.id_dok is not null) then 1 else 0 end as DPD
    from zamowienie z inner join
        kontrahent k on k.id_kontrahenta = z.id_kontrahenta  left join
         (select distinct( id_dok )from _DPD_listy where typ = 202) d on d.id_dok = z.id_zamowienia
    where z.numer like '%{nr}%' and z.data >= {from} and z.data <= {to} and k.nazwa like '%{kontrahent}%' and z.ID_MAGAZYNU ='{magazyn}'";
        }
        
        public static string DokumentyMagazynowe(string nr, int from, int to, string kontrahent,string magazyn)
        {
            return
                $@"select id_dok_magazynowego id, k.id_kontrahenta, numer, k.nazwa, cast(m.data-36163 as datetime) data,m.wartosc_brutto , case when(d.id_dok is not null) then 1 else 0 end as DPD
    from dokument_magazynowy m inner join 
        kontrahent k on m.id_kontrahenta = k.id_kontrahenta left join
         (select distinct( id_dok )from _DPD_listy where typ = 1) d on d.id_dok = m.id_dok_magazynowego
    where m.numer like '%{nr}%' and m.data >= {from} and m.data <= {to} and k.nazwa like '%{kontrahent}%' and m.ID_MAGAZYNU = '{magazyn}'";
        }

        public static string DokumentyHandlowe(string nr, int from, int to, string kontrahent,string magazyn)
        {
            return
                $@"select id_dokumentu_handlowego id, k.id_kontrahenta, numer, k.nazwa, cast(h.data_wystawienia-36163 as datetime) data, h.wartosc_brutto ,h.FORMA_PLATNOSCI, case when(d.id_dok is not null) then 1 else 0 end as DPD
    from dokument_handlowy h inner join 
        kontrahent k on h.id_kontrahenta = k.id_kontrahenta left join
         (select distinct( id_dok )from _DPD_listy where typ = 4) d on d.id_dok = h.id_dokumentu_handlowego
    where h.numer like '%{nr}%' and h.data_wystawienia >= {from} and h.data_wystawienia <= {to} and k.nazwa like '%{kontrahent}%' and h.ID_MAGAZYNU = '{magazyn}'";
        }

        public static string CreateDpdListy()
        {
            var query = @"IF (not EXISTS (SELECT * 
                FROM INFORMATION_SCHEMA.TABLES 
                WHERE TABLE_SCHEMA = 'dbo' 
                AND  TABLE_NAME = '_DPD_LISTY'))
                BEGIN
                    create table _DPD_LISTY(
                        id int identity primary key,
	                    id_dok int  null,
	                    typ int  null,
	                    numer_dok varchar(500),
	                    nr_listu varchar(max) not null,
	                    nr_protokolu varchar(10) null,
	                    etykieta bit null,
	                    data_listu datetime not null,
	                    data_protokolu datetime null,
	                    sessionType varchar (15),
	                    adres_nadawcy_nazwa varchar(100),
	                    adres_nadawcy_imieNazwisko varchar(100),
	                    adres_nadawcy_ulica varchar(100),
	                    adres_nadawcy_kod varchar(10),
	                    adres_nadawcy_kraj varchar(2),
	                    adres_nadawcy_miejscowosc varchar(50),
	                    adres_nadawcy_tel varchar(100),
	                    adres_nadawcy_mail varchar(100),

	                    adres_odbiorcy_nazwa varchar(100),
	                    adres_odbiorcy_imieNazwisko varchar(100),
	                    adres_odbiorcy_ulica varchar(100),
	                    adres_odbiorcy_kod varchar(10),
	                    adres_odbiorcy_kraj varchar(2),
	                    adres_odbiorcy_miejscowosc varchar(50),
	                    adres_odbiorcy_tel varchar(100),
	                    adres_odbiorcy_mail varchar(100),

	                    ref1 varchar(100),
	                    ref2 varchar(100),
	                    platnik varchar(25),
	                    platnikFID int,
                        ID_ETYKIETY_SYS [numeric](18, 0) NOT NULL default(0)
                )
                END";

            query += AddColumn("_DPD_LISTY", "ID_ETYKIETY_SYS", "[numeric](18, 0) NOT NULL default(0)");
            return query;
        }

        public static string CreateDpdPaczki()
        {
            var query = @"IF (not EXISTS (SELECT * 
                FROM INFORMATION_SCHEMA.TABLES 
                WHERE TABLE_SCHEMA = 'dbo' 
                AND  TABLE_NAME = '_DPD_PACZKI'))
                BEGIN
                    create table _DPD_PACZKI(
                        id int foreign key references _DPD_LISTY(id),
	                    lp int,
	                    waga decimal(14,2),
	                    zawartosc varchar(300),
	                    uwagi varchar(200),
	                    dlugosc int,
	                    szerokosc int,
	                    wysokosc int,
                        ID_ETYKIETY_SYS [numeric](18, 0) NOT NULL default(0)
                )
                END";

            query += AddColumn("_DPD_PACZKI", "ID_ETYKIETY_SYS", "[numeric](18, 0) NOT NULL default(0)");
            return query;
        }
        
        private static string AddColumn(string tableName, string field, string properties)
        {

            return
                      $@" --dodajemy {field}
                           IF NOT EXISTS (
                             SELECT *
                             FROM   sys.columns
                             WHERE  object_id = OBJECT_ID(N'{tableName}') AND name = '{field}'
                            )
                            BEGIN
                                    ALTER TABLE {tableName}
                                    ADD {field} {properties}
                            END ";
        }

        public static string CreateDpdUslugi()
        {
            var query = @"IF (not EXISTS (SELECT * 
                FROM INFORMATION_SCHEMA.TABLES 
                WHERE TABLE_SCHEMA = 'dbo' 
                AND  TABLE_NAME = '_DPD_USLUGI'))
                BEGIN
                    create table _DPD_USLUGI(
                        id int foreign key references _DPD_LISTY(id),
	                    doreczenieGwarantowane bit,
	                    doreczenieGwarantowaneTyp varchar(15),
	                    doreczenieGwarantowaneWartosc varchar(5),
	                    doreczenieDoRakWlasnych bit,
	                    doreczenieNaAdresPryw bit,
	                    pobranie bit,
	                    pobranieKwota decimal(14,2),
	                    odbiorWlasny bit,
	                    odbiorWlasnyTyp varchar(10),
	                    dokumentyZwrotne bit,
	                    DOX bit,
	                    przesylkaZwrotna bit,
	                    opony bit,
	                    oponyExport bit,
	                    przesylkaWartosciowa bit,
	                    przesylkaWartosciowaKwota decimal(14,2),
                        przesylkaPaletowa bit,
	                    pudo bit not null default(0),
	                    pudoWartosc varchar(50),
	                    duty bit not null default(0),
	                    dutyWartosc varchar(50),
	                    dpdExpress bit not null default(0),
                        ID_ETYKIETY_SYS [numeric](18, 0) NOT NULL default(0)
                )
                END";

            query += AddColumn("_DPD_USLUGI", "przesylkaPaletowa", "bit");
            query += AddColumn("_DPD_USLUGI", "pudo", "bit not null default(0)");
            query += AddColumn("_DPD_USLUGI", "pudoWartosc", "varchar(50)");
            query += AddColumn("_DPD_USLUGI", "duty", "bit not null default(0)");
            query += AddColumn("_DPD_USLUGI", "dutyWartosc", "varchar(50)");
            query += AddColumn("_DPD_USLUGI", "dpdExpress", "bit not null default(0)");
            query += AddColumn("_DPD_USLUGI", "ID_ETYKIETY_SYS", "[numeric](18, 0) NOT NULL default(0)");

            return query;
        }

        public static string CreateMapowaniePobrania()
        {
            var query = @"IF (not EXISTS (SELECT * 
                FROM INFORMATION_SCHEMA.TABLES 
                WHERE TABLE_SCHEMA = 'dbo' 
                AND  TABLE_NAME = '_DPD_MAPOWANIEPOBRANIA'))
                BEGIN
                create table _DPD_MAPOWANIEPOBRANIA(
                        id Int,
	                    nazwa VARCHAR(255),
	                    is_enabled Int,
                        ID_ETYKIETY_SYS [numeric](18, 0) NOT NULL default(0)
                )
                END";

            query += AddColumn("_DPD_MAPOWANIEPOBRANIA", "ID_ETYKIETY_SYS", "[numeric](18, 0) NOT NULL default(0)");
            return query;
        }

        public static string CreateMapowanieFormDostawy_Blokowanie()
        {
            var query = @"IF (not EXISTS (SELECT * 
                FROM INFORMATION_SCHEMA.TABLES 
                WHERE TABLE_SCHEMA = 'dbo' 
                AND  TABLE_NAME = '_DPD_MAPOWANIEFORMYDOSTAWY_BLOKOWANIE'))
                BEGIN
                create table _DPD_MAPOWANIEFORMYDOSTAWY_BLOKOWANIE(
                        id Int,
	                    nazwa VARCHAR(255),
	                    is_enabled Int,
                )
                END";

            return query;
        }

        public static string CreateMapowanieFormDostawy_DokumentZwrotny()
        {
            var query = @"IF (not EXISTS (SELECT * 
                FROM INFORMATION_SCHEMA.TABLES 
                WHERE TABLE_SCHEMA = 'dbo' 
                AND  TABLE_NAME = '_DPD_MAPOWANIEFORMYDOSTAWY_DOKUMENTZWROTNY'))
                BEGIN
                create table _DPD_MAPOWANIEFORMYDOSTAWY_DOKUMENTZWROTNY(
                        id Int,
	                    nazwa VARCHAR(255),
	                    is_enabled Int,
                )
                END";
            return query;
            
        }

        public static string CreateOpDodV792(string kodGrupy, string nazwa, string kodOperacji, string path)
        {
            return
                $@"
INSERT INTO [MAG_OPERACJE_DODATKOWE]
           ([KOD_GRUPY]
           ,[NAZWA]
           ,[SKROT_KLAWISZOWY]
           ,[SKROT_KLAWISZOWY_KOD]
           ,[KOD_OPERACJI]
           ,[GRUPOWANIE]
           ,[FUNKCJA_ZEWNETRZNA]
           ,[PARAMETRY]
           ,[SCIEZKA]
           ,[INFO]
           ,[ODSWIEZ_LISTE]
           ,[KASUJ_ZAZNACZENIE])
     VALUES
           ('{kodGrupy}',  -- DOKMAG_BRW	DOKHAN_BRW	 ZAM_BRW
           '{nazwa}',
           '', 
           0,
           '1111_000_{kodOperacji}',
           0,
           1,
           '0001000010000',
           '{path}',
           null,
           0, 
           1)

";
        }
        
        internal static string MailKontrahenta(string idKontrahenta)
        {
            return
                $@"select case when k.ADRES_EMAIL = '' then kk.E_MAIL
			when k.ADRES_EMAIL is null then  kk.E_MAIL
			else k.ADRES_EMAIL end as adres_email
from kontrahent k left join KONTAKT kk on k.ID_KONTRAHENTA = kk.ID_KONTRAHENTA
where k.id_kontrahenta = {idKontrahenta} and kk.DOMYSLNY = 1";
        }

        internal static string MailKontrahentaBezDomyslnego(string idKontrahenta)
        {
            return
                $@"select top 1 k.ADRES_EMAIL as adres_email
                                    from kontrahent k 
                                    where k.id_kontrahenta = {idKontrahenta}";
        }

        public static string GetMagazynNazwaId()
        {
            return @"select NAZWA, ID_MAGAZYNU from MAGAZYN";
        }

        public static string CheckRejestrZmianStatusZamOpis(string idDokumentu)
        {
            return @"select count(*) from MAG_REJESTR_ZMIAN_STATUSOW_ZAMOWIEN_OPIS
	                            where id_rejestru_zmian =
		                            (select id_rejestru_zmian from MAG_REJESTR_ZMIAN_STATUSOW_ZAMOWIEN as A
		                            where dataiczas = 
			                            ( select max(dataiczas) 
				                            from MAG_REJESTR_ZMIAN_STATUSOW_ZAMOWIEN as B
				                            where B.id_dokumentu =" + idDokumentu + "))";
        }

        public static string UpdateRejestrZmianStatusZamOpis(string idDokumentu, string nrListu)
        {
            return @"update MAG_REJESTR_ZMIAN_STATUSOW_ZAMOWIEN_OPIS
                            set opis = '" + nrListu + @"' 
                            where id_opisu = (
                            select id_opisu from MAG_REJESTR_ZMIAN_STATUSOW_ZAMOWIEN_OPIS
	                            where id_rejestru_zmian =
		                            (select id_rejestru_zmian from MAG_REJESTR_ZMIAN_STATUSOW_ZAMOWIEN as A
		                            where dataiczas = 
			                            ( select max(dataiczas) 
				                            from MAG_REJESTR_ZMIAN_STATUSOW_ZAMOWIEN as B
				                            where B.id_dokumentu =" + idDokumentu + ")))";
        }

        public static string InsertRejestrZmianStatusZamOpis(string idDokumentu, string nrListu)
        {
            return $@"INSERT INTO [dbo].[MAG_REJESTR_ZMIAN_STATUSOW_ZAMOWIEN_OPIS]
                   ([ID_REJESTRU_ZMIAN]
                   ,[ID_DOKUMENTU]
                   ,[OPIS])

                   VALUES
                           (
                            (select id_rejestru_zmian 
		                    from MAG_REJESTR_ZMIAN_STATUSOW_ZAMOWIEN as A
		                    where dataiczas = 
			                     (select max(dataiczas) 
				                  from MAG_REJESTR_ZMIAN_STATUSOW_ZAMOWIEN as B
				                  where B.id_dokumentu ={idDokumentu}))
                           ,{idDokumentu}
                           ,'{nrListu}')";
        }

        public static string CreateDpdNumkat()
        {
           
            var query = @"IF (not EXISTS (SELECT * 
                FROM INFORMATION_SCHEMA.TABLES 
                WHERE TABLE_SCHEMA = 'dbo' 
                AND  TABLE_NAME = '_DPD_NUMKAT'))
                BEGIN
                    create table _DPD_NUMKAT
                    (
	                    numer int not null,
	                    nazwa varchar(100),
	                    domyslny bit,
                        ID_ETYKIETY_SYS [numeric](18, 0) NOT NULL default(0)
                    )
                END";

            query += AddColumn("_DPD_NUMKAT", "ID_ETYKIETY_SYS", "[numeric](18, 0) NOT NULL default(0)");

            return query;
        }
    }
}
