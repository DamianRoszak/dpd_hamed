﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using DPD.Helpers;

namespace DPD
{
    internal static class Raport
    {
        private static List<Przesylka> _przesylki = new List<Przesylka>();
        private static List<Paczka> _paczki = new List<Paczka>();

        private static string _queryPobierzPrzesylki;
        private static string _queryPobierzPaczki;

        private static DateTime _dataEtykiety;
        private static DateTime _dataProtokolu;

        private static string _sciezka;

        private static string PrzygotujNaglowek()
        {
            var sb = new StringBuilder();
            sb.Append("Numer Paczki;");
            sb.Append("Numer Płatnika;");
            sb.Append("Nazwa nadawcy;");
            sb.Append("Imię i nazwisko nadawcy;");
            sb.Append("Adres nadawcy;");
            sb.Append("Kod pocztowy nadawcy;");
            sb.Append("Miasto nadawcy;");
            sb.Append("Kraj nadawcy;");

            sb.Append("Nazwa odbiorcy;");
            sb.Append("Imię i nazwisko odbiorcy;");
            sb.Append("Adres odbiorcy;");
            sb.Append("Kod pocztowy odbiorcy;");
            sb.Append("Miasto odbiorcy;");
            sb.Append("Kraj odbiorcy;");

            sb.Append("Ref1;");
            sb.Append("Ref2;");
            sb.Append("Zawartość;");
            sb.Append("COD;");
            sb.Append("Ubezpieczenie;");
            sb.Append("Usługi;");
            sb.Append("Waga;");
            sb.Append("Paczka;");

            sb.Append("Data wygenerowania etykiety;");
            sb.Append("Data wygenerowania protokolu;");


            return sb.ToString();
        }

        private static void WyczyscListy()
        {
            _przesylki = new List<Przesylka>();
            _paczki = new List<Paczka>();
        }

        private static void PrzygotujZapytanieFiltr(DateTime from, DateTime to)
        {
            _queryPobierzPaczki =
                $@"select p.* from _DPD_PACZKI p
	inner join _DPD_LISTY l on l.id = p.id
where l.data_listu > '{@from
                    .ToShortDateString()} 00:00:00'
    and l.data_listu < '{to.ToShortDateString()} 23:59:59'";

            _queryPobierzPrzesylki =
                $@"
select l.id, nr_listu, platnikFID, 
	adres_nadawcy_nazwa, adres_nadawcy_imieNazwisko, adres_nadawcy_ulica, adres_nadawcy_kod, adres_nadawcy_miejscowosc, adres_nadawcy_kraj,
	adres_odbiorcy_nazwa, adres_odbiorcy_imieNazwisko, adres_odbiorcy_ulica, adres_odbiorcy_kod, adres_odbiorcy_miejscowosc, adres_odbiorcy_kraj,
	ref1, ref2, u.pobranieKwota, case when u.przesylkaWartosciowaKwota = 0 then null else u.przesylkaWartosciowaKwota end przesylkaWartosciowaKwota, 
	(case when dox = 1 then 'DOX,' else '' end) +
					(case when dokumentyZwrotne = 1 then 'DZ, ' else '' end)+
					(case when przesylkaZwrotna = 1 then 'PZ, ' else '' end)+
					(case when doreczenieDoRakWlasnych = 1 then 'DRW, ' else '' end)+
					(case when opony = 1 then 'Opony, ' else '' end)+
					(case when oponyExport = 1 then 'Opony int, ' else '' end)+
                    (case when przesylkaPaletowa = 1 then 'Palety, ' else '' end)+
					(case when doreczenieNaAdresPryw = 1 then 'PRYW, ' else '' end)+
					(case when odbiorWlasny = 1 then 
								 (case when odbiorWlasnyTyp = 'Osoba' then 'ODB PRYW, '
									   when odbiorWlasnyTyp = 'Firma' then 'ODB  FIRMA, ' 
								  end)
						  else '' end) +
					(case when doreczenieGwarantowane = 1 then 
							 (case when doreczenieGwarantowaneTyp = 'DPD 9:30' then 'DOR 9:30, '
								   when doreczenieGwarantowaneTyp = 'DPD 12:00' then 'DOR 12:00, ' 
								   when doreczenieGwarantowaneTyp = 'Sobota' then 'SOB, '  
								   when doreczenieGwarantowaneTyp = 'DPD na godzinę' then 'DOR ' + doreczenieGwarantowaneWartosc   
							  end)
						  else '' end ) Usługi,
	 l.data_listu, l.data_protokolu--, u.*
from _DPD_LISTY l
	inner join _DPD_USLUGI u on l.id = u.id
where data_listu > '{@from
                    .ToShortDateString()} 00:00:00'
    and data_listu < '{to.ToShortDateString()} 23:59:59'";
        }

        private static void PrzygotujZapytanieNaPodstawieId(List<string> idki)
        {
            _queryPobierzPaczki =
                $@"select p.* from _DPD_PACZKI p
	inner join _DPD_LISTY l on l.id = p.id
where l.id in ({string.Join(
                    ",", idki.ToArray())})";

            _queryPobierzPrzesylki =
                $@"
select l.id, nr_listu, platnikFID, 
	adres_nadawcy_nazwa, adres_nadawcy_imieNazwisko, adres_nadawcy_ulica, adres_nadawcy_kod, adres_nadawcy_miejscowosc, adres_nadawcy_kraj,
	adres_odbiorcy_nazwa, adres_odbiorcy_imieNazwisko, adres_odbiorcy_ulica, adres_odbiorcy_kod, adres_odbiorcy_miejscowosc, adres_odbiorcy_kraj,
	ref1, ref2, u.pobranieKwota, case when u.przesylkaWartosciowaKwota = 0 then null else u.przesylkaWartosciowaKwota end przesylkaWartosciowaKwota, 
	(case when dox = 1 then 'DOX,' else '' end) +
					(case when dokumentyZwrotne = 1 then 'DZ, ' else '' end)+
					(case when przesylkaZwrotna = 1 then 'PZ, ' else '' end)+
					(case when doreczenieDoRakWlasnych = 1 then 'DRW, ' else '' end)+
					(case when opony = 1 then 'Opony, ' else '' end)+
					(case when oponyExport = 1 then 'Opony int, ' else '' end)+
                    (case when przesylkaPaletowa = 1 then 'Palety, ' else '' end)+
					(case when doreczenieNaAdresPryw = 1 then 'PRYW, ' else '' end)+
					(case when odbiorWlasny = 1 then 
								 (case when odbiorWlasnyTyp = 'Osoba' then 'ODB PRYW, '
									   when odbiorWlasnyTyp = 'Firma' then 'ODB  FIRMA, ' 
								  end)
						  else '' end) +
					(case when doreczenieGwarantowane = 1 then 
							 (case when doreczenieGwarantowaneTyp = 'DPD 9:30' then 'DOR 9:30, '
								   when doreczenieGwarantowaneTyp = 'DPD 12:00' then 'DOR 12:00, ' 
								   when doreczenieGwarantowaneTyp = 'Sobota' then 'SOB, '  
								   when doreczenieGwarantowaneTyp = 'DPD na godzinę' then 'DOR ' + doreczenieGwarantowaneWartosc   
							  end)
						  else '' end ) Usługi,
	 l.data_listu, l.data_protokolu--, u.*
from _DPD_LISTY l
	inner join _DPD_USLUGI u on l.id = u.id
where l.id in ({string
                    .Join(",", idki.ToArray())})";
        }

        public static void Generuj(string _sciezka, DateTime from, DateTime to)
        {
            Raport._sciezka = _sciezka;

            PrzygotujZapytanieFiltr(from, to);

            Generuj();
        }

        public static void Generuj(string _sciezka, List<string>idki)
        {
            Raport._sciezka = _sciezka;

            PrzygotujZapytanieNaPodstawieId(idki);

            Generuj();
        }

        private static void Generuj()
        {
            WyczyscListy();

            PrzygotujDane();

            DodajPaczkiDoPrzesylek();

            using (var txt = (TextWriter)new StreamWriter(_sciezka, false, Encoding.GetEncoding(1250)))
            {
                var naglowek = PrzygotujNaglowek();

                txt.WriteLine(naglowek);

                foreach (var przesylka in _przesylki)
                {
                    foreach (var paczka in przesylka.Paczki)
                    {
                        var sb = new StringBuilder();
                        sb.Append($"{paczka.NrPaczki};");
                        sb.Append($"{przesylka.NrFid};");
                        sb.Append($"{przesylka.NazwaNadawcy};");
                        sb.Append($"{przesylka.ImieNazwiskoNadawcy};");
                        sb.Append($"{przesylka.AdresNadawcy};");
                        sb.Append($"{przesylka.KodPocztowyNadawcy};");
                        sb.Append($"{przesylka.MiejscowoscNadawcy};");
                        sb.Append($"{przesylka.KrajNadawcy};");

                        sb.Append($"{przesylka.NazwaOdbiorcy};");
                        sb.Append($"{przesylka.ImieNazwiskoOdbiorcy};");
                        sb.Append($"{przesylka.AdresOdbiorcy};");
                        sb.Append($"{przesylka.KodPocztowyOdbiorcy};");
                        sb.Append($"{przesylka.MiejscowoscOdbiorcy};");
                        sb.Append($"{przesylka.KrajOdbiorcy};");

                        sb.Append($"{przesylka.Ref1};");
                        sb.Append($"{przesylka.Ref2};");
                        sb.Append($"{paczka.Zawartosc};");
                        sb.Append($"{przesylka.Cod};");
                        sb.Append($"{przesylka.Ubezpieczenie};");
                        sb.Append($"{przesylka.Uslugi};");
                        sb.Append($"{paczka.Waga};");
                        sb.Append($"=\"{paczka.Lp}/{przesylka.Paczki.Count}\";");

                        sb.Append($"{przesylka.DataWygenerowaniaEtykiety};");
                        sb.Append($"{przesylka.DataWygenerowaniaProtokolu};");


                        txt.WriteLine(sb.ToString());
                    }
                }
            }
        }

        private static void PrzygotujDane()
        {
            PrzygotujPrzesylki();
            PrzygotujPaczki();
        }

        private static void DodajPaczkiDoPrzesylek()
        {
            foreach (var przesylka in _przesylki)
            {
                var paczkiPrzesylki = _paczki.Where(p => p.Id == przesylka.Id).OrderBy(p => p.Lp).ToList();

                var numery = przesylka.NumeryPaczek.Split(';');

                for (var i = 0; i < paczkiPrzesylki.Count(); i++)
                    paczkiPrzesylki[i].NrPaczki = numery[i];

                przesylka.Paczki = paczkiPrzesylki;
            }
        }

        private static void PrzygotujPrzesylki()
        {
            using (var conn = new SqlConnection(Sql.ConnStr))
            {
                using (var cmd = new SqlCommand(_queryPobierzPrzesylki, conn))
                {
                    conn.Open();
                    var rdr = cmd.ExecuteReader();

                    if (rdr.HasRows)
                    {
                        while (rdr.Read())
                        {
                            var nowaPrzesylka = new Przesylka();
                            nowaPrzesylka.Id = rdr["id"].ToString();
                            nowaPrzesylka.NumeryPaczek = rdr["nr_listu"].ToString();
                            nowaPrzesylka.NrFid = rdr["platnikFID"].ToString();
                            nowaPrzesylka.NazwaNadawcy = rdr["adres_nadawcy_nazwa"].ToString();
                            nowaPrzesylka.ImieNazwiskoNadawcy = rdr["adres_nadawcy_imieNazwisko"].ToString();
                            nowaPrzesylka.AdresNadawcy = rdr["adres_nadawcy_ulica"].ToString();
                            nowaPrzesylka.KodPocztowyNadawcy = rdr["adres_nadawcy_kod"].ToString();
                            nowaPrzesylka.MiejscowoscNadawcy = rdr["adres_nadawcy_miejscowosc"].ToString();
                            nowaPrzesylka.KrajNadawcy = rdr["adres_nadawcy_kraj"].ToString();

                            nowaPrzesylka.NazwaOdbiorcy = rdr["adres_Odbiorcy_nazwa"].ToString();
                            nowaPrzesylka.ImieNazwiskoOdbiorcy = rdr["adres_Odbiorcy_imieNazwisko"].ToString();
                            nowaPrzesylka.AdresOdbiorcy = rdr["adres_Odbiorcy_ulica"].ToString();
                            nowaPrzesylka.KodPocztowyOdbiorcy = rdr["adres_Odbiorcy_kod"].ToString();
                            nowaPrzesylka.MiejscowoscOdbiorcy = rdr["adres_Odbiorcy_miejscowosc"].ToString();
                            nowaPrzesylka.KrajOdbiorcy = rdr["adres_Odbiorcy_kraj"].ToString();

                            nowaPrzesylka.Ref1 = rdr["ref1"].ToString();
                            nowaPrzesylka.Ref2 = rdr["ref2"].ToString();
                            nowaPrzesylka.Cod = rdr["pobranieKwota"].ToString();
                            nowaPrzesylka.Ubezpieczenie = rdr["przesylkaWartosciowaKwota"].ToString();
                            nowaPrzesylka.Uslugi = rdr["Usługi"].ToString();
                            nowaPrzesylka.DataWygenerowaniaEtykiety = rdr["data_listu"].ToString();
                            nowaPrzesylka.DataWygenerowaniaProtokolu = rdr["data_protokolu"].ToString();

                            _przesylki.Add(nowaPrzesylka);
                        }
                    }
                }
            }
        }

        private static void PrzygotujPaczki()
        {
            using (var conn = new SqlConnection(Sql.ConnStr))
            {
                using (var cmd = new SqlCommand(_queryPobierzPaczki, conn))
                {
                    conn.Open();
                    var rdr = cmd.ExecuteReader();

                    if (rdr.HasRows)
                    {
                        while (rdr.Read())
                        {
                            var nowaPaczka = new Paczka();
                            nowaPaczka.Id = rdr["id"].ToString();
                            nowaPaczka.Zawartosc = rdr["zawartosc"].ToString();
                            nowaPaczka.Waga = rdr["waga"].ToString();
                            nowaPaczka.Lp = Convert.ToInt32(rdr["lp"].ToString());

                            _paczki.Add(nowaPaczka);
                        }
                    }
                }
            }
        }

        private class Przesylka
        {
            public List<Paczka> Paczki = new List<Paczka>();
            public string Id;
            public string NumeryPaczek;
            public string NrFid;
            public string NazwaNadawcy;
            public string ImieNazwiskoNadawcy;
            public string AdresNadawcy;
            public string KodPocztowyNadawcy;
            public string MiejscowoscNadawcy;
            public string KrajNadawcy;

            public string NazwaOdbiorcy;
            public string ImieNazwiskoOdbiorcy;
            public string AdresOdbiorcy;
            public string KodPocztowyOdbiorcy;
            public string MiejscowoscOdbiorcy;
            public string KrajOdbiorcy;

            public string Ref1;
            public string Ref2;
            public string Cod;
            public string Ubezpieczenie;
            public string Uslugi;
            public string DataWygenerowaniaEtykiety;
            public string DataWygenerowaniaProtokolu;

        }

        private class Paczka
        {
            public string Id;
            public string Zawartosc;
            public string Waga;
            public int Lp;
            public string NrPaczki;
        }
    }
}
