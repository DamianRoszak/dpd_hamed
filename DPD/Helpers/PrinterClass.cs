﻿using System;
using System.Collections.Generic;
using System.Drawing.Printing;
using System.Linq;
using System.Text;
using DPD.Repos;
using PdfiumViewer;

namespace DPD.Helpers
{
    public static class PrinterClass
    {
        public static Exception PrintFile(string filePath, string printerName)
        {
            try
            {
                return PrintProcess(filePath, printerName) ? null : new Exception("Błąd drukowania");
            }
            catch (Exception ex)
            {
                Log.AddLog(ex.ToString());
                return ex;
            }
        }

        private static bool PrintProcess(string filePath, string printerName)
        {
            var printerSettings = new PrinterSettings
            {
                PrinterName = printerName,
                Copies = 1,
                Duplex = Duplex.Simplex
            };

            var pageSettings = new PageSettings(printerSettings)
            {
                Margins = new Margins(0, 0, 0, 0)
            };

            foreach (PaperSize paperSize in printerSettings.PaperSizes)
            {
                pageSettings.PaperSize = paperSize;
                break;
            }

            using (var document = PdfDocument.Load(filePath))
            {
                using (var printDocument = document.CreatePrintDocument())
                {
                    printDocument.PrinterSettings = printerSettings;
                    printDocument.DefaultPageSettings = pageSettings;
                    printDocument.PrintController = new StandardPrintController();
                    printDocument.Print();
                }
            }
            return true;
        }
    }
}
