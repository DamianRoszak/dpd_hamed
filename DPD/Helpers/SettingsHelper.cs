﻿using System;
using System.Collections.Specialized;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Xml;
using DPD.Repos;
using static DPD.Properties.Settings;

namespace DPD.Helpers
{
    internal static class SettingsHelper
    {
        private static string _configv100 = @"C:\Users\MARCIN KRAWCZYK\AppData\Local\DPD\DPD.exe_Url_pbyrhtc0hq3cy345pzfxk2kljsxiqmql\1.0.0.0\user.config";

        private static string DirSearch(string sDir, string version)
        {
            try
            {
                foreach (var dir in Directory.GetDirectories(sDir))
                {
                    if (dir.Contains("DPD.exe") || dir.Contains("DefaultDomain_Path"))
                        foreach (var d in Directory.GetDirectories(dir))
                            if (d.Split(new[] { "\\" }, StringSplitOptions.None).Last() == version)
                                foreach (var f in Directory.GetFiles(d))
                                    if (f.Split(new[] { "\\" }, StringSplitOptions.None).Last() == "user.config")
                                        return f;
                    return DirSearch(dir, version);
                }
            }
            catch (Exception excpt)
            {
                Console.WriteLine(excpt.Message);
            }
            return null;
        }

        public static bool LoadPropertiesFromOlderVersion(string version)
        {
            var path = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData).Replace("Roaming", "Local") + "\\DPD\\";
            _configv100 = DirSearch(path, version);
            //MessageBox.Show(configv100);

            if (_configv100 == null)
            {
                //MessageBox.Show("Nie znaleziono user.config");
                Log.AddLog("Nie znaleziono user.config dla wersji" + version);
                return false;
            }
            var settingInt = new[] { "authDb" };
            var settingDecimal = new[] { "domyslnaWaga" };
            var settingBool = new[] { "handUwagi", "handPD", "magUwagi", "magPD", "zamKomentarz","zamInfDod","zamUwagi","DokumentyZwrotne","Zablokowane",
            "zamPD","adresNadawcyZUstawien","bnrDok","bUwagi","cbAktywnyAdresDostawy","bPole1", "bPole2","bPole3","bPole4","bPole5","bPole6","bPole7","bPole8",
            "bPole9","bPole10", "zmienStatus","domyslnaWagaZMaga","etykieciarka","autoWydrukPoZapisie","bWartoscBrutto","bWlaczSzablon",
            "bDoreczenieGwarantowane","bDoreczenieNaAdresPrywatny","bPobranieCOD","bOdbiorWlasny","bDokumentyZwrotne",
            "bDOX","bPrzesylkaZwrotna","bOpony","bOponyMiedzynarodowe","bPrzesylkaWartosciowa","bDoreczenieDoRakWlasnych",
            "bPobranieZWfMaga","bPrzesylkaWartosciowaZWfMaga"};

            var section = "userSettings";
            var filePath = _configv100;
            var file = filePath;
            var xDoc = new XmlDocument();
            var nameValueColl = new NameValueCollection();

            var map = new ExeConfigurationFileMap();
            map.ExeConfigFilename = file;
            var config = ConfigurationManager.OpenMappedExeConfiguration(map, ConfigurationUserLevel.None);
            var xml = config.GetSection(section).SectionInformation.GetRawXml();
            xDoc.LoadXml(xml);

            var xList = xDoc.ChildNodes[0].ChildNodes[0];
            //MessageBox.Show(xList.ChildNodes.Count.ToString());

            foreach (XmlNode xNodo in xList)
            {
                try
                {
                    if (settingBool.Contains(xNodo.Attributes[0].Value))
                        Default[xNodo.Attributes[0].Value] = Convert.ToBoolean(xNodo.InnerText);
                    else if (settingDecimal.Contains(xNodo.Attributes[0].Value))
                        Default[xNodo.Attributes[0].Value] = Convert.ToDecimal(xNodo.InnerText);
                    else if (settingInt.Contains(xNodo.Attributes[0].Value))
                        Default[xNodo.Attributes[0].Value] = Convert.ToInt32(xNodo.InnerText);
                    else
                        Default[xNodo.Attributes[0].Value] = xNodo.InnerText;
                }
                catch (Exception)
                {
                }
            }

            Default.Save();

            if (File.Exists(_configv100))
                File.Delete(_configv100);

            return true;
        }

    }
}
