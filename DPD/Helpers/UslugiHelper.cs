﻿using System.Linq;
using System.Windows.Forms;
using static DPD.Properties.Settings;

namespace DPD.Helpers
{
    public class UslugiHelper
    {
        public bool DoreczenieGwarantowane { get; set; }
        public string DoreczenieGwarantowaneWartosc { get; set; }
        public string DoreczenieGwarantowaneNaGodzine { get; set; }
        public bool DoreczenieDoRakWlasnych { get; set; }
        public bool DoreczenieNaAdersPrywatny { get; set; }
        public bool Pobranie { get; set; }
        public string PobranieWartosc { get; set; }
        public bool PobranieZWfMaga { get; set; }
        public bool OdbiorWlasny { get; set; }
        public string OdbiorWlasnyWartosc { get; set; }
        public bool DokumentyZwrotne { get; set; }
        public bool Dox { get; set; }
        public bool PrzesylkaZwrotna { get; set; }
        public bool Opony { get; set; }
        public bool OponyMiedzynarodowe { get; set; }
        public bool PrzesylkaWartosciowa { get; set; }
        public bool PrzesylkaWartosciowaZWfMaga { get; set; }
        public string PrzesylkaWartosciowaWartosc { get; set; }
        public bool PrzesylkaPaletowa { get; set; }
        public bool Pudo { get; set; }
        public string PudoWartosc { get; set; }
        public bool Duty { get; set; }
        public string DutyValue { get; set; }
        public bool DpdExpress { get; set; }

        public static UslugiHelper PobierzUslugiZUstawien()
        {
            var uslugi = new UslugiHelper
            {
                PrzesylkaPaletowa = Default.bServicePallet,
                DoreczenieGwarantowane = Default.bDoreczenieGwarantowane,
                DoreczenieNaAdersPrywatny = Default.bDoreczenieNaAdresPrywatny,
                DoreczenieDoRakWlasnych = Default.bDoreczenieDoRakWlasnych,
                Pobranie = Default.bPobranieCOD,
                PobranieZWfMaga = Default.bPobranieZWfMaga,
                OdbiorWlasny = Default.bOdbiorWlasny,
                DokumentyZwrotne = Default.bDokumentyZwrotne,
                Dox = Default.bDOX,
                PrzesylkaZwrotna = Default.bPrzesylkaZwrotna,
                Opony = Default.bOpony,
                OponyMiedzynarodowe = Default.bOponyMiedzynarodowe,
                PrzesylkaWartosciowa = Default.bPrzesylkaWartosciowa,
                PrzesylkaWartosciowaZWfMaga = Default.bPrzesylkaWartosciowaZWfMaga,
                Pudo = Default.bPudo,
                DpdExpress = Default.bDpdExpress,
                Duty = Default.bDuty,
                DutyValue = Default.vDuty
            };

            if (Default.bDoreczenieGwarantowane)
            {
                uslugi.DoreczenieGwarantowaneWartosc = Default.vDoreczenieGwarantowane;
                if (Default.vDoreczenieGwarantowane == "DPD na godzinę")
                    uslugi.DoreczenieGwarantowaneNaGodzine = Default.vDoreczenieGwarantowaneGodzina;
            }

            if (Default.bPobranieCOD && !Default.bPobranieZWfMaga)
                uslugi.PobranieWartosc = Default.vPobranieCOD;

            if (Default.bOdbiorWlasny)
                uslugi.OdbiorWlasnyWartosc = Default.vOdbiorWlasny;
            if (Default.bPrzesylkaWartosciowa)
                uslugi.PrzesylkaWartosciowaWartosc = Default.vPrzesylkaWartosciowa;

            if (Default.bPudo)
                uslugi.PudoWartosc = Default.vPudo;

            return uslugi;
        }
    }

    internal static class ExtensionUslugi
    {
        public static void PrzeniesDaneDoKontrolek(this UslugiHelper uslugi, Control control)
        {
            ((CheckBox) control.Controls.Find("chPallet", false).FirstOrDefault()).Checked = uslugi.PrzesylkaPaletowa;

            ((CheckBox) control.Controls.Find("chDorGwaran", false).FirstOrDefault()).Checked =
                uslugi.DoreczenieGwarantowane;

            if (uslugi.DoreczenieGwarantowane)
            {
                ((ComboBox) control.Controls.Find("cbDoreczenie", false).FirstOrDefault()).SelectedItem =
                    uslugi.DoreczenieGwarantowaneWartosc;

                if (uslugi.DoreczenieGwarantowaneWartosc == "DPD na godzinę")
                    ((TextBox) control.Controls.Find("txtNaOkreslonaGodzine", false).FirstOrDefault()).Text =
                        uslugi.DoreczenieGwarantowaneNaGodzine;
            }

            ((CheckBox) control.Controls.Find("chDorNaAdrPryw", false).FirstOrDefault()).Checked =
                uslugi.DoreczenieNaAdersPrywatny;

            ((CheckBox) control.Controls.Find("chDorDoRakWl", false).FirstOrDefault()).Checked =
                uslugi.DoreczenieDoRakWlasnych;

            var ctrl = control.Controls.Find("chPobranie", false).FirstOrDefault();
            if (ctrl != null)
            {
                ((CheckBox) ctrl).Checked = uslugi.Pobranie;
                if (uslugi.Pobranie && !uslugi.PobranieZWfMaga)
                    ((TextBox) control.Controls.Find("txtPobranie", false).FirstOrDefault()).Text =
                        uslugi.PobranieWartosc;
            }

            ((CheckBox) control.Controls.Find("chOdbiorWlasny", false).FirstOrDefault()).Checked = uslugi.OdbiorWlasny;

            ctrl = control.Controls.Find("chOdbiorWlasny", false).FirstOrDefault();
            if (ctrl != null)
            {
                ((CheckBox) ctrl).Checked = uslugi.OdbiorWlasny;
                if (uslugi.OdbiorWlasny)
                    if (uslugi.OdbiorWlasnyWartosc == "Osoba")
                        ((RadioButton) control.Controls.Find("gbOdbiorWlasny", false).FirstOrDefault().Controls
                            .Find("rbOsoba", false).FirstOrDefault()).Checked = true;
                    else
                        ((RadioButton) control.Controls.Find("gbOdbiorWlasny", false).FirstOrDefault().Controls
                            .Find("rbFirma", false).FirstOrDefault()).Checked = true;
            }

            ((CheckBox) control.Controls.Find("chDokZwrot", false).FirstOrDefault()).Checked = uslugi.DokumentyZwrotne;
            ((CheckBox) control.Controls.Find("chPrzesList", false).FirstOrDefault()).Checked = uslugi.Dox;
            ((CheckBox) control.Controls.Find("chPrzesZwrot", false).FirstOrDefault()).Checked =
                uslugi.PrzesylkaZwrotna;
            ((CheckBox) control.Controls.Find("chOpony", false).FirstOrDefault()).Checked = uslugi.Opony;
            ((CheckBox) control.Controls.Find("chTiresExport", false).FirstOrDefault()).Checked =
                uslugi.OponyMiedzynarodowe;

            ((CheckBox) control.Controls.Find("chDeklWart", false).FirstOrDefault()).Checked =
                uslugi.PrzesylkaWartosciowa;

            if (uslugi.PrzesylkaWartosciowa && !uslugi.PrzesylkaWartosciowaZWfMaga)
                ((TextBox) control.Controls.Find("gbDeklarowanaWartosc", false).FirstOrDefault().Controls
                    .Find("txtDeklarowanaWartosc", false).FirstOrDefault()).Text = uslugi.PrzesylkaWartosciowaWartosc;

            var ctrlPudo = control.Controls.Find("chPudo", false).FirstOrDefault();
            if (ctrlPudo != null)
            {
                ((CheckBox) ctrlPudo).Checked = uslugi.Pudo;
                if (uslugi.Pudo)
                    ((TextBox) control.Controls.Find("txtPudo", false).FirstOrDefault()).Text = uslugi.PudoWartosc;
            }

            var ctrlDuty = control.Controls.Find("chDuty", false).FirstOrDefault();
            if (ctrlDuty != null)
            {
                ((CheckBox)ctrlDuty).Checked = uslugi.Duty;
                if (uslugi.Duty)
                {
                    var dutyText = (TextBox)control.Controls.Find("txtDuty", false).FirstOrDefault();
                    if(dutyText!=null)
                        dutyText.Text = uslugi.DutyValue;
                }
            }

            var dpdExpress = (CheckBox) control.Controls.Find("chDpdExpress", false)?.FirstOrDefault();
            if(dpdExpress!=null)
                dpdExpress.Checked = uslugi.DpdExpress;
        }
    }
}