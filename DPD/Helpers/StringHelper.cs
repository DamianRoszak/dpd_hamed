﻿namespace DPD.Helpers
{
    public static class StringHelper
    {
        public static string Substring2(this string text, int length)
        {
            if (text.Length > length)
                return text.Substring(0, length);
            return text;
        }
    }
}
