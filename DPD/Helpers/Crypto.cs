﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace DPD.Helpers
{
    internal static class Crypto
    {

        private static readonly byte[] KeyPassword = Convert.FromBase64String("aRNPnOlaQw7PtpRQF9xndg==");

        public static string DecryptPasswordString(string text)
        {
            try
            {
                return DecryptPassword(Convert.FromBase64String(text));
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }

        public static string EncryptPasswordString(string text)
        {
            try
            {
                return Convert.ToBase64String(EncryptPassword(text));
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }
        private static byte[] EncryptPassword(string plainText)
        {
            try
            {

                if (plainText == null || plainText.Length <= 0)
                    return new byte[0];

                byte[] encrypted;

                using (var rijAlg = Rijndael.Create())
                {
                    rijAlg.Key = KeyPassword;
                    rijAlg.IV = KeyPassword;

                    var encryptor = rijAlg.CreateEncryptor(rijAlg.Key, rijAlg.IV);

                    using (var msEncrypt = new MemoryStream())
                    {
                        using (var csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                        {
                            using (var swEncrypt = new StreamWriter(csEncrypt))
                            {
                                swEncrypt.Write(plainText);
                            }
                            encrypted = msEncrypt.ToArray();
                        }
                    }
                }
                return encrypted;
            }
            catch (Exception)
            {
                return new byte[0];
            }
        }

        private static string DecryptPassword(byte[] cipherText)
        {
            try
            {
                if (cipherText == null || cipherText.Length <= 0)
                    return "";

                using (var rijAlg = Rijndael.Create())
                {
                    rijAlg.Key = KeyPassword;
                    rijAlg.IV = KeyPassword;

                    var decryptor = rijAlg.CreateDecryptor(rijAlg.Key, rijAlg.IV);

                    using (var msDecrypt = new MemoryStream(cipherText))
                    {
                        using (var csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                        {
                            using (var srDecrypt = new StreamReader(csDecrypt))
                            {
                                return srDecrypt.ReadToEnd();
                            }
                        }
                    }
                }
            }
            catch (Exception)
            {
                return "";
            }

        }

       
    }
}
