﻿using System;

namespace DPD.Helpers
{
    internal static class Extensions
    {
        public static decimal ToDecimal(this string txt)
        {
            decimal val = 0;

            try
            {
                val = Convert.ToDecimal(txt.Replace(",", "."));
            }
            catch (Exception)
            {
                try
                {
                    val = Convert.ToDecimal(txt);
                }
                catch (Exception)
                {

                }
            }
            
            return val;
        }
    }
}
