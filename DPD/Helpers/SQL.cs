﻿using System;
using System.Data;
using System.Data.SqlClient;
using DPD.Repos;

namespace DPD.Helpers
{
    public static class Sql
    {
        public static string ConnStr;

        public static bool CheckConnection()
        {
            try
            {
                using (var conn = new SqlConnection(ConnStr))
                {
                    conn.Open();
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
        public static DataTable PobierzDane(string query)
        {
            DataTable dt = new DataTable();
            try
            {
                //nawiozanie polaczenia
                System.Data.SqlClient.SqlConnection MSDEconn;
                MSDEconn = new SqlConnection();
                MSDEconn.ConnectionString = Sql.ConnStr;
                MSDEconn.Open();
                System.Data.SqlClient.SqlCommand MSDEcommand = new SqlCommand();
                MSDEcommand.Connection = MSDEconn;
                MSDEcommand.CommandText = query;
                SqlDataReader reader = MSDEcommand.ExecuteReader();
                if (reader.HasRows)
                {
                    dt = new DataTable();
                    dt.Load(reader);
                }
            }
            catch (SqlException e)
            {
                //MessageBox.Show(string.Format("Błąd bazy przy get data: {0}\r\n zapytanie: {1}\r\n", e.Message, query), LogType.Blad);
                Log.AddLog(e.ToString());
            }
            return dt;
        }
        
        public static bool RunQuery(string query, bool log = true)
        {
            try
            {
                using (var conn = new SqlConnection(ConnStr))
                {
                    conn.Open();
                    using (var cmd = new SqlCommand(query, conn))
                    {
                        if (query != null)
                            cmd.ExecuteNonQuery();
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                if(log)
                    Log.AddLog("QUERY:" + query + ";" + ex);

                return false;
            }
        }

        public static bool RunQueryWithoutTry(string query)
        {
                using (var conn = new SqlConnection(ConnStr))
                {
                    conn.Open();
                    using (var cmd = new SqlCommand(query, conn))
                    {
                        if (query != null)
                            cmd.ExecuteNonQuery();
                    }
                }
                return true;
        }


        public static string RunQueryScalar(string query)
        {
            try
            {
                using (var conn = new SqlConnection(ConnStr))
                {
                    conn.Open();
                    using (var cmd = new SqlCommand(query, conn))
                    {
                        var result = cmd.ExecuteScalar().ToString();
                        return result;
                    }
                }
            }
            catch (Exception ex)
            {
                //MessageBox.Show("BŁĄD połączenia z bazą");
                Log.AddLog("QUERY:" + query + ";" + ex);
                return null;
            }
        }

        public static string RunQueryScalarWithoutTry(string query)
        {
                using (var conn = new SqlConnection(ConnStr))
                {
                    conn.Open();
                    using (var cmd = new SqlCommand(query, conn))
                    {
                        var result = cmd.ExecuteScalar().ToString();
                        return result;
                    }
                }
        }
    }
}
