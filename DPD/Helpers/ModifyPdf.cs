﻿using System.Drawing;
using System.IO;
using PdfSharp.Drawing;
using PdfSharp.Pdf;
using PdfSharp.Pdf.IO;

namespace DPD.Helpers
{
    public static class ModifyPdf
    {
        private const int Width = 290;
        private const int Height = 415;

        private static readonly Point Left = new Point(-Width, 0);
        private static readonly Point LeftUp = new Point(-Width, -Height);
        private static readonly Point Up = new Point(0, -Height);
        private static readonly Point RightUp = new Point(Width, -Height);
        private static readonly Point Right = new Point(Width, 0);
        private static readonly Point RightDown = new Point(Width, Height);
        private static readonly Point Down = new Point(0, Height);
        private static readonly Point LeftDown = new Point(-Width, Height);
        private const int FirstPage = 0;

        private static int _quarter = 1;
        private static string _pdfPath = "";
        private static string _tmpPath = "";
        private static int _packageCount = 0;
        private static int _pageCount = 0;

        public static void Create(int _quarter, string _pdfPath, int _packageCount)
        {
            if (_quarter == 1)
                return;

            _tmpPath = $"{new FileInfo(_pdfPath).DirectoryName}\\tmp";
            if (!Directory.Exists(_tmpPath))
            {
                var di = Directory.CreateDirectory(_tmpPath);
                di.Attributes = FileAttributes.Directory | FileAttributes.Hidden;
            }
            ModifyPdf._quarter = _quarter;
            ModifyPdf._pdfPath = _pdfPath;
            ModifyPdf._packageCount = _packageCount;
            _pageCount = ((ModifyPdf._packageCount + ModifyPdf._quarter - 1) % 4) == 0 ? ((ModifyPdf._packageCount + ModifyPdf._quarter - 1) / 4) : ((ModifyPdf._packageCount + ModifyPdf._quarter - 1) / 4) + 1;

            var pdfNew = new PdfDocument();

            GeneratePages();
            GenerateSingle(pdfNew);

            var pdfAll = new PdfDocument();
            for (var i = 0; i < ModifyPdf._packageCount; i++)
            {
                var x = XPdfForm.FromFile($"{_tmpPath}\\{i}e.pdf");
                if (i == 0 || (((i + (ModifyPdf._quarter - 1)) % 4) == 0))
                    pdfAll.AddPage();
                var nrStrony = (i + (ModifyPdf._quarter - 1)) / 4;
                var g = XGraphics.FromPdfPage(pdfAll.Pages[nrStrony], XGraphicsPdfPageOptions.Append);
                switch ((i + (ModifyPdf._quarter - 1)) % 4)
                {
                    case 0: { g.DrawImage(x, 0, 0); break; }
                    case 1: { g.DrawImage(x, Down); break; }
                    case 2: { g.DrawImage(x, Right); break; }
                    case 3: { g.DrawImage(x, RightDown); break; }
                }
                //DrawFrame(g);
               // pdfAll.Save(ModifyPdf._pdfPath);
            }
            pdfAll.Save(ModifyPdf._pdfPath);

            ClearTmpFolder(_tmpPath);
        }

        private static void GeneratePages()
        {
            var pdf = PdfReader.Open(_pdfPath, PdfDocumentOpenMode.Import);

            for (var i = 0; i < pdf.Pages.Count; i++)
            {
                var newPdf = new PdfDocument();
                newPdf.AddPage(pdf.Pages[i]);
                newPdf.Save($"{_tmpPath}\\{i}.pdf");
            }
        }

        private static void GenerateSingle(PdfDocument pdf)
        {
            for (var i = 0; i < _packageCount; i++)
            {
                pdf = new PdfDocument();
                var pageNr = i / 4;
                var x = XPdfForm.FromFile($"{_tmpPath}\\{pageNr}.pdf");

                var pag = new PdfPage();
                pag.Width = 300;
                pag.Height = 415;
                pdf.AddPage(pag);
                var g = XGraphics.FromPdfPage(pag, XGraphicsPdfPageOptions.Append);

                switch (i % 4)
                {
                    case 0: { g.DrawImage(x, 0, 0); break; }
                    case 1: { g.DrawImage(x, Up); break; }
                    case 2: { g.DrawImage(x, Left); break; }
                    case 3: { g.DrawImage(x, LeftUp); break; }
                }

                pdf.Save($"{_tmpPath}\\{i}e.pdf");
            }
        }

        private static void ClearTmpFolder(string path)
        {
            if (Directory.Exists(path))
            {
                var files = Directory.GetFiles(path);

                foreach (var file in files)
                    File.Delete(file);
            }
        }

        private static void Q2(PdfDocument pdfNew)
        {
            for (var i = 0; i < _pageCount; i++)
            {
                if (i == FirstPage)
                {
                    var x = XPdfForm.FromFile($"{_tmpPath}\\{i}.pdf");
                    pdfNew.AddPage();
                    var g = XGraphics.FromPdfPage(pdfNew.Pages[0], XGraphicsPdfPageOptions.Append);
                    g.DrawImage(x, RightUp);
                    g.DrawImage(x, Down);
                    DrawFrame(g);
                }
                else
                {
                    var x = XPdfForm.FromFile($"{_tmpPath}\\{i}.pdf");
                    var x1 = XPdfForm.FromFile($"{_tmpPath}\\{(i - 1)}.pdf");
                    pdfNew.AddPage();
                    var g = XGraphics.FromPdfPage(pdfNew.Pages[i], XGraphicsPdfPageOptions.Append);
                    g.DrawImage(x, RightUp);
                    //if(liczbaPaczek % 4 == 
                    g.DrawImage(x1, LeftUp);
                    g.DrawImage(x, Down);
                    DrawFrame(g);
                }
            }
        }

        private static void Q3(PdfDocument pdfNew)
        {
            for (var i = 0; i < _pageCount; i++)
            {
                if (i == FirstPage)
                {
                    var x = XPdfForm.FromFile($"{_tmpPath}\\{i}.pdf");
                    pdfNew.AddPage();
                    var g = XGraphics.FromPdfPage(pdfNew.Pages[0], XGraphicsPdfPageOptions.Append);
                    g.DrawImage(x, Right);
                    DrawFrame(g);
                }
                else
                {
                    var x = XPdfForm.FromFile($"{_tmpPath}\\{i}.pdf");
                    var x1 = XPdfForm.FromFile($"{_tmpPath}\\{(i - 1)}.pdf");
                    pdfNew.AddPage();
                    var g = XGraphics.FromPdfPage(pdfNew.Pages[i], XGraphicsPdfPageOptions.Append);
                    g.DrawImage(x, Left);
                    g.DrawImage(x1, Right);
                    DrawFrame(g);
                }
            }
        }

        private static void Q4(PdfDocument pdfNew)
        {
            for (var i = 0; i < _pageCount; i++)
            {
                if (i == FirstPage)
                {
                    var x = XPdfForm.FromFile($"{_tmpPath}\\{i}.pdf");
                    pdfNew.AddPage();
                    var g = XGraphics.FromPdfPage(pdfNew.Pages[0], XGraphicsPdfPageOptions.Append);
                    g.DrawImage(x, RightDown);
                    DrawFrame(g);
                }
                else
                {
                    var x = XPdfForm.FromFile($"{_tmpPath}\\{i}.pdf");
                    var x1 = XPdfForm.FromFile($"{_tmpPath}\\{(i - 1)}.pdf");
                    var x2 = XPdfForm.FromFile($"{_tmpPath}\\{(i + 1)}.pdf");
                    pdfNew.AddPage();
                    var g = XGraphics.FromPdfPage(pdfNew.Pages[i], XGraphicsPdfPageOptions.Append);
                    g.DrawImage(x, Up);
                    g.DrawImage(x, LeftDown);
                    g.DrawImage(x2, RightDown);
                    DrawFrame(g);
                }
            }
        }

        private static void DrawFrame(XGraphics gfx)
        {
            XBrush brush = XBrushes.White;

            gfx.DrawRectangle(brush, 0, 0, gfx.PageSize.Width, 15); // gorna 

            gfx.DrawRectangle(brush, 0, gfx.PageSize.Height - 25, gfx.PageSize.Width, 25); // dolna 

            gfx.DrawRectangle(brush, 0, 0, 5, gfx.PageSize.Height); // lewa

            gfx.DrawRectangle(brush, gfx.PageSize.Width - 5, 0, 5, gfx.PageSize.Height); //  prawa
        }

        private static void ChangeLabelQuarterSingle(int quarter, string pdfPath)
        {
            var pdf = new PdfDocument();
            pdf.AddPage();

            //XPdfForm pdfForm = XPdfForm.FromFile("1.pdf");
            var pdfForm = XPdfForm.FromFile(pdfPath);

            var gfx = XGraphics.FromPdfPage(pdf.Pages[0], XGraphicsPdfPageOptions.Append);

            switch (quarter)
            {
                case 2: { gfx.DrawImage(pdfForm, 290, 0); break; } // lewy dolny
                case 3: { gfx.DrawImage(pdfForm, 0, 400); break; } // prawy gorny 
                case 4: { gfx.DrawImage(pdfForm, 290, 400); break; }// prawy dolny 
            }

            //pdf.Save("3.pdf");
            pdf.Save(pdfPath);
        }

        public static void DoublePageProtocol(string protocolPath)
        {
            var pdf = new PdfDocument();
            var protocol = PdfReader.Open(protocolPath, PdfDocumentOpenMode.Import);

            foreach (PdfPage page in protocol.Pages)
                pdf.AddPage(page);

            foreach (PdfPage page in protocol.Pages)
                pdf.AddPage(page);

            pdf.Save(protocolPath);
        }
    }
}
